<?php
    use console\utilities\InitManager\migrations\GeneralMigration;

    $urlManagerFrontendConfigPath = dirname(__DIR__) . '/../frontend/config/urlManager.php';
    $urlManagerBackendConfigPath = dirname(__DIR__) . '/../backend/config/urlManager.php';

    $config = [
        'id' => 'fs-console-test',
        'language' => 'en-US',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'controllerNamespace' => 'console\controllers',
        'controllerMap' => [
            'fixture' => [
                'class' => 'yii\console\controllers\FixtureController',
                'namespace' => 'common\fixtures',
            ],
            'migrate' => [
                'class' => 'yii\console\controllers\MigrateController',
                'migrationNamespaces' => [
                    (new GeneralMigration())->getNamespace(),
                ],
                'migrationPath' => null
            ],
        ],
        'components' => [
            'db' => require dirname(dirname(__DIR__)) . '/common/config/db-test.php',
            'cache' => [
                'class' => 'yii\caching\FileCache',
                'cachePath' => '@frontend/runtime/cache'
            ],
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'backend\utilities\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
            'mailer' => [
                'class' => 'common\utilities\Mailer',
            ],
            'smsGate' => [
                'class' => 'common\utilities\SmsGate',
            ],
            'urlManagerFrontend' => array_merge(
                require $urlManagerFrontendConfigPath,
                ['class' => 'yii\web\urlManager']
            ),
            'urlManagerBackend' => array_merge(
                require $urlManagerBackendConfigPath,
                ['class' => 'yii\web\urlManager']
            )
        ],
    ];

    return $config;
