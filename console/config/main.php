<?php
    use console\utilities\InitManager\migrations\BoilerplateMigration;
    use console\utilities\InitManager\migrations\GeneralMigration;

    $dbConfigPath = dirname(dirname(__DIR__)) . '/common/config/db.php';
    $urlManagerFrontendConfigPath = dirname(__DIR__) . '/../frontend/config/urlManager.php';
    $urlManagerBackendConfigPath = dirname(__DIR__) . '/../backend/config/urlManager.php';

    $config = [
        'id' => 'fs-console',
        'language' => 'en-US',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
        'controllerNamespace' => 'console\controllers',
        'controllerMap' => [
            'fixture' => [
                'class' => 'yii\console\controllers\FixtureController',
                'namespace' => 'common\fixtures',
            ],
            'migrate' => [
                'class' => 'yii\console\controllers\MigrateController',
                'migrationNamespaces' => [
                    (new GeneralMigration())->getNamespace(),
                    (new BoilerplateMigration())->getNamespace()
                ],
                'migrationPath' => null
            ],
        ],
        'components' => [
            'authManager' => [
                'class' => 'yii\rbac\DbManager',
            ],
            'cache' => [
                'class' => 'yii\caching\FileCache',
                'cachePath' => '@frontend/runtime/cache'
            ],
            'log' => [
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'backend\utilities\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
            'mailer' => [
                'class' => 'common\utilities\Mailer',
            ],
            'smsGate' => [
                'class' => 'common\utilities\SmsGate',
            ],
            'accountService' => [
                'class' => 'console\utilities\InitManager\services\AccountService'
            ],
            'urlManagerFrontend' => array_merge(
                require $urlManagerFrontendConfigPath,
                ['class' => 'yii\web\urlManager']
            ),
            'urlManagerBackend' => array_merge(
                require $urlManagerBackendConfigPath,
                ['class' => 'yii\web\urlManager']
            )
        ],
    ];

    if (file_exists($dbConfigPath)) {
        $dbConfig = require $dbConfigPath;

        if (is_array($dbConfig)) {
            $config['components']['db'] = $dbConfig;
        }
    }

    return $config;
