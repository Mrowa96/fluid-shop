<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class GeneratorHasNotGenerateAnything
     * @package console\utilities\InitManager\exceptions
     */
    class GeneratorHasNotGenerateAnything extends \Exception
    {

    }