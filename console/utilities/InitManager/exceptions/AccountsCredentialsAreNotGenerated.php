<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class AccountsCredentialsAreNotGenerated
     * @package console\utilities\InitManager\exceptions
     */
    class AccountsCredentialsAreNotGenerated extends \Exception
    {

    }