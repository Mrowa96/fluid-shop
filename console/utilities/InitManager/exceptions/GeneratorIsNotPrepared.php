<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class GeneratorIsNotPrepared
     * @package console\utilities\InitManager\exceptions
     */
    class GeneratorIsNotPrepared extends \Exception
    {

    }