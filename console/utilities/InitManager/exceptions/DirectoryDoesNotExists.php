<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class DirectoryDoesNotExists
     * @package console\utilities\InitManager\exceptions
     */
    class DirectoryDoesNotExists extends \Exception
    {

    }