<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class LackOfDatabaseConnection
     * @package console\utilities\InitManager\exceptions
     */
    class LackOfDatabaseConnection extends \Exception
    {

    }