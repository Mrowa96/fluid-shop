<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class UserAlreadyExists
     * @package console\utilities\InitManager\exceptions
     */
    class UserAlreadyExists extends \Exception
    {

    }