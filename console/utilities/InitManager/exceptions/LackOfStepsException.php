<?php
    namespace console\utilities\InitManager\exceptions;

    /**
     * Class LackOfStepsException
     * @package console\utilities\InitManager\exceptions
     */
    class LackOfStepsException extends \Exception
    {

    }