<?php
    namespace console\utilities\InitManager;

    use console\utilities\InitManager\exceptions\LackOfStepsException;
    use console\utilities\InitManager\steps\Step;

    /**
     * Class InitManager
     * @package console\utilities\InitManager
     *
     * @property Step[] $steps
     */
    class InitManager
    {
        protected $steps;

        public function __construct()
        {
            $this->steps = [];
        }

        public function addStep(Step $step)
        {
            $this->steps[] = $step;

            return $this;
        }

        /**
         * @return $this
         * @throws LackOfStepsException
         */
        public function run()
        {
            if (!empty($this->steps)) {
                foreach ($this->steps as $index => $step) {
                    $step->before();
                    $step->action();
                    $step->after();

                    unset($this->steps[$index]);
                }

                return $this;
            }

            throw new LackOfStepsException('steps property is empty');
        }

        /**
         * @param Step $step
         * @return $this
         */
        public function runOne(Step $step)
        {
            $step->before();
            $step->action();
            $step->after();

            return $this;
        }
    }