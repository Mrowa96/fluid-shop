<?php
    namespace console\utilities\InitManager\templates;

    /**
     * Class AppConfigTestTemplate
     * @package console\utilities\InitManager\generators
     */
    class AppConfigTestTemplate extends AppConfigTemplate implements TemplateInterface
    {
        /**
         * @return string
         */
        public function getOutputFilePath()
        {
            return \Yii::getAlias("@common") . '/config/test.php';
        }

        /**
         * @return array
         */
        public function getRawOutput()
        {
            $cookieValidationKey = \Yii::$app->security->generateRandomString();

            $output = <<<STR
<?php return [
    'basePath' => dirname(__DIR__),
    'components' => [
        'db' => require dirname(__DIR__). "/config/db-test.php",
        'cache' => [
            'class' => \yii\caching\FileCache::className(),
        ],
        'request' => [
            'cookieValidationKey' => "$cookieValidationKey"
        ],
        'mailer' => [
            'class' => \common\utilities\Mailer::className()
        ],
        'smsGate' => [
            'class' => \common\utilities\SmsGate::class
        ],
        'authManager' => [
            'class' => \yii\\rbac\DbManager::className()
        ],
        'user' => [
            'identityClass' => \common\models\User::className(),
        ]
    ]
];
STR;

            return $output;
        }
    }