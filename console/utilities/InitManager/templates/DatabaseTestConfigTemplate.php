<?php
    namespace console\utilities\InitManager\templates;

    /**
     * Class DatabaseTestConfigTemplate
     * @package console\utilities\InitManager\generators
     */
    class DatabaseTestConfigTemplate extends DatabaseConfigTemplate implements TemplateInterface
    {
        /**
         * @return string
         */
        public function getOutputFilePath()
        {
            return \Yii::getAlias("@common") . '/config/db-test.php';
        }
    }