<?php
    namespace console\utilities\InitManager\templates;

    use Symfony\Component\Filesystem\Filesystem;
    use yii\db\Connection;

    /**
     * Class DatabaseConfigTemplate
     * @package console\utilities\InitManager\generators
     *
     * @property string $host
     * @property string $dbName
     * @property string $username
     * @property string $password
     * @property string $tablePrefix
     */
    class DatabaseConfigTemplate implements TemplateInterface
    {
        public $host;
        public $dbName;
        public $username;
        public $password;
        public $tablePrefix;

        /**
         * @return string
         */
        public function getOutputFilePath()
        {
            return \Yii::getAlias("@common") . '/config/db.php';
        }

        /**
         * @return array
         */
        public function getEditableProperties()
        {
            return [
                'host' => 'Set database host',
                'dbName' => 'Set database name',
                'username' => 'Set database username',
                'password' => 'Set database password',
                'tablePrefix' => 'Set database tables prefix',
            ];
        }

        /**
         * @return bool
         */
        public function hasEditableProperties()
        {
            return !empty($this->getEditableProperties());
        }

        /**
         * @return bool
         */
        public function isOutputFileExists()
        {
            $fs = new Filesystem();

            return $fs->exists($this->getOutputFilePath());
        }

        /**
         * @param bool $populateWithExistingData
         * @return void
         */
        public function populate($populateWithExistingData = true)
        {
            if ($populateWithExistingData && $this->isOutputFileExists()) {
                $fileContent = require $this->getOutputFilePath();

                if (is_array($fileContent) && !empty($fileContent)) {
                    foreach ($fileContent as $key => $value) {
                        if (property_exists($this, $key)) {
                            $this->{$key} = $value;
                        }
                    }
                }
            }
        }

        /**
         * @return array
         */
        public function getRawOutput()
        {
            return [
                'class' => Connection::className(),
                'dsn' => 'mysql:host=' . $this->host . ';dbname=' . $this->dbName,
                'username' => $this->username,
                'password' => $this->password,
                'charset' => 'utf8',
                'tablePrefix' => $this->tablePrefix
            ];
        }
    }