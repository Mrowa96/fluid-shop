<?php
    namespace console\utilities\InitManager\templates;

    use Symfony\Component\Filesystem\Filesystem;

    /**
     * Class BootstrapTemplate
     * @package console\utilities\InitManager\generators
     */
    class BootstrapTemplate implements TemplateInterface
    {
        public $domainLink;
        public $manageDomainLink;

        /**
         * @return string
         */
        public function getOutputFilePath()
        {
            return \Yii::getAlias("@common") . '/config/bootstrap.php';
        }

        /**
         * @return bool
         */
        public function isOutputFileExists()
        {
            $fs = new Filesystem();

            return $fs->exists($this->getOutputFilePath());
        }

        /**
         * @return array
         */
        public function getEditableProperties()
        {
            return [
                'domainLink' => 'Set up domain url',
                'manageDomainLink' => 'Set up admin panel domain url'
            ];
        }

        /**
         * @return bool
         */
        public function hasEditableProperties()
        {
            return !empty($this->getEditableProperties());
        }

        /**
         * @param bool $populateWithExistingData
         * @return void
         */
        public function populate($populateWithExistingData = true)
        {
            //TODO?
        }

        /**
         * @return string
         */
        public function getRawOutput()
        {
            $output = <<<STR
<?php
    Yii::setAlias('frontendModuleId', 'fs-frontend');
    Yii::setAlias('backendModuleId', 'fs-backend');

    Yii::setAlias('common', dirname(__DIR__));
    Yii::setAlias('frontend', dirname(dirname(__DIR__)) . '/frontend');
    Yii::setAlias('backend', dirname(dirname(__DIR__)) . '/backend');
    Yii::setAlias('console', dirname(dirname(__DIR__)) . '/console');

    Yii::setAlias('data', Yii::getAlias("@frontend")."/web/data");
    Yii::setAlias('frontendVar', Yii::getAlias("@frontend")."/var/data");
    Yii::setAlias('backendVar', Yii::getAlias("@backend")."/var/data");

    Yii::setAlias('dataProducts', Yii::getAlias("@data")."/products");
    Yii::setAlias('dataBanners', Yii::getAlias("@data")."/banners");
    Yii::setAlias('dataAvatars', Yii::getAlias("@data")."/avatars");
    Yii::setAlias('dataLogos', Yii::getAlias("@data")."/logos");
    Yii::setAlias('dataProductsExtra', Yii::getAlias("@data")."/products-extra");
    Yii::setAlias('dataInvoices', Yii::getAlias("@backendVar")."/invoices");
    Yii::setAlias('dataRma', Yii::getAlias("@backendVar")."/rma");
    Yii::setAlias('dataTemp', Yii::getAlias("@backendVar")."/temp");
    Yii::setAlias('dataCredentials', Yii::getAlias("@backendVar")."/credentials");
    Yii::setAlias('dataBackendTranslations', Yii::getAlias("@backendVar")."/translations");
    Yii::setAlias('dataFrontendTranslations', Yii::getAlias("@frontendVar")."/translations");
    Yii::setAlias('dataSubscribers', Yii::getAlias("@backendVar")."/subscribers");

    Yii::setAlias('domainLink', ".$this->domainLink");
    Yii::setAlias('frontendLink', "http://$this->domainLink");
    Yii::setAlias('backendLink', "http://$this->manageDomainLink");
    Yii::setAlias('dataLink', Yii::getAlias("@frontendLink")."/data");
    Yii::setAlias('licence', Yii::getAlias("@backendVar")."/licence/licence.json");

    Yii::setAlias("payuCache", Yii::getAlias("@frontend") . '/runtime/payu_cache');
    Yii::setAlias("googleMerchantData", Yii::getAlias("@frontend") . '/var/data/google-merchant');
STR;

            return $output;
        }
    }