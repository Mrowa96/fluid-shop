<?php
    namespace console\utilities\InitManager\templates;

    /**
     * Interface TemplateInterface
     * @package console\utilities\InitManager\generators
     */
    interface TemplateInterface
    {
        /**
         * @return string
         */
        public function getOutputFilePath();

        /**
         * @return bool
         */
        public function isOutputFileExists();

        /**
         * @return array
         */
        public function getEditableProperties();

        /**
         * @return bool
         */
        public function hasEditableProperties();

        /**
         * @return mixed
         */
        public function getRawOutput();

        /**
         * @param bool $populateWithExistingData
         * @return void
         */
        public function populate($populateWithExistingData = true);
    }