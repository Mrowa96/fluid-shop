<?php
    namespace console\utilities\InitManager\templates;

    use Symfony\Component\Filesystem\Filesystem;

    /**
     * Class AppConfigTemplate
     * @package console\utilities\InitManager\generators
     */
    class AppConfigTemplate implements TemplateInterface
    {
        /**
         * @return string
         */
        public function getOutputFilePath()
        {
            return \Yii::getAlias("@common") . '/config/main.php';
        }

        /**
         * @return bool
         */
        public function isOutputFileExists()
        {
            $fs = new Filesystem();

            return $fs->exists($this->getOutputFilePath());
        }

        /**
         * @return array
         */
        public function getEditableProperties()
        {
            return [];
        }

        /**
         * @return bool
         */
        public function hasEditableProperties()
        {
            return !empty($this->getEditableProperties());
        }

        /**
         * @param bool $populateWithExistingData
         * @return void
         */
        public function populate($populateWithExistingData = true)
        {
            //TODO?
        }

        /**
         * @return array
         */
        public function getRawOutput()
        {
            $cookieValidationKey = \Yii::$app->security->generateRandomString();
            $userIdentityCookie = \Yii::$app->security->generateRandomString();

            $output = <<<STR
<?php return [
    'vendorPath' => dirname(dirname(__DIR__)) . '/vendor',
    'components' => [
        'db' => require dirname(__DIR__). "/config/db.php",
        'cache' => [
            'class' => \yii\caching\FileCache::className(),
        ],
        'session' => [
            'class' => \yii\web\Session::className(),
            'cookieParams' => [
                'path' => '/',
                'domain' => \Yii::getAlias("@domainLink")
            ]
        ],
        'request' => [
            'cookieValidationKey' => "$cookieValidationKey"
        ],
        'mailer' => [
            'class' => \common\utilities\Mailer::className()
        ],
        'smsGate' => [
            'class' => \common\utilities\SmsGate::class
        ],
        'authManager' => [
            'class' => \yii\\rbac\DbManager::className()
        ],
        'user' => [
            'identityClass' => \common\models\User::className(),
            'enableAutoLogin' => true,
            'identityCookie' => [
                'name' => "$userIdentityCookie",
                'path' => '/',
                'domain' => \Yii::getAlias("@domainLink"),
            ]
        ]
    ]
];
STR;

            return $output;
        }
    }