<?php
    namespace console\utilities\InitManager\generators;

    use console\utilities\InitManager\exceptions\GeneratorIsNotPrepared;
    use console\utilities\InitManager\exceptions\InvalidOutputType;
    use Symfony\Component\Filesystem\Filesystem;

    /**
     * Class ArrayGenerator
     * @package console\utilities\InitManager\generators
     */
    class ArrayGenerator extends AbstractGenerator
    {
        /**
         * @throws GeneratorIsNotPrepared
         * @throws InvalidOutputType
         * @return void
         */
        public function generate()
        {
            if ($this->isPrepared()) {
                $fs = new Filesystem();
                $output = $this->template->getRawOutput();

                if (is_array($output)) {
                    $wrappedOutput = '<?php return ' . var_export($output, true) . ';';

                    $fs->dumpFile($this->template->getOutputFilePath(), $wrappedOutput);

                    $this->generated = true;
                } else {
                    throw new InvalidOutputType('Invalid output exception');
                }

            } else {
                throw new GeneratorIsNotPrepared('Generator is not prepared. Call prepare method first.');
            }
        }
    }