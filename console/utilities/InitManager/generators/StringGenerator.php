<?php
    namespace console\utilities\InitManager\generators;

    use console\utilities\InitManager\exceptions\GeneratorIsNotPrepared;
    use console\utilities\InitManager\exceptions\InvalidOutputType;
    use Symfony\Component\Filesystem\Filesystem;

    /**
     * Class StringGenerator
     * @package console\utilities\InitManager\generators
     */
    class StringGenerator extends AbstractGenerator
    {
        /**
         * @throws GeneratorIsNotPrepared
         * @throws InvalidOutputType
         * @return void
         */
        public function generate()
        {
            if ($this->isPrepared()) {
                $fs = new Filesystem();
                $output = $this->template->getRawOutput();

                if (is_string($output)) {
                    $fs->dumpFile($this->template->getOutputFilePath(), $output);

                    $this->generated = true;
                } else {
                    throw new InvalidOutputType('Invalid output exception');
                }

            } else {
                throw new GeneratorIsNotPrepared('Generator is not prepared. Call prepare method first.');
            }
        }
    }