<?php
    namespace console\utilities\InitManager\generators;

    use console\utilities\InitManager\exceptions\GeneratorHasNotGenerateAnything;
    use console\utilities\InitManager\templates\TemplateInterface;
    use yii\helpers\Console;

    /**
     * Class AbstractGenerator
     * @package console\utilities\InitManager\generators
     *
     * @property TemplateInterface $template
     * @property bool $prepared
     * @property bool $generated
     */
    abstract class AbstractGenerator
    {
        protected $template;
        protected $prepared;
        protected $generated;

        /**
         * ArrayGenerator constructor.
         * @param TemplateInterface $template
         */
        public final function __construct(TemplateInterface $template)
        {
            $this->template = $template;
            $this->prepared = false;
            $this->generated = false;
        }

        /**
         * @return bool
         */
        public final function isPrepared()
        {
            return $this->prepared;
        }

        /**
         * @return bool
         */
        public final function isGenerated()
        {
            return $this->generated;
        }

        /**
         * @return $this
         */
        public function prepare()
        {
            $this->template->populate();

            if ($this->template->hasEditableProperties()) {
                foreach ($this->template->getEditableProperties() as $property => $prompt) {
                    if (property_exists($this->template, $property)) {
                        $this->template->{$property} = Console::input($prompt . ': ');
                    }

                }
            }

            $this->prepared = true;

            return $this;
        }

        /**
         * @return mixed
         * @throws GeneratorHasNotGenerateAnything
         */
        public function getOutput()
        {
            if ($this->isGenerated()) {
                return require $this->template->getOutputFilePath();
            }

            throw new GeneratorHasNotGenerateAnything('Generate method of Generator was not called or failed.');
        }
    }