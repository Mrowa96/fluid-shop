<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\templates\AppConfigTemplate;
    use console\utilities\InitManager\templates\AppConfigTestTemplate;
    use console\utilities\InitManager\generators\StringGenerator;
    use console\utilities\InitManager\templates\BootstrapTemplate;

    /**
     * Class AppConfigurationStep
     * @package console\utilities\InitManager
     *
     * @property StringGenerator $configGenerator
     * @property StringGenerator $testConfigGenerator
     * @property StringGenerator $bootstrapGenerator
     */
    class AppConfigurationStep extends BaseStep implements Step
    {
        protected $configGenerator;
        protected $testConfigGenerator;
        protected $bootstrapGenerator;

        public function before()
        {
            $this->configGenerator = new StringGenerator(new AppConfigTemplate());
            $this->testConfigGenerator = new StringGenerator(new AppConfigTestTemplate());
            $this->bootstrapGenerator = new StringGenerator(new BootstrapTemplate());
        }

        public function action()
        {
            $this->configGenerator->prepare()->generate();
            $this->testConfigGenerator->prepare()->generate();
            $this->bootstrapGenerator->prepare()->generate();
        }

        public function after()
        {

        }
    }