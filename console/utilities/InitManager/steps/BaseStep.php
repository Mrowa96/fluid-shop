<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\output\ConsoleObserver;
    use console\utilities\InitManager\output\LogObserver;
    use console\utilities\InitManager\output\Output;

    /**
     * Class BaseStep
     * @package console\utilities\InitManager\steps
     *
     * @property Output $output
     */
    abstract class BaseStep
    {
        protected $output;

        /**
         * BaseStep constructor.
         */
        public function __construct()
        {
            $this->output = new Output();

            $this->output->attach(new LogObserver())->attach(new ConsoleObserver());
        }
    }