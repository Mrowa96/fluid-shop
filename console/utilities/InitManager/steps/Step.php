<?php
    namespace console\utilities\InitManager\steps;

    /**
     * Interface Step
     * @package console\utilities\InitManager
     */
    interface Step
    {
        public function before();
        public function action();
        public function after();
    }