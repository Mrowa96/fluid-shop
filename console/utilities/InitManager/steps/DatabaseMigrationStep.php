<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\exceptions\LackOfDatabaseConnection;
    use console\utilities\InitManager\migrations\MigrationInterface;
    use yii\console\controllers\MigrateController;

    /**
     * Class DatabaseMigrationStep
     * @package console\utilities\InitManager
     *
     * @property MigrationInterface $migration
     */
    class DatabaseMigrationStep extends BaseStep implements Step
    {
        protected $migration;

        public function __construct(MigrationInterface $migration)
        {
            parent::__construct();

            $this->migration = $migration;
        }

        public function before()
        {
            if (!\Yii::$app->db) {
                throw new LackOfDatabaseConnection('You have to provide database connection first.');
            }

            //TODO Send description to stdout
        }

        public function action()
        {
            try {
                $migrationController = new MigrateController('migrate', \Yii::$app);
                $migrationController->runAction('up', [
                    'migrationNamespaces' => $this->migration->getNamespace(),
                    'interactive' => false
                ]);

            } catch (\Exception $error) {
                print_r($error->getMessage());die();
                //TODO Set property status to false
                //$this->output->addMessage((new Message())->setException($error));
            }
        }

        public function after()
        {
            //TODO Read status property and send message
        }
    }