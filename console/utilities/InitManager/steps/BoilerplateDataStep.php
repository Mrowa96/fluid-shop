<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\exceptions\LackOfDatabaseConnection;
    use ZipArchive;

    /**
     * Class BoilerplateDataStep
     * @package console\utilities\InitManager
     */
    class BoilerplateDataStep extends BaseStep implements Step
    {
        public function before()
        {
            if (!\Yii::$app->db) {
                throw new LackOfDatabaseConnection('You have to provide database connection first.');
            }

            //TODO Send description to stdout
        }

        public function action()
        {
            try {
                $zip = new ZipArchive;

                if ($zip->open(\Yii::getAlias("@console") . '/migrations/boilerplate/data/boilerplate_data.zip') === true) {
                    $zip->extractTo(\Yii::getAlias("@frontend") . '/web/data');
                    $zip->close();
                } else {
                    echo "error";
                    //TODO Throw exception
                }

            } catch (\Exception $error) {
                print_r($error->getMessage());
                die();
                //TODO Set property status to false
            }
        }

        public function after()
        {
            //TODO Read status property and send message
        }
    }