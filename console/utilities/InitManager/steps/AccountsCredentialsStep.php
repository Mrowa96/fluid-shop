<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\services\AccountService;
    use console\utilities\InitManager\services\AccountService\AdminAccount;
    use console\utilities\InitManager\services\AccountService\GoogleAccount;
    use console\utilities\InitManager\services\AccountService\SystemAccount;

    /**
     * Class AccountsCredentialsStep
     * @package console\utilities\InitManager
     *
     * @property AccountService $accountService
     */
    class AccountsCredentialsStep implements Step
    {
        protected $accountService;

        public function before()
        {
            $this->accountService = \Yii::$app->accountService;
        }

        public function action()
        {
            $passwords = $this->accountService->generatePasswords();

            foreach ($passwords as $user => $password) {
                echo $user . " - " . $password . PHP_EOL;
            }

            $this->accountService->setAccount(new AdminAccount(), $this->accountService->getGeneratedPassword('admin'));
            $this->accountService->setAccount(new GoogleAccount(), $this->accountService->getGeneratedPassword('google'));
            $this->accountService->setAccount(new SystemAccount(), $this->accountService->getGeneratedPassword('system'));
        }

        public function after()
        {
            // TODO: Implement after() method.
        }
    }