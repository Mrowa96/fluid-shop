<?php
    namespace console\utilities\InitManager\steps;

    use console\utilities\InitManager\generators\ArrayGenerator;
    use console\utilities\InitManager\templates\DatabaseConfigTemplate;
    use console\utilities\InitManager\templates\DatabaseTestConfigTemplate;
    use yii\helpers\Console;

    /**
     * Class DatabaseConfigurationStep
     * @package console\utilities\InitManager
     *
     * @property ArrayGenerator $mainConfig
     * @property ArrayGenerator $testConfig
     */
    class DatabaseConfigurationStep extends BaseStep implements Step
    {
        protected $mainConfig;
        protected $testConfig;

        public function before()
        {
            $this->mainConfig = new ArrayGenerator(new DatabaseConfigTemplate());
            $this->testConfig = new ArrayGenerator(new DatabaseTestConfigTemplate());
        }

        public function action()
        {
            Console::output('Set up main database conenction:');
            $this->mainConfig->prepare()->generate();

            Console::output('Set up test database:');
            $this->testConfig->prepare()->generate();
        }

        public function after()
        {
            \Yii::$app->set('db', $this->mainConfig->getOutput());
        }
    }