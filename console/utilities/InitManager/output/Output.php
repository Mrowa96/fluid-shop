<?php
    namespace console\utilities\InitManager\output;

    use SplObserver;
    use SplSubject;

    //TODO Rewrite

    /**
     * Class Output
     * @package console\utilities\InitManager\output
     *
     * @property Message[] $messages
     * @property SplObserver[] $observers
     */
    final class Output implements SplSubject
    {
        private $messages;
        private $observers;

        /**
         * Output constructor.
         */
        public function __construct()
        {
            $this->observers = [];
        }

        /**
         * @param SplObserver $observer
         * @return $this
         */
        public function attach(SplObserver $observer)
        {
            $this->observers[spl_object_hash($observer)] = $observer;

            return $this;
        }

        /**
         * @param SplObserver $observer
         * @return $this
         */
        public function detach(SplObserver $observer)
        {
            unset($this->observers[spl_object_hash($observer)]);

            return $this;
        }

        /**
         * @return $this
         */
        public function notify()
        {
            foreach ($this->observers as $observer) {
                $observer->update($this);
            }

            return $this;
        }

        /**
         * @param Message $message
         * @return $this
         */
        public function addMessage(Message $message)
        {
            $this->messages[] = $message;
            $this->notify();

            return $this;
        }

        /**
         * @return Message[]
         */
        public function getMessages()
        {
            return $this->messages;
        }

        /**
         * @return Message|null
         */
        public function getLastMessage()
        {
            if (!empty($this->messages)) {
                $message = end($this->messages);

                reset($this->messages);

                return $message;
            }

            return null;
        }
    }