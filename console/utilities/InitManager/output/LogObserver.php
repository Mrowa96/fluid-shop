<?php
    namespace console\utilities\InitManager\output;

    use SplObserver;
    use SplSubject;

    /**
     * Class LogObserver
     * @package console\utilities\InitManager\output
     */
    class LogObserver implements SplObserver
    {
        /**
         * @param Output|SplSubject $subject
         */
        public function update(SplSubject $subject)
        {
            $message = $subject->getLastMessage();

            if ($message) {
                switch ($message->getStatus()) {
                    case Message::STATUS_FAIL:
                        \Yii::error($message->getText());
                        break;
                    case Message::STATUS_SUCCESS:
                        \Yii::info($message->getText());
                        break;
                }
            }

        }
    }