<?php
    namespace console\utilities\InitManager\output;

    use SplObserver;
    use SplSubject;

    /**
     * Class ConsoleObserver
     * @package console\utilities\InitManager\output
     */
    class ConsoleObserver implements SplObserver
    {
        /**
         * @param Output|SplSubject $subject
         */
        public function update(SplSubject $subject)
        {
            $message = $subject->getLastMessage();

            if ($message) {
                echo $message->getText() . PHP_EOL;
            }
        }
    }