<?php
    namespace console\utilities\InitManager\output;

    /**
     * Class Message
     * @package console\utilities\InitManager\steps
     *
     * @property string $text
     * @property string $status
     */
    class Message
    {
        const STATUS_FAIL = 0;
        const STATUS_SUCCESS = 1;

        protected $text;
        protected $status;

        /**
         * Message constructor.
         * @param string $text
         * @param string $status
         */
        public function __construct($text = null, $status = null)
        {
            $this->text = $text;
            $this->status = $status;
        }

        /**
         * @return mixed
         */
        public function getStatus()
        {
            return $this->status;
        }

        /**
         * @param mixed $status
         */
        public function setStatus($status)
        {
            $this->status = $status;
        }

        /**
         * @return string
         */
        public function getText()
        {
            return $this->text;
        }

        /**
         * @param string $text
         */
        public function setText($text)
        {
            $this->text = $text;
        }

        /**
         * @param \Exception $exception
         * @return $this
         */
        public function setException(\Exception $exception)
        {
            $this->setStatus(Message::STATUS_FAIL);
            $this->setText($exception->getMessage());

            return $this;
        }
    }