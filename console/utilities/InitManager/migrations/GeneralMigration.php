<?php
    namespace console\utilities\InitManager\migrations;

    /**
     * Class GeneralMigration
     * @package console\utilities\InitManager\migrations
     */
    class GeneralMigration implements MigrationInterface
    {
        public function getNamespace()
        {
            return 'console\migrations\general';
        }

        public function getPath()
        {
            return '@console/migrations/general';
        }
    }