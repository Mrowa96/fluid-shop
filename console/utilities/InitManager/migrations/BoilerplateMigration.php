<?php
    namespace console\utilities\InitManager\migrations;

    /**
     * Class BoilerplateMigration
     * @package console\utilities\InitManager\migrations
     */
    class BoilerplateMigration implements MigrationInterface
    {
        public function getNamespace()
        {
            return 'console\migrations\boilerplate';
        }

        public function getPath()
        {
            return '@console/migrations/boilerplate';
        }
    }