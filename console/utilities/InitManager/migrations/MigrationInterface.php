<?php
    namespace console\utilities\InitManager\migrations;

    /**
     * Interface MigrationInterface
     * @package console\utilities\InitManager\migrations
     */
    interface MigrationInterface
    {
        /**
         * @return string
         */
        public function getNamespace();

        /**
         * @return string
         */
        public function getPath();
    }