<?php
    namespace console\utilities\InitManager\services\AccountService;

    /**
     * Class AdminAccount
     * @package console\utilities\InitManager\services\AccountService
     */
    class AdminAccount implements AccountInterface
    {
        /**
         * @return string
         */
        public function getLogin()
        {
            return 'admin';
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'Administrator';
        }

        /**
         * @return string
         */
        public function getEmail()
        {
            return 'administrator@fluid-shop.pl';
        }

        /**
         * @return string
         */
        public function getAvatar()
        {
            return 'avatars/men3.png';
        }

        /**
         * @return string
         */
        public function getLanguage()
        {
            return 'EN-US';
        }

        /**
         * @return string
         */
        public function getRole()
        {
            return 'admin';
        }
    }