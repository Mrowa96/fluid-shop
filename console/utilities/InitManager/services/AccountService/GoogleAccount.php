<?php
    namespace console\utilities\InitManager\services\AccountService;

    /**
     * Class GoogleAccount
     * @package console\utilities\InitManager\services\AccountService
     */
    class GoogleAccount implements AccountInterface
    {
        /**
         * @return string
         */
        public function getLogin()
        {
            return 'google';
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'Google';
        }

        /**
         * @return string
         */
        public function getEmail()
        {
            return 'administrator-google@fluid-shop.pl';
        }

        /**
         * @return string
         */
        public function getAvatar()
        {
            return 'avatars/men4.png';
        }

        /**
         * @return string
         */
        public function getLanguage()
        {
            return 'EN-US';
        }

        /**
         * @return string
         */
        public function getRole()
        {
            return 'google';
        }
    }