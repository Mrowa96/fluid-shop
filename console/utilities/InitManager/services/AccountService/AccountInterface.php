<?php
    namespace console\utilities\InitManager\services\AccountService;

    /**
     * Interface AccountInterface
     * @package console\utilities\InitManager\services\AccountService
     */
    interface AccountInterface
    {
        /**
         * @return string
         */
        public function getLogin();

        /**
         * @return string
         */
        public function getName();

        /**
         * @return string
         */
        public function getEmail();

        /**
         * @return string
         */
        public function getAvatar();

        /**
         * @return string
         */
        public function getLanguage();

        /**
         * @return string
         */
        public function getRole();
    }