<?php
    namespace console\utilities\InitManager\services\AccountService;

    /**
     * Class SystemAccount
     * @package console\utilities\InitManager\services\AccountService
     */
    class SystemAccount implements AccountInterface
    {
        /**
         * @return string
         */
        public function getLogin()
        {
            return 'system';
        }

        /**
         * @return string
         */
        public function getName()
        {
            return 'System';
        }

        /**
         * @return string
         */
        public function getEmail()
        {
            return 'administrator-system@fluid-shop.pl';
        }

        /**
         * @return string
         */
        public function getAvatar()
        {
            return 'avatars/men5.png';
        }

        /**
         * @return string
         */
        public function getLanguage()
        {
            return 'EN-US';
        }

        /**
         * @return string
         */
        public function getRole()
        {
            return 'system';
        }
    }