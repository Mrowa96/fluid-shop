<?php
    namespace console\utilities\InitManager\services;

    use common\models\User;
    use common\utilities\Language;
    use console\utilities\InitManager\exceptions\AccountsCredentialsAreNotGenerated;
    use console\utilities\InitManager\exceptions\DirectoryDoesNotExists;
    use console\utilities\InitManager\exceptions\UserAlreadyExists;
    use console\utilities\InitManager\services\AccountService\AccountInterface;
    use Symfony\Component\Filesystem\Filesystem;
    use Symfony\Component\Finder\Finder;
    use yii\helpers\Json;

    /**
     * Class AccountService
     * @package console\utilities\InitManager\services
     *
     * @property Filesystem $fs
     * @property string $credentialsDirPath
     */
    class AccountService
    {
        const PASSWORD_LENGTH = 8;

        protected $fs;
        protected $credentialsDirPath;

        public function __construct()
        {
            $this->fs = new Filesystem();
            $this->credentialsDirPath = \Yii::getAlias('@dataCredentials');
        }

        /**
         * @param AccountInterface $account
         * @param null $password
         * @return bool
         * @throws UserAlreadyExists
         */
        public function setAccount(AccountInterface $account, $password = null)
        {
            if (!User::find()->where(['username' => $account->getLogin()])->one()) {
                $user = new User();
                $language = new Language();

                $user->username = $account->getLogin();
                $user->name = $account->getName();
                $user->email = $account->getEmail();
                $user->avatar = $account->getAvatar();
                $user->auth_key = User::generateAuthKey();
                $user->deletable = 0;

                if (is_null($password)) {
                    $password = \Yii::$app->security->generateRandomString(AccountService::PASSWORD_LENGTH);
                }

                $user->setPassword($password);
                $language->setUserLanguage($user, $account->getLanguage());

                if ($user->save()) {
                    return $user->setRole($account->getRole());
                }

                return false;
            }

            throw new UserAlreadyExists('User already exists.');
        }

        /**
         * @return array || void
         */
        public function generatePasswords()
        {
            if ($this->fs->exists($this->credentialsDirPath)) {
                $filename = $this->generateFilename();
                $passwords = [
                    'system' => \Yii::$app->security->generateRandomString(AccountService::PASSWORD_LENGTH),
                    'admin' => \Yii::$app->security->generateRandomString(AccountService::PASSWORD_LENGTH),
                    'google' => \Yii::$app->security->generateRandomString(AccountService::PASSWORD_LENGTH)
                ];

                $this->fs->dumpFile($filename, Json::encode($passwords));

                return $passwords;
            }
        }

        /**
         * @param string $name
         * @return string
         * @throws AccountsCredentialsAreNotGenerated
         * @throws DirectoryDoesNotExists
         */
        public function getGeneratedPassword($name)
        {
            $fs = new Filesystem();
            $finder = new Finder();
            $credentialsDirPath = \Yii::getAlias('@dataCredentials');

            if ($fs->exists($credentialsDirPath)) {
                $finder->files()->in($credentialsDirPath)->sortByModifiedTime()->name('*.json');

                $data = iterator_to_array($finder);

                if (!empty($data)) {
                    $newest = json_decode(file_get_contents(end($data)->getRealPath()));

                    if (is_a($newest, \stdClass::class) && property_exists($newest, $name)) {
                        return $newest->{$name};
                    }
                }

                throw new AccountsCredentialsAreNotGenerated('Accounts credentials are not generated.');
            }

            throw new DirectoryDoesNotExists('Directory does not exists.');
        }

        /**
         * @return string
         */
        protected function generateFilename()
        {
            $randomString = \Yii::$app->security->generateRandomString(8);
            $currentDate = (new \DateTime())->format("Y-m-d H:i:s");

            return $this->credentialsDirPath . '/credentials_' . $currentDate . '_' . $randomString . '.json';
        }
    }