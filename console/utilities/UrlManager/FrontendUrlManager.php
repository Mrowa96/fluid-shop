<?php
    namespace console\utilities\UrlManager;

    /**
     * Class FrontendUrlManager
     * @package console\utilities\UrlManager
     */
    class FrontendUrlManager extends UrlManagerInterface
    {
        protected function init()
        {
            $this->basePath = \Yii::getAlias('@frontendLink');
            $this->configPath = dirname(__DIR__) . '/../../frontend/config/urlManager.php';
            $this->name = 'frontend';
        }
    }