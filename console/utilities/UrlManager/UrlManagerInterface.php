<?php
    namespace console\utilities\UrlManager;

    /**
     * Class UrlManagerInterface
     * @package console\utilities
     *
     * @property string $basePath
     * @property string $configPath
     * @property string $name
     */
    abstract class UrlManagerInterface
    {
        protected $basePath;
        protected $configPath;
        protected $name;

        /**
         * UrlManagerInterface constructor.
         */
        public function __construct()
        {
            $this->init();
        }

        /**
         * @return string
         */
        public function getBasePath()
        {
            return $this->basePath;
        }

        /**
         * @return string
         * @throws UrlManagerException
         */
        public function getConfigPath()
        {
            if (file_exists($this->configPath)) {
                return $this->configPath;
            }

            throw new UrlManagerException('Configuration file not found');
        }

        /**
         * @return array
         */
        public function getConfig()
        {
            return require $this->getConfigPath();
        }

        /**
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        abstract protected function init();
    }