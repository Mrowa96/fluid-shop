<?php
    namespace console\utilities\UrlManager;

    /**
     * Class BackendUrlManager
     * @package console\utilities\UrlManager
     */
    class BackendUrlManager extends UrlManagerInterface
    {
        protected function init()
        {
            $this->basePath = \Yii::getAlias('@backendLink');
            $this->configPath = dirname(__DIR__) . '/../../backend/config/urlManager.php';
            $this->name = 'backend';
        }
    }