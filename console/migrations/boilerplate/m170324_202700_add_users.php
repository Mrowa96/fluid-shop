<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202700_add_users extends Migration
    {
        public function up()
        {
            $users = Json::decode(file_get_contents(__DIR__ . '/schema/users.json'), false);

            if (!empty($users)) {
                foreach ($users as $user) {
                    $this->insert('{{%user}}', [
                        'id' => $user->id,
                        'username' => $user->username,
                        'name' => $user->name,
                        'email' => $user->email,
                        'avatar' => $user->avatar,
                        'auth_key' => $user->auth_key,
                        'password_hash' => $user->password_hash,
                        'language' => $user->language,
                        'status' => $user->status,
                        'deletable' => $user->deletable,
                        'created_at' => $user->created_at,
                        'updated_at' => $user->updated_at,
                        'deleted' => $user->deleted
                    ]);

                    if (!empty($user->auth_assignment)) {
                        $this->insert('{{%auth_assignment}}', [
                            'user_id' => $user->id,
                            'item_name' => $user->auth_assignment->item_name,
                            'created_at' => $user->auth_assignment->created_at
                        ]);
                    }
                }
            }
        }
    }