<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_222700_add_settings extends Migration
    {
        public function up()
        {
            $settings = Json::decode(file_get_contents(__DIR__ . '/schema/settings.json'), false);

            if (!empty($settings)) {
                foreach ($settings as $setting) {
                    $this->update('{{%settings}}', [
                        'value' => $setting->value
                    ], [
                        'id' => $setting->id
                    ]);
                }
            }
        }
    }