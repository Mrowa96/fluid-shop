<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_transports extends Migration
    {
        public function up()
        {
            $transports = Json::decode(file_get_contents(__DIR__ . '/schema/transports.json'), false);

            if (!empty($transports)) {
                foreach ($transports as $transport) {
                    $this->insert('{{%transport}}', [
                        'id' => $transport->id,
                        'transport_api_id' => $transport->transport_api_id,
                        'name' => $transport->name,
                        'icon' => $transport->icon,
                        'description' => $transport->description,
                        'range_from' => $transport->range_from,
                        'range_to' => $transport->range_to,
                        'cost' => $transport->cost,
                        'active' => $transport->active,
                        'deleted' => $transport->deleted
                    ]);
                }
            }
        }
    }