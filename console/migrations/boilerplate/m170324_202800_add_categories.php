<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_categories extends Migration
    {
        public function up()
        {
            $categories = Json::decode(file_get_contents(__DIR__ . '/schema/categories.json'), false);

            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $this->insert('{{%category}}', [
                        'id' => $category->id,
                        'parent_id' => $category->parent_id,
                        'discount_id' => $category->discount_id,
                        'name' => $category->name,
                        'url' => $category->url,
                        'description' => $category->description,
                        'seo_description' => $category->seo_description,
                        'seo_keywords' => $category->seo_keywords,
                        'created_date' => $category->created_date,
                        'modified_date' => $category->modified_date,
                        'deleted' => $category->deleted
                    ]);
                }
            }
        }
    }