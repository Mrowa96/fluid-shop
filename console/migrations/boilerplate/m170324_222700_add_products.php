<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_222700_add_products extends Migration
    {
        public function up()
        {
            $products = Json::decode(file_get_contents(__DIR__ . '/schema/products.json'), false);

            if (!empty($products)) {
                foreach ($products as $product) {
                    $this->insert('{{%product}}', [
                        'id' => $product->id,
                        'category_id' => $product->category_id,
                        'discount_id' => $product->discount_id,
                        'name' => $product->name,
                        'brand' => $product->brand,
                        'processed_name' => $product->processed_name,
                        'description' => $product->description,
                        'additional_field' => $product->additional_field,
                        'previous_cost' => $product->previous_cost,
                        'cost' => $product->cost,
                        'quantity' => $product->quantity,
                        'available' => $product->available,
                        'bought_count' => $product->bought_count,
                        'code' => $product->code,
                        'type' => $product->type,
                        'modified_date' => $product->modified_date,
                        'created_date' => $product->created_date,
                        'recommended_product' => $product->recommended_product,
                        'promotional_product' => $product->promotional_product,
                        'check_out_product' => $product->check_out_product,
                        'deleted' => $product->deleted
                    ]);

                    if (!empty($product->attributes)) {
                        foreach ($product->attributes as $attribute) {
                            $this->insert('{{%product_attribute}}', [
                                "product_id" => $product->id,
                                "attribute_id" => $attribute->attribute_id,
                                "attribute_option_id" => $attribute->attribute_option_id
                            ]);
                        }
                    }

                    if (!empty($product->images)) {
                        foreach ($product->images as $image) {
                            $this->insert('{{%product_image}}', [
                                "product_id" => $product->id,
                                "name" => $image->name,
                                "thumb_path" => $image->thumb_path,
                                "path" => $image->path,
                                "main" => $image->main
                            ]);
                        }
                    }

                    if (!empty($product->reviews)) {
                        foreach ($product->reviews as $review) {
                            $this->insert('{{%product_review}}', [
                                "product_id" => $product->id,
                                "user_id" => $review->user_id,
                                "rating" => $review->rating,
                                "text" => $review->text,
                                "accepted" => $review->accepted,
                                "created_date" => $review->created_date
                            ]);
                        }
                    }

                    if (!empty($product->upSells)) {
                        foreach ($product->upSells as $upSell) {
                            $this->insert('{{%product_sell_up}}', [
                                "product_id" => $product->id,
                                "up_product_id" => $upSell->up_product_id,
                                "cost" => $upSell->cost
                            ]);
                        }
                    }
                }
            }
        }
    }