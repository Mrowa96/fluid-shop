<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_payments extends Migration
    {
        public function up()
        {
            $payments = Json::decode(file_get_contents(__DIR__ . '/schema/payments.json'), false);

            if (!empty($payments)) {
                foreach ($payments as $payment) {
                    $this->insert('{{%payment}}', [
                        'id' => $payment->id,
                        'payment_api_id' => $payment->payment_api_id,
                        'name' => $payment->name,
                        'icon' => $payment->icon,
                        'description' => $payment->description,
                        'cost' => $payment->cost,
                        'active' => $payment->active,
                        'deleted' => $payment->deleted
                    ]);
                }
            }
        }
    }