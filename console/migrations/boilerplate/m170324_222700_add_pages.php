<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_222700_add_pages extends Migration
    {
        public function up()
        {
            $pages = Json::decode(file_get_contents(__DIR__ . '/schema/pages.json'), false);

            if (!empty($pages)) {
                foreach ($pages as $page) {
                    $this->insert('{{%page}}', [
                        'id' => $page->id,
                        'page_category_id' => $page->page_category_id,
                        'title' => $page->title,
                        'url' => $page->url,
                        'content' => $page->content,
                        'type' => $page->type,
                        'seo_keywords' => $page->seo_keywords,
                        'seo_description' => $page->seo_description,
                        'add_to_header' => $page->add_to_header,
                        'add_to_footer' => $page->add_to_footer,
                        'created_date' => $page->created_date,
                        'modified_date' => $page->modified_date,
                        'deleted' => $page->deleted
                    ]);
                }
            }
        }
    }