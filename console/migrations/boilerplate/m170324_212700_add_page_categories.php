<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_212700_add_page_categories extends Migration
    {
        public function up()
        {
            $categories = Json::decode(file_get_contents(__DIR__ . '/schema/page_categories.json'), false);

            if (!empty($categories)) {
                foreach ($categories as $category) {
                    $this->insert('{{%page_category}}', [
                        'id' => $category->id,
                        'name' => $category->name,
                        'deleted' => $category->deleted
                    ]);
                }
            }
        }
    }