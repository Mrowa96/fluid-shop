<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_products_extra extends Migration
    {
        public function up()
        {
            $products = Json::decode(file_get_contents(__DIR__ . '/schema/products_extra.json'), false);

            if (!empty($products)) {
                foreach ($products as $product) {
                    $this->insert('{{%product}}', [
                        'id' => $product->id,
                        'name' => $product->name,
                        'cost' => $product->cost,
                        'description' => $product->description,
                        'available' => $product->available,
                        'quantity' => $product->quantity,
                        'code' => $product->code,
                        'type' => $product->type,
                        'created_date' => $product->created_date,
                        'modified_date' => $product->modified_date
                    ]);
                }
            }
        }
    }