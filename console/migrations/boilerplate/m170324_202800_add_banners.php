<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_banners extends Migration
    {
        public function up()
        {
            $banners = Json::decode(file_get_contents(__DIR__ . '/schema/banners.json'), false);

            if (!empty($banners)) {
                foreach ($banners as $banner) {
                    $this->insert('{{%banner}}', [
                        'id' => $banner->id,
                        'type' => $banner->type,
                        'name' => $banner->name,
                        'link' => $banner->link,
                        'path' => $banner->path,
                        'thumb_path' => $banner->thumb_path,
                        'display_on_mobile' => $banner->display_on_mobile,
                        'display_on_desktop' => $banner->display_on_desktop
                    ]);
                }
            }
        }
    }