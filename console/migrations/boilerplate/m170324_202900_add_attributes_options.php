<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202900_add_attributes_options extends Migration
    {
        public function up()
        {
            $options = Json::decode(file_get_contents(__DIR__ . '/schema/attributes_options.json'), false);

            if (!empty($options)) {
                foreach ($options as $option) {
                    $this->insert('{{%attribute_option}}', [
                        'id' => $option->id,
                        'attribute_id' => $option->attribute_id,
                        'name' => $option->name
                    ]);
                }
            }
        }
    }