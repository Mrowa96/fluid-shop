<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_202800_add_attributes extends Migration
    {
        public function up()
        {
            $attributes = Json::decode(file_get_contents(__DIR__ . '/schema/attributes.json'), false);

            if (!empty($attributes)) {
                foreach ($attributes as $attribute) {
                    $this->insert('{{%attribute}}', [
                        'id' => $attribute->id,
                        'name' => $attribute->name
                    ]);
                }
            }
        }
    }