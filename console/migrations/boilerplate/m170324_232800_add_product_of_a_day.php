<?php
    namespace console\migrations\boilerplate;

    use yii\db\Migration;
    use yii\helpers\Json;

    class m170324_232800_add_product_of_a_day extends Migration
    {
        public function up()
        {
            $product = Json::decode(file_get_contents(__DIR__ . '/schema/product_of_a_day.json'), false);

            $this->insert('{{%product_of_a_day}}', [
                'id' => $product->id,
                'product_id' => $product->product_id,
                'cost' => $product->cost,
                'quantity' => $product->quantity,
                'expire_date' => $product->expire_date,
                'active' => $product->active,
                'deleted' => $product->deleted
            ]);
        }
    }