<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161017_212634_initialize_grid_view_config_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%grid_view_config}}', [
                'id' => $this->primaryKey()->notNull(),
                'user_id' => $this->integer()->notNull(),
                'model_id' => $this->integer()->notNull(),
                'property' => $this->string(128)->notNull(),
                'value' => $this->smallInteger()->null()->defaultValue(0),
                'type' => $this->string(64)->null()->defaultValue('text'),
                'class' => $this->string(128)->null(),
                'position' => $this->integer()
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-grid_view_config-user_id', '{{%grid_view_config}}', 'user_id');
            $this->createIndex('idx-grid_view_config-model_id', '{{%grid_view_config}}', 'model_id');
        }

        public function down()
        {
            echo "m161017_212634_initialize_grid_config_table cannot be reverted.\n";

            return false;
        }
    }
