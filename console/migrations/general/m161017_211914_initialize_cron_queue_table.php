<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161017_211914_initialize_cron_queue_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%cron_queue}}', [
                'id' => $this->primaryKey()->notNull(),
                'data' => $this->text()->null(),
                'type' => $this->string(128)->notNull(),
                'description' => $this->text()->null(),
                'status' => $this->integer()->defaultValue(0)
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161017_211914_initialize_cron_queue_table cannot be reverted.\n";

            return false;
        }
    }
