<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_115950_initialize_page extends Migration
    {
        public function up()
        {
            $this->createTable('{{%page}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'page_category_id' => $this->integer(11)->null(),
                'title' => $this->string(256)->notNull(),
                'url' => $this->string(512)->notNull(),
                'content' => $this->text()->notNull(),
                'type' => "ENUM('standard', 'contact') NOT NULL DEFAULT 'standard'",
                'seo_keywords' => $this->string(512)->null(),
                'seo_description' => $this->text()->null(),
                'add_to_header' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'add_to_footer' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'created_date' => $this->dateTime()->notNull(),
                'modified_date' => $this->dateTime()->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-page-page_category_id', '{{%page}}', 'page_category_id');
        }

        public function down()
        {
            echo "m161112_115950_initialize_page cannot be reverted.\n";

            return false;
        }
    }
