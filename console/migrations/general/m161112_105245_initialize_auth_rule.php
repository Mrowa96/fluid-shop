<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_105245_initialize_auth_rule extends Migration
    {
        public function up()
        {
            $this->createTable('{{%auth_rule}}', [
                'name' => $this->string(64)->notNull(),
                'data' => $this->text()->notNull(),
                'created_at' => $this->integer(11)->notNull(),
                'updated_at' => $this->integer(11)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->addPrimaryKey('prx-auth_rule-name', '{{%auth_rule}}', 'name');
        }

        public function down()
        {
            echo "m161112_105245_initialize_auth_rule cannot be reverted.\n";

            return false;
        }
    }
