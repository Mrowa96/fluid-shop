<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_110233_initialize_i18n_source_frontend extends Migration
    {
        public function up()
        {
            $this->createTable('{{%i18n_source_frontend}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'category' => $this->string(255)->null(),
                'message' => $this->text()->null(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161112_110233_initialize_i18n_source_frontend cannot be reverted.\n";

            return false;
        }
    }
