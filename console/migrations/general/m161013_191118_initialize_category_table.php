<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_191118_initialize_category_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%category}}', [
                'id' => $this->primaryKey()->notNull(),
                'parent_id' => $this->integer()->defaultValue(0)->notNull(),
                'discount_id' => $this->integer()->null(),
                'name' => $this->string(256)->notNull(),
                'url' => $this->string(256)->notNull(),
                'description' => $this->text()->notNull(),
                'seo_description' => $this->text()->null(),
                'seo_keywords' => $this->string(256)->null(),
                'created_date' => $this->dateTime(),
                'modified_date' => $this->dateTime(),
                'deleted' => $this->smallInteger(4)->defaultValue(0)
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-category-discount_id', '{{%category}}', 'discount_id');
        }

        public function down()
        {
            echo "m161013_191118_initialize_category_table cannot be reverted.\n";

            return false;
        }
    }
