<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_120909_initialize_product extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'category_id' => $this->integer(11)->null(),
                'discount_id' => $this->integer(11)->null(),
                'name' => $this->string(256)->notNull(),
                'brand' => $this->string(256)->null(),
                'processed_name' => $this->string(256)->notNull(),
                'description' => $this->text()->null(),
                'additional_field' => $this->text()->null(),
                'previous_cost' => $this->double()->notNull(),
                'cost' => $this->double()->notNull(),
                'quantity' => $this->integer(11)->notNull(),
                'available' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'bought_count' => $this->integer(11)->notNull()->defaultValue(0),
                'code' => $this->string(128)->notNull(),
                'type' => "ENUM('standard', 'extra') NOT NULL DEFAULT 'standard'",
                'modified_date' => $this->dateTime()->notNull(),
                'created_date' => $this->dateTime()->notNull(),
                'recommended_product' => $this->smallInteger(4)->notNull()->defaultValue(0),
                'promotional_product' => $this->smallInteger(4)->notNull()->defaultValue(0),
                'check_out_product' => $this->smallInteger(4)->notNull()->defaultValue(0),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product-discount_id', '{{%product}}', 'discount_id');
            $this->createIndex('idx-product-category_id', '{{%product}}', 'category_id');
        }

        public function down()
        {
            echo "m161112_120909_initialize_product cannot be reverted.\n";

            return false;
        }
    }
