<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_114415_initialize_order_detail extends Migration
    {
        public function up()
        {
            $this->createTable('{{%order_detail}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'order_id' => $this->integer(20)->notNull(),
                'order_detail_related_id' => $this->integer(11)->null(),
                'product_id' => $this->integer(11)->notNull(),
                'product_quantity' => $this->integer(11)->notNull(),
                'product_type' => $this->integer(11)->notNull()->defaultValue(0),
                'product_cost' => $this->double()->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-order_detail-order_id', '{{%order_detail}}', 'order_id');
            $this->createIndex('idx-order_detail-product_id', '{{%order_detail}}', 'product_id');
            $this->createIndex('idx-order_detail-order_detail_related_id', '{{%order_detail}}', 'id');
        }

        public function down()
        {
            echo "m161112_114415_initialize_order_detail cannot be reverted.\n";

            return false;
        }
    }
