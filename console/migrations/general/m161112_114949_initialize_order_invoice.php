<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_114949_initialize_order_invoice extends Migration
    {
        public function up()
        {
            $this->createTable('{{%order_invoice}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'order_id' => $this->integer(20)->notNull(),
                'name' => $this->string(256)->notNull(),
                'created_date' => $this->dateTime()->notNull(),
                'modified_date' => $this->dateTime()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-order_invoice-order_id', '{{%order_invoice}}', 'order_id');
        }

        public function down()
        {
            echo "m161112_114949_initialize_order_invoice cannot be reverted.\n";

            return false;
        }
    }
