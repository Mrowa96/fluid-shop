<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161017_212138_initialize_discount_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%discount}}', [
                'id' => $this->primaryKey()->notNull(),
                'threshold_percentage' => $this->integer()->null()->defaultValue(0),
                'threshold_numeric' => $this->integer()->null()->defaultValue(0),
                'code' => $this->string(128)->null(),
                'amount' => $this->integer()->null(),
                'expire' => $this->dateTime()->null(),
                'dynamic' => $this->smallInteger()->defaultValue(0),
                'deleted' => $this->smallInteger()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161017_212138_initialize_discount_table cannot be reverted.\n";

            return false;
        }
    }
