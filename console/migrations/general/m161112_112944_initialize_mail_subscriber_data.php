<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_112944_initialize_mail_subscriber_data extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail_subscriber_data}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'mail_id' => $this->integer(11)->notNull(),
                'mail_subscriber_id' => $this->integer(11)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-mail_subscriber_data-mail_id', '{{%mail_subscriber_data}}', 'mail_id');
            $this->createIndex('idx-mail_subscriber_data-mail_subscriber_id', '{{%mail_subscriber_data}}', 'mail_subscriber_id');
        }

        public function down()
        {
            echo "m161112_112944_initialize_mail_subscriber_data cannot be reverted.\n";

            return false;
        }
    }
