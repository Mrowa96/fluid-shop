<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_124139_initialize_rma_images extends Migration
    {
        public function up()
        {
            $this->createTable('{{%rma_images}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'rma_id' => $this->integer(11)->notNull(),
                'thumb_path' => $this->string(768)->notNull(),
                'path' => $this->string(512)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-rma_images-order_id', '{{%rma_images}}', 'rma_id');
        }

        public function down()
        {
            echo "m161112_124139_initialize_rma_images cannot be reverted.\n";

            return false;
        }
    }
