<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_113117_initialize_order extends Migration
    {
        public function up()
        {
            $this->createTable('{{%order}}', [
                'id' => $this->primaryKey(20)->notNull(),
                'user_id' => $this->integer(11)->null(),
                'address_id' => $this->integer(11)->null(),
                'transport_id' => $this->integer(11)->null(),
                'payment_id' => $this->integer(11)->null(),
                'order_status_id' => $this->integer(11)->notNull(),
                'key' => $this->string(128)->null(),
                'type' => "ENUM('standard') NOT NULL DEFAULT 'standard'",
                'cost' => $this->double()->null(),
                'note' => $this->text()->null(),
                'data' => $this->text()->null(),
                'basket_create_date' => $this->dateTime()->notNull(),
                'create_date' => $this->dateTime()->notNull(),
                'modify_date' => $this->dateTime()->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-order-key', '{{%order}}', 'key', true);
            $this->createIndex('idx-order-address_id', '{{%order}}', 'address_id');
            $this->createIndex('idx-order-transport_id', '{{%order}}', 'transport_id');
            $this->createIndex('idx-order-payment_id', '{{%order}}', 'payment_id');
            $this->createIndex('idx-order-order_status_id', '{{%order}}', 'order_status_id');
        }

        public function down()
        {
            echo "m161112_113117_initialize_order cannot be reverted.\n";

            return false;
        }
    }
