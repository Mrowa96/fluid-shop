<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_115250_initialize_order_status extends Migration
    {
        public function up()
        {
            $this->createTable('{{%order_status}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(256)->notNull(),
                'message' => $this->text()->notNull(),
                'description' => $this->text()->null(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_115250_initialize_order_status cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->insert('{{%order_status}}', [
                'name' => 'New (not complete)',
                'message' => 'Order was created, but is still unconfirmed.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'New (complete)',
                'message' => 'Order was created and confirmed.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'Paid',
                'message' => 'Order is paid.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'In progress',
                'message' => 'Order is in progress.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'Prepared to send',
                'message' => 'Order is prepared to be send.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'Send',
                'message' => 'Order was send.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'Finished',
                'message' => 'Order is finished.'
            ]);
            $this->insert('{{%order_status}}', [
                'name' => 'Canceled',
                'message' => 'Order is canceled.'
            ]);
        }
    }
