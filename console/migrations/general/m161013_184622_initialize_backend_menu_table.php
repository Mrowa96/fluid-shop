<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_184622_initialize_backend_menu_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%backend_menu}}', [
                'id' => $this->primaryKey()->notNull(),
                'parent_id' => $this->integer()->defaultValue(0)->notNull(),
                'name' => $this->string(256)->notNull(),
                'symbol' => $this->string(64)->notNull(),
                'route' => $this->string(256)->notNull(),
                'routeOptions' => $this->string(256)->null(),
                'icon' => $this->string(128)->notNull()->defaultValue('fa fa-th'),
                'position' => $this->integer()->notNull(),
                'admin_access' => $this->smallInteger()->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161013_184622_initialize_backend_menu_table cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            /** Main */
            $this->quickInsert('Home', 'site', 'fa fa-dashboard', 1);
            $this->quickInsert('Products', '#', 'fa fa-table', 2);
            $this->quickInsert('Orders', '#', 'fa fa-shopping-basket', 3);
            $this->quickInsert('Categories', 'category', 'fa fa-sitemap', 4);
            $this->quickInsert('Payments', 'payment', 'fa fa-credit-card', 5);
            $this->quickInsert('Transports', 'transport', 'fa fa-truck', 6);
            $this->quickInsert('Discounts', 'discount', 'fa fa-money', 7);
            $this->quickInsert('Rma', 'rma', 'fa fa-database', 8);
            $this->quickInsert('Mailing', 'mail', 'fa fa-newspaper-o', 9);
            $this->quickInsert('Banners', 'banner', 'fa fa-image', 10);
            $this->quickInsert('Pages', '#', 'fa fa-th', 11);
            $this->quickInsert('Users', 'user', 'fa fa-users', 12);
            $this->quickInsert('Languages', 'languages', 'fa fa-language', 13, 0, 1);
            $this->quickInsert('Settings', 'settings', 'fa fa-cogs', 14, 0, 1);

            /** First sub-level */
            $this->quickInsert('Manage', 'product', 'fa fa-circle-thin', 1, 2);
            $this->quickInsert('Attributes', 'attribute', 'fa fa-filter', 2, 2);
            $this->quickInsert('Extras', 'product-extra', 'fa fa-gift', 3, 2);
            $this->quickInsert('Product of a day', 'product-of-a-day', 'fa fa-th', 4, 2);

            $this->quickInsert('Manage', 'order', 'fa fa-circle-thin', 1, 3);
            $this->quickInsert('Statuses', 'order-status', 'fa fa-braille', 2, 3);

            $this->quickInsert('Manage', 'page', 'fa fa-circle-thin', 1, 11);
            $this->quickInsert('Page categories', 'page-category', 'fa fa-th', 2, 11);

            $this->quickInsert('Frontend languages', 'language', 'fa fa-tag', 1, 13, 1, 'module=frontend');
            $this->quickInsert('Backend languages', 'language', 'fa fa-tag', 2, 13, 1, 'module=backend');
        }
        protected function quickInsert($name, $route, $icon, $position, $parentId = 0, $admin = 0, $routeOptions = null)
        {
            $this->insert('{{%backend_menu}}', [
                'parent_id' => $parentId,
                'name' => $name,
                'symbol' => str_replace("-", "_", mb_strtoupper($route)),
                'route' => $route,
                'icon' => $icon,
                'position' => $position,
                'admin_access' => $admin,
                'routeOptions' => $routeOptions
            ]);
        }
    }
