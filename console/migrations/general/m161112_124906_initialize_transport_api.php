<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_124906_initialize_transport_api extends Migration
    {
        public function up()
        {
            $this->createTable('{{%transport_api}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(256)->notNull(),
                'symbol' => $this->string(256)->notNull(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161112_124906_initialize_transport_api cannot be reverted.\n";

            return false;
        }
    }
