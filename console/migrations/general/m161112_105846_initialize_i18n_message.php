<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_105846_initialize_i18n_message extends Migration
    {
        public function up()
        {
            $this->createTable('{{%i18n_message}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'source_id' => $this->integer(11)->notNull(),
                'i18n_id' => $this->integer(11)->notNull(),
                'translation' => $this->text()->null(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-i18n_message-id', '{{%i18n_message}}', 'id', true);
            $this->createIndex('idx-i18n_message-i18n_id', '{{%i18n_message}}', 'i18n_id');
        }

        public function down()
        {
            echo "m161112_105846_initialize_i18n_message cannot be reverted.\n";

            return false;
        }
    }
