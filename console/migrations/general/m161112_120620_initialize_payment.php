<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_120620_initialize_payment extends Migration
    {
        public function up()
        {
            $this->createTable('{{%payment}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'payment_api_id' => $this->integer(11)->null(),
                'name' => $this->string(128)->notNull(),
                'cost' => $this->double()->defaultValue(0),
                'icon' => $this->string(256)->null(),
                'description' => $this->text()->null(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-payment-payment_api_id', '{{%payment}}', 'payment_api_id');
        }

        public function down()
        {
            echo "m161112_120620_initialize_payment cannot be reverted.\n";

            return false;
        }
    }
