<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_123939_initialize_rma extends Migration
    {
        public function up()
        {
            $this->createTable('{{%rma}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'order_id' => $this->integer(20)->notNull(),
                'user_id' => $this->integer(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'text' => $this->text()->notNull(),
                'status' => $this->smallInteger(4)->notNull()->defaultValue(0),
                'created_date' => $this->dateTime()->notNull(),
                'modified_date' => $this->dateTime()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-rma-order_id', '{{%rma}}', 'order_id');
            $this->createIndex('idx-rma-user_id', '{{%rma}}', 'user_id');
            $this->createIndex('idx-rma-product_id', '{{%rma}}', 'product_id');
        }

        public function down()
        {
            echo "m161112_123939_initialize_rma cannot be reverted.\n";

            return false;
        }
    }
