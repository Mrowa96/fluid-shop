<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_105442_initialize_i18n extends Migration
    {
        public function up()
        {
            $this->createTable('{{%i18n}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(64)->notNull(),
                'symbol' => $this->string(10)->notNull(),
                'module' => $this->string(64)->defaultValue('backend'),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-i18n-user_id', '{{%i18n}}', 'id');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_105442_iinitialize_i18n cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->insert('{{%i18n}}', [
                'name' => 'Polski',
                'symbol' => 'pl-PL',
                'module' => 'backend',
            ]);
            $this->insert('{{%i18n}}', [
                'name' => 'English',
                'symbol' => 'en-US',
                'module' => 'frontend',
            ]);
            $this->insert('{{%i18n}}', [
                'name' => 'English',
                'symbol' => 'en-US',
                'module' => 'backend',
            ]);
            $this->insert('{{%i18n}}', [
                'name' => 'Polski',
                'symbol' => 'pl-PL',
                'module' => 'frontend',
            ]);
        }
    }
