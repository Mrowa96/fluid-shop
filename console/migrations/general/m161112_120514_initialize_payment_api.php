<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_120514_initialize_payment_api extends Migration
    {
        public function up()
        {
            $this->createTable('{{%payment_api}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(256)->notNull(),
                'symbol' => $this->string(256)->notNull(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_120514_initialize_payment_api cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->insert('{{%payment_api}}', [
                'id' => 1,
                'name' => 'PayU',
                'symbol' => 'PAYU',
                'active' => 1,
            ]);

            $this->insert('{{%payment_api}}', [
                'id' => 2,
                'name' => 'PayPal',
                'symbol' => 'PAYPAL',
                'active' => 1,
            ]);
        }
    }
