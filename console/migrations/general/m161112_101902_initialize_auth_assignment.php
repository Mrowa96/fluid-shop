<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_101902_initialize_auth_assignment extends Migration
    {
        public function up()
        {
            $this->createTable('{{%auth_assignment}}', [
                'item_name' => $this->string(64)->notNull(),
                'user_id' => $this->string(64)->notNull(),
                'created_at' => $this->integer(11)->null()
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-auth_assignment-user_id', '{{%auth_assignment}}', 'user_id');
        }

        public function down()
        {
            echo "m161112_101902_initialize_auth_assignment cannot be reverted.\n";

            return false;
        }
    }
