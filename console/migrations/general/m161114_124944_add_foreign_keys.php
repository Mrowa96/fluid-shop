<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161114_124944_add_foreign_keys extends Migration
    {
        public function up()
        {
            $this->setUpAddressTable();

            $this->setUpAttributeOptionTable();
            $this->setUpAuthAssignmentTable();
            $this->setUpAuthItemTable();
            $this->setUpAuthItemChildTable();

            $this->setUpCategoryTable();

            $this->setUpFavoriteTable();

            $this->setUpGridViewConfigTable();

            $this->setUpI18nMessageTable();

            $this->setUpMailTable();
            $this->setUpMailMessageTagTable();

            $this->setUpOrderTable();
            $this->setUpOrderDetailTable();
            $this->setUpOrderInvoiceTable();
            $this->setUpOrderLogTable();

            $this->setUpPageTable();

            $this->setUpPaymentsTable();

            $this->setUpProductTable();
            $this->setUpProductAttributeTable();
            $this->setUpProductImageTable();
            $this->setUpProductOfADayTable();
            $this->setUpProductReviewTable();
            $this->setUpProductSellCrossTable();
            $this->setUpProductSellUpTable();

            $this->setUpRmaTable();
            $this->setUpRmaImagesTable();

            $this->setUpTransportTable();
        }

        public function down()
        {
            echo "m161013_183834_add_foreign_keys cannot be reverted.\n";

            return false;
        }

        protected function setUpAddressTable()
        {
            $this->addForeignKey(
                'fk-address-user_id',
                '{{%address}}',
                'user_id',
                '{{%user}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpAttributeOptionTable()
        {
            $this->addForeignKey(
                'fk-attribute_option-attribute_id',
                '{{%attribute_option}}',
                'attribute_id',
                '{{%attribute}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpAuthAssignmentTable()
        {
            $this->addForeignKey(
                'fk-auth_assignment-item_name',
                '{{%auth_assignment}}',
                'item_name',
                '{{%auth_item}}',
                'name',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpAuthItemTable()
        {
            $this->addForeignKey(
                'fk-auth_item-rule_name',
                '{{%auth_item}}',
                'rule_name',
                '{{%auth_rule}}',
                'name',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpAuthItemChildTable()
        {
            $this->addForeignKey(
                'fk-auth_item_child-parent',
                '{{%auth_item_child}}',
                'parent',
                '{{%auth_item}}',
                'name',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-auth_item_child-child',
                '{{%auth_item_child}}',
                'child',
                '{{%auth_item}}',
                'name',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpCategoryTable()
        {
            $this->addForeignKey(
                'fk-category-discount_id',
                '{{%category}}',
                'discount_id',
                '{{%discount}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpFavoriteTable()
        {
            $this->addForeignKey(
                'fk-favorite-product_id',
                '{{%favorite}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-favorite-user_id',
                '{{%favorite}}',
                'user_id',
                '{{%user}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpGridViewConfigTable()
        {
            $this->addForeignKey(
                'fk-grid_view_config-user_id',
                '{{%grid_view_config}}',
                'user_id',
                '{{%user}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-grid_view_config-model_id',
                '{{%grid_view_config}}',
                'model_id',
                '{{%grid_view_models}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpI18nMessageTable()
        {
            $this->addForeignKey(
                'fk-i18n_message-i18n_id',
                '{{%i18n_message}}',
                'i18n_id',
                '{{%i18n}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpMailTable()
        {
            $this->addForeignKey(
                'fk-mail-mail_message_id',
                '{{%mail}}',
                'mail_message_id',
                '{{%mail_message}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpMailMessageTagTable()
        {
            $this->addForeignKey(
                'fk-mail_message_tag-mail_message_id',
                '{{%mail_message_tag}}',
                'mail_message_id',
                '{{%mail_message}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpOrderTable()
        {
            $this->addForeignKey(
                'fk-order-payment_id',
                '{{%order}}',
                'payment_id',
                '{{%payment}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order-address_id',
                '{{%order}}',
                'address_id',
                '{{%address}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order-transport_id',
                '{{%order}}',
                'transport_id',
                '{{%transport}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order-order_status_id',
                '{{%order}}',
                'order_status_id',
                '{{%order_status}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpOrderDetailTable()
        {
            $this->addForeignKey(
                'fk-order_detail-product_id',
                '{{%order_detail}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order_detail-order_id',
                '{{%order_detail}}',
                'order_id',
                '{{%order}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order_detail-order_detail_related_id',
                '{{%order_detail}}',
                'order_detail_related_id',
                '{{%order_detail}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpOrderInvoiceTable()
        {
            $this->addForeignKey(
                'fk-order_invoice-order_id',
                '{{%order_invoice}}',
                'order_id',
                '{{%order}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpOrderLogTable()
        {
            $this->addForeignKey(
                'fk-order_log-order_id',
                '{{%order_log}}',
                'order_id',
                '{{%order}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order_log-user_id',
                '{{%order_log}}',
                'user_id',
                '{{%user}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-order_log-order_status_id',
                '{{%order_log}}',
                'order_status_id',
                '{{%order_status}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpPageTable()
        {
            $this->addForeignKey(
                'fk-page-page_category_id',
                '{{%page}}',
                'page_category_id',
                '{{%page_category}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpPaymentsTable()
        {
            $this->addForeignKey(
                'fk-payment-payment_api_id',
                '{{%payment}}',
                'payment_api_id',
                '{{%payment_api}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpProductTable()
        {
            $this->addForeignKey(
                'fk-product-discount_id',
                '{{%product}}',
                'discount_id',
                '{{%discount}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-product-category_id',
                '{{%product}}',
                'category_id',
                '{{%category}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductAttributeTable()
        {
            $this->addForeignKey(
                'fk-product_attribute-attribute_id',
                '{{%product_attribute}}',
                'attribute_id',
                '{{%attribute}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-product_attribute-product_id',
                '{{%product_attribute}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-product_attribute-attribute_option_id',
                '{{%product_attribute}}',
                'attribute_option_id',
                '{{%attribute_option}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductImageTable()
        {
            $this->addForeignKey(
                'fk-product_image-product_id',
                '{{%product_image}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductOfADayTable()
        {
            $this->addForeignKey(
                'fk-product_of_a_day-product_id',
                '{{%product_of_a_day}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductReviewTable()
        {
            $this->addForeignKey(
                'fk-product_review-product_id',
                '{{%product_review}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductSellCrossTable()
        {
            $this->addForeignKey(
                'fk-product_sell_cross-product_id',
                '{{%product_sell_cross}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-product_sell_cross-cross_product_id',
                '{{%product_sell_cross}}',
                'cross_product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpProductSellUpTable()
        {
            $this->addForeignKey(
                'fk-product_sell_up-product_id',
                '{{%product_sell_up}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-product_sell_up-up_product_id',
                '{{%product_sell_up}}',
                'up_product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpRmaTable()
        {
            $this->addForeignKey(
                'fk-rma-product_id',
                '{{%rma}}',
                'product_id',
                '{{%product}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-rma-order_id',
                '{{%rma}}',
                'order_id',
                '{{%order}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
            $this->addForeignKey(
                'fk-rma-user_id',
                '{{%rma}}',
                'user_id',
                '{{%user}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
        protected function setUpRmaImagesTable()
        {
            $this->addForeignKey(
                'fk-rma_images-rma_id',
                '{{%rma_images}}',
                'rma_id',
                '{{%rma}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }

        protected function setUpTransportTable()
        {
            $this->addForeignKey(
                'fk-transport-transport_api_id',
                '{{%transport}}',
                'transport_api_id',
                '{{%transport_api}}',
                'id',
                'CASCADE',
                'CASCADE'
            );
        }
    }
