<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161017_215644_initialize_grid_view_models_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%grid_view_models}}', [
                'id' => $this->primaryKey()->notNull(),
                'namespace' => $this->string(128)
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161017_215644_initialize_grid_models_table cannot be reverted.\n";

            return false;
        }
    }
