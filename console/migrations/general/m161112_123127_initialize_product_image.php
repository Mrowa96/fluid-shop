<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_123127_initialize_product_image extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product_image}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'name' => $this->string(64)->notNull(),
                'thumb_path' => $this->string(256)->notNull(),
                'path' => $this->string(192)->notNull(),
                'main' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product_image-product_id', '{{%product_image}}', 'product_id');
        }

        public function down()
        {
            echo "m161112_123127_initialize_product_image cannot be reverted.\n";

            return false;
        }
    }
