<?php
    namespace console\migrations\general;

    use common\utilities\TranslationsImporter;
    use yii\db\Migration;

    class m161116_191914_add_translations extends Migration
    {
        public function up()
        {
            $translationsImporter = new TranslationsImporter();

            $translationsImporter->importTranslations(TranslationsImporter::MODULE_BACKEND);
            $translationsImporter->importTranslations(TranslationsImporter::MODULE_FRONTEND);
        }

        public function down()
        {
            echo "m161116_191914_add_translations cannot be reverted.\n";

            return false;
        }
    }
