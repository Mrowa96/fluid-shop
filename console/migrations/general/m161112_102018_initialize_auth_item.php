<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_102018_initialize_auth_item extends Migration
    {
        public function up()
        {
            $this->createTable('{{%auth_item}}', [
                'name' => $this->string(64)->notNull(),
                'type' => $this->integer(11)->null(),
                'description' => $this->text()->null(),
                'rule_name' => $this->string(64)->null(),
                'data' => $this->text()->null(),
                'created_at' => $this->integer(11)->null(),
                'updated_at' => $this->integer(11)->null(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->addPrimaryKey('prx-auth_item-name', '{{%auth_item}}', 'name');
            $this->createIndex('idx-auth_item-rule_name', '{{%auth_item}}', 'rule_name');
            $this->createIndex('idx-auth_item-type', '{{%auth_item}}', 'type');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_102018_initialize_auth_item cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            /** Main type */
            $this->insert('{{%auth_item}}', [
                'name' => 'system',
                'description' => 'System',
                'type' => 1,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'admin',
                'description' => 'Administrator',
                'type' => 1,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'google',
                'description' => 'Google',
                'type' => 1,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'worker',
                'description' => 'Worker',
                'type' => 1,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'client',
                'description' => 'Client',
                'type' => 1,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);

            /** Sub type */
            $this->insert('{{%auth_item}}', [
                'name' => 'configure grid view',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'create user',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'delete self user',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'delete users',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'display settings',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'get google merchant data',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'save settings',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'update self user',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'update users',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'view self user',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
            $this->insert('{{%auth_item}}', [
                'name' => 'view users',
                'type' => 2,
                'created_at' => (new \DateTime())->getTimestamp(),
                'updated_at' => (new \DateTime())->getTimestamp(),
            ]);
        }
    }
