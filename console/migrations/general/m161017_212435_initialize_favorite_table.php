<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161017_212435_initialize_favorite_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%favorite}}', [
                'id' => $this->primaryKey()->notNull(),
                'product_id' => $this->integer()->notNull(),
                'user_id' => $this->integer()->notNull(),
                'created_date' => $this->dateTime()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-favorite-product_id', '{{%favorite}}', 'product_id');
            $this->createIndex('idx-favorite-user_id', '{{%favorite}}', 'user_id');
        }

        public function down()
        {
            echo "m161017_212435_initialize_favorite_table cannot be reverted.\n";

            return false;
        }
    }
