<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_123816_initialize_product_sell_up extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product_sell_up}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'up_product_id' => $this->integer(11)->notNull(),
                'cost' => $this->double()->notNull()
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product_sell_up-product_id', '{{%product_sell_up}}', 'product_id');
            $this->createIndex('idx-product_sell_up-up_product_id', '{{%product_sell_up}}', 'up_product_id');
        }

        public function down()
        {
            echo "m161112_123816_initialize_product_sell_up cannot be reverted.\n";

            return false;
        }
    }
