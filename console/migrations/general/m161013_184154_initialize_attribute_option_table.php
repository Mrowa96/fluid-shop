<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_184154_initialize_attribute_option_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%attribute_option}}', [
                'id' => $this->primaryKey()->notNull(),
                'attribute_id' => $this->integer()->notNull(),
                'name' => $this->string(128)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-attribute_option-attribute_id', '{{%attribute_option}}', 'attribute_id');
        }

        public function down()
        {
            echo "m161013_184154_initialize_attribute_option_table cannot be reverted.\n";

            return false;
        }
    }
