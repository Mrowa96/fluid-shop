<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_110339_initialize_mail extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'mail_message_id' => $this->integer(11)->notNull(),
                'status' => $this->integer(4)->null()->defaultValue(0),
                'send_date' => $this->dateTime()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-mail-mail_message_id', '{{%mail}}', 'mail_message_id');
        }

        public function down()
        {
            echo "m161112_110339_initialize_mail cannot be reverted.\n";

            return false;
        }
    }
