<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_120346_initialize_page_category extends Migration
    {
        public function up()
        {
            $this->createTable('{{%page_category}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(128)->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161112_120346_initialize_page_category cannot be reverted.\n";

            return false;
        }
    }
