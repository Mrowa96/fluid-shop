<?php
    namespace console\migrations\general;

    use backend\models\MailMessage;
use yii\db\Migration;

    class m161112_112630_initialize_mail_message_tag extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail_message_tag}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'mail_message_id' => $this->integer(11)->notNull(),
                'name' => $this->string(125)->notNull(),
                'description' => $this->text()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-mail_message_tag-mail_message_id', '{{%mail_message_tag}}', 'mail_message_id');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_112630_initialize_mail_message_tag cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'order_key', 'Order identifier');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'order_create_date', 'Order create date');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'items', 'All items in the order. Format: Product name, quantity - cost PLN');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'order_total_cost', 'Total cost of the order');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'payment_name', 'Payment name');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'transport_name', 'Transport name');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'transport_cost', 'Cost of the transport');
            $this->quickInsert(MailMessage::ORDER_CONFIRMATION, 'full_address', 'Full address');

            $this->quickInsert(MailMessage::PASSWORD_RESET_TOKEN, 'reset_password_url', 'Reset password url');

            $this->quickInsert(MailMessage::ORDER_CHANGE_STATUS, 'order_key', 'Order identifier');
            $this->quickInsert(MailMessage::ORDER_CHANGE_STATUS, 'order_status_message', 'Message of changed status');
        }

        protected function quickInsert($messageId, $name, $content)
        {
            $this->insert('{{%mail_message_tag}}', [
                'mail_message_id' => $messageId,
                'name' => $name,
                'description' => $content,
            ]);
        }
    }
