<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_123333_initialize_product_of_a_day extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product_of_a_day}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'cost' => $this->double()->notNull(),
                'quantity' => $this->integer(11)->notNull(),
                'expire_date' => $this->dateTime()->notNull(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product_of_a_day-product_id', '{{%product_of_a_day}}', 'product_id');
        }

        public function down()
        {
            echo "m161112_123333_initialize_product_of_a_day cannot be reverted.\n";

            return false;
        }
    }
