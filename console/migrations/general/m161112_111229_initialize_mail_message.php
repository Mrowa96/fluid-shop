<?php
    namespace console\migrations\general;

    use backend\models\MailMessage;
    use yii\db\Migration;

    class m161112_111229_initialize_mail_message extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail_message}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(256)->notNull(),
                'title' => $this->string(256)->notNull(),
                'content' => $this->text()->notNull(),
                'add_footer' => $this->integer(4)->notNull(),
                'add_header' => $this->integer(4)->notNull(),
                'deletable' => $this->integer(4)->notNull()->defaultValue(1),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_111229_initialize_mail_message cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            /** Order confirmation */
            $this->insert('{{%mail_message}}', [
                'id' => MailMessage::ORDER_CONFIRMATION,
                'name' => 'order_confirmation',
                'title' => 'Podsumowanie zamówienia',
                'content' => '<p>Zamówienie o numerze {order_id} zostało złożone {order_create_date}.</p>
                                <p>Zamówienie składa się z:</p>
                                <p>{items}</p>
                                <p>Będzie ono wysłąno na adres:</p>
                                <p>{full_address}</p>
                                <p>Wybrano płatność: {payment_name}</p>
                                <p>Wybrano transport: {transport_name} - {transport_cost} PLN</p>
                                <p>&nbsp;</p>
                                <p>Koszt zamówienia: {order_total_cost} PLN</p>',
                'add_footer' => 1,
                'add_header' => 1,
                'deletable' => 0,
            ]);

            /** Password reset request */
            $this->insert('{{%mail_message}}', [
                'id' => MailMessage::PASSWORD_RESET_TOKEN,
                'name' => 'password_reset_request',
                'title' => 'Przypomnienie hasła',
                'content' => '<p>By zresetować swoje hasło przejdź pod ten adres {reset_password_url}</p>',
                'add_footer' => 1,
                'add_header' => 1,
                'deletable' => 0,
            ]);

            $this->insert('{{%mail_message}}', [
                'id' => MailMessage::ORDER_CHANGE_STATUS,
                'name' => 'order_change_status',
                'title' => 'Status zamówienia został zmieniony',
                'content' => '<p>Status zamówienia o numerze {order_key} został zaktualizowany.</p>
                                <p>{order_status_message}</p>',
                'add_footer' => 1,
                'add_header' => 1,
                'deletable' => 0,
            ]);

            $this->insert('{{%mail_message}}', [
                'id' => MailMessage::FORGOTTEN_BASKET,
                'name' => 'remind_about_forgotten_basket',
                'title' => 'Przypomnienie o porzuconym koszyku',
                'content' => '<p>Zapomniałeś o swoim koszyku.</p>',
                'add_footer' => 1,
                'add_header' => 1,
                'deletable' => 0,
            ]);
        }
    }
