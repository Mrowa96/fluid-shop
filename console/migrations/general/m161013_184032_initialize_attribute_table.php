<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_184032_initialize_attribute_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%attribute}}', [
                'id' => $this->primaryKey()->notNull(),
                'name' => $this->string(128)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161013_184032_initialize_attribute_table cannot be reverted.\n";

            return false;
        }
    }
