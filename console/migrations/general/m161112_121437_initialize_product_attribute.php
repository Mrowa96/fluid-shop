<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_121437_initialize_product_attribute extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product_attribute}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'attribute_id' => $this->integer(11)->notNull(),
                'attribute_option_id' => $this->integer(11)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product_attribute-attribute_id', '{{%product_attribute}}', 'attribute_id');
            $this->createIndex('idx-product_attribute-product_id', '{{%product_attribute}}', 'product_id');
            $this->createIndex('idx-product_attribute-attribute_option_id', '{{%product_attribute}}', 'attribute_option_id');
        }

        public function down()
        {
            echo "m161112_121437_initialize_product_attribute cannot be reverted.\n";

            return false;
        }
    }
