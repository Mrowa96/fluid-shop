<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_112527_initialize_mail_message_part extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail_message_part}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(128)->notNull(),
                'title' => $this->string(128)->notNull(),
                'content' => $this->text()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_112527_initialize_mail_message_part cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->insert('{{%mail_message_part}}', [
                'name' => 'header',
                'title' => 'Nagłówek',
                'content' => '<p>Witamy!</p>'
            ]);

            $this->insert('{{%mail_message_part}}', [
                'name' => 'footer',
                'title' => 'Stopka',
                'content' => '<p>Pozdrawiamy</p>'
            ]);
        }
    }
