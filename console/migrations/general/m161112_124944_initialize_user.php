<?php
    namespace console\migrations\general;

    use common\models\User;
    use fluid\fileManager\Directory;
    use yii\db\Migration;
    use yii\helpers\Json;

    class m161112_124944_initialize_user extends Migration
    {
        public function up()
        {
            $this->createTable('{{%user}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'username' => $this->string(255)->notNull(),
                'name' => $this->string(255)->notNull(),
                'email' => $this->string(255)->notNull(),
                'avatar' => $this->string(255)->null(),
                'auth_key' => $this->string(32)->notNull(),
                'password_hash' => $this->string(255)->notNull(),
                'password_reset_token' => $this->string(255)->null(),
                'language' => $this->string(10)->null(),
                'status' => $this->smallInteger(6)->notNull()->defaultValue(User::STATUS_ACTIVE),
                'deletable' => $this->smallInteger(4)->notNull()->defaultValue(0),
                'created_at' => $this->dateTime()->notNull(),
                'updated_at' => $this->dateTime()->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-user-username', '{{%user}}', 'username', true);
            $this->createIndex('idx-user-email', '{{%user}}', 'email', true);
            $this->createIndex('idx-user-password_reset_token', '{{%user}}', 'password_reset_token', true);
        }

        public function down()
        {
            echo "m161112_124944_initialize_user cannot be reverted.\n";

            return false;
        }
    }
