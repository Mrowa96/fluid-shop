<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_115115_initialize_order_log extends Migration
    {
        public function up()
        {
            $this->createTable('{{%order_log}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'order_id' => $this->integer(20)->notNull(),
                'user_id' => $this->integer(11)->null(),
                'order_status_id' => $this->integer(11)->notNull(),
                'date' => $this->dateTime()->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-order_log-order_id', '{{%order_log}}', 'order_id');
            $this->createIndex('idx-order_log-order_status_id', '{{%order_log}}', 'order_status_id');
            $this->createIndex('idx-order_log-user_id', '{{%order_log}}', 'user_id');
        }

        public function down()
        {
            echo "m161112_115115_initialize_order_log cannot be reverted.\n";

            return false;
        }
    }
