<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_105029_initialize_auth_item_child extends Migration
    {
        public function up()
        {
            $this->createTable('{{%auth_item_child}}', [
                'parent' => $this->string(64)->notNull(),
                'child' => $this->string(64)->notNull(),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-auth_item_child-parent', '{{%auth_item_child}}', 'parent');
            $this->createIndex('idx-auth_item_child-child', '{{%auth_item_child}}', 'child');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_105029_initialize_auth_item_child cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'configure grid view',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'create user',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'delete users',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'display settings',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'save settings',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'update users',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'admin',
                'child' => 'view users',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'google',
                'child' => 'get google merchant data',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'worker',
                'child' => 'delete self user',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'worker',
                'child' => 'update self user',
            ]);
            $this->insert('{{%auth_item_child}}', [
                'parent' => 'worker',
                'child' => 'view self user',
            ]);
        }
    }
