<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_184924_initialize_banner_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%banner}}', [
                'id' => $this->primaryKey()->notNull(),
                'type' => $this->smallInteger()->notNull(),
                'name' => $this->string(192)->notNull(),
                'link' => $this->string(256)->notNull(),
                'path' => $this->string(512)->notNull(),
                'thumb_path' => $this->string(512)->notNull(),
                'display_on_mobile' => $this->smallInteger(4)->defaultValue(1),
                'display_on_desktop' => $this->smallInteger(4)->defaultValue(1),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');
        }

        public function down()
        {
            echo "m161013_184924_initialize_banner_table cannot be reverted.\n";

            return false;
        }
    }
