<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161013_181835_initialize_address_table extends Migration
    {
        public function up()
        {
            $this->createTable('{{%address}}', [
                'id' => $this->primaryKey()->notNull(),
                'user_id' => $this->integer()->null(),
                'name' => $this->string(256)->notNull(),
                'surname' => $this->string(256)->notNull(),
                'email' => $this->string(128)->notNull(),
                'street' => $this->string(256)->notNull(),
                'building_no' => $this->string(64)->notNull(),
                'city' => $this->string(128)->notNull(),
                'zip' => $this->string(6)->notNull(),
                'phone_no' => $this->string(24)->notNull(),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-address-user_id', '{{%address}}', 'user_id');
        }

        public function down()
        {
            echo "m161013_181835_initialize_address_table cannot be reverted.\n";

            return false;
        }
    }
