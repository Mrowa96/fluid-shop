<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_123517_initialize_product_review extends Migration
    {
        public function up()
        {
            $this->createTable('{{%product_review}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'product_id' => $this->integer(11)->notNull(),
                'user_id' => $this->integer(11)->notNull(),
                'rating' => $this->integer(11)->notNull(),
                'text' => $this->text()->notNull(),
                'created_date' => $this->dateTime()->notNull(),
                'accepted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-product_review-product_id', '{{%product_review}}', 'product_id');
            $this->createIndex('idx-product_review-user_id', '{{%product_review}}', 'user_id');
        }

        public function down()
        {
            echo "m161112_123517_initialize_product_review cannot be reverted.\n";

            return false;
        }
    }
