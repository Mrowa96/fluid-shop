<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_112816_initialize_mail_subscriber extends Migration
    {
        public function up()
        {
            $this->createTable('{{%mail_subscriber}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'name' => $this->string(128)->notNull(),
                'email' => $this->string(128)->notNull(),
                'join_date' => $this->dateTime()->notNull(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-mail_subscriber-email', '{{%mail_subscriber}}', 'email', true);
        }

        public function down()
        {
            echo "m161112_112816_initialize_mail_subscriber cannot be reverted.\n";

            return false;
        }
    }
