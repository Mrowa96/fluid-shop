<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_124233_initialize_settings extends Migration
    {
        public function up()
        {
            $this->createTable('{{%settings}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'key' => $this->string(128)->notNull(),
                'value' => $this->string(256)->null(),
                'default' => $this->string(256)->null(),
                'description' => $this->text()->null(),
                'module' => $this->string(64)->notNull()->defaultValue('SYSTEM'),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->insertData();
        }

        public function down()
        {
            echo "m161112_124233_initialize_settings cannot be reverted.\n";

            return false;
        }

        protected function insertData()
        {
            /** SYSTEM */
            $this->quickInsert('site_title', 'SYSTEM', 'Fluid Shop');
            $this->quickInsert('currency', 'SYSTEM');
            $this->quickInsert('file_manager', 'SYSTEM');
            $this->quickInsert('dropped_basket_remind', 'SYSTEM');
            $this->quickInsert('search_index_name', 'SYSTEM', 'fluid-shop-' . \Yii::$app->security->generateRandomString(16));
            $this->quickInsert('layout_configuration', 'SYSTEM');
            $this->quickInsert('logo', 'SYSTEM');
            $this->quickInsert('language_level', 'SYSTEM', 'global', 'global', '
                Option sets level of language translation.
                Accepted values: cookie, user, global
                Description:
                 - cookie (individual for every computer and browser)
                 - user (every user can set different language)
                 - global (the same language for all user on veery computer)'
            );
            $this->quickInsert('language', 'SYSTEM', 'pl-PL', 'pl-PL');
            $this->quickInsert('footer_text', 'SYSTEM', 'Copyright © Tavoite. All rights reserved.');

            /** INFORMATION */
            $this->quickInsert('name', 'INFORMATION');
            $this->quickInsert('email', 'INFORMATION');
            $this->quickInsert('phone', 'INFORMATION');
            $this->quickInsert('website', 'INFORMATION');
            $this->quickInsert('street', 'INFORMATION');
            $this->quickInsert('city', 'INFORMATION');
            $this->quickInsert('zip', 'INFORMATION');

            /** SMTP */
            $this->quickInsert('host', 'SMTP');
            $this->quickInsert('login', 'SMTP');
            $this->quickInsert('password', 'SMTP');
            $this->quickInsert('port', 'SMTP');
            $this->quickInsert('encryption', 'SMTP');

            /** PAYU */
            $this->quickInsert('environment', 'PAYU');
            $this->quickInsert('oauth_client_id', 'PAYU');
            $this->quickInsert('oauth_client_secret', 'PAYU');
            $this->quickInsert('active', 'PAYU', 0);

            /** PAYPAL */
            $this->quickInsert('client_id', 'PAYPAL');
            $this->quickInsert('client_secret', 'PAYPAL');
            $this->quickInsert('active', 'PAYPAL', 0);
            $this->quickInsert('environment', 'PAYPAL');

            /** SMS */
            $this->quickInsert('login', 'SMS');
            $this->quickInsert('password', 'SMS');
            $this->quickInsert('sender', 'SMS');
        }
        protected function quickInsert($key, $module = 'SYSTEM', $value = '', $default = '', $description = '')
        {
            $this->insert('{{%settings}}', [
                'key' => $key,
                'module' => $module,
                'value' => $value,
                'default' => $default,
                'description' => $description,
            ]);
        }
    }
