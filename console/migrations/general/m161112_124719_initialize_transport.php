<?php
    namespace console\migrations\general;

    use yii\db\Migration;

    class m161112_124719_initialize_transport extends Migration
    {
        public function up()
        {
            $this->createTable('{{%transport}}', [
                'id' => $this->primaryKey(11)->notNull(),
                'transport_api_id' => $this->integer(11)->null(),
                'name' => $this->string(128)->notNull(),
                'icon' => $this->string(256)->null(),
                'description' => $this->text()->null(),
                'range_from' => $this->double()->notNull(),
                'range_to' => $this->double()->notNull(),
                'cost' => $this->double()->notNull(),
                'active' => $this->smallInteger(4)->notNull()->defaultValue(1),
                'deleted' => $this->smallInteger(4)->notNull()->defaultValue(0),
            ], 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB');

            $this->createIndex('idx-transport-transport_api_id', '{{%transport}}', 'transport_api_id');
        }

        public function down()
        {
            echo "m161112_124719_initialize_transport cannot be reverted.\n";

            return false;
        }
    }
