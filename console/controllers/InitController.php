<?php
    namespace console\controllers;

    use console\utilities\InitManager\migrations\AccountsMigration;
    use console\utilities\InitManager\migrations\BoilerplateMigration;
    use console\utilities\InitManager\migrations\GeneralMigration;
    use console\utilities\InitManager\steps\AccountsCredentialsStep;
    use console\utilities\InitManager\steps\AppConfigurationStep;
    use console\utilities\InitManager\steps\BoilerplateDataStep;
    use console\utilities\InitManager\steps\DatabaseConfigurationStep;
    use console\utilities\InitManager\steps\DatabaseMigrationStep;
    use console\utilities\InitManager\InitManager;
    use yii\console\Controller;

    /**
     * Initialize your own copy of Fluid Shop
     * @package console\controllers
     *
     * @property InitManager $initManager
     */
    class InitController extends Controller
    {
        protected $initManager;

        public function init()
        {
            parent::init();

            $this->initManager = new InitManager();
        }

        /**
         * Generates app configuration
         */
        public function actionGenerateAppConfiguration()
        {
            $this->initManager->addStep(new AppConfigurationStep())->run();
        }

        /**
         * Creates main and test database connections and run main migrations
         */
        public function actionSetDatabaseConnection()
        {
            $this->initManager->addStep(new DatabaseConfigurationStep())->run();
        }

        /**
         * Creates basic accounts credentials like admin account and save it to file and update database
         */
        public function actionSetAccountsCredentials()
        {
            $this->initManager->addStep(new AccountsCredentialsStep())->run();
        }

        /**
         * Inject required data to database
         */
        public function actionInjectRequiredDatabaseData()
        {
            $this->initManager->addStep(new DatabaseMigrationStep(new GeneralMigration()))->run();
        }

        /**
         * Injects boilerplate data e.g. products to database
         */
        public function actionInjectBoilerplateDatabaseData()
        {
            $this->initManager->addStep(new DatabaseMigrationStep(new BoilerplateMigration()))
                ->addStep(new BoilerplateDataStep())->run();
        }

        /**
         * Prepares preview environment with boilerplate data
         */
        public function actionPreparePreviewEnvironment()
        {
            $this->initManager->addStep(new DatabaseMigrationStep(new GeneralMigration()))
                ->addStep(new AccountsCredentialsStep())
                ->addStep(new DatabaseMigrationStep(new BoilerplateMigration()))
                ->addStep(new BoilerplateDataStep())
                ->run();
        }
    }