<?php
    namespace console\controllers;

    use PhpAmqpLib\Connection\AMQPStreamConnection;
    use Yii;
    use yii\helpers\Json;
    use backend\models\CronQueue;
    use common\models\Settings;
    use common\utilities\ElasticSearch;
    use backend\models\OrderInvoice;
    use common\models\Order;
    use common\models\User;
    use common\models\MailSubscriber;
    use backend\models\Mail;
    use backend\models\MailMessage;
    use backend\models\MailSubscriberData;
    use common\models\Product;
    use common\utilities\Mailer;
    use yii\web\UrlManager;
    use common\models\OrderStatus;
    use common\utilities\GoogleMerchant;
    use common\utilities\TranslationsImporter;
    use common\models\Category;
    use common\models\Page;
    use common\utilities\Sitemap;

    /**
     * Allows you to generate files, send emails and more.
     * @package console\controllers
     *
     * @property UrlManager $frontendUrlManager
     * @property array $excludedPublicMethods
     */
    class CronController extends BaseController
    {
        const UNIVERSAL_DATA_PACKAGE = 5000;

        /**
         * Sends newsletters to subscribers
         */
        public function actionSendNewsletter()
        {
            /**
             * @var CronQueue $newsletters
             * @var Mail $mailModel
             * @var MailMessage $message
             * @var MailSubscriber $subscriber
             * @var Mailer $mailer
             */
            $newsletters = CronQueue::find()
                ->where(['type' => CronQueue::TYPE_NEWSLETTER, 'status' => CronQueue::STATUS_WAITING])
                ->all();

            if (!empty($newsletters)) {
                $mailer = Yii::$app->mailer;

                foreach ($newsletters as $newsletter) {
                    $data = Json::decode($newsletter->data, false);

                    if (isset($data->id)) {
                        $mailModel = Mail::findOne($data->id);

                        if ($mailModel) {
                            $subscribersId = MailSubscriberData::find()->where(['mail_id' => $mailModel->id])->all();
                            $message = MailMessage::findOne($mailModel->mail_message_id);

                            if ($message && $subscribersId) {
                                try {
                                    $message->prepareForSent();

                                    foreach ($subscribersId AS $subscriberId) {
                                        $subscriber = MailSubscriber::findOne($subscriberId);

                                        $mailer->compose()
                                            ->setFrom($mailer->getHost())
                                            ->setTo($subscriber->email)
                                            ->setSubject($message->title)
                                            ->setHtmlBody($message->content)
                                            ->send();
                                    }
                                } catch (\Exception $ex) {
                                    $this->catchException($ex);

                                    return false;
                                }
                            }
                        }
                    }

                    $newsletter->status = 1;
                    $newsletter->save();
                }

                echo "All newsletters was send successfully." . PHP_EOL;
            } else {
                echo "No new newsletters to send." . PHP_EOL;
            }
        }

        /**
         * Generates order invoices
         */
        public function actionGenerateInvoices()
        {
            /** @var Order[] $orders */
            $orders = Order::getWithoutInvoice();

            if (!empty($orders)) {
                try {
                    foreach ($orders as $order) {
                        $invoice = new OrderInvoice();
                        $invoice->order_id = $order->id;

                        $invoice->save();
                    }

                    echo "Invoices was generated successfully." . PHP_EOL;
                } catch (\Exception $ex) {
                    $this->catchException($ex);
                }
            } else {
                echo "Nothing to do" . PHP_EOL;
            }

            exit;
        }

        /**
         * Generates frontend sitemap
         */
        public function actionGenerateSitemap()
        {
            try {
                $sitemap = new Sitemap();

                $sitemap->add(Product::className());
                $sitemap->add(Page::className());
                $sitemap->add(Category::className());
                $sitemap->generate();

                echo "Sitemap was generated successfully" . PHP_EOL;
            } catch (\Exception $ex) {
                $this->catchException($ex);
            }

            exit;
        }

        /**
         * Sends reminders about forgotten basket
         */
        public function actionRemindAboutBaskets()
        {
            /**
             * @var Order $basket
             * @var MailMessage $message
             * @var Mailer $mailer
             */
            $mailer = Yii::$app->mailer;
            $message = MailMessage::findOne(MailMessage::FORGOTTEN_BASKET);
            $basketDropTime = Settings::getOne('dropped_basket_remind', Settings\System::MODULE);
            $basketCreateDateMin = new \DateTime('-' . $basketDropTime);
            $baskets = Order::find()->where(['order_status_id' => OrderStatus::STATUS_NEW_NOT_COMPLETE])
                ->andWhere(['>=', 'basket_create_date', $basketCreateDateMin->format("Y-m-d H:i:s")])->all();

            if ($message && !empty($baskets)) {
                try {
                    foreach ($baskets as $basket) {
                        if ($basket->user_id) {
                            $email = User::findOne($basket->user_id)->email;
                        } else if ($basket->address && $basket->address->email) {
                            $email = $basket->address->email;
                        }

                        if (!empty($email)) {
                            $mailer->compose()
                                ->setFrom($mailer->getHost())
                                ->setTo($email)
                                ->setSubject($message->title)
                                ->setHtmlBody($message->content)
                                ->send();

                            echo "Remind about basket(" . $basket->id . ") was send" . PHP_EOL;
                        }
                    }

                } catch (\Exception $ex) {
                    $this->catchException($ex);
                }
            }

            exit;
        }

        /**
         * Creates products indexes for search
         */
        public function actionCreateProductsSearchIndexes()
        {
            try {
                $es = new ElasticSearch();
                $es->indexAll();

                echo "Search indexes created" . PHP_EOL;
            } catch (\Exception $ex) {
                $this->catchException($ex);
            }

            exit;
        }

        /**
         * Creates Google Merchant output file (pass true if you want to generate without cron queue)
         * @param bool $force
         */
        public function actionIndexProductsForGoogle($force = false)
        {
            /** @var CronQueue $requests */
            $requests = CronQueue::find()
                ->where(['type' => CronQueue::TYPE_GOOGLE_MERCHANT, 'status' => CronQueue::STATUS_WAITING])
                ->all();

            if (!empty($requests) || $force == true) {
                try {
                    $googleMerchant = new GoogleMerchant();

                    $googleMerchant->setUrlManager($this->frontendUrlManager);
                    $googleMerchant->indexAll();

                    if (!empty($requests)) {
                        /** @var CronQueue $request */
                        foreach ($requests as $request) {
                            $request->status = CronQueue::STATUS_DONE;
                            $request->save();
                        }
                    }

                    echo "Google Merchant products was generated successfully." . PHP_EOL;
                } catch (\Exception $ex) {
                    $this->catchException($ex);
                }
            } else {
                echo "Nothing to do" . PHP_EOL;
            }

            exit;
        }

        /**
         * Clones existing products - only for dev purposes
         * @param int $quantity
         * @param null $productId
         */
        public function actionMultiCloneProduct($quantity, $productId = null)
        {
            $originalQuantity = $quantity;

            /** @var Product $product */
            if ($productId) {
                $product = Product::findOne($productId);
            } else {
                $product = Product::find()->one();
            }

            if ($product) {
                try {
                    while ($quantity > 0) {
                        $product->cloneProduct();
                        $quantity--;
                    }

                    echo "Product " . $product->name . " was successfully cloned x" . $originalQuantity . PHP_EOL;

                    (new ElasticSearch())->indexAll();

                    echo "Product indexes for search were created." . PHP_EOL;
                } catch (\Exception $ex) {
                    $this->catchException($ex);
                }
            }
        }

        /**
         * Update translation if new version exists
         * @param string $module
         */
        public function actionUpdateTranslations($module)
        {
            try {
                $translationsImporter = new TranslationsImporter();

                $translationsImporter->importTranslations($module);

                echo "Translations for module: " . $module . ' updated successfully' . PHP_EOL;
            } catch (\Exception $ex) {
                $this->catchException($ex);
            }
        }

        /**
         * Listen on fluid-shop-send-credentials queue from RabbitMQ
         */
        public function actionSendEmailWithCredentialsListener()
        {
            $connection = new AMQPStreamConnection('localhost', 5672, 'guest', 'guest');

            $channel = $connection->channel();

            $channel->queue_declare('fluid-shop-send-credentials', false, false, false, false);
            $channel->basic_consume('fluid-shop-send-credentials', '', false, true, false, false, function ($msg) {
                $data = json_decode($msg->body,  true);

                if (array_key_exists('email', $data)) {
                    try {
                        /** @var Mailer $mailer */
                        $mailer = Yii::$app->mailer;
                        $frontendLink = Yii::getAlias('@frontendLink');
                        $backendLink = Yii::getAlias('@backendLink');
                        $adminPassword = Yii::$app->accountService->getGeneratedPassword('admin');

                        $mailer->compose()
                            ->setFrom($mailer->getLogin())
                            ->setTo($data['email'])
                            ->setSubject('Dane dostępowe do wersji preview Fluid Shop')
                            ->setHtmlBody(
                                <<<TEXT
Witaj,<br><br>
Dostaliśmy informację, że chcesz przetestować Naszą platformę E-commerce.<br><br>
Oto dane, które są Ci potrzebne:<br>
<b>Część dla klientów</b>:<br>
Url: <a href="{$frontendLink}">Strona główna</a><br>
Login: test<br>
Hasło: aaabbb1234<br><br>
<b>Część dla biznesu</b>:<br>
Url: <a href="{$backendLink}">Panel administracyjny</a><br>
Login: admin<br>
Hasło: {$adminPassword}<br><br>
Pozdrawiamy!<br>
Zespół Tavoite
TEXT

                            )->send();

                        echo "Send to: " . $data['email'] . PHP_EOL;
                    } catch (\Exception $ex) {
                        $this->catchException($ex);
                    }
                }
            });

            while (count($channel->callbacks)) {
                $channel->wait();
            }

            $channel->close();
            $connection->close();
        }

        /**
         * @param \Exception $ex
         */
        protected function catchException(\Exception $ex)
        {
            Yii::error($ex, "cron");
            echo $ex->getMessage() . PHP_EOL;
        }
    }