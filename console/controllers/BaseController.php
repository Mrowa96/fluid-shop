<?php
    namespace console\controllers;

    use yii\console\Controller;
    use yii\web\UrlManager;
    use console\utilities\UrlManager\BackendUrlManager;
    use console\utilities\UrlManager\FrontendUrlManager;
    use console\utilities\UrlManager\UrlManagerInterface;

    /**
     * Class BaseController
     * @package console\controllers
     *
     * @property UrlManager $frontendUrlManager
     * @property UrlManager $backendUrlManager
     */
    class BaseController extends Controller
    {
        protected $frontendUrlManager;
        protected $backendUrlManager;

        public function init()
        {
            parent::init();

            $this->setUrlManager(new FrontendUrlManager());
            $this->setUrlManager(new BackendUrlManager());
        }

        /**
         * @param UrlManagerInterface $urlManager
         */
        protected function setUrlManager(UrlManagerInterface $urlManager)
        {
            $property = $urlManager->getName() . 'UrlManager';

            $this->{$property} = new UrlManager();

            foreach ($urlManager->getConfig() as $key => $value) {
                if (property_exists($this->{$property}, $key)) {
                    $this->{$property}->{$key} = $value;
                }
            }

            $this->{$property}->setBaseUrl($urlManager->getBasePath());
            $this->{$property}->init();
        }
    }