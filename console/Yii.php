<?php
    class Yii extends \yii\BaseYii
    {
        /**
         * @var WebApplication the application instance
         */
        public static $app;
    }

    /**
     * @property yii\web\urlManager $urlManagerBackend
     * @property yii\web\urlManager $urlManagerFrontend
     * @property console\utilities\InitManager\services\AccountService $accountService
     */
    class WebApplication extends yii\web\Application
    {
    }
