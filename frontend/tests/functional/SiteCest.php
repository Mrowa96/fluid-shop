<?php
    namespace frontend\tests\functional;

    use common\models\Page;
    use frontend\tests\FunctionalTester;
    use common\fixtures\Page as PageFixture;
    use common\fixtures\PageCategory as PageCategoryFixture;

    /**
     * Class HomeCest
     * @package frontend\tests\functional
     */
    class HomeCest
    {
        public function _before(FunctionalTester $I)
        {
            $I->haveFixtures([
                'page' => [
                    'class' => PageCategoryFixture::className(),
                    'dataFile' => codecept_data_dir() . 'page_category.php'
                ],
            ]);
            $I->haveFixtures([
                'page' => [
                    'class' => PageFixture::className(),
                    'dataFile' => codecept_data_dir() . 'page.php'
                ],
            ]);
        }

        public function checkSiteHome(FunctionalTester $I)
        {
            $I->amOnPage('/');
            $I->canSeeResponseCodeIs(200);
        }

        public function checkSiteLogin(FunctionalTester $I)
        {
            $I->amOnRoute('site/login');
            $I->seeElement('#login-form');
        }

        public function checkSiteLogout(FunctionalTester $I)
        {
            $I->amOnRoute('site/logout');
            $I->canSeeResponseCodeIs(200);
        }

        public function checkSiteSignup(FunctionalTester $I)
        {
            $I->amOnRoute('site/signup');
            $I->seeElement('#signup-form');
        }

        public function checkSitePage(FunctionalTester $I)
        {
            /** @var Page $page */
            $page = $I->grabFixture('page', 0);
            $I->amOnRoute('site/page', ['id' => $page->id, 'url' => $page->url]);
            $I->see($page->content);
        }

        public function checkSiteContact(FunctionalTester $I)
        {
            /** @var Page $page */
            $page = $I->grabFixture('page', 1);
            $I->amOnRoute('site/page', ['id' => $page->id, 'url' => $page->url]);
            $I->seeElement("#contact-form");
        }

        public function checkSiteRequestPasswordReset(FunctionalTester $I)
        {
            $I->amOnRoute('site/request-password-reset');
            $I->seeElement('#request-password-reset-form');
        }
    }