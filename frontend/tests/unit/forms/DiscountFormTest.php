<?php
    namespace frontend\tests\unit\forms;

    use Codeception\Test\Unit;
    use common\fixtures\Discount as DiscountFixture;
    use common\models\Discount;
    use frontend\tests\UnitTester;
    use frontend\forms\Discount as DiscountForm;

    /**
     * @property UnitTester tester
     */
    class DiscountFormTest extends Unit
    {
        protected $tester;

        public function _before()
        {
            $this->tester->haveFixtures([
                'discount' => [
                    'class' => DiscountFixture::className(),
                    'dataFile' => codecept_data_dir() . 'discount.php'
                ],
            ]);
        }

        public function testSendValidCode()
        {
            $model = new DiscountForm;

            /** @var Discount $discount */
            $discount = $this->tester->grabFixture('discount', 0);

            $model->code = $discount->code;

            $this->assertTrue($model->validate());
        }

        public function testSendInvalidCode()
        {
            $model = new DiscountForm;

            /** @var Discount $discount */
            $discount = $this->tester->grabFixture('discount', 1);

            $model->code = $discount->code;
            $model->validate();

            $this->assertTrue($model->hasErrors());
        }
    }