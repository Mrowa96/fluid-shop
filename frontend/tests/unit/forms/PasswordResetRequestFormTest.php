<?php
    namespace frontend\tests\unit\forms;

    use Yii;
    use frontend\forms\PasswordResetRequest as PasswordResetRequestForm;
    use common\fixtures\User as UserFixture;
    use common\fixtures\Settings as SettingsFixture;
    use common\models\User;
    use Codeception\Test\Unit;
    use frontend\tests\UnitTester;

    /**
     * Class PasswordResetRequestFormTest
     * @package frontend\tests\unit\forms
     *
     * @property UnitTester $tester;
     */
    class PasswordResetRequestFormTest extends Unit
    {
        protected $tester;

        public function _before()
        {
            $this->tester->haveFixtures([
                'user' => [
                    'class' => UserFixture::className(),
                    'dataFile' => codecept_data_dir() . 'user.php'
                ],
                //TODO Remove when test dump is prepared
                'settings' => [
                    'class' => SettingsFixture::className(),
                    'dataFile' => codecept_data_dir() . 'settings.php'
                ],
            ]);
        }

        public function testSendMessageWithWrongEmailAddress()
        {
            $model = new PasswordResetRequestForm();
            $model->email = 'not-existing-email@example.com';

            expect_not($model->sendEmail());
        }

        public function testNotSendEmailsToInactiveUser()
        {
            $user = $this->tester->grabFixture('user', 1);

            $model = new PasswordResetRequestForm();
            $model->email = $user['email'];
            expect_not($model->sendEmail());
        }

        public function testSendEmailSuccessfully()
        {
            $userFixture = $this->tester->grabFixture('user', 0);

            $model = new PasswordResetRequestForm();
            $model->email = $userFixture['email'];
            $user = User::findOne(['password_reset_token' => $userFixture['password_reset_token']]);

            expect_that($model->sendEmail());
            expect_that($user->password_reset_token);

            $emailMessage = $this->tester->grabLastSentEmail();
            expect('valid email is sent', $emailMessage)->isInstanceOf('yii\mail\MessageInterface');
            expect($emailMessage->getTo())->hasKey($model->email);
            expect($emailMessage->getFrom())->hasKey(Yii::$app->params['supportEmail']);
        }
    }
