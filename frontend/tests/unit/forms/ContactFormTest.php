<?php
    namespace frontend\tests\unit\forms;

    use Codeception\Test\Unit;
    use frontend\forms\Contact as ContactForm;
    use frontend\tests\UnitTester;

    /**
     * @property UnitTester tester
     */
    class ContactFormTest extends Unit
    {
        protected $tester;

        public function testSendEmail()
        {
            $model = new ContactForm();

            $model->attributes = [
                'name' => 'Tester',
                'email' => 'tester@example.com',
                'subject' => 'very important letter subject',
                'body' => 'body of current message',
            ];

            expect_that($model->sendEmail('admin@example.com'));

            $this->tester->seeEmailIsSent();

            $emailMessage = $this->tester->grabLastSentEmail();

            expect('valid email is sent', $emailMessage)->isInstanceOf('yii\mail\MessageInterface');
            expect($emailMessage->getTo())->hasKey('admin@example.com');
            expect($emailMessage->getFrom())->hasKey('tester@example.com');
            expect($emailMessage->getSubject())->equals('very important letter subject');
            expect($emailMessage->toString())->contains('body of current message');
        }
    }
