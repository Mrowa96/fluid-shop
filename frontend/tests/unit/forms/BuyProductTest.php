<?php
    namespace frontend\tests\unit\forms;

    use Codeception\Test\Unit;
    use common\models\Product;
    use frontend\forms\BuyProduct;
    use frontend\tests\UnitTester;
    use common\fixtures\Product as ProductFixture;
    use frontend\forms\BuyProduct as BuyProductForm;

    /**
     * @property UnitTester tester
     */
    class BuyProductTest extends Unit
    {
        protected $tester;

        public function _before()
        {
            $this->tester->haveFixtures([
                'product' => [
                    'class' => ProductFixture::className(),
                    'dataFile' => codecept_data_dir() . 'product.php'
                ],
            ]);
        }

        public function testSendValidData()
        {
            $model = new BuyProductForm();

            /** @var Product $product */
            $product = $this->tester->grabFixture('product', 0);

            $model->attributes = [
                'id' => $product->id,
                'quantity' => 1,
            ];

            $this->assertTrue($model->validate());
        }

        public function testSendInvalidData()
        {
            $model = new BuyProductForm;

            $model->attributes = [
                'id' => -26,
                'quantity' => -100
            ];

            $this->assertFalse($model->validate());
        }
    }