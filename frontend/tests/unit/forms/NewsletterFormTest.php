<?php
    namespace frontend\tests\unit\forms;

    use Codeception\Test\Unit;
    use frontend\tests\UnitTester;
    use frontend\forms\Newsletter as NewsletterForm;

    /**
     * Class NewsletterFormTest
     * @package frontend\tests\unit\forms
     *
     * @property UnitTester $tester
     */
    class NewsletterFormTest extends Unit
    {
        protected $tester;

        public function testSignUpValidData()
        {
            $model = new NewsletterForm();

            $model->attributes = $this->getValidData();

            $this->assertTrue($model->assign());
        }

        public function testSignUpInvalidEmail()
        {
            $model = new NewsletterForm();

            $model->attributes = $this->getDataWithInvalidEmail();

            $this->assertFalse($model->assign());
        }

        public function testSignUpInvalidName()
        {
            $model = new NewsletterForm();

            $model->attributes = $this->getDataWithInvalidName();

            $this->assertFalse($model->assign());
        }

        public function testSignUpInvalidAgree()
        {
            $model = new NewsletterForm();

            $model->attributes = $this->getDataWithInvalidAgree();

            $this->assertFalse($model->assign());
        }

        /**
         * @return array
         */
        protected function getValidData()
        {
            return [
                'name' => 'Test valid',
                'email' => 'test@test.pl',
                'agree' => 1
            ];
        }

        /**
         * @return array
         */
        protected function getDataWithInvalidEmail()
        {
            return [
                'name' => 'Test valid',
                'email' => 'test-test',
                'agree' => 1
            ];
        }

        /**
         * @return array
         */
        protected function getDataWithInvalidAgree()
        {
            return [
                'name' => 'Test valid',
                'email' => 'test@test.pl',
                'agree' => 0
            ];
        }

        /**
         * @return array
         */
        protected function getDataWithInvalidName()
        {
            return [
                'name' => null,
                'email' => 'test@test.pl',
                'agree' => 1
            ];
        }
    }