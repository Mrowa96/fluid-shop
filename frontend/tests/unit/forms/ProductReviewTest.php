<?php
    namespace frontend\tests\unit\forms;

    use Codeception\Test\Unit;
    use frontend\forms\ProductReview;
    use frontend\tests\UnitTester;
    use common\fixtures\Product as ProductFixture;
    use common\fixtures\User as UserFixture;
    use Yii;

    /**
     * Class NewsletterFormTest
     * @package frontend\tests\unit\forms
     *
     * @property UnitTester $tester
     */
    class ProductReviewTest extends Unit
    {
        protected $tester;

        public function _before()
        {
            $this->tester->haveFixtures([
                'product' => [
                    'class' => ProductFixture::className(),
                    'dataFile' => codecept_data_dir() . 'product.php'
                ],
                'user' => [
                    'class' => UserFixture::className(),
                    'dataFile' => codecept_data_dir() . 'user.php'
                ]
            ]);
        }

        public function testValidData()
        {
            Yii::$app->user->login($this->tester->grabFixture('user', 0));

            $model = new ProductReview();

            $model->attributes = [
                'rating' => 3,
                'text' => 'Review text',
                'product_id' => $this->tester->grabFixture('product', 0)['id'],
            ];

            $this->assertTrue($model->saveReview());
        }

        public function testInvalidData()
        {
            Yii::$app->user->login($this->tester->grabFixture('user', 0));

            $model = new ProductReview();

            $model->attributes = [
                'rating' => 'a',
                'text' => 77,
                'product_id' => 'a'
            ];

            $this->assertFalse($model->saveReview());
        }
    }