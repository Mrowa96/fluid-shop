<?php
    namespace frontend\tests\unit\services;

    use Codeception\Test\Unit;
    use frontend\services\ManageBar;
    use frontend\tests\UnitTester;

    /**
     * Class ManageBarTest
     * @package frontend\tests\unit\services
     *
     * @property UnitTester $tester
     * @property ManageBar $manageBar
     */
    class ManageBarTest extends Unit
    {
        protected $tester;
        protected $manageBar;

        public function _before()
        {
            $this->manageBar = new ManageBar();
        }

        public function testAddAction()
        {
            $parsedName = $this->manageBar->addAction('Test', '/');

            $this->tester->assertTrue(is_string($parsedName));
        }

        public function testGetActions()
        {
            $this->manageBar->addAction('Test 2', '/');

            $this->tester->assertNotEmpty($this->manageBar->getActions());
        }

        public function testRemoveAction()
        {
            $parsedName = $this->manageBar->addAction('Test 3', '/');

            $this->manageBar->removeAction($parsedName);

            $this->tester->assertArrayNotHasKey($parsedName, $this->manageBar->getActions());
        }

        public function testRemoveAllAction()
        {
            $this->manageBar->addAction('Test 4', '/');
            $this->manageBar->addAction('Test 5', '/');
            $this->manageBar->removeAll();

            $this->tester->assertEmpty($this->manageBar->getActions());
        }
    }