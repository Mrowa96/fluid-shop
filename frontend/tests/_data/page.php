<?php
    return [
        [
            'page_category_id' => 1,
            'title' => 'Test page',
            'url' => \common\utilities\Utility::uppercaseToDash('Test page'),
            'content' => 'Test page content',
            'type' => \common\models\Page::TYPE_STANDARD,
            'seo_keywords' => 'Test, page, keywords',
            'seo_description' => 'Test page description'
        ],
        [
            'page_category_id' => 2,
            'title' => 'Contact',
            'url' => \common\utilities\Utility::uppercaseToDash('Contact'),
            'content' => 'Contact page content',
            'type' => \common\models\Page::TYPE_CONTACT,
            'seo_keywords' => 'Test, contact, keywords',
            'seo_description' => 'Test contact description'
        ]
    ];