<?php
    return [
        [
            'threshold_percentage' => 25,
            'code' => 'TEST_DISCOUNT_VALID',
            'amount' => 100,
            'dynamic' => 1
        ],
        [
            'threshold_percentage' => 50,
            'code' => 'TEST_DISCOUNT_INVALID',
            'amount' => 5,
            'expire' => '2017-01-01 00:00:00',
            'dynamic' => 1
        ],
    ];