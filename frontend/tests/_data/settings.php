<?php
    return [
        [
            'key' => 'host',
            'value' => Yii::$app->params['supportEmail'],
            'module' => \common\models\Settings\Smtp::MODULE
        ],
        [
            'key' => 'site_title',
            'value' => 'Fluid Shop Test',
            'module' => \common\models\Settings\System::MODULE
        ]
    ];
