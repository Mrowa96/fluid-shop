<?php
    return [
        [
            'name' => 'Test product',
            'processed_name' => \common\utilities\Utility::uppercaseToDash('Test product'),
            'description' => 'Test description',
            'additional_field' => '',
            'cost' => 99.99,
            'quantity' => 100,
            'available' => 1,
            'code' => 'TEST_CODE_1',
            'created_date' => (new DateTime())->format('Y-m-d H:i:s'),
            'modified_date' => (new DateTime())->format('Y-m-d H:i:s')
        ],
    ];