<?php
    namespace frontend\assets;

    use yii\web\AssetBundle;

    class ScriptAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $js = ['assets/dist/js/app.js'];
    }