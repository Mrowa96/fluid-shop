<?php
    namespace frontend\assets;

    use yii\web\AssetBundle;
    use yii\web\View;

    /**
     * Main frontend application asset bundle.
     */
    class AppAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $css = [
            'assets/dist/css/app.css',
        ];

        public $jsOptions = ['position' => View::POS_END];
        public $depends = [
            'yii\web\YiiAsset',
        ];
    }
