<?php
    return [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'baseUrl' => Yii::getAlias("@frontendLink"),
        'rules' => [
            '/' => 'site/index',
            'login' => 'site/login',
            'signup' => 'site/signup',
            'logout' => 'site/logout',
            'kontakt' => 'site/contact',
            'crash' => 'site/crash',
            'get-google-merchant-data' => 'site/get-google-merchant-data',
            'assign-to-newsletter' => 'site/assign-to-newsletter',
            'request-password-reset' => 'site/request-password-reset',
            'reset-password' => 'site/reset-password',
            'product/detail/<url:[A-Za-z0-9\-]+>,<id:\d+>' => 'product/detail',
            'p/<url:[A-Za-z0-9\-]+>,<id:\d+>' => 'site/page',
            'kategoria/<url:[A-Za-z0-9\-]+>,<id:\d+>' => 'category/index',
            'basket/after-complete,<paid:[0-1{0,1}]>' => 'basket/after-complete',
            Yii::getAlias("@frontendLink") => 'site/index'
        ],
        'suffix' => '.html'
    ];