<?php
    $config = [
        'id' => Yii::getAlias('@frontendModuleId'),
        'language' => 'pl-PL',
        'basePath' => dirname(__DIR__),
        'bootstrap' => ['log'],
        'controllerNamespace' => 'frontend\controllers',
        'components' => [
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'layoutService' => 'frontend\services\Layout',
            'basket' => frontend\services\Basket\Factory::create(),
            'basketHelper' => 'frontend\services\Basket\Helper',
            'manageBarService' => 'frontend\services\ManageBar',
            'alertsService' => 'common\services\Alerts',
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'urlManager' => require_once 'urlManager.php',
            'urlManagerBackend' => array_merge(
                require_once dirname(__DIR__) . '/../backend/config/urlManager.php',
                ['class' => 'yii\web\urlManager']
            ),
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'frontend\services\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
            'i18nBackend' => [
                'class' => 'backend\utilities\i18n',
            ],
            'assetManager' => [
                'basePath' => '@webroot/cache',
                'baseUrl' => '@web/cache',
                'appendTimestamp' => YII_ENV === 'prod' ? true : false,
                'bundles' => [
                    'yii\web\JqueryAsset' => false,
                    'yii\bootstrap\BootstrapPluginAsset' => false,
                    'yii\bootstrap\BootstrapAsset' => false,
                    'yii\web\YiiAsset' => [
                        'depends' => [
                            'frontend\assets\ScriptAsset',
                        ],
                    ],
                ],
            ],
        ],
        'params' => [
            'user.passwordResetTokenExpire' => 3600,
        ]
    ];

    if (YII_ENV === 'dev') {
        $config['bootstrap'][] = 'debug';
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1']
        ];
    }

    return $config;
