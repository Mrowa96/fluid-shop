<?php
    $config = [
        'id' => Yii::getAlias('@frontendModuleId') . '-test',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'frontend\controllers',
        'components' => [
            'assetManager' => [
                'basePath' => __DIR__ . '/../web/cache',
                'bundles' => [
                    'yii\web\JqueryAsset' => false,
                    'yii\bootstrap\BootstrapPluginAsset' => false,
                    'yii\bootstrap\BootstrapAsset' => false,
                    'yii\web\YiiAsset' => [
                        'depends' => [
                            'frontend\assets\ScriptAsset',
                        ],
                    ],
                ],
            ],
            'urlManager' => require 'urlManager.php',
            'urlManagerBackend' => array_merge(
                require dirname(__DIR__) . '/../backend/config/urlManager.php',
                ['class' => 'yii\web\urlManager']
            ),
            'layoutService' => 'frontend\services\Layout',
            'manageBarService' => 'frontend\services\ManageBar',
            'alertsService' => 'common\services\Alerts',
            'basket' => frontend\services\Basket\Factory::create(),
            'basketHelper' => 'frontend\services\Basket\Helper',
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'frontend\services\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
            'i18nBackend' => [
                'class' => 'backend\utilities\i18n',
            ],
        ],
        'params' => [
            'user.passwordResetTokenExpire' => 3600,
            'supportEmail' => 'test@test.pl'
        ]
    ];

    return yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../../common/config/test.php'),
        $config
    );
