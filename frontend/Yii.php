<?php
    class Yii extends \yii\BaseYii
    {
        /**
         * @var WebApplication the application instance
         */
        public static $app;
    }

    /**
     * @property yii\web\urlManager $urlManagerBackend
     * @property yii\web\urlManager $urlManagerFrontend
     * @property frontend\services\Layout $layoutService
     * @property frontend\services\Basket $basket
     * @property frontend\services\Basket\Helper $basketHelper
     * @property frontend\services\ManageBar $manageBarService
     * @property common\services\Alerts $alertsService
     */
    class WebApplication extends yii\web\Application
    {
    }
