<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;

    /**
     * Class CookieLawWidget
     * @package frontend\components
     *
     * @property string $text
     */
    class CookieLawWidget extends Widget
    {
        public $text;

        public function run()
        {
            if (empty($text)) {
                $this->text = Yii::t("site", "We use cookies to ensure that we give you the best experience on our website.");
            }

            return $this->render('@frontend/views/components/cookie-law', [
                'text' => $this->text,
            ]);
        }
    }