<?php
    namespace frontend\components;

    use yii\base\Widget;

    /**
     * Class UserMenuWidget
     * @package frontend\components
     *
     * @property string $id
     */
    class UserMenuWidget extends Widget
    {
        public $id;

        public function run()
        {
            return $this->render('@frontend/views/components/user-menu', [
                'id' => $this->id,
            ]);
        }
    }