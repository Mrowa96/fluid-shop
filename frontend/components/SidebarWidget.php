<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use frontend\services\Layout\Sidebar;
    use frontend\utilities\Menu\MenuItem;

    /**
     * Class SidebarWidget
     * @package frontend\components
     *
     * @property Sidebar $sidebar
     * @property MenuItem[] $components
     * @property bool $hidden
     */
    class SidebarWidget extends Widget
    {
        protected $sidebar;
        protected $components;
        protected $hidden = true;

        public function run()
        {
            if (Yii::$app->layoutService->hasSidebar()) {
                $this->sidebar = Yii::$app->layoutService->getSidebar();

                if ($this->sidebar->getDisplay() === true) {
                    $this->hidden = false;
                    $this->components = $this->sidebar->getComponents();
                }
            }

            return $this->render('@frontend/views/components/sidebar', [
                'hidden' => $this->hidden,
                'components' => $this->components
            ]);
        }
    }