<?php
    namespace frontend\components;

    use frontend\utilities\Menu;
    use frontend\utilities\Menu\CategoriesMinimalDataProvider;
    use yii\base\Widget;

    /**
     * Class HeaderNavigationWidget
     * @package frontend\components
     *
     * @property Menu $menu
     */
    class CategoriesDropdownWidget extends Widget
    {
        public $menu;

        public function run()
        {
            if (!$this->menu) {
                $this->menu = new Menu(new CategoriesMinimalDataProvider(), [
                    'itemClass' => 'category',
                    'addWrapper' => false
                ]);
            }

            return $this->render('@frontend/views/components/categories-dropdown', [
                'menu' => $this->menu
            ]);
        }
    }