<?php
    namespace frontend\components;

    use common\models\Page;
    use common\models\PageCategory;
    use yii\base\Widget;

    /**
     * Class FooterWidget
     * @package frontend\components
     *
     * @property PageCategory[] $groupedPages
     */
    class FooterWidget extends Widget
    {
        public $groupedPages;

        public function run()
        {
            $this->groupedPages = Page::getForFooter();

            return $this->render('@frontend/views/components/footer', [
                'groupedPages' => $this->groupedPages,
            ]);
        }
    }