<?php
    namespace frontend\components;

    use frontend\utilities\Menu;
    use frontend\utilities\Menu\CategoriesMinimalDataProvider;
    use frontend\utilities\Menu\PagesDataProvider;
    use yii\base\Widget;

    /**
     * Class NavigationMobileDropdownWidget
     * @package frontend\components
     *
     * @property Menu $categoriesMenu
     * @property Menu $pagesMenu
     */
    class NavigationMobileDropdownWidget extends Widget
    {
        public $categoriesMenu;
        public $pagesMenu;

        public function run()
        {
            if (!$this->categoriesMenu) {
                $this->categoriesMenu = new Menu(new CategoriesMinimalDataProvider(), ['addWrapper' => false]);
            }

            $this->pagesMenu = new Menu(new PagesDataProvider(), ['addWrapper' => false]);

            return $this->render('@frontend/views/components/navigation-mobile-dropdown', [
                'categoriesMenu' => $this->categoriesMenu,
                'pagesMenu' => $this->pagesMenu
            ]);
        }
    }