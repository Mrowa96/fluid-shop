<?php
    namespace frontend\components;

    use common\models\Product;
    use yii\base\Widget;

    /**
     * Class GroupedProducts
     * @package frontend\components
     *
     * @property Product[] $products
     * @property string $title
     * @property int $quantity
     * @property string $type
     */
    class GroupedProductsWidget extends Widget
    {
        public $title = '';
        public $quantity = 6;
        public $type = ProductWidget::TYPE_RECOMMENDED;
        public $products = null;

        public function run()
        {
            if (is_null($this->products)) {
                $this->products = Product::getByGroup($this->type, $this->quantity);
            } else {
                if (!is_array($this->products)) {
                    $this->products = [];
                }
            }

            return $this->render('@frontend/views/components/grouped-products', [
                'products' => $this->products,
                'title' => $this->title,
                'type' => $this->type
            ]);
        }
    }