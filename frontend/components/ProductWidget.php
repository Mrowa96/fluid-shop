<?php
    namespace frontend\components;

    use common\models\Product;
    use yii\base\Exception;
    use yii\base\Widget;

    /**
     * Class ProductWidget
     * @package frontend\components
     *
     * @property Product $product
     * @property string $type
     */
    class ProductWidget extends Widget
    {
        public $product;
        public $type = ProductWidget::TYPE_STANDARD;

        /** Physical types (as column in db) */
        const TYPE_RECOMMENDED = 'recommended';
        const TYPE_PROMOTIONAL = 'promotional';
        const TYPE_CHECK_OUT = 'check_out';

        /** Abstract types */
        const TYPE_ON_LIST = 'on-list';
        const TYPE_CROSS_SELL = 'cross-sell';
        const TYPE_STANDARD = 'standard';

        public function run()
        {
            if (!$this->product) {
                throw new Exception('Product must be defined');
            }

            switch ($this->type) {
                case self::TYPE_CROSS_SELL:
                    $viewType = self::TYPE_CROSS_SELL;
                    break;

                case self::TYPE_STANDARD:
                case self::TYPE_ON_LIST:
                case self::TYPE_RECOMMENDED:
                case self::TYPE_PROMOTIONAL:
                case self::TYPE_CHECK_OUT:
                default:
                    $viewType = self::TYPE_STANDARD;
                    break;
            }

            return $this->render('@frontend/views/components/product/' . $viewType, [
                'product' => $this->product,
                'type' => $this->type,
                'viewType' => $viewType
            ]);
        }
    }