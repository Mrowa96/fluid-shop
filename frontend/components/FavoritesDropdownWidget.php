<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use common\models\Favorite;

    /**
     * Class FavoritesDropdownWidget
     * @package frontend\components
     *
     * @property Favorite[] $favorites
     */
    class FavoritesDropdownWidget extends Widget
    {
        public $favorites = [];

        public function run()
        {
            if (!Yii::$app->user->isGuest) {
                $this->favorites = Favorite::find()->where(['user_id' => Yii::$app->user->id])->all();
            }

            return $this->render('@frontend/views/components/favorites-dropdown', [
                'favorites' => $this->favorites
            ]);
        }
    }