<?php
    namespace frontend\components;

    use yii\base\Widget;

    /**
     * Class SearchWidget
     * @package frontend\components
     *
     * @property string $id
     */
    class SearchWidget extends Widget
    {
        public $id;
        protected $phrase;

        public function run()
        {
            return $this->render('@frontend/views/components/search', [
                'id' => $this->id,
                'phrase' => isset($this->getView()->params['phrase']) ? $this->getView()->params['phrase'] : '',
            ]);
        }
    }