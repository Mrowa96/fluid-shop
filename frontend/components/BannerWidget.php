<?php
    namespace frontend\components;

    use common\models\Banner;
    use yii\base\Widget;

    /**
     * Class BannerWidget
     * @package frontend\components
     *
     * @property Banner[] $banners
     * @property string $type
     * @property bool|int $limit
     * @property bool $pagination
     * @property bool $buttons
     * @property bool $scrollbar
     * @property bool $random
     */
    class BannerWidget extends Widget
    {
        public $banners;
        public $type = Banner::FIRST_BANNER;
        public $limit = 1;
        public $pagination = true;
        public $buttons = true;
        public $scrollbar = false;
        public $random = false;

        public function run()
        {
            $this->banners = Banner::getByType($this->type, $this->limit, $this->random);

            return $this->render('@frontend/views/components/banner', [
                'banners' => $this->banners,
                'type' => $this->type,
                'pagination' => $this->pagination,
                'buttons' => $this->buttons,
                'scrollbar' => $this->scrollbar,
            ]);
        }
    }