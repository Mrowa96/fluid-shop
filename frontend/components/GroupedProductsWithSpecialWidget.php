<?php
    namespace frontend\components;

    use common\models\Product;
    use common\models\ProductOfADay;

    /**
     * Class GroupedProductsWithSpecialWidget
     * @package frontend\components
     *
     * @property ProductOfADay $specialProduct
     */
    class GroupedProductsWithSpecialWidget extends GroupedProductsWidget
    {
        protected $specialProduct;

        public function run()
        {
            $this->products = Product::getByGroup($this->type, $this->quantity);
            $this->specialProduct = ProductOfADay::getActive();

            return $this->render('@frontend/views/components/grouped-products-with-special', [
                'products' => $this->products,
                'specialProduct' => $this->specialProduct,
                'title' => $this->title,
                'type' => $this->type
            ]);
        }
    }