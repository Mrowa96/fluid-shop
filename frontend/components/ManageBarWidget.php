<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use frontend\services\ManageBar\Action;

    /**
     * Class ManageBarWidget
     * @package frontend\components
     *
     * @property bool $displayPanelUrl
     * @property string $panelUrl
     * @property Action[] $actions
     */
    class ManageBarWidget extends Widget
    {
        public $displayPanelUrl = true;
        protected $panelUrl;
        protected $actions;

        public function run()
        {
            $this->panelUrl = Yii::getAlias("@backendLink");
            $this->actions = Yii::$app->manageBarService->getActions();

            if (Yii::$app->user->can("admin") || Yii::$app->user->can("worker")) {
                return $this->render('@frontend/views/components/manage-bar', [
                    'displayPanelUrl' => $this->displayPanelUrl,
                    'panelUrl' => $this->panelUrl,
                    'actions' => $this->actions
                ]);
            }
        }
    }