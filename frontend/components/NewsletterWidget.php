<?php
    namespace frontend\components;

    use frontend\forms\Newsletter;
    use yii\base\Widget;

    /**
     * Class NewsletterWidget
     * @package frontend\components
     *
     * @property string $title
     * @property Newsletter $model
     */
    class NewsletterWidget extends Widget
    {
        public $title;
        protected $model;

        public function run()
        {
            $this->model = new Newsletter();

            return $this->render('@frontend/views/components/newsletter', [
                'model' => $this->model,
                'title' => $this->title
            ]);
        }
    }