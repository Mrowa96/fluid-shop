<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use frontend\services\Basket;

    /**
     * Class HeaderWidget
     * @package frontend\components
     *
     * @property Basket $basket
     * @property string $currentController
     */
    class HeaderWidget extends Widget
    {
        protected $basket;
        protected $currentController;

        public function run()
        {
            $this->currentController = Yii::$app->controller->id;
            $this->basket = Yii::$app->basketHelper->get();

            return $this->render('@frontend/views/components/header', [
                'basket' => $this->basket,
                'currentController' => $this->currentController
            ]);
        }
    }