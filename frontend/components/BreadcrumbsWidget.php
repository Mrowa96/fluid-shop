<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use frontend\services\Layout\Breadcrumbs;

    /**
     * Class BreadcrumbsWidget
     * @package frontend\components
     *
     * @property Breadcrumbs $breadcrumbs
     */
    class BreadcrumbsWidget extends Widget
    {
        protected $breadcrumbs;

        public function run()
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                $this->breadcrumbs = Yii::$app->layoutService->getBreadcrumbs();
            }

            return $this->render('@frontend/views/components/breadcrumbs', [
                'breadcrumbs' => $this->breadcrumbs,
            ]);
        }
    }