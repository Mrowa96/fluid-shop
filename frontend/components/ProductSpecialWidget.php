<?php
    namespace frontend\components;

    use common\models\Product;
    use common\models\ProductOfADay;
    use yii\base\Exception;
    use yii\base\Widget;

    /**
     * Class ProductSpecialWidget
     * @package frontend\components
     *
     * @property ProductOfADay $product
     * @property string $type
     */
    class ProductSpecialWidget extends Widget
    {
        public $product;

        public function run()
        {
            if (!$this->product) {
                $this->product = ProductOfADay::getActive();
            }

            return $this->render('@frontend/views/components/product-special', [
                'product' => $this->product
            ]);
        }
    }