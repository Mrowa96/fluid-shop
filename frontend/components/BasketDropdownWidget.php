<?php
    namespace frontend\components;

    use Yii;
    use yii\base\Widget;
    use frontend\services\Basket;

    /**
     * Class BasketDropdownWidget
     * @package frontend\components
     *
     * @property Basket $basket
     */
    class BasketDropdownWidget extends Widget
    {
        protected $basket;

        const BASKET_CONTROLLER = 'basket';

        public function run()
        {
            $this->basket = Yii::$app->basketHelper->get();

            return $this->render('@frontend/views/components/basket/list-dropdown', [
                'basket' => $this->basket
            ]);
        }
    }