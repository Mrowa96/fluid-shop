<?php
    defined('YII_DEBUG') or (getenv('YII_DEBUG')) ? define('YII_DEBUG', (bool) getenv('YII_DEBUG')) : define('YII_DEBUG', false);
    defined('YII_ENV') or (getenv('YII_ENV')) ? define('YII_ENV', getenv('YII_ENV')) : define('YII_ENV', 'prod');

    date_default_timezone_set('Europe/Warsaw');

    require(__DIR__ . '/../../vendor/autoload.php');
    require(__DIR__ . '/../../vendor/yiisoft/yii2/Yii.php');
    require(__DIR__ . '/../../common/config/bootstrap.php');
    require(__DIR__ . '/../../common/config/constants.php');

    $config = yii\helpers\ArrayHelper::merge(
        require(__DIR__ . '/../../common/config/main.php'),
        require(__DIR__ . '/../config/main.php')
    );

    $application = new yii\web\Application($config);
    $application->run();
