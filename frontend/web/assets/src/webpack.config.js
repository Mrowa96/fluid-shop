let webpack = require('webpack');

module.exports = {
    context: __dirname,
    entry: "./scripts/index.ts",
    output: {
        filename: "app.js",
        path: __dirname + "/scripts/_output/"
    },
    resolve: {
        extensions: ["", ".webpack.js", ".web.js", ".ts", ".js"],
        alias: {
            jquery: "jquery/src/jquery"
        }
    },
    module: {
        loaders: [
            {
                test: /\.ts$/,
                loader: "awesome-typescript-loader"
            }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        })
    ]
};