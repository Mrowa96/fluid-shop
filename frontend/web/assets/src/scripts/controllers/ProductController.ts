import BaseController from "./BaseController";

export default class Product extends BaseController {
    protected $element: JQuery;
    protected $buyForm: JQuery;
    protected $upSellsWrapper: JQuery;
    protected $crossSellsWrapper: JQuery;
    protected $upSellsInputs: JQuery;
    protected $crossSellsBlocks: JQuery;
    protected $buyButtons: JQuery;
    protected bought: {upSell: number, crossSell: number} = {
        upSell: 0,
        crossSell: 0
    };

    public detail() {
        this.$element = $(".product-detail, .product-detail-special");

        if (this.$element.length) {
            this.$buyForm = $(this.$element).find('#buy-form');

            if (this.$buyForm.length) {
                this.handleImagesSlider();
                this.handleUpSells();
                this.handleCrossSells();
                this.handleBuyButtons();
                this.handleComments();
            }
        }
    }

    protected handleImagesSlider() {
        let $swiperSlider = this.$element.find(".swiper-container.swiper-slider"),
            $swiperThumbs = this.$element.find(".swiper-container.swiper-thumbs");

        if ($swiperSlider.length) {
            let swiperSlider = new Swiper($swiperSlider.get(0), {
                loop: false,
                nextButton: $swiperSlider.find(".swiper-button-next").get(0),
                prevButton: $swiperSlider.find(".swiper-button-prev").get(0)
            });

            if ($swiperThumbs.length) {
                let swiperThumbs = new Swiper($swiperThumbs.get(0), {
                    centeredSlides: true,
                    slidesPerView: 'auto',
                    touchRatio: 0.1,
                    slideToClickedSlide: true
                });

                swiperSlider.params.control = swiperThumbs;
                swiperThumbs.params.control = swiperSlider;
            }
        }
    }

    /** Assign listeners to up sells checkboxes */
    protected handleUpSells() {
        this.$upSellsWrapper = $(this.$element).find(".up-sells-block");
        this.$upSellsInputs = $(this.$upSellsWrapper).find('.up-sells input[type="checkbox"]');

        if (this.$upSellsInputs.length) {
            let hiddenUpSellSelector = 'up-sell-hidden-';
            let $buyButton = $(this.$upSellsWrapper).find(".btn-buy");

            $buyButton.addClass("disabled");

            this.$upSellsInputs.each((index, input) => {
                let $checkbox = $(input);

                if ($checkbox.data('id')) {
                    $checkbox.change(() => {
                        if ($checkbox.is(":checked")) {
                            let $hiddenInput = $("<input>");

                            $hiddenInput.attr({
                                type: 'hidden',
                                value: 1,
                                name: 'BuyProduct[upSells][' + $checkbox.data('id') + ']',
                                id: hiddenUpSellSelector + $checkbox.data('id')
                            });

                            this.bought.upSell++;
                            $(this.$buyForm).append($hiddenInput);
                        }
                        else {
                            this.bought.upSell--;
                            $(this.$buyForm).find("#" + hiddenUpSellSelector + $checkbox.data('id')).remove();
                        }

                        if (this.bought.upSell > 0) {
                            $buyButton.removeClass("disabled");
                        }
                        else {
                            $buyButton.addClass("disabled");
                        }
                    })
                }
            })
        }
    }

    /** Assign listeners to cross sells blocks */
    protected handleCrossSells() {
        this.$crossSellsWrapper = $(this.$element).find(".cross-sells-block");
        this.$crossSellsBlocks = $(this.$crossSellsWrapper).find('.btn-add-or-remove');

        if (this.$crossSellsBlocks.length) {
            let hiddenCrossSellSelector = 'cross-sell-hidden-';
            let $buyButton = $(this.$crossSellsWrapper).find(".btn-buy");

            $buyButton.addClass("disabled");

            this.$crossSellsBlocks.each((index, element) => {
                let $link = $(element);

                if ($link.data('id')) {
                    $link.click((e) => {
                        e.preventDefault();

                        if (!$link.hasClass("added")) {
                            let $hiddenInput = $("<input>");

                            $link.addClass("added");

                            $hiddenInput.attr({
                                type: 'hidden',
                                value: 1,
                                name: 'BuyProduct[crossSells][' + $link.data('id') + ']',
                                id: hiddenCrossSellSelector + $link.data('id')
                            });

                            this.bought.crossSell++;
                            $(this.$buyForm).append($hiddenInput);
                        }
                        else {
                            $link.removeClass("added");

                            this.bought.crossSell--;
                            $(this.$buyForm).find("#" + hiddenCrossSellSelector + $link.data('id')).remove();
                        }


                        if (this.bought.crossSell > 0) {
                            $buyButton.removeClass("disabled");
                        }
                        else {
                            $buyButton.addClass("disabled");
                        }
                    })
                }
            })
        }
    }

    /** Assign functions to buy buttons which submits form */
    protected handleBuyButtons() {
        this.$buyButtons = this.$element.find(".btn-buy");

        if (this.$buyButtons.length) {
            this.$buyButtons.each((i, element) => {
                let $button = $(element);

                $button.click((e) => {
                    e.preventDefault();

                    if (!$button.hasClass("disabled")) {
                        $(this.$buyForm).submit();
                    }

                    return false;
                })
            })
        }
    }

    /** Assign function to comments button which load more comments */
    protected handleComments() {
        let btn = $(this.$element).find(".btn-load-more"),
            reviews = $(this.$element).find(".reviews");

        $(btn).click((e) => {
            e.preventDefault();

            $(reviews).find("li.hidden").each((index, li) => {
                if (index < 5) {
                    $(li).removeClass("hidden");
                }
            });

            if (!$(reviews).find("li.hidden").length) {
                $(btn).hide();
            }
        })
    }
}
