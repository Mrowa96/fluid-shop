import BaseController from "./BaseController";

export default class Basket extends BaseController {
    public index() {
        let $quantityColumn = $(".quantity-column"),
            $form = $quantityColumn.find("form"),
            $input = $quantityColumn.find(".quantity-input"),
            $plusBtn = $quantityColumn.find(".plus-btn"),
            $minusBtn = $quantityColumn.find(".minus-btn"),
            delay: any;

        $input.change(() => {
            $form.submit();
        });
        $minusBtn.click(() => {
            if (parseInt($input.val()) - 1 > 0) {
                $input.val(parseInt($input.val()) - 1);
                clearTimeout(delay);

                delay = setTimeout(() => {
                    $input.trigger("change");
                }, 1000);
            }
        });
        $plusBtn.click(() => {
            $input.val(parseInt($input.val()) + 1);
            clearTimeout(delay);

            delay = setTimeout(() => {
                $input.trigger("change");
            }, 1000);
        });
    }
}
