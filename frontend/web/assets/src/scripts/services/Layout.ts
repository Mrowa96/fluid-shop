import "jquery";
import "foundation-sites";
import Dropdown from "./Dropdown";
import IDropdownConfig from "./Dropdown/IDropdownConfig";
import * as Swiper from 'swiper';
import AlertWidget from "../../../../../../common/assets/scripts/widgets/AlertWidget";
import CookieLaw from "./CookieLaw";

export default class Layout {

    protected $header: JQuery;
    protected $content: JQuery;
    protected $footer: JQuery;
    public static dropdownArray: Array<Dropdown> = [];
    public static readonly BREAKPOINT_TABLET: number = 1024;
    public static readonly BREAKPOINT_MOBILE: number = 640;
    protected static readonly FAVORITES_URL = '/client/get-favorites-list-ajax.html';

    /**
     * Method must be called after document load e.g. in jQuery ready method.
     */
    public init(): boolean {
        if (this.initElements()) {
            this.manageHeaderIcons();
            this.handleBanners();
            this.handleCheckboxes();
            this.handleRadios();
            this.handleAlerts();
            this.handleCookieLaw();
            this.handleSidebarNavigation();

            return true;
        }

        return false;
    }

    protected initElements(): boolean {
        this.$header = $("#main-header");
        this.$content = $("#main-content");
        this.$footer = $("#main-footer");

        if (this.$header.length && this.$content.length && this.$footer) {
            return true;
        }
        else {
            console.warn("Some base jQuery element is not initialized, probably missing.");
            return false;
        }
    }

    protected handleAlerts() {
        let $alertsWrapper = $("#alerts-wrapper");

        if ($alertsWrapper.length) {
            let alertWidget = new AlertWidget($alertsWrapper);

            if (alertWidget.hasAlerts()) {
                alertWidget.showAll();
            }
        }
    }

    protected manageHeaderIcons() {
        let $searchBtn = this.$header.find(".search-btn"),
            $searchBar = this.$header.find("#search-bar-dropdown"),
            $favoritesBtn = this.$header.find(".favorites-btn"),
            $favoritesList = this.$header.find("#favorites-list-dropdown"),
            $basketBtn = this.$header.find(".basket-btn"),
            $basketList = this.$header.find("#basket-list-dropdown"),
            $categoriesBtn = this.$header.find(".categories-btn"),
            $categoriesList = this.$header.find("#category-list-dropdown"),
            $mobileMenuBtn = this.$header.find(".mobile-navigation-button"),
            $mobileMenuList = this.$header.find("#navigation-mobile-dropdown"),
            $userMenuBtn = this.$header.find(".user-menu-btn"),
            $userMenu = this.$header.find("#user-menu-dropdown");

        let searchDropdownConfig: IDropdownConfig = {
            align: Dropdown.ALIGN_NONE
        };
        let categoriesDropdownConfig: IDropdownConfig = {
            align: Dropdown.ALIGN_NONE
        };
        let basketDropdownConfig: IDropdownConfig = {
            afterInit: ($emitter: JQuery, $target: JQuery) => {
                $target.find(".delete-btn  > a").each((i, button) => {
                    $(button).click((event) => {
                        let liElement = $(button).parents("li"),
                            price = parseFloat($(liElement).data("item-price")),
                            quantity = parseInt($(liElement).data("item-quantity"));

                        event.preventDefault();

                        $.ajax({
                            url: $(button).attr("href"),
                            success: (response) => {
                                if (response && response.success) {
                                    if ($(liElement).data('item-id')) {
                                        let relatedChildren = $target.find('li[data-item-parent-id="' + $(liElement).data('item-id') + '"]');

                                        $(liElement).hide();

                                        if ($(relatedChildren).length) {
                                            $(relatedChildren).hide();
                                        }

                                        if (!$target.find("li:visible").length) {
                                            $target.find(".empty-block").removeClass("hidden");
                                        }
                                    }
                                }
                            }
                        });
                    })
                })
            }
        };

        new Dropdown($categoriesBtn, $categoriesList, categoriesDropdownConfig);
        new Dropdown($searchBtn, $searchBar, searchDropdownConfig);
        new Dropdown($userMenuBtn, $userMenu);
        new Dropdown($favoritesBtn, $favoritesList);
        new Dropdown($basketBtn, $basketList, basketDropdownConfig);
        new Dropdown($mobileMenuBtn, $mobileMenuList);
    }

    protected handleCookieLaw() {
        let cookieLaw: CookieLaw = new CookieLaw();

        if (!cookieLaw.isAccepted()) {
            cookieLaw.displayBar();
        }
    }

    protected handleBanners() {
        let swiperOptions: SwiperOptions = {
            loop: true,
            speed: 500,
            autoplay: 5000
        };

        this.$content.find(".banner-widget").each((o, element) => {
            let $banner = $(element),
                $swiperContainer = $banner.find(".swiper-container");

            if ($banner.data('pagination')) {
                swiperOptions.pagination = $banner.find(".swiper-pagination").get(0);
                swiperOptions.paginationClickable = true;
            }

            if ($banner.data('buttons')) {
                swiperOptions.nextButton = $banner.find(".swiper-button-next").get(0);
                swiperOptions.prevButton = $banner.find(".swiper-button-prev").get(0);
            }

            if ($banner.data('scrollbar')) {
                swiperOptions.scrollbar = $banner.find(".swiper-scrollbar").get(0);
            }

            if ($swiperContainer) {
                new Swiper($swiperContainer.get(0), swiperOptions);
            }
        });
    }

    protected handleCheckboxes() {
        let $checkboxes = this.$content.find('input[type="checkbox"]');

        $checkboxes.each((i, element) => {
            let $checkbox = $(element),
                $label = $(element).parent("label"),
                $formGroup = $checkbox.parents('.form-group');

            if ($checkbox.length && $formGroup.length) {
                $formGroup.addClass("checkbox-wrap");
                $label.addClass("checkbox-label");

                $checkbox.after($("<div></div>").addClass("checkbox-box"));
            }
        })
    }

    protected handleSidebarNavigation() {
        let $navigations = $("#main-sidebar").find(".navigation"),
            swiperOptions: SwiperOptions = {
                loop: false,
                slidesPerView: 2
            };

        if ($navigations.length) {
            if ($(document).width() < Layout.BREAKPOINT_TABLET) {
                changeToMobileView();
            }

            $(window).resize(() => {
                if ($(document).width() < Layout.BREAKPOINT_TABLET) {
                    changeToMobileView();
                }
                else {
                    changeToDesktopView();
                }
            });
        }

        function changeToMobileView() {
            $navigations.each((i, element) => {
                let $navigation = $(element),
                    $list = $navigation.find("ul"),
                    $items = $list.find("li");

                $navigation.css("display", "block").addClass("swiper-container");
                $list.addClass("swiper-wrapper");
                $items.addClass("swiper-slide");

                new Swiper($navigation.get(0), swiperOptions);
            })
        }

        function changeToDesktopView() {
            $navigations.each((i, element) => {
                let $navigation = $(element),
                    $list = $navigation.find("ul"),
                    $items = $list.find("li"),
                    swiper: Swiper = (<any>$navigation.get(0))['swiper'];

                $navigation.removeClass("swiper-container");
                $list.removeClass("swiper-wrapper");
                $items.removeClass("swiper-slide");

                if (typeof(swiper) !== "undefined") {
                    swiper.destroy(false, true);
                }
            })
        }
    }

    protected handleRadios() {
        let $radios = this.$content.find('input[type="radio"]');

        $radios.each((i, element) => {
            let $radio = $(element),
                $label = $(element).parent().find("label"),
                $formGroup = $radio.parents('.form-group');

            if ($radio.length && $formGroup.length) {
                $formGroup.addClass("radio-wrap");
                $label.addClass("radio-label");

                $label.prepend($("<div></div>").addClass("radio-box"));
            }
        })
    }

    public static hideDropdowns(activeDropdown: Dropdown) {
        Layout.dropdownArray.forEach((dropdown: Dropdown) => {
            if (dropdown.$target.hasClass(Dropdown.CLASS_VISIBLE) && dropdown.$target.attr("id") !== activeDropdown.$target.attr("id")) {
                dropdown.$target.removeClass(Dropdown.CLASS_VISIBLE).addClass(Dropdown.CLASS_INVISIBLE);
            }
        });
    }
};

