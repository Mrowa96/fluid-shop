export default class CookieLaw {
    protected accepted: boolean;
    protected hidden: boolean;
    protected $bar: JQuery;

    protected readonly BAR_ID: string = 'cookie-law-bar';
    protected readonly BAR_HIDDEN_CLASS: string = 'hidden';
    protected readonly BAR_TRANSITION: string = "bottom .7s ease";
    protected readonly LS_KEY_NAME: string = 'COOKIE_LAW_ACCEPTED';

    public constructor() {
        this.$bar = $("#" + this.BAR_ID);
        this.accepted = this.existsInStorage();

        this.initialize();
    }

    public isAccepted() {
        return this.accepted;
    }

    public displayBar() {
        this.$bar.css("bottom", 0);
        this.hidden = false;
    }

    public hideBar() {
        this.$bar.css("bottom", "-" + (2 * this.$bar.outerHeight(true)) + "px");
        this.hidden = true;
    }

    protected initialize() {
        if (!this.$bar.data("initialized")) {
            this.addListeners();

            this.$bar.removeClass(this.BAR_HIDDEN_CLASS);

            this.hideBar();

            this.$bar.css("transition", this.BAR_TRANSITION).data('initialized', true);
        }
    }

    protected addListeners() {
        this.$bar.find(".btn").click((e) => {
            e.preventDefault();

            this.hideBar();
            this.addToStorage();

            return false;
        });

        $(window).resize(() => {
            if (this.hidden) {
                /** Recalculate value of bottom */
                this.hideBar();
            }
        })
    }

    protected addToStorage(): void {
        localStorage.setItem(this.LS_KEY_NAME, '1');
    }

    protected removeFromStorage(): void {
        if (this.existsInStorage()) {
            localStorage.removeItem(this.LS_KEY_NAME);
        }
    }

    protected existsInStorage(): boolean {
        return !!localStorage.getItem(this.LS_KEY_NAME);
    }
}