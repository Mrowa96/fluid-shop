import BaseController from "../controllers/BaseController";

export default class Route {
    public name: string;
    public pattern: string|Array<string>;
    public controllerObject: BaseController;
    public actionName: string;
    public strict: boolean;

    public constructor(name: string, pattern: string|Array<string>, controllerObject: BaseController, actionName: string, strict: boolean = false) {
        this.name = name;
        this.pattern = pattern;
        this.controllerObject = controllerObject;
        this.actionName = actionName;
        this.strict = strict;
    }
}