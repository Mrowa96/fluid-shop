import Layout from "./Layout";
import IDropdownConfig from "./Dropdown/IDropdownConfig";

export default class Dropdown {
    get config(): IDropdownConfig {
        if(this._config){
            return this._config;
        }
        else{
            return {};
        }
    }

    set config(value: IDropdownConfig) {
        this._config = value;
    }
    get $target(): JQuery {
        return this._$target;
    }

    set $target(value: JQuery) {
        this._$target = value;
    }

    get $emitter(): JQuery {
        return this._$emitter;
    }

    set $emitter(value: JQuery) {
        this._$emitter = value;
    }

    private _$emitter: JQuery;
    private _$target: JQuery;
    private _config: IDropdownConfig;

    public static readonly CLASS_VISIBLE = 'visible';
    public static readonly CLASS_INVISIBLE = 'invisible';
    public static readonly CLASS_ELEMENT = 'custom-dropdown';
    public static readonly ATTR_INITIALIZED = 'initialized';
    public static readonly ATTR_EVENT_ASSIGNED = 'eventAssigned';
    public static readonly ATTR_DATA_LOADED = 'loaded';
    public static readonly ALIGN_RIGHT = 'right';
    public static readonly ALIGN_LEFT = 'left';
    public static readonly ALIGN_NONE = 'none';

    public constructor($emitter: JQuery, $target: JQuery, config?: IDropdownConfig) {
        this._$emitter = $emitter;
        this._$target = $target;
        this._config = config;

        this.init();
    }

    public static changeVisibility($target: JQuery): void {
        if ($target.hasClass(Dropdown.CLASS_VISIBLE)) {
            $target.removeClass(Dropdown.CLASS_VISIBLE);
            $target.addClass(Dropdown.CLASS_INVISIBLE);
        }
        else {
            $target.removeClass(Dropdown.CLASS_INVISIBLE);
            $target.addClass(Dropdown.CLASS_VISIBLE);
        }
    }

    protected init(): void {
        this.calculatePosition();

        this.$emitter.click((e) => {
            Layout.hideDropdowns(this);

            e.preventDefault();

            if (this.config.hasOwnProperty("onClick") && this.config.onClick) {
                this.config.onClick(this.$emitter, this.$target);
            }
            else {
                Dropdown.changeVisibility(this.$target);
            }
        });
        $(window).resize(() => {
            this.calculatePosition();
        });

        this.$emitter.data(Dropdown.ATTR_EVENT_ASSIGNED, true);
        this.$target.data(Dropdown.ATTR_INITIALIZED, true).addClass(Dropdown.CLASS_ELEMENT);

        Layout.dropdownArray.push(this);

        if(this.config.hasOwnProperty("afterInit")){
            this.config.afterInit(this.$emitter, this.$target);
        }
    }

    protected calculatePosition(): void {
        if(this.config.hasOwnProperty("align")){
            switch (this.config.align) {
                case Dropdown.ALIGN_LEFT:
                    this.$target.css({
                        left: this.$emitter.offset().left
                    });

                    break;
                case Dropdown.ALIGN_RIGHT:
                default:
                    this.$target.css({
                        right: ($(window).width() - (this.$emitter.offset().left + this.$emitter.outerWidth()))
                    });

                    break;
            }
        }
        else {
            if (this.$emitter.offset()) {
                this.$target.css({
                    right: ($(window).width() - (this.$emitter.offset().left + this.$emitter.outerWidth()))
                });
            }
        }
    }
}