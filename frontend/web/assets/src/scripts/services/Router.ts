import HomeController from "../controllers/HomeController";
import CategoryController from "../controllers/CategoryController";
import BasketController from "../controllers/BasketController";
import ProductController from "../controllers/ProductController";
import BaseController from "../controllers/BaseController";
import App from "../App";
import Route from "./Route";

export default class Router {
    protected url: string;
    protected match: Route;
    protected routes: Array<Route>;

    public constructor(url: string) {
        this.url = url;
        this.routes = this.loadRoutes();
    }

    public route(): boolean {
        for (let route of this.routes) {
            if (Array.isArray(route.pattern)) {
                for (let pattern of route.pattern) {
                    if (this.matchPattern(pattern, route)) {
                        return true;
                    }
                }
            }
            else {
                if (this.matchPattern(route.pattern, route)) {
                    return true;
                }
            }
        }

        return false;
    }

    public initMatch(): void {
        if (this.match instanceof Route) {
            let controller = this.match.controllerObject,
                action = this.match.actionName;

            App.setController(this.match.controllerObject);

            controller.beforeAction();

            if (typeof (<any>controller)[action] !== 'undefined') {
                (<any>controller)[action]();
            }

            controller.afterAction();
        }
    }

    public initBaseMatch(): void {
        let controller = new BaseController();

        App.setController(controller);

        controller.beforeAction();
        controller.afterAction();
    }

    protected matchPattern(pattern: string, route: Route): boolean {
        //TODO Change to regexp
        let match: Route | null = null;

        pattern = pattern.trim();

        if (route.strict === true) {
            if (this.url === pattern) {
                match = route;
            }
        }
        else {
            if (this.url.indexOf(pattern) !== -1) {
                match = route;
            }
        }

        if (match instanceof Route) {
            this.match = match;

            return true;
        }

        return false;
    }

    protected loadRoutes(): Array<Route> {
        return [
            new Route("home", ['/', '/site/index'], new HomeController(), "index", true),
            new Route("categoryIndex", ['/kategoria/'], new CategoryController(), "index", false),
            new Route("basketIndex", ['/basket/index'], new BasketController(), "index", true),
            new Route("productDetail", ['/product/detail', '/product/detail-special'], new ProductController(), "detail", false)
        ];
    }
}