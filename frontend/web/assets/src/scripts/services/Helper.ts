export default class Helper {
    private constructor() {}

    public static generateRandomKey(length: number): string {
        let key: string = "";

        while (key.length < length) {
            key += Math.random().toString(16).substring(2);
        }

        return key.substring(0, length);
    }
}