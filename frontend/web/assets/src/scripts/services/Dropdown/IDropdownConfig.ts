interface IDropdownConfig {
    align?: string;
    onClick?($emitter: JQuery, $target: JQuery): void;
    afterInit?($emitter: JQuery, $target: JQuery): void
}

export default IDropdownConfig;