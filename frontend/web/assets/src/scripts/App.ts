import Router from './services/Router';
import Url from './services/Url';
import BaseController from "./controllers/BaseController";

export default class App {
    protected Router: Router;
    protected Url: Url;

    protected static controller: BaseController;

    constructor() {
        $(document).ready(() => {
            this.Url = new Url();
            this.Router = new Router(this.Url.getPathnameShort());

            if (this.Router.route()) {
                this.Router.initMatch();
            }
            else {
                this.Router.initBaseMatch();
            }

            console.info("Fluid Shop is working.");
        });
    }

    public static setController(controller: BaseController) {
        this.controller = controller;
    }

    public static getController(): BaseController {
        return this.controller;
    }
}

