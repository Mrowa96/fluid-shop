let gulp = require('gulp'),
    sass = require('gulp-sass'),
    env = require('gulp-environments'),
    sourcemaps = require('gulp-sourcemaps'),
    cleanCSS = require('gulp-clean-css'),
    webpackStream = require('webpack-stream'),
    webpack = require('webpack'),
    prod = env.production,
    dev = env.development;

const STYLES_INPUTS = ['./styles/**/*.scss', '../../../../common/assets/styles/**/*.scss'];
const SCRIPT_INPUT_FILE = './scripts/index.ts';
const OUTPUT_DIR = '../dist';
const FONTS_INPUTS = [
    "./node_modules/bootstrap-sass/assets/fonts/bootstrap/**.*",
    "./node_modules/font-awesome/fonts/**.*"
];

gulp.task('scripts', () => {
    let webpackConfig = Object.create(require('./webpack.config.js'));

    if (dev()) {
        webpackConfig.watch = true;
        webpackConfig.devtool = "source-map";
    }
    else {
        webpackConfig.plugins = webpackConfig.plugins.concat(
            new webpack.DefinePlugin({
                "process.env": {
                    "NODE_ENV": JSON.stringify("production")
                }
            }),
            new webpack.optimize.DedupePlugin(),
            new webpack.optimize.UglifyJsPlugin({
                compress: {
                    warnings: false
                }
            })
        );
    }

    return gulp.src(SCRIPT_INPUT_FILE)
        .pipe(webpackStream(webpackConfig))
        .pipe(dev(sourcemaps.init({loadMaps: true})))
        .pipe(dev(sourcemaps.write('./')))
        .pipe(gulp.dest(OUTPUT_DIR + '/js'))
});

gulp.task('styles', ['fonts'], () => {
    return gulp.src(STYLES_INPUTS)
        .pipe(dev(sourcemaps.init()))
        .pipe(sass().on('error', sass.logError))
        .pipe(prod(cleanCSS({compatibility: 'ie10', restructuring: false, processImport: false})))
        .pipe(dev(sourcemaps.write('./')))
        .pipe(gulp.dest(OUTPUT_DIR + "/css"));
});

gulp.task('fonts', () => {
    return gulp.src(FONTS_INPUTS).pipe(gulp.dest(OUTPUT_DIR + "/fonts"));
});

gulp.task('watch-styles', ['styles'], () => {
    return gulp.watch(STYLES_INPUTS, ['styles']);
});

if (dev()) {
    gulp.task('default', ['scripts', 'watch-styles']);
}
else {
    gulp.task('default', ['scripts', 'styles']);
}
