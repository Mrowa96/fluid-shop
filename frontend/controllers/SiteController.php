<?php
    namespace frontend\controllers;

    use common\services\Alerts\ErrorAlert;
    use common\services\Alerts\SuccessAlert;
    use Yii;
    use yii\base\InvalidParamException;
    use yii\bootstrap\Html;
    use yii\filters\auth\HttpBasicAuth;
    use yii\web\BadRequestHttpException;
    use yii\filters\AccessControl;
    use frontend\forms\PasswordResetRequest;
    use frontend\forms\ResetPassword;
    use frontend\forms\Signup;
    use frontend\forms\Contact;
    use common\forms\Login;
    use common\models\Page;
    use yii\web\NotFoundHttpException;
    use common\models\User;
    use frontend\forms\Newsletter;

    class SiteController extends BaseController
    {
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'only' => ['logout', 'signup'],
                    'rules' => [
                        [
                            'actions' => ['signup'],
                            'allow' => true,
                            'roles' => ['?'],
                        ],
                        [
                            'actions' => ['logout'],
                            'allow' => true,
                            'roles' => ['@'],
                        ],
                    ],
                ],
                'authenticator' => [
                    'class' => HttpBasicAuth::className(),
                    'only' => ['get-google-merchant-data'],
                    'auth' => function ($username, $password) {
                        /** @var User $user */
                        $user = User::findByUsername($username);

                        if ($user && $user->validatePassword($password)) {
                            if (\Yii::$app->authManager->checkAccess($user->id, 'get google merchant data')) {
                                return $user;
                            }
                        }

                        return null;
                    },
                ],
            ];
        }

        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function actionIndex()
        {
            Yii::$app->layoutService->setContentContainer(false);
            Yii::$app->layoutService->removeBreadcrumbs();

            $this->setTitle('Fluid Shop');

            return $this->render('index');
        }

        /**
         * @param string $pageId
         * @return string|\yii\web\Response
         */
        public function actionContact($pageId)
        {
            $model = new Contact();
            /** @var Page $page */
            $page = Page::find()->where(['id' => $pageId, 'deleted' => 0, 'type' => Page::TYPE_CONTACT])->one();

            if ($page) {
                if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                    if ($model->sendEmail()) {
                        Yii::$app->alertsService->addAlert(new SuccessAlert(Yii::t("contact", "Message was successfully send")));
                    } else {
                        Yii::$app->alertsService->addAlert(new ErrorAlert(Yii::t("contact", "Unexpected error occurred during message send")));
                    }

                    return $this->refresh();
                } else {
                    Yii::$app->manageBarService->addAction(Yii::t("contact", "Edit contact page"),
                        Yii::$app->urlManagerBackend->createUrl(['page/update', 'id' => $page->id])
                    );

                    $this->view->registerMetaTag([
                        'keywords' => $page->seo_keywords,
                        'description' => $page->seo_description,
                    ]);
                    $this->setTitle($page->title);
                    $this->addBreadcrumb();

                    return $this->render('contact', [
                        'model' => $model,
                        'page' => $page
                    ]);
                }
            } else {
                return $this->goHome();
            }
        }

        /**
         * @param string $url
         * @param $id
         * @return string|\yii\web\Response
         * @throws NotFoundHttpException
         */
        public function actionPage($url, $id)
        {
            /** @var Page $page */
            $page = Page::find()->where(['id' => $id, 'url' => $url, 'deleted' => 0])->one();

            if ($page) {
                switch ($page->type) {
                    case Page::TYPE_CONTACT:
                        return $this->redirect(['site/contact', 'pageId' => $page->id]);
                        break;
                    case Page::TYPE_STANDARD:
                    default:
                        Yii::$app->manageBarService->addAction(Yii::t("page", "Edit page"),
                            Yii::$app->urlManagerBackend->createUrl(['page/update', 'id' => $page->id])
                        );

                        $this->view->registerMetaTag([
                            'keywords' => $page->seo_keywords,
                            'description' => $page->seo_description,
                        ]);

                        $this->setTitle($page->title);
                        $this->addBreadcrumb();

                        return $this->render('page', [
                            'data' => $page
                        ]);
                        break;
                }
            } else {
                throw new NotFoundHttpException();
            }
        }

        public function actionLogin()
        {
            if (!Yii::$app->user->isGuest) {
                return $this->goHome();
            }

            $model = new Login();

            if ($model->load(Yii::$app->request->post()) && $model->login()) {
                if ($ref = Yii::$app->request->get('ref')) {
                    return $this->redirect($ref);
                } else {
                    return $this->goBack();
                }
            } else {
                $this->setTitle(Yii::t("site", "Login"));
                $this->addBreadcrumb();

                return $this->render('login', [
                    'model' => $model,
                ]);
            }
        }

        public function actionLogout()
        {
            Yii::$app->user->logout();

            return $this->goHome();
        }

        public function actionSignup()
        {
            $model = new Signup();

            if ($model->load(Yii::$app->request->post())) {
                if ($user = $model->signup()) {
                    if (Yii::$app->getUser()->login($user)) {
                        if ($ref = Yii::$app->request->get('ref')) {
                            return $this->redirect($ref);
                        } else {
                            return $this->goHome();
                        }
                    }
                }
            }

            $this->setTitle(Yii::t("site", "Sign up"));
            $this->addBreadcrumb();

            return $this->render('signup', [
                'model' => $model,
            ]);
        }

        public function actionRequestPasswordReset()
        {
            $model = new PasswordResetRequest();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->sendEmail()) {
                    Yii::$app->alertsService->addAlert(new SuccessAlert(Yii::t("site", 'Check your email for further instructions.')));

                    return $this->goHome();
                } else {
                    Yii::$app->alertsService->addAlert(new ErrorAlert(Yii::t("site", 'Sorry, we are unable to reset password for email provided.')));
                }
            }

            $this->setTitle(Yii::t("site", "Reset your password"));
            $this->addBreadcrumb();

            return $this->render('request-password-reset-token', [
                'model' => $model,
            ]);
        }

        /**
         * @param string $token
         * @return string|\yii\web\Response
         * @throws BadRequestHttpException
         */
        public function actionResetPassword($token)
        {
            try {
                $model = new ResetPassword($token);
            } catch (InvalidParamException $e) {
                throw new BadRequestHttpException($e->getMessage());
            }

            if ($model->load(Yii::$app->request->post()) && $model->validate() && $model->resetPassword()) {
                Yii::$app->alertsService->addAlert(new SuccessAlert(Yii::t("site", 'New password was saved.')));

                return $this->goHome();
            }

            $this->setTitle(Yii::t("site", "Reset password"));
            $this->addBreadcrumb();

            return $this->render('reset-password', [
                'model' => $model,
            ]);
        }

        public function actionAssignToNewsletter()
        {
            $model = new Newsletter();

            if ($model->load(Yii::$app->request->post()) && $model->assign()) {
                return $this->goBack();
            }

            return $this->goBack();
        }

        public function actionGetGoogleMerchantData()
        {
            //TODO Rewrite this shit
            header('Content-Type: text/xml');

            $dir = Yii::getAlias("@googleMerchantData") . DIRECTORY_SEPARATOR;
            $lastMod = 0;
            $lastModFile = '';
            foreach (scandir($dir) as $entry) {
                if (is_file($dir . $entry) && filectime($dir . $entry) > $lastMod) {
                    $lastMod = filectime($dir . $entry);
                    $lastModFile = $dir . $entry;
                }
            }

            if (realpath($lastModFile)) {
                Yii::$app->user->logout();
                echo file_get_contents(realpath($lastModFile));
                exit;
            }
        }

        /**
         * @param null|string $label
         */
        protected function addBreadcrumb($label = null)
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                if (is_null($label)) {
                    $label = $this->view->title;
                }

                Yii::$app->layoutService->getBreadcrumbs()->addLabel($label);
            }
        }
    }
