<?php
    namespace frontend\controllers;

    use frontend\utilities\Menu\MobileCategoriesDataProvider;
    use Yii;
    use yii\db\Expression;
    use yii\db\Query;
    use yii\helpers\Url;
    use yii\web\NotFoundHttpException;
    use yii\data\Pagination;
    use common\models\Category;
    use common\models\Product;
    use common\models\ProductAttribute;
    use frontend\services\Layout\Sidebar;
    use frontend\utilities\ListOrder;
    use frontend\utilities\Menu;
    use frontend\utilities\Menu\CategoriesDataProvider;
    use frontend\utilities\ProductsFilter;

    class CategoryController extends BaseController
    {
        /**
         * @param int $id
         * @param string $url
         * @return string
         * @throws NotFoundHttpException
         */
        public function actionIndex($id, $url)
        {
            /** @var Category $category */
            $category = Category::find()->where(['id' => $id, 'url' => $url, 'deleted' => 0])->one();

            if ($category) {
                /** @var Query $productsQuery */
                $productsQuery = Product::createQueryForCategory($category);

                if (Yii::$app->request->get('priceMin')) {
                    $productsQuery->having(['>', '(`fs_product`.cost) - product_discount - category_discount', Yii::$app->request->get('priceMin')]);
                }
                if (Yii::$app->request->get('priceMax')) {
                    $productsQuery->andHaving(['<', '(`fs_product`.cost) - product_discount - category_discount', Yii::$app->request->get('priceMax')]);
                }
                if (Yii::$app->request->get('filter')) {
                    $filtersRaw = Yii::$app->request->get('filter');
                    $filters = [];

                    if (!empty($filtersRaw)) {
                        $productsQuery->leftJoin(ProductAttribute::tableName(), ProductAttribute::tableName() . '.product_id = ' . Product::tableName() . '.id');

                        foreach ($filtersRaw AS $key => $value) {
                            $splited = explode("-", $key);

                            if (count($splited) === 2) {
                                $filters[$splited[0]][] = $splited[1];
                            }
                        }

                        foreach ($filters AS $attributeId => $options) {
                            $filterQuery = (new Query)
                                ->select([new Expression('1')])
                                ->from(ProductAttribute::tableName())
                                ->where(ProductAttribute::tableName() . '.product_id = ' . Product::tableName() . '.id')
                                ->andWhere([ProductAttribute::tableName() . '.attribute_id' => $attributeId])
                                ->andWhere([ProductAttribute::tableName() . '.attribute_option_id' => $options]);

                            $productsQuery->andWhere(['exists', $filterQuery]);
                        }
                    }
                }

                $pagination = new Pagination([
                    'totalCount' => $productsQuery->count(),
                    'pageSize' => Yii::$app->request->get('per-page') ? Yii::$app->request->get('per-page') : $this->defaultPageSize
                ]);

                $productsQuery->offset($pagination->offset)->limit($pagination->limit)->orderBy($this->getOrderType());

                $this->view->registerMetaTag(["name" => 'keywords', 'content' => $category->seo_keywords]);
                $this->view->registerMetaTag(["name" => 'description', 'content' => $category->seo_description]);

                $sidebar = new Sidebar();
                $sidebar->addComponent(new Menu(new MobileCategoriesDataProvider($category), [
                    'wrapperClass' => 'navigation navigation-mobile'
                ]));
                $sidebar->addComponent(new Menu(new CategoriesDataProvider(), [
                    'wrapperClass' => 'navigation hide-for-mobile'
                ]));
                $sidebar->addComponent(new ProductsFilter($category));
                Yii::$app->layoutService->setSidebar($sidebar);

                $this->setTitle($category->name);
                $this->setBreadCrumbs($category);

                return $this->render('index', [
                    'products' => $productsQuery->all(),
                    'pagination' => $pagination
                ]);
            } else {
                throw new NotFoundHttpException('The requested category does not exist.');
            }
        }

        /**
         * @param Category $category
         */
        private function setBreadCrumbs(Category $category)
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                $parentCategory = $category->parent;

                while ($parentCategory) {
                    Yii::$app->layoutService->getBreadcrumbs()->addLink($parentCategory->name,
                        Url::toRoute(['category/index', 'url' => $parentCategory->url, 'id' => $parentCategory->id])
                    );

                    $parentCategory = $parentCategory->parent;
                }


                Yii::$app->layoutService->getBreadcrumbs()->reverseData();
                Yii::$app->layoutService->getBreadcrumbs()->addLabel($category->name);
            }
        }

        /**
         * @return null|string
         */
        private function getOrderType()
        {
            $orderBy = (int)Yii::$app->request->get('order-by');

            if ($orderBy) {
                switch ($orderBy) {
                    case ListOrder::PRICE_ASC:
                        $order = 'cost ASC';
                        break;
                    case ListOrder::PRICE_DESC:
                        $order = 'cost DESC';
                        break;
                    case ListOrder::NAME_DESC:
                        $order = 'name DESC';
                        break;
                    case ListOrder::NAME_ASC:
                    default:
                        $order = 'name ASC';
                        break;
                }

                return $order;
            } else {
                return null;
            }
        }
    }