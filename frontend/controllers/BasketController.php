<?php
    namespace frontend\controllers;

    use common\models\Order;
    use common\models\OrderStatus;
    use common\models\User;
    use common\services\Alerts\Alert;
    use ReflectionClass;
    use Yii;
    use yii\filters\AccessControl;
    use frontend\services\Basket;
    use common\models\Payment;
    use common\models\Transport;
    use common\models\Settings;
    use frontend\utilities\Payment\PayU;
    use common\models\Settings\PayPal as PayPalModel;
    use common\models\Settings\PayU as PayUModel;
    use frontend\utilities\Payment\PayPal;
    use yii\helpers\Url;
    use yii\web\Response;
    use common\models\PaymentApi;
    use common\forms\Login;
    use frontend\forms\BuyProduct;
    use frontend\forms\Discount;
    use frontend\services\Basket\Item as BasketItem;

    class BasketController extends BaseController
    {
        const STEP_LIST = 1;
        const STEP_ACCOUNT = 2;
        const STEP_ADDRESS = 3;
        const STEP_PAYMENT_AND_TRANSPORT = 4;
        const STEP_SUMMARY = 5;
        const STEP_AFTER_COMPLETE = 6;

        const MESSAGE_ORDER_PAYMENT_FAIL = 'Error occurred during payment.';
        const MESSAGE_ORDER_NOT_PAID = 'Order was send, but is not paid.';
        const MESSAGE_ORDER_ERROR = 'Order fail.';
        const MESSAGE_PAYMENT_EARLY_ERROR = 'Payment is not configured.';

        /* @var $basket Basket */
        protected $basket;

        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => [
                                'index', 'add-product', 'add-product-special',
                                'remove-product', 'remove-product-special',
                                'remove-basket', 'account', 'as-guest',
                                'recount-product-quantity', 'set-discount',
                                'after-complete'
                            ],
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => false,
                            'actions' => [
                                'account', 'as-guest'
                            ],
                            'roles' => ['client', 'worker', 'admin'],
                        ],
                        [
                            'allow' => true,
                            'roles' => ['client', 'worker', 'admin'],
                        ],
                        [
                            'allow' => false,
                            'actions' => ['*'],
                            'roles' => ['system']
                        ]
                    ],
                    'denyCallback' => function ($role, $action) {
                        if ($this->basket->isGuest()) {
                            $controller = $action->controller;
                            $actionName = $action->actionMethod;
                            $result = $controller->$actionName();

                            if ($result instanceof Response) {
                                //todo check
                                //var_dump($result);
                            } else {
                                echo $result;
                            }
                        } else if (Yii::$app->authManager->getAssignment('system', Yii::$app->user->id)) {
                            return $this->redirectWithMessage(['site/index'],
                                Yii::t("basket", "You don't have access to basket"), "error");
                        } else if (Yii::$app->authManager->getAssignment('admin', Yii::$app->user->id) ||
                            Yii::$app->authManager->getAssignment('client', Yii::$app->user->id) ||
                            Yii::$app->authManager->getAssignment('worker', Yii::$app->user->id)
                        ) {
                            return $this->redirect(['basket/index']);
                        } else {
                            return $this->redirect(['basket/account']);
                        }
                    }
                ],
            ];
        }

        public function beforeAction($action)
        {
            switch ($action->id) {
                case "index":
                case "after-complete":
                    $this->basket = Yii::$app->basketHelper->get();
                    break;
                default:
                    $this->basket = Yii::$app->basket;
                    break;
            }

            return parent::beforeAction($action);
        }

        public function actionIndex()
        {
            $this->setTitle(Yii::t("basket", "List"));
            $this->setBreadcrumbs();

            $discount = new Discount();

            if (Yii::$app->request->isPost) {
                if ($discount->load(Yii::$app->request->post()) && $discount->validate()) {
                    Yii::$app->basket->setDiscount($discount->code);
                }
            }

            return $this->render("index", [
                'discountModel' => $discount
            ]);
        }

        public function actionAccount()
        {
            if (!$this->basket->isGuest()) {
                $loginForm = new Login();

                if ($loginForm->load(Yii::$app->request->post()) && $loginForm->login()) {
                    if ($ref = $loginForm->ref) {
                        return $this->redirect($ref);
                    } else {
                        return $this->goBack();
                    }
                } else {
                    $this->setTitle(Yii::t("basket", "Account"));
                    $this->setBreadcrumbs();

                    return $this->render('account', [
                        'model' => $loginForm,
                    ]);
                }
            }

            return $this->redirect(['basket/address']);
        }

        public function actionAsGuest()
        {
            if (Yii::$app->user->isGuest) {
                $this->basket->asGuest();
            }

            return $this->redirect(['basket/address']);
        }

        public function actionAddress()
        {
            $model = $this->basket->getAddress();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                if ($this->basket->setAddress($model)) {
                    return $this->redirect(['basket/payment-and-transport']);
                }
            }

            $this->setTitle(Yii::t("basket", "Address"));
            $this->setBreadcrumbs(self::STEP_ADDRESS);

            return $this->render('address', [
                'model' => $model
            ]);
        }

        public function actionPaymentAndTransport()
        {
            $payments = Payment::find()->where(['deleted' => 0, 'active' => 1])->all();
            $transports = Transport::find()->where(['deleted' => 0, 'active' => 1])->all();

            if (Yii::$app->request->isPost) {
                $payment = Yii::$app->request->post('Payment');
                $transport = Yii::$app->request->post('Transport');

                if (!empty($payment['id']) && !empty($transport['id'])) {
                    $paymentModel = Payment::findOne($payment['id']);
                    $transportModel = Transport::findOne($transport['id']);

                    if ($paymentModel && $transportModel) {
                        $this->basket->setPayment($paymentModel);
                        $this->basket->setTransport($transportModel);

                        return $this->redirect(['basket/summary']);
                    } else {
                        return $this->refresh();
                    }
                }
            }

            $this->setTitle(Yii::t("basket", "Payment ad transport"));
            $this->setBreadcrumbs(self::STEP_PAYMENT_AND_TRANSPORT);

            return $this->render("payment-and-transport", [
                'payments' => $payments,
                'transports' => $transports,
            ]);
        }

        public function actionSummary()
        {
            if ($this->basket->canBeCompleted()) {
                $this->setTitle(Yii::t("basket", "Summary"));
                $this->setBreadcrumbs(self::STEP_SUMMARY);

                return $this->render('summary', [
                    'basket' => $this->basket
                ]);
            }

            return $this->goBack();
        }

        public function actionComplete()
        {
            if ($this->basket->canBeCompleted() && $this->basket->complete()) {
                /** @var PaymentApi $api */
                $api = $this->basket->getPayment()->api;

                if ($api) {
                    switch ($api->symbol) {
                        case "PAYU":
                            $payment = new PayU();
                            $reflectionModel = new ReflectionClass(PayUModel::className());
                            break;
                        case "PAYPAL":
                            $payment = new PayPal();
                            $reflectionModel = new ReflectionClass(PayPalModel::className());
                            break;
                        default:
                            return $this->redirect(['basket/after-complete']);
                            break;
                    }

                    if (Settings::getOne('active', $reflectionModel->getConstant('MODULE'))) {
                        $payment->pay($this->basket);

                        if ($payment->hasError()) {
                            return $this->redirectWithMessage(['site/index'], Yii::t("basket", self::MESSAGE_ORDER_PAYMENT_FAIL), Alert::TYPE_ERROR);
                        }
                    } else {
                        return $this->redirectWithMessage(['site/index'], Yii::t("basket", self::MESSAGE_PAYMENT_EARLY_ERROR), Alert::TYPE_ERROR);
                    }
                } else {
                    /** @var Order $order */
                    $order = Order::find()->where(['key' => $this->basket->getOrderKey()])->one();

                    if ($order) {
                        $order->setStatus(OrderStatus::STATUS_NEW_COMPLETE, User::getSystemUser());

                        return $this->redirect(['basket/after-complete']);
                    } else {
                        return $this->redirectWithMessage(['basket/after-complete'], Yii::t("basket", self::MESSAGE_ORDER_ERROR), Alert::TYPE_ERROR);
                    }
                }
            } else {
                return $this->redirectWithMessage(['site/index'], Yii::t("basket", self::MESSAGE_ORDER_ERROR), Alert::TYPE_ERROR);
            }
        }

        /**
         * @param bool $paid
         * @return string
         */
        public function actionAfterComplete($paid = false)
        {
            $this->setTitle(Yii::t("basket", "After complete"));
            $this->setBreadcrumbs(self::STEP_AFTER_COMPLETE);

            return $this->render("after-complete", [
                'paid' => $paid,
            ]);
        }

        public function actionAddProduct()
        {
            if (Yii::$app->request->isPost) {
                $form = new BuyProduct();

                if ($form->load(Yii::$app->request->post()) && $form->addToBasket()) {
                    return $this->redirect(['basket/index']);
                }

            } else if (Yii::$app->request->isGet) {
                $id = Yii::$app->request->get("id");
                $quantity = Yii::$app->request->get("quantity", 1);

                if ($this->basket->addItem($id, $quantity)) {
                    return $this->redirect(['basket/index']);
                }
            }

            return $this->goBack();
        }

        public function actionAddProductSpecial($id)
        {
            if ($this->basket->addItemSpecial($id)) {
                return $this->redirect(['basket/index']);
            }

            return $this->goBack();
        }

        public function actionRemoveProduct($id, $type = BasketItem::TYPE_STANDARD, $relatedParentId = null)
        {
            if (Yii::$app->request->isAjax) {
                Yii::$app->response->format = Response::FORMAT_JSON;

                if ($this->basket->removeItem($id, $type, $relatedParentId)) {
                    return ['success' => true];
                }

                return ['success' => false];
            } else {
                if ($this->basket->removeItem($id, $type, $relatedParentId)) {
                    return $this->redirectWithMessage(['basket/index'], "Usunięto pomyślnie");
                }

                return $this->goBack();
            }
        }

        public function actionRemoveProductSpecial($id)
        {
            if ($this->basket->removeItemSpecial($id)) {
                if (Yii::$app->request->isAjax) {
                    Yii::$app->response->format = Response::FORMAT_JSON;

                    return [
                        'success' => true
                    ];
                } else {
                    return $this->redirectWithMessage(['basket/index'], "Usunięto pomyślnie");
                }
            }
        }

        public function actionRecountProductQuantity($id, $type)
        {
            if (Yii::$app->request->isPost) {
                $item = $this->basket->getItem($id, false, $type);
                $quantity = Yii::$app->request->post("quantity");

                if ($item && $item->setQuantity($quantity)) {
                    return $this->redirect(['basket/index']);
                } else {
                    return $this->redirectWithMessage(['basket/index'], "Nie mozna zmienic ilości");
                }
            } else {
                return $this->redirect(['basket/index']);
            }
        }

        public function actionRemoveBasket()
        {
            if ($this->basket->remove()) {
                return $this->redirect(['site/index']);
            }
        }

        /**
         * @param int $step
         */
        private function setBreadcrumbs($step = self::STEP_LIST)
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                $breadcrumbs = Yii::$app->layoutService->getBreadcrumbs();

                $breadcrumbs->addLabel(Yii::t("basket", "Basket"));

                switch ($step) {
                    case self::STEP_ADDRESS:
                        $breadcrumbs->addLink(Yii::t("basket", "List"), Url::toRoute(['basket/index']));
                        break;
                    case self::STEP_PAYMENT_AND_TRANSPORT:
                        $breadcrumbs->addLink(Yii::t("basket", "List"), Url::toRoute(['basket/index']));
                        $breadcrumbs->addLink(Yii::t("basket", "Address"), Url::toRoute(['basket/address']));
                        break;
                    case self::STEP_SUMMARY:
                        $breadcrumbs->addLink(Yii::t("basket", "List"), Url::toRoute(['basket/index']));
                        $breadcrumbs->addLink(Yii::t("basket", "Address"), Url::toRoute(['basket/address']));
                        $breadcrumbs->addLink(Yii::t("basket", "Payment and transport"), Url::toRoute(['basket/payment-and-transport']));
                        break;
                    case self::STEP_AFTER_COMPLETE:
                        $breadcrumbs->addLink(Yii::t("basket", "List"), Url::toRoute(['basket/index']));
                        break;
                }

                $breadcrumbs->addLabel($this->view->title);
            }
        }
    }
