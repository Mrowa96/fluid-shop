<?php
    namespace frontend\controllers;

    use Yii;
    use yii\web\Controller;
    use common\services\Alerts\Alert;
    use frontend\services\Layout\Breadcrumbs;
    use common\services\Alerts\Builder as AlertBuilder;

    class BaseController extends Controller
    {
        const LAYOUT_CLEAR = 'clear';

        protected $defaultPageSize = 10;

        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        /**
         * @param \yii\base\Action $action
         * @return bool
         */
        public function beforeAction($action)
        {
            Yii::$app->layoutService->setContentContainer(true);
            Yii::$app->layoutService->setBreadcrumbs(new Breadcrumbs());

            return parent::beforeAction($action);
        }

        /**
         * @param string $title
         */
        protected function setTitle($title)
        {
            $this->view->title = $title;
        }

        /**
         * @param array $url
         * @param string $message
         * @param string $type
         * @param int $statusCode
         * @return \yii\web\Response
         */
        protected function redirectWithMessage(array $url, $message, $type = Alert::TYPE_SUCCESS, $statusCode = 302)
        {
            Yii::$app->alertsService->addAlert(AlertBuilder::build($message, $type));

            return parent::redirect($url, $statusCode);
        }

        /**
         * @param string $view
         * @param string $message
         * @param array $params
         * @param string $type
         * @return string
         */
        protected function renderWithMessage($view, $message, $params = [], $type = Alert::TYPE_SUCCESS)
        {
            Yii::$app->alertsService->addAlert(AlertBuilder::build($message, $type));

            return parent::render($view, $params);
        }
    }
