<?php
    namespace frontend\controllers;

    use common\services\Alerts\SuccessAlert;
    use Yii;
    use common\models\Favorite;
    use common\models\ProductOfADay;
    use common\models\Product;
    use frontend\forms\ProductReview as ProductReviewForm;
    use yii\helpers\Url;
    use yii\web\NotFoundHttpException;
    use frontend\forms\BuyProduct;

    class ProductController extends BaseController
    {
        /**
         * @param string $url
         * @param int $id
         * @return string
         * @throws NotFoundHttpException
         */
        public function actionDetail($url, $id)
        {
            /** @var Product $product */
            $product = Product::find()->where(['id' => $id, 'processed_name' => $url, 'type' => Product::TYPE_STANDARD])->one();

            if ($product) {
                Yii::$app->manageBarService->addAction(Yii::t("site", "Edit product"),
                    Yii::$app->urlManagerBackend->createUrl(['product/update', 'id' => $product->id])
                );

                $this->setBreadCrumbs($product);
                $this->setTitle($product->name);

                return $this->render('detail', [
                    'isSpecial' => false,
                    'product' => $product,
                    'reviewForm' => new ProductReviewForm(),
                    'buyForm' => new BuyProduct(),
                ]);
            } else {
                throw new NotFoundHttpException("Product does not exists.");
            }
        }

        /**
         * @param int $id
         * @return string
         * @throws NotFoundHttpException
         */
        public function actionDetailSpecial($id)
        {
            /** @var ProductOfADay $productOfADay */
            $productOfADay = ProductOfADay::find()->where(['id' => $id])->one();

            if ($productOfADay->product) {
                Yii::$app->manageBarService->addAction(Yii::t("product", "Edit product"),
                    Yii::$app->urlManagerBackend->createUrl(['product/update', 'id' => $productOfADay->product->id])
                );
                Yii::$app->manageBarService->addAction(Yii::t("product", "Edit product of a day"),
                    Yii::$app->urlManagerBackend->createUrl(['product-of-a-day/update', 'id' => $productOfADay->id])
                );

                $this->setBreadCrumbs($productOfADay->product, true);
                $this->setTitle($productOfADay->product->name);

                return $this->render('detail-special', [
                    'isSpecial' => true,
                    'productOfADay' => $productOfADay,
                    'product' => $productOfADay->product,
                    'buyForm' => new BuyProduct()
                ]);
            } else {
                throw new NotFoundHttpException("Product does not exists.");
            }
        }

        public function actionSaveReview()
        {
            if (Yii::$app->request->isPost && !Yii::$app->user->isGuest) {
                $form = new ProductReviewForm();

                if ($form->load(Yii::$app->request->post()) && $form->saveReview()) {
                    $product = Product::findOne($form->product_id);

                    Yii::$app->alertsService->addAlert(new SuccessAlert(Yii::t("product", "Review was saved and it is waiting for acceptance.")));

                    return $this->redirect(['product/detail', 'id' => $product->id, 'url' => $product->processed_name]);
                }
            }

            return $this->redirect(['site/index']);
        }

        /**
         * @param int $id
         * @return \yii\web\Response
         */
        public function actionAddToFavorites($id)
        {
            if (!Yii::$app->user->isGuest) {
                $favorite = new Favorite();

                $favorite->product_id = $id;

                if ($favorite->save()) {
                    Yii::$app->alertsService->addAlert(new SuccessAlert(Yii::t("product", "Product was saved to favorites list")));
                }
            }

            return $this->redirect(Yii::$app->request->referrer);
        }

        /**
         * @param Product $product
         * @param bool $special
         */
        private function setBreadCrumbs(Product $product, $special = false)
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                if ($special === false) {
                    $category = $product->category;

                    while (!is_null($category)) {
                        Yii::$app->layoutService->getBreadcrumbs()->addLink($category->name,
                            Url::toRoute(['category/index', 'url' => $category->url, 'id' => $category->id])
                        );

                        $category = $category->parent;
                    }

                    Yii::$app->layoutService->getBreadcrumbs()->reverseData();
                } else {
                    Yii::$app->layoutService->getBreadcrumbs()->addLabel(Yii::t("product", "Product of a day"));
                }

                Yii::$app->layoutService->getBreadcrumbs()->addLabel($product->name);
            }
        }
    }
