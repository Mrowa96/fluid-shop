<?php
    namespace frontend\controllers;

    use Yii;
    use common\models\Order;
    use common\models\OrderStatus;
    use common\models\User;
    use frontend\utilities\Payment\PayPal;

    class PaymentController extends BaseController
    {
        const MESSAGE_COMPLETE = "Operation completed.";
        const MESSAGE_FAIL_STATUS_CHANGE = "Cannot change order status. Please contact with owner.";
        const MESSAGE_FAIL_ORDER = "Cannot find order by given id.";
        const MESSAGE_FAIL_ERROR = "Unexpected error occurred.";
        const MESSAGE_CANCELED = "Payment was canceled.";

        public function actionPaypalComplete()
        {
            $paymentId = Yii::$app->request->get('paymentId');
            $payerId = Yii::$app->request->get('PayerID');
            $orderKey = Yii::$app->request->get('orderKey');

            $result = (new PayPal())->complete($paymentId, $payerId);

            if ($result && $result->getState() === "approved") {
                /** @var Order $order */
                $order = Order::find()->where(['key' => Order::decryptKey($orderKey)])->one();

                if ($order) {
                    if (User::getSystemUser() && $order->setStatus(OrderStatus::STATUS_PAID, User::getSystemUser())) {
                        return $this->redirect(['basket/after-complete', 'paid' => true]);
                    } else {
                        return $this->redirectWithMessage(['site/index'],
                            Yii::t("payment", self::MESSAGE_FAIL_STATUS_CHANGE), "error");
                    }
                } else {
                    return $this->redirectWithMessage(['site/index'],
                        Yii::t("payment", self::MESSAGE_FAIL_ORDER), "error");
                }
            } else {
                return $this->redirectWithMessage(['site/index'],
                    Yii::t("payment", self::MESSAGE_FAIL_ERROR), "error");
            }
        }

        public function actionPaypalCancel()
        {
            return $this->redirectWithMessage(['site/index'], Yii::t("payment", self::MESSAGE_CANCELED), "error");
        }

        public function actionPayuComplete()
        {
            $orderKey = Yii::$app->request->get('orderKey');

            if ($orderKey) {
                /** @var Order $order */
                $order = Order::find()->where(['key' => Order::decryptKey($orderKey)])->one();

                if ($order) {
                    if (User::getSystemUser() && $order->setStatus(OrderStatus::STATUS_PAID, User::getSystemUser())) {
                        return $this->redirect(['basket/after-complete', 'paid' => true]);
                    } else {
                        return $this->redirectWithMessage(['site/index'],
                            Yii::t("payment", self::MESSAGE_FAIL_STATUS_CHANGE), "error");
                    }
                } else {
                    return $this->redirectWithMessage(['site/index'],
                        Yii::t("payment", self::MESSAGE_FAIL_ORDER), "error");
                }
            }

            return $this->redirectWithMessage(['site/index'],
                Yii::t("payment", self::MESSAGE_FAIL_ERROR));
        }

        public function actionPayuCancel()
        {
            return $this->redirectWithMessage(['site/index'], Yii::t("payment", self::MESSAGE_CANCELED), "error");
        }
    }