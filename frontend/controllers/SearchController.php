<?php
    namespace frontend\controllers;

    use Yii;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use common\utilities\ElasticSearch;

    class SearchController extends BaseController
    {
        public function actionByPhrase()
        {
            $phrase = Yii::$app->request->post('phrase');

            if (!empty($phrase)) {
                $elasticSearch = new ElasticSearch();

                $this->view->params['phrase'] = Html::encode($phrase);
                $this->setTitle(Yii::t("search", "Search results"));

                if (Yii::$app->layoutService->hasBreadcrumbs()) {
                    Yii::$app->layoutService->getBreadcrumbs()->addLabel($this->view->title);
                    Yii::$app->layoutService->getBreadcrumbs()->addLabel(Html::encode($phrase));
                }

                return $this->render('results', [
                    'data' => $elasticSearch->queryProducts($phrase),
                ]);
            } else {
                return $this->redirect(Url::base());
            }
        }
    }