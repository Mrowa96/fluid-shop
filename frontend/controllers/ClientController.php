<?php
    namespace frontend\controllers;

    use common\services\Alerts\ErrorAlert;
    use frontend\utilities\Menu;
    use frontend\utilities\Menu\ClientDataProvider;
    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\filters\AccessControl;
    use common\models\Favorite;
    use common\models\Order;
    use common\models\Product;
    use common\models\Rma;
    use yii\helpers\Url;
    use yii\web\Response;
    use common\models\Address;
    use frontend\forms\ChangePassword;
    use frontend\services\Layout\Sidebar;

    class ClientController extends BaseController
    {
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'roles' => ['client', 'worker', 'admin'],
                        ],
                        [
                            'allow' => false,
                            'actions' => ['*'],
                            'roles' => ['system']
                        ]
                    ],
                    'denyCallback' => function ($role, $action) {
                        if (Yii::$app->authManager->getAssignment('system', Yii::$app->user->id)) {
                            return $this->redirectWithMessage(['site/index'],
                                Yii::t("client", "You don't have an access to use client panel"), "error");
                        }
                    }
                ],
            ];
        }

        public function beforeAction($action)
        {
            $parentAction = parent::beforeAction($action);

            $sidebar = new Sidebar();
            $sidebar->addComponent(new Menu(new ClientDataProvider()));

            Yii::$app->layoutService->setSidebar($sidebar);

            return $parentAction;
        }

        public function actionIndex()
        {
            $this->setTitle(Yii::t("client", "My account"));
            $this->setBreadcrumbs(false);

            return $this->render("index", [
                'client' => Yii::$app->user->identity
            ]);
        }

        public function actionChangePassword()
        {
            $model = new ChangePassword();

            if ($model->load(Yii::$app->request->post()) && $model->validate()) {
                if ($model->changePassword()) {
                    $this->redirectWithMessage(['client/index'], Yii::t("client", "Operation completed"));
                } else {
                    //TODO Something
                    Yii::$app->alertsService->addAlert(new ErrorAlert(Yii::t("client", 'Sorry, we are unable to reset password for email provided.')));
                }
            }
            $this->setTitle(Yii::t("client", "Change password"));
            $this->setBreadcrumbs();

            return $this->render('change-password', [
                'model' => $model,
            ]);
        }

        public function actionLogout()
        {
            return $this->redirect(['site/logout']);
        }

        public function actionOrdersList()
        {
            $orders = new ActiveDataProvider([
                'query' => Order::find()->where(['user_id' => Yii::$app->user->id]),
                'sort' => [
                    'defaultOrder' => [
                        'create_date' => SORT_DESC
                    ],
                    'attributes' => []
                ]
            ]);

            $this->setTitle(Yii::t("client", "Orders list"));
            $this->setBreadcrumbs();

            return $this->render("orders-list", [
                'orders' => $orders
            ]);
        }

        /**
         * @param int $id
         * @return string
         */
        public function actionOrderDetails($id)
        {
            $order = Order::findOne($id);
            $orderDetails = new ActiveDataProvider([
                'query' => $order->getOrderDetails()
            ]);

            $this->setTitle(Yii::t("client", "Orders details"));
            $this->setBreadcrumbs();

            return $this->render("order-details", [
                'order' => $order,
                'orderDetails' => $orderDetails,
            ]);
        }

        public function actionAddressManage()
        {
            $address = Address::find()->where(['user_id' => Yii::$app->user->id])->one();

            $this->setTitle(Yii::t("client", "Address"));
            $this->setBreadcrumbs();

            if (!$address) {
                $address = new Address();
            }

            if ($address->load(Yii::$app->request->post()) && $address->save()) {
                return $this->redirect(['client/index']);
            }

            return $this->render('address-manage', [
                'model' => $address
            ]);
        }

        /**
         * @param int $productId
         * @param int $orderId
         * @return string|Response
         */
        public function actionComplaintCreate($productId, $orderId)
        {
            /** @var Order $order */
            $order = Order::find()->where(['id' => $orderId, 'deleted' => 0])->one();
            /** @var Product $product */
            $product = Product::find()->where(['id' => $productId])->one();

            $this->setTitle(Yii::t("client", "Make a complaint"));
            $this->setBreadcrumbs();

            if ($order && $product && $order->isProductInOrder($productId) && $order->productNotComplaint($product)) {
                $rma = new Rma();

                $rma->order_id = $orderId;
                $rma->product_id = $productId;
                $rma->user_id = Yii::$app->user->id;

                if ($rma->load(Yii::$app->request->post()) && $rma->save()) {
                    return $this->redirect(['client/complaint-list']);
                } else {
                    return $this->render('complaint-create', [
                        'model' => $rma,
                        'product' => Product::findOne($productId),
                    ]);
                }
            } else {
                return $this->redirect(['order-details', 'id' => $orderId]);
            }
        }

        public function actionComplaintList()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Rma::find()->where(['user_id' => Yii::$app->user->id])
            ]);

            $this->setTitle(Yii::t("client", "Complaints list"));
            $this->setBreadcrumbs();

            return $this->render('complaint-list', [
                'dataProvider' => $dataProvider
            ]);
        }

        public function actionFavoritesList()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => Favorite::find()->where(['user_id' => Yii::$app->user->id]),
                'sort' => false
            ]);

            $this->setTitle(Yii::t("client", "Favorites list"));
            $this->setBreadcrumbs();

            return $this->render('favorites-list', [
                'dataProvider' => $dataProvider
            ]);
        }

        /**
         * @param bool $addTitle
         */
        protected function setBreadcrumbs($addTitle = true)
        {
            if (Yii::$app->layoutService->hasBreadcrumbs()) {
                Yii::$app->layoutService->getBreadcrumbs()->addLink(Yii::t("client", "My account"), Url::toRoute(['client/index']));

                if ($addTitle === true) {
                    Yii::$app->layoutService->getBreadcrumbs()->addLabel($this->view->title);
                }
            }
        }
    }