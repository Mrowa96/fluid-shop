<?php
    namespace frontend\services\Layout;

    /**
     * Class Sidebar
     * @package frontend\services\Layout
     *
     * @property bool $display
     * @property SidebarComponent[] $components
     */
    class Sidebar
    {
        protected $display;
        protected $components;

        /**
         * Sidebar constructor.
         * @param bool $display
         */
        public function __construct($display = true)
        {
            $this->display = $display;
        }

        /**
         * @return bool
         */
        public function getDisplay()
        {
            return $this->display;
        }

        /**
         * @param bool $display
         * @return $this
         */
        public function setDisplay($display)
        {
            $this->display = $display;

            return $this;
        }

        /**
         * @return SidebarComponent[]
         */
        public function getComponents()
        {
            return $this->components;
        }

        /**
         * @param SidebarComponent $component
         * @return $this
         */
        public function addComponent(SidebarComponent $component)
        {
            $this->components[] = $component;

            return $this;
        }
    }