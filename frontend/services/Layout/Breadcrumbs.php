<?php
    namespace frontend\services\Layout;

    /**
     * Class Breadcrumbs
     * @package frontend\services\Layout
     *
     * @property array $data
     * @property bool $displayMain
     */
    class Breadcrumbs
    {
        protected $data;
        protected $displayMain;

        /**
         * Breadcrumbs constructor.
         * @param bool $displayMain
         */
        public function __construct($displayMain = true)
        {
            $this->data = [];
            $this->displayMain = $displayMain;
        }

        /**
         * @param string $label
         * @param string $url
         */
        public function addLink($label, $url)
        {
            $this->data[] = [
                'url' => $url,
                'label' => $label
            ];
        }

        /**
         * @param string $label
         */
        public function addLabel($label)
        {
            $this->data[] = $label;
        }

        public function reverseData()
        {
            $this->data = array_reverse($this->data);
        }

        /**
         * @return array
         */
        public function getData()
        {
            return $this->data;
        }

        /**
         * @return bool
         */
        public function getDisplayMain()
        {
            return $this->displayMain;
        }

        /**
         * @param bool $displayMain
         */
        public function setDisplayMain($displayMain)
        {
            $this->displayMain = $displayMain;
        }
    }