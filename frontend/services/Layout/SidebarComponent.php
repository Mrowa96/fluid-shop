<?php
    namespace frontend\services\Layout;

    /**
     * Interface SidebarComponent
     * @package frontend\services\Layout
     */
    interface SidebarComponent
    {
        public function render();
    }