<?php
    namespace frontend\services;

    use frontend\services\ManageBar\Action;

    /**
     * Class ManageBar
     * @package frontend\services
     *
     * @property Action[] $actions
     */
    class ManageBar
    {
        protected $actions;

        public function __construct()
        {
            $this->actions = [];
        }

        /**
         * @param string $name
         * @param string $url
         *
         * @return string
         */
        public function addAction($name, $url)
        {
            $action = new Action($name, $url);

            $this->actions[$action->getParsedName()] = $action;

            return $action->getParsedName();
        }

        /**
         * @param string $name
         *
         * @return void
         */
        public function removeAction($name)
        {
            if (!empty($this->actions[$name])) {
                unset($this->actions[$name]);
            }
        }

        /**
         * @return Action[]
         */
        public function getActions()
        {
            return $this->actions;
        }

        /**
         * @return void
         */
        public function removeAll()
        {
            unset($this->actions);

            $this->actions = [];
        }
    }