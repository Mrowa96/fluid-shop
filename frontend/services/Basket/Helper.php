<?php
    namespace frontend\services\Basket;

    use frontend\services\Basket;
    use Yii;

    /**
     * Class Helper
     * @package frontend\services\Basket
     */
    class Helper
    {
        /**
         * @return bool
         */
        public function exists()
        {
            return Yii::$app->session->has('basket');
        }

        /**
         * @return Basket|null
         */
        public function get()
        {
            return Yii::$app->session->get('basket');
        }

        /**
         * @param Basket $basket
         * @return bool
         */
        public function set(Basket $basket)
        {
            return Yii::$app->session->set('basket', $basket);
        }
    }