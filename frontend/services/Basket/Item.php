<?php
    namespace frontend\services\Basket;

    use Yii;
    use common\models\OrderDetail;
    use common\models\Product;
    use common\models\ProductOfADay;
    use common\models\ProductSellCross;
    use common\models\ProductSellUp;
    use common\models\Settings;
    use common\models\Settings\System;
    use frontend\services\Basket\Exception as BasketException;

    /**
     * Class Item
     * @package frontend\services\Basket
     *
     * @property Product $product
     * @property OrderDetail $model
     * @property int $quantity
     * @property double $cost
     * @property integer $orderId
     * @property integer $type
     * @property ProductOfADay|ProductSellCross|ProductSellUp $typedObject
     * @property Item relatedParent
     * @property Item[] $relatedChildren
     *
     * @var int TYPE_STANDARD
     * @var int TYPE_SPECIAL
     * @var int TYPE_UP_SELL
     * @var int TYPE_CROSS_SELL
     */
    class Item
    {
        const TYPE_STANDARD = 0;
        const TYPE_SPECIAL = 1;
        const TYPE_UP_SELL = 2;
        const TYPE_CROSS_SELL = 3;

        protected $product;
        protected $model;
        protected $quantity;
        protected $cost;
        protected $orderId;
        protected $type;
        protected $typedObject;
        protected $relatedParent;
        protected $relatedChildren = [];

        /**
         * BasketItem constructor.
         * @param $orderId
         * @param int $type
         */
        public function __construct($orderId, $type = self::TYPE_STANDARD)
        {
            $this->orderId = $orderId;
            $this->type = $type;
        }

        /**
         * @param Product $product
         * @throws BasketException
         */
        public function setProduct(Product $product)
        {
            if (!$this->model) {
                $this->product = $product;
                $this->cost = $product->cost;
            } else {
                throw new BasketException(Yii::t("basket", "Cannot change already set up product."), 200);
            }
        }

        /**
         * @param int $quantity
         * @return bool
         */
        public function setQuantity($quantity = 1)
        {
            $this->quantity = (int)$quantity;

            if ($this->model) {
                $this->model->product_quantity = $quantity;

                return $this->model->save();
            }

            return true;
        }

        /**
         * @param $cost
         * @return bool
         */
        public function setCost($cost)
        {
            $this->cost = $cost;

            if ($this->model) {
                $this->model->product_cost = $cost;

                return $this->model->save();
            }

            return true;
        }

        /**
         * @param $object
         * @return bool
         */
        public function setTypedObject($object)
        {
            $this->typedObject = $object;

            return true;
        }

        /**
         * @param Item $related
         */
        public function setRelatedParent(Item $related)
        {
            $related->setRelatedChild($this);
            $this->relatedParent = $related;
        }

        /**
         * @param Item $related
         */
        public function setRelatedChild(Item $related)
        {
            $this->relatedChildren[] = $related;
        }

        public function getProduct()
        {
            return $this->product;
        }

        public function getId()
        {
            return $this->product->id;
        }

        public function getName()
        {
            return $this->product->name;
        }

        public function getCode()
        {
            return $this->product->code;
        }

        public function getDescription()
        {
            return $this->product->description;
        }

        /**
         * @param bool $withCurrency
         * @return float|string
         */
        public function getCost($withCurrency = false)
        {
            if ($withCurrency === true) {
                return $this->cost . Settings::getOne('currency', System::MODULE);
            } else {
                return $this->cost;
            }
        }

        public function getModel()
        {
            return $this->model;
        }

        public function hasRelatedParent()
        {
            return !is_null($this->relatedParent);
        }

        public function getRelatedParent()
        {
            return ($this->relatedParent) ? $this->relatedParent : null;
        }

        public function getRelatedChildren()
        {
            return $this->relatedChildren;
        }

        /**
         * @param $id
         * @param $type
         * @return Item|null
         */
        public function getRelatedChild($id, $type)
        {
            $item = null;

            /** @var Item $itemChild */
            foreach ($this->getRelatedChildren() as $itemChild) {
                if ($itemChild->getId() == $id && $itemChild->getType() == $type) {
                    $item = $itemChild;

                    break;
                }
            }

            return $item;
        }

        /**
         * @param Item $item
         * @return bool|false|int
         * @throws \Exception
         */
        public function removeRelatedChild(Item $item)
        {
            foreach ($this->relatedChildren as $index => $childItem) {
                if ($childItem === $item) {
                    array_splice($this->relatedChildren, $index, 1);

                    return $item->getModel()->delete();
                }
            }

            return false;
        }

        public function getQuantity()
        {
            return ($this->quantity > 0) ? $this->quantity : 1;
        }

        public function getType()
        {
            return $this->type;
        }

        public function getTypedObject()
        {
            return ($this->typedObject && $this->getType() !== self::TYPE_STANDARD) ? $this->typedObject : null;
        }

        public function isStandard()
        {
            return $this->type === self::TYPE_STANDARD;
        }

        public function isSpecial()
        {
            return $this->type === self::TYPE_SPECIAL;
        }

        public function isUpSell()
        {
            return $this->type === self::TYPE_UP_SELL;
        }

        public function isCrossSell()
        {
            return $this->type === self::TYPE_CROSS_SELL;
        }

        public function save()
        {
            $this->model = new OrderDetail();

            $this->model->order_id = $this->orderId;
            $this->model->product_id = $this->product->id;
            $this->model->product_cost = $this->getCost();
            $this->model->product_quantity = $this->getQuantity();
            $this->model->product_type = $this->getType();

            if ($this->getRelatedParent()) {
                $this->model->order_detail_related_id = $this->getRelatedParent()->model->id;
            }

            return $this->model->save();
        }
    }