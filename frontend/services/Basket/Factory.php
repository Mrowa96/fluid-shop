<?php
    namespace frontend\services\Basket;

    use frontend\services\Basket;
    use Yii;

    /**
     * Class Factory
     * @package frontend\services\Basket
     */
    class Factory
    {
        /**
         * @return \Closure
         */
        public static function create()
        {
            return function () {
                if (Yii::$app->session->has('basket')) {
                    return Yii::$app->session->get('basket');
                } else {
                    return new Basket();
                }
            };
        }
    }