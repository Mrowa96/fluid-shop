<?php
    namespace frontend\services;

    use frontend\services\Layout\Breadcrumbs;
    use frontend\utilities\Menu;
    use yii\base\Object;
    use frontend\services\Layout\Sidebar;

    /**
     * Class Layout
     * @package frontend\services
     *
     * @property Sidebar $sidebar
     * @property bool $contentContainer
     * @property Breadcrumbs $breadcrumbs
     */
    class Layout extends Object
    {
        protected $sidebar;
        protected $contentContainer;
        protected $breadcrumbs;

        /**
         * @param Sidebar $sidebar
         *
         */
        public function setSidebar(Sidebar $sidebar)
        {
            $this->sidebar = $sidebar;
        }

        /**
         * @return Sidebar|null
         */
        public function getSidebar()
        {
            return $this->sidebar;
        }

        /**
         * @return bool
         */
        public function hasSidebar()
        {
            return $this->sidebar instanceof Sidebar;
        }

        /**
         * @return Breadcrumbs
         */
        public function getBreadcrumbs()
        {
            return $this->breadcrumbs;
        }

        /**
         * @param Breadcrumbs $breadcrumbs
         */
        public function setBreadcrumbs(Breadcrumbs $breadcrumbs)
        {
            $this->breadcrumbs = $breadcrumbs;
        }

        /**
         * @return bool
         */
        public function hasBreadcrumbs()
        {
            return $this->breadcrumbs instanceof Breadcrumbs;
        }

        /**
         * @return void
         */
        public function removeBreadcrumbs()
        {
            $this->breadcrumbs = null;
        }

        /**
         * @return bool
         */
        public function getContentContainer()
        {
            return $this->contentContainer;
        }

        /**
         * @param bool $contentContainer
         */
        public function setContentContainer($contentContainer)
        {
            $this->contentContainer = $contentContainer;
        }
    }