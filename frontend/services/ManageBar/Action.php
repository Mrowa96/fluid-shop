<?php
    namespace frontend\services\ManageBar;

    /**
     * Class Action
     * @package frontend\services\ManageBar
     *
     * @property string $name;
     * @property string $parsedName;
     * @property string $url;
     */
    class Action
    {
        protected $name;
        protected $parsedName;
        protected $url;

        public function __construct($name, $url)
        {
            $this->name = $name;
            $this->parsedName = $this->parseName($name);
            $this->url = $url;
        }

        /**
         * @return string
         */
        public function getName()
        {
            return $this->name;
        }

        /**
         * @return string
         */
        public function getUrl()
        {
            return $this->url;
        }

        /**
         * @return string
         */
        public function getParsedName()
        {
            return $this->parsedName;
        }

        /**
         * @param string $name
         * @return string
         */
        protected function parseName($name)
        {
            return str_replace(" ", "-", mb_strtolower((string)$name));
        }
    }