<?php
    namespace frontend\services;

    use Yii;
    use common\models\Product;
    use common\models\Address;
    use common\models\Order;
    use common\models\OrderStatus;
    use common\models\Payment;
    use common\models\Transport;
    use backend\models\MailMessage;
    use common\models\Discount;
    use common\models\ProductOfADay;
    use common\models\ProductSellCross;
    use common\models\ProductSellUp;
    use common\models\Settings;
    use common\models\Settings\System;
    use common\models\User;
    use common\utilities\Mailer;
    use frontend\services\Basket\Item as BasketItem;
    use frontend\services\Basket\Exception as BasketException;

    /**
     * Class Basket
     * @package frontend\services
     *
     * @property Order $order
     * @property Discount $discount
     * @property array|BasketItem[] $items
     * @property BasketItem $itemSpecial
     * @property bool $isGuest
     */
    class Basket
    {
        protected $order;
        protected $discount;
        protected $items;
        protected $itemSpecial;
        protected $isGuest;

        public function __construct()
        {
            $this->items = [];
            $this->isGuest = false;
            $this->discount = null;

            $this->order = new Order();
            $this->order->basket_create_date = (new \DateTime())->format("Y-m-d H:i:s");
            $this->order->order_status_id = OrderStatus::STATUS_NEW_NOT_COMPLETE;

            if (!Yii::$app->user->isGuest) {
                $this->order->user_id = Yii::$app->user->id;
            }

            if ($this->order->save(false)) {
                Yii::$app->basketHelper->set($this);
            }
        }

        /**
         * @param $id
         * @param $quantity
         * @return Product|BasketItem
         * @throws BasketException
         */
        public function addItem($id, $quantity)
        {
            /** @var Product $product */
            $product = Product::find()->where(['id' => $id, 'deleted' => 0, 'available' => 1])->one();

            if ($product) {
                if ($this->hasItem($id)) {
                    $item = $this->getItem($id);
                    $item->setQuantity($item->getQuantity() + $quantity);
                } else {
                    $item = new BasketItem($this->order->id);
                    $item->setProduct($product);
                    $item->setQuantity($quantity);
                    $item->setCost($product->getCost(false, true));

                    if ($item->save()) {
                        $this->items[] = $item;
                    }
                }

                if ($this->discount && $item) {
                    $item->setCost($this->discount->recountCost($item->getCost()));
                }

                return $item;
            } else {
                throw new BasketException(Yii::t("basket", "Cannot find product with given id."), 100);
            }
        }

        /**
         * @param $id
         * @return BasketItem
         * @throws BasketException
         */
        public function addItemSpecial($id)
        {
            /** @var ProductOfADay $productOfADay */
            /** @var Product $product */
            $productOfADay = ProductOfADay::find()->where(['id' => $id])->one();

            if ($productOfADay) {
                $product = $productOfADay->product;

                if ($this->hasItemSpecial() === false) {
                    $item = new BasketItem($this->order->id, BasketItem::TYPE_SPECIAL);
                    $item->setProduct($product);
                    $item->setTypedObject($productOfADay);
                    $item->setCost($productOfADay->cost);

                    if ($item->save()) {
                        $this->itemSpecial = $item;
                    }
                } else {
                    throw new BasketException(Yii::t("basket", "Cannot add more than one piece of special product to the basket"), 105);
                }

                return $item;
            } else {
                throw new BasketException(Yii::t("basket", "Cannot find product special with given id."), 100);
            }
        }

        /**
         * @param $id
         * @param BasketItem $related
         * @return Product|BasketItem
         * @throws BasketException
         */
        public function addItemUpSell($id, BasketItem $related)
        {
            /** @var ProductSellUp $upSell */
            $upSell = ProductSellUp::find()->where(['id' => $id])->one();

            if ($upSell->product->deleted == 0 && $upSell->product->available == 1 && $upSell->related->hasUpSell($id)) {
                if ($upSell->product) {
                    if ($this->hasItem($upSell->product->id, BasketItem::TYPE_UP_SELL)) {
                        $item = $this->getItem($id, false, BasketItem::TYPE_UP_SELL);
                        $item->setQuantity($item->getQuantity() + 1);
                    } else {
                        $item = new BasketItem($this->order->id, BasketItem::TYPE_UP_SELL);
                        $item->setProduct($upSell->product);
                        $item->setRelatedParent($related);
                        $item->setTypedObject($upSell);
                        $item->setCost($upSell->cost);

                        $item->save();
                    }

                    return $item;
                }
            } else {
                throw new BasketException(Yii::t("basket", "Cannot find product with given id."), 130);
            }
        }

        /**
         * @param $id
         * @param BasketItem $related
         * @return Product|BasketItem
         * @throws BasketException
         */
        public function addItemCrossSell($id, BasketItem $related)
        {
            /** @var ProductSellCross $crossSell */
            $crossSell = ProductSellCross::find()->where(['id' => $id])->one();

            if ($crossSell->product->deleted == 0 && $crossSell->product->available == 1 && $crossSell->related->hasCrossSell($id)) {
                if ($crossSell->product) {
                    if ($this->hasItem($crossSell->product->id, BasketItem::TYPE_CROSS_SELL)) {
                        $item = $this->getItem($id, false, BasketItem::TYPE_CROSS_SELL);
                        $item->setQuantity($item->getQuantity() + 1);
                    } else {
                        $item = new BasketItem($this->order->id, BasketItem::TYPE_CROSS_SELL);
                        $item->setProduct($crossSell->product);
                        $item->setCost($crossSell->cost);
                        $item->setTypedObject($crossSell);
                        $item->setRelatedParent($related);

                        $item->save();
                    }

                    return $item;
                }
            } else {
                throw new BasketException(Yii::t("basket", "Cannot find product with given id."), 131);
            }
        }

        /**
         * @param $id
         * @param int $type
         * @return bool
         */
        public function hasItem($id, $type = BasketItem::TYPE_STANDARD)
        {
            foreach ($this->items AS $item) {
                if ($item->getId() == $id && $item->getType() == $type) {
                    return true;
                }
            }

            return false;
        }

        /**
         * @param array $ids
         * @param int $type
         * @return bool
         */
        public function hasItems(array $ids, $type = BasketItem::TYPE_STANDARD)
        {
            if (!empty($ids)) {
                $foundQuantity = 0;

                foreach ($ids AS $id) {
                    foreach ($this->items AS $item) {
                        if ($item->getId() == $id && $item->getType() == $type) {
                            $foundQuantity++;
                        }
                    }
                }

                return count($ids) === $foundQuantity;
            }

            return false;
        }

        public function hasItemSpecial()
        {
            return $this->itemSpecial instanceof BasketItem;
        }

        /**
         * @param $id
         * @param bool $onlyData
         * @param int $type
         * @return Product|BasketItem
         * @throws BasketException
         */
        public function getItem($id, $onlyData = false, $type = BasketItem::TYPE_STANDARD)
        {
            foreach ($this->items AS $item) {
                if ($item->getId() == $id && $item->getType() == $type) {
                    if ($onlyData === true) {
                        return $item->getProduct();
                    } else {
                        return $item;
                    }
                }
            }

            throw new BasketException(Yii::t("basket", "Cannot find product with given id."), 101);
        }

        /**
         * @param bool $onlyData
         * @param bool $withSpecial
         * @param bool $withRelatedChildren
         * @return Product[] | BasketItem[]
         */
        public function getItems($onlyData = false, $withSpecial = true, $withRelatedChildren = false)
        {
            $results = [];

            if ($onlyData === true) {
                foreach ($this->items AS $item) {
                    $results[] = $item->getProduct();

                    if ($withRelatedChildren === true) {
                        $itemChildren = $item->getRelatedChildren();

                        foreach ($itemChildren AS $itemChild) {
                            $results[] = $itemChild->getProduct();
                        }
                    }
                }

                if ($withSpecial && $this->hasItemSpecial()) {
                    $results[] = $this->getItemSpecial()->getProduct();
                }
            } else {
                $results = $this->items;

                if ($withSpecial && $this->hasItemSpecial()) {
                    $results[] = $this->getItemSpecial();
                }

                if ($withRelatedChildren === true) {
                    foreach ($this->items AS $item) {
                        $itemChildren = $item->getRelatedChildren();

                        foreach ($itemChildren AS $itemChild) {
                            $results[] = $itemChild;
                        }
                    }
                }
            }

            return $results;
        }

        public function getItemSpecial()
        {
            return $this->itemSpecial;
        }

        /**
         * @param bool $withSpecial
         * @param bool $withRelatedChildren
         * @return int
         */
        public function countItems($withSpecial = true, $withRelatedChildren = false)
        {
            $counter = 0;

            foreach ($this->items AS $item) {
                $counter += $item->getQuantity();

                if ($withRelatedChildren === true && $item->getRelatedChildren()) {
                    foreach ($item->getRelatedChildren() AS $childItem) {
                        $counter += $childItem->getQuantity();
                    }
                }
            }

            if ($withSpecial === true && $this->hasItemSpecial()) {
                $counter++;
            }

            return $counter;
        }

        /**
         * @param $id
         * @param int $type
         * @param null $relatedParentId
         * @return bool
         * @throws BasketException
         */
        public function removeItem($id, $type = BasketItem::TYPE_STANDARD, $relatedParentId = null)
        {
            try {
                foreach ($this->items as $index => $item) {
                    if ($type != BasketItem::TYPE_STANDARD && $type != BasketItem::TYPE_SPECIAL && $relatedParentId) {
                        /** @var BasketItem $relatedParent */
                        $relatedParent = $this->getItemRelatedParent($relatedParentId);

                        if ($relatedParent) {
                            $childItem = $relatedParent->getRelatedChild($id, $type);

                            if ($childItem) {
                                $relatedParent->removeRelatedChild($childItem);
                            }
                        } else {
                            return false;
                        }

                        break;
                    } else {
                        if ($item->getId() == $id && $item->getType() == $type) {
                            $item->getModel()->delete();
                            array_splice($this->items, $index, 1);

                            break;
                        }
                    }
                }

                return true;
            } catch (\Exception $ex) {
                throw new BasketException(Yii::t("basket", "Problem occurred during removing product from basket."), 102);
            }
        }

        /**
         * @param $id
         * @return bool
         * @throws BasketException
         */
        public function removeItemSpecial($id)
        {
            try {
                if ($this->itemSpecial->getId() == $id) {
                    $this->itemSpecial->getModel()->delete();
                    $this->itemSpecial = null;
                }

                return true;
            } catch (\Exception $ex) {
                throw new BasketException(Yii::t("basket", "Problem occurred during removing product from basket."), 102);
            }
        }

        public function removeItems()
        {
            try {
                foreach ($this->items AS $item) {
                    $item->getModel()->delete();
                }

                unset($this->items);

                return true;
            } catch (\Exception $ex) {
                throw new BasketException(Yii::t("basket", "Problem occurred during removing all products from basket."), 103);
            }
        }

        /**
         * @param Address $address
         * @return bool
         */
        public function setAddress(Address $address)
        {
            $this->order->address_id = $address->id;

            return $this->order->save(false);
        }

        /**
         * @return Address
         */
        public function getAddress()
        {
            if ($this->order->address instanceof Address) {
                $address = $this->order->address;
            } else if (!empty($this->order->address_id)) {
                $address = Address::findOne($this->order->address_id);
            } else {
                $address = new Address();

                if (!Yii::$app->user->isGuest) {
                    /** @var User $user */
                    $user = User::findOne(Yii::$app->user->id);

                    if ($user) {
                        $address->load(['Address' => $user->getAddress(true)]);
                    }
                }
            }

            return $address;
        }

        /**
         * @return bool
         */
        public function hasAddress()
        {
            return !empty($this->order->address_id);
        }

        /**
         * @param Payment $payment
         * @return bool
         */
        public function setPayment(Payment $payment)
        {
            $this->order->payment_id = $payment->id;
            unset($this->order->payment);

            return $this->order->save(false);
        }

        public function getPayment()
        {
            return $this->order->payment;
        }

        /**
         * @param Transport $transport
         * @return bool
         */
        public function setTransport(Transport $transport)
        {
            $this->order->transport_id = $transport->id;
            unset($this->order->transport);

            return $this->order->save(false);
        }

        public function getTransport()
        {
            return $this->order->transport;
        }

        /**
         * @param string $code
         * @return bool
         */
        public function setDiscount($code)
        {
            $discount = Discount::getDiscount($code);

            if ($discount && !$this->discount) {
                foreach ($this->items AS $item) {
                    $newCost = $discount->recountCost($item->getCost());

                    if ($item->setCost($newCost)) {
                        $discount->amount--;
                        $discount->save();
                    }
                }

                $this->discount = $discount;

                return true;
            }

            return false;
        }

        public function getDiscount()
        {
            return $this->discount;
        }

        public function isGuest()
        {
            return $this->isGuest;
        }

        public function asGuest()
        {
            $this->isGuest = true;
        }

        public function getOrderKey()
        {
            return $this->order->key;
        }

        /**
         * @param bool $withCurrency
         * @param bool $withTransport
         * @param bool $withRelatedChildren
         * @return float|int|mixed|string
         */
        public function getTotalCost($withCurrency = false, $withTransport = true, $withRelatedChildren = false)
        {
            $items = $this->getItems();
            $total = 0;

            foreach ($items AS $item) {
                $total += $item->getCost() * $item->getQuantity();

                if ($withRelatedChildren === true && $item->getRelatedChildren()) {
                    foreach ($item->getRelatedChildren() AS $childItem) {
                        $total += $childItem->getCost() * $childItem->getQuantity();
                    }
                }
            }

            if ($withTransport === true) {
                if ($this->getTransport()) {
                    $total += $this->getTransport()->cost;
                }
            }

            if ($withCurrency === true) {
                return $total . Settings::getOne('currency', System::MODULE);
            } else {
                return $total;
            }
        }

        public function notEmpty()
        {
            return (count($this->getItems()) > 0) ? true : false;
        }

        public function remove()
        {
            $this->removeItems();
            $this->order->delete();

            return Yii::$app->session->remove("basket");
        }

        public function sendConfirmation()
        {
            /**
             * @var MailMessage $message
             * @var Mailer $mailer
             */
            $message = MailMessage::find()->where(['name' => 'order_confirmation'])->one();
            $mailer = Yii::$app->mailer;

            if ($message) {
                $message->prepareForSent();
                $message->replaceTags([
                    'order_key' => $this->order->key,
                    'order_create_date' => $this->order->create_date,
                    'order_total_cost' => $this->getTotalCost(),
                    'payment_name' => $this->getPayment()->name,
                    'transport_name' => $this->getTransport()->name,
                    'transport_cost' => $this->getTransport()->cost,
                    'full_address' => $this->getAddress()->getInReadableForm(),
                ]);

                $mailer->compose()
                    ->setFrom($mailer->getHost())
                    ->setTo($this->order->address->email)
                    ->setSubject($message->title)
                    ->setHtmlBody($message->content)
                    ->send();
            }
        }

        public function canBeCompleted()
        {
            if ($this->order->address_id && $this->getTransport() && $this->getPayment()) {
                return true;
            }

            return false;
        }

        public function complete()
        {
            $this->order->cost = $this->getTotalCost(false, false, true);
            $this->order->order_status_id = OrderStatus::STATUS_NEW_COMPLETE;
            $this->order->key = $this->order->generateKey();
            $this->order->create_date = (new \DateTime())->format("Y-m-d H:i:s");
            $this->order->modify_date = $this->order->create_date;

            /** @var Product $product */
            foreach ($this->getItems(true) as $product) {
                $product->bought_count++;
                $product->save();
            }

            Yii::$app->session->remove("basket");

            if (User::getSystemUser()) {
                return $this->order->save() && $this->order->onStatusChange(User::getSystemUser()->id);
            }

            return false;
        }

        /**
         * @param $id
         * @return BasketItem|null
         */
        protected function getItemRelatedParent($id)
        {
            $relatedParent = null;

            foreach ($this->items AS $item) {
                if ($item->getModel()->id == $id) {
                    $relatedParent = $item;

                    break;
                }
            }

            return $relatedParent;
        }
    }