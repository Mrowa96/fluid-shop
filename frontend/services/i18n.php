<?php
    namespace frontend\services;

    /**
     * Class i18n
     * @package frontend\services
     *
     * @property string $sourceMessageTable
     * @property string $module
     */
    class i18n extends \common\utilities\i18n
    {
        public $sourceMessageTable = '{{%i18n_source_frontend}}';
        public $module = 'frontend';
    }
