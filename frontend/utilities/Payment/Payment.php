<?php
    namespace frontend\utilities\Payment;

    use frontend\services\Basket;

    abstract class Payment
    {
        /** @var Basket $basket * */
        protected $basket;
        protected $order;
        protected $error;

        public function hasError()
        {
            return $this->error !== null;
        }

        public function getError()
        {
            return $this->error;
        }

        public abstract function pay(Basket $basket);

        public abstract function complete($paymentId, $payerId);

        protected abstract function setUpAuthorization();

        protected abstract function configureOrder();
    }