<?php
    namespace frontend\utilities\Payment;

    use Yii;
    use yii\helpers\Json;
    use yii\helpers\Url;
    use frontend\services\Basket;
    use PayPal\Api\Details;
    use PayPal\Api\Amount;
    use PayPal\Api\Item;
    use PayPal\Api\ItemList;
    use PayPal\Api\Payer;
    use PayPal\Api\Payment as PayPalPayment;
    use PayPal\Api\RedirectUrls;
    use PayPal\Api\Transaction;
    use PayPal\Auth\OAuthTokenCredential;
    use PayPal\Rest\ApiContext;
    use common\models\Settings;
    use common\models\Settings\System;
    use common\models\Order;
    use frontend\services\Basket\Item as BasketItem;
    use PayPal\Api\PaymentExecution;
    use PayPal\Exception\PayPalConnectionException;
    use common\models\Settings\PayPal as PayPalModel;

    class PayPal extends Payment
    {
        public function pay(Basket $basket)
        {
            $this->basket = $basket;

            try {
                $apiContext = $this->setUpAuthorization();
                $payment = $this->configureOrder();

                if ($payment->create($apiContext)) {
                    $approvalUrl = $payment->getApprovalLink();

                    return Yii::$app->response->redirect($approvalUrl);
                }
            } catch (PayPalConnectionException $ex) {
                Yii::error($ex->getData());

                $this->error = $ex;

                if (YII_ENV === 'dev') {
                    print_r(Json::decode($ex->getData()));
                    die();
                }
            }

            return false;
        }

        public function complete($paymentId, $payerId)
        {
            $apiContext = $this->setUpAuthorization();
            $payment = PayPalPayment::get($paymentId, $apiContext);
            $execution = new PaymentExecution();
            $execution->setPayerId($payerId);

            try {
                return $payment->execute($execution, $apiContext);
            } catch (\Exception $ex) {
                Yii::error($ex->getMessage());

                $this->error = $ex;

                if (YII_ENV === 'dev') {
                    print_r(Json::decode($ex->getMessage()));
                    die();
                }
            }

            return false;
        }

        protected function setUpAuthorization()
        {
            $apiContext = new ApiContext(new OAuthTokenCredential(
                Settings::getOne('client_id', PayPalModel::MODULE),
                Settings::getOne('client_secret', PayPalModel::MODULE)
            ));

            if (Settings::getOne('environment', PayPalModel::MODULE) == "sandbox") {
                $apiContext->setConfig([
                    'mode' => 'sandbox',
                    'cache.enabled' => false,
                    'log.LogEnabled' => true,
                    'log.LogLevel' => 'DEBUG',
                    'log.FileName' => Yii::getAlias("@frontend") . '/runtime/logs/payu.log',
                    'http.CURLOPT_CONNECTTIMEOUT' => 30
                ]);
            } else {
                $apiContext->setConfig([
                    'cache.enabled' => true,
                    'log.LogEnabled' => true,
                    'log.LogLevel' => 'INFO',
                    'log.FileName' => Yii::getAlias("@frontend") . '/runtime/logs/payu.log'
                ]);
            }

            return $apiContext;
        }

        protected function configureOrder()
        {
            $payer = new Payer();

            $basketItems = $this->basket->getItems(false, true, true);
            $payer->setPaymentMethod("paypal");

            $itemList = new ItemList();
            $items = [];

            foreach ($basketItems as $index => $basketItem) {
                /** @var BasketItem $basketItem */
                $item = new Item();
                $item->setName($basketItem->getName())
                    ->setCurrency(Settings::getOne('currency', System::MODULE))
                    ->setQuantity($basketItem->getQuantity())
                    ->setSku($basketItem->getCode())
                    ->setPrice($basketItem->getCost());

                $items[] = $item;
            }

            $itemList->setItems($items);

            $details = new Details();
            $details->setShipping($this->basket->getTransport()->cost)
                ->setTax(0)
                ->setSubtotal($this->basket->getTotalCost(false, false, true));

            $amount = new Amount();
            $amount->setCurrency(Settings::getOne('currency', System::MODULE))
                ->setTotal($this->basket->getTotalCost(false, true, true))
                ->setDetails($details);

            $transaction = new Transaction();
            $transaction->setAmount($amount)
                ->setItemList($itemList);

            $redirectUrls = new RedirectUrls();
            $redirectUrls->setReturnUrl(Url::toRoute(['payment/paypal-complete',
                'orderKey' => Order::encryptKey($this->basket->getOrderKey())], true))
                ->setCancelUrl(Url::toRoute(['payment/paypal-cancel',
                    'orderKey' => Order::encryptKey($this->basket->getOrderKey())], true));

            $payment = new PayPalPayment();
            $payment->setIntent("sale")
                ->setPayer($payer)
                ->setRedirectUrls($redirectUrls)
                ->setTransactions([$transaction]);

            return $payment;
        }
    }