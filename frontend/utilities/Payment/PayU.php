<?php
    namespace frontend\utilities\Payment;

    use common\models\Order;
    use common\models\OrderStatus;
    use common\models\Transport;
    use common\models\User;
    use OpenPayU_Result;
    use Yii;
    use yii\helpers\Url;
    use OpenPayU_Configuration;
    use OpenPayU_Order;
    use frontend\services\Basket;
    use common\models\Settings;
    use common\models\Settings\System;
    use frontend\services\Basket\Item as BasketItem;
    use OauthCacheFile;

    class PayU extends Payment
    {
        public function pay(Basket $basket)
        {
            $this->basket = $basket;

            try {
                $this->setUpAuthorization();
                $this->configureOrder();

                /** @var OpenPayU_Result $response */
                if ($response = OpenPayU_Order::create($this->order)) {
                    if (!$response->getError()) {
                        $redirectUrl = $response->getResponse()->redirectUri;
                        $status = $response->getResponse()->status->statusCode;

                        if ($status == "SUCCESS") {
                            return Yii::$app->response->redirect($redirectUrl);
                        }
                    }
                }
            } catch (\Exception $ex) {
                Yii::error($ex);

                $this->error = $ex;

                if (YII_ENV === 'dev') {
                    print_r($ex->getMessage());
                    die();
                }
            }

            return false;
        }

        public function complete($paymentId, $payerId)
        {
        }

        protected function configureOrder()
        {
            /* @var BasketItem[] $items */
            $items = $this->basket->getItems(false, true, true);
            $address = $this->basket->getAddress();
            /** @var Transport $transport */
            $transport = $this->basket->getTransport();

            $this->order = [
                'continueUrl' => Url::toRoute(['payment/payu-complete', 'orderKey' => Order::encryptKey($this->basket->getOrderKey())], true),
                'notifyUrl' => Url::toRoute(['payment/payu-cancel', 'orderKey' => Order::encryptKey($this->basket->getOrderKey())], true),
                'customerIp' => Yii::$app->request->getUserIP(),
                'merchantPosId' => OpenPayU_Configuration::getOauthClientId(),
                'description' => 'New order',
                'currencyCode' => Settings::getOne('currency', System::MODULE),
                'totalAmount' => $this->basket->getTotalCost() * 100,
                'extOrderId' => Order::encryptKey($this->basket->getOrderKey()),
                'buyer' => [
                    'email' => $address->email,
                    'phone' => $address->phone_no,
                    'firstName' => $address->name,
                    'lastName' => $address->surname,
                ]
            ];

            foreach ($items AS $item) {
                $this->order['products'][] = [
                    'name' => $item->getName(),
                    'unitPrice' => $item->getCost() * 100,
                    'quantity' => $item->getQuantity(),
                ];
            }

            $this->order['products'][] = [
                'name' => $transport->name,
                'unitPrice' => $transport->cost * 100,
                'quantity' => 1,
            ];

            return true;
        }

        protected function setUpAuthorization()
        {
            OpenPayU_Configuration::setEnvironment(Settings::getOne('environment', Settings\PayU::MODULE));
            OpenPayU_Configuration::setOauthClientId(Settings::getOne('oauth_client_id', Settings\PayU::MODULE));
            OpenPayU_Configuration::setOauthClientSecret(Settings::getOne('oauth_client_secret', Settings\PayU::MODULE));
            OpenPayU_Configuration::setOauthTokenCache(new OauthCacheFile(Yii::getAlias("@payuCache")));
        }
    }