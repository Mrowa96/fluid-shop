<?php
    namespace frontend\utilities\Menu;

    /**
     * Class MenuItem
     * @package frontend\utilities\Menu
     *
     * @property string $label
     * @property string $url
     * @property bool $active
     * @property MenuItem[] $children
     * @property MenuItem $parent
     */
    class MenuItem
    {
        protected $label;
        protected $url;
        protected $active;
        protected $children;
        protected $parent;

        /**
         * MenuItem constructor.
         * @param string|null $label
         * @param string|null $url
         * @param bool $active
         */
        public function __construct($label = null, $url = null, $active = false)
        {
            $this->label = $label;
            $this->url = $url;
            $this->active = $active;
        }

        /**
         * @return string
         */
        public function getLabel()
        {
            return $this->label;
        }

        /**
         * @param string $label
         * @return $this
         */
        public function setLabel($label)
        {
            $this->label = $label;

            return $this;
        }

        /**
         * @return string
         */
        public function getUrl()
        {
            return $this->url;
        }

        /**
         * @param string $url
         * @return $this
         */
        public function setUrl($url)
        {
            $this->url = $url;

            return $this;
        }

        /**
         * @return boolean
         */
        public function isActive()
        {
            return $this->active;
        }

        /**
         * @param boolean $active
         * @return $this
         */
        public function setActive($active)
        {
            $this->active = $active;

            return $this;
        }

        /**
         * @return MenuItem[]
         */
        public function getChildren()
        {
            return $this->children;
        }

        /**
         * @return bool
         */
        public function hasChildren()
        {
            return !empty($this->children);
        }

        /**
         * @param MenuItem $child
         * @return $this
         */
        public function addChild(MenuItem $child)
        {
            $this->children[] = $child;

            return $this;
        }

        /**
         * @return MenuItem
         */
        public function getParent()
        {
            return $this->parent;
        }

        /**
         * @return bool
         */
        public function hasParent()
        {
            return $this->parent instanceof MenuItem;
        }

        /**
         * @param MenuItem $parent
         * @return $this
         */
        public function setParent(MenuItem $parent)
        {
            $this->parent = $parent;

            return $this;
        }
    }