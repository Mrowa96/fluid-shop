<?php
    namespace frontend\utilities\Menu;

    use common\models\Category;
    use yii\helpers\Url;

    /**
     * Class CategoriesDataProvider
     * @package frontend\utilities\Menu
     */
    class CategoriesDataProvider extends DataProvider
    {
        const ITEMS_MAP_NAME = 'categories-map';

        /**
         * @return void
         */
        protected function prepareItems()
        {
            if (\Yii::$app->cache->exists(CategoriesDataProvider::ITEMS_MAP_NAME) && YII_ENV === 'prod') {
                $this->items = \Yii::$app->cache->get(CategoriesDataProvider::ITEMS_MAP_NAME);
            } else {
                $categories = Category::getMain();

                foreach ($categories as $category) {
                    $this->items[] = $this->addItem($category);
                }

                \Yii::$app->cache->set(CategoriesDataProvider::ITEMS_MAP_NAME, $this->items);
            }

            $this->prepared = true;
        }

        /**
         * @param Category $category
         * @param MenuItem|null $parent
         * @return MenuItem
         */
        protected function addItem(Category $category, MenuItem $parent = null)
        {
            $menuItem = new MenuItem();

            $menuItem->setLabel($category->name)->setUrl(Url::toRoute(['category/index',
                'id' => $category->id,
                'url' => $category->url
            ]));

            if (!is_null($parent)) {
                $parent->addChild($menuItem);
                $menuItem->setParent($parent);
            }

            foreach ($category->children as $child) {
                $this->addItem($child, $menuItem);
            }

            if (\Yii::$app->request->getHostInfo() . '/' . \Yii::$app->request->getPathInfo() === $menuItem->getUrl()) {
                $menuItem->setActive(true);

                $tempParent = $menuItem->getParent();

                while ($tempParent) {
                    $tempParent->setActive(true);

                    $tempParent = $tempParent->getParent();
                }
            }

            return $menuItem;
        }
    }