<?php
    namespace frontend\utilities\Menu;

    use common\models\Page;
    use yii\helpers\Url;

    /**
     * Class PagesDataProvider
     * @package frontend\utilities\Menu
     */
    class PagesDataProvider extends DataProvider
    {
        /**
         * @return void
         */
        protected function prepareItems()
        {
            $pages = Page::getForHeader();

            foreach ($pages as $page) {
                $this->items[] = $this->addItem($page);
            }

            $this->prepared = true;
        }

        /**
         * @param Page $page
         * @return MenuItem
         */
        protected function addItem(Page $page)
        {
            $menuItem = new MenuItem();

            $menuItem->setLabel($page->title)->setUrl(Url::toRoute(['site/page',
                'id' => $page->id,
                'url' => $page->url
            ]));

            if (\Yii::$app->request->getAbsoluteUrl() === $menuItem->getUrl()) {
                $menuItem->setActive(true);
            }

            return $menuItem;
        }
    }