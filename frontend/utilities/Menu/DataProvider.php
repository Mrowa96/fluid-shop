<?php
    namespace frontend\utilities\Menu;

    /**
     * Interface DataProvider
     * @package frontend\utilities\Menu
     *
     * @property MenuItem[] $items
     * @property bool $prepared
     */
    abstract class DataProvider
    {
        protected $items;
        protected $prepared;

        /**
         * DataProvider constructor.
         */
        public function __construct()
        {
            $this->items = [];
            $this->prepared = false;
        }

        /**
         * @return MenuItem[]
         */
        public function getItems()
        {
            if (!$this->prepared) {
                $this->prepareItems();
            }

            return $this->items;
        }

        /**
         * @return bool
         */
        public function hasItems()
        {
            if (!$this->prepared) {
                $this->prepareItems();
            }

            return !empty($this->items);
        }

        /**
         * @return void
         */
        abstract protected function prepareItems();
    }