<?php
    namespace frontend\utilities\Menu;

    use Yii;
    use yii\helpers\Url;

    /**
     * Class ClientDataProvider
     * @package frontend\utilities\Menu
     */
    class ClientDataProvider extends DataProvider
    {
        /**
         * @return void
         */
        protected function prepareItems()
        {
            $this->addItem(Yii::t("client", "Address"), Url::toRoute(['client/address-manage']));
            $this->addItem(Yii::t("client", "Orders"), Url::toRoute(['client/orders-list']));
            $this->addItem(Yii::t("client", "Favorites"), Url::toRoute(['client/favorites-list']));
            $this->addItem(Yii::t("client", "Rma"), Url::toRoute(['client/complaint-list']));
            $this->addItem(Yii::t("client", "Change password"), Url::toRoute(['client/change-password']));

            $this->prepared = true;
        }

        /**
         * @param string $label
         * @param string $url
         *
         * @return void
         */
        protected function addItem($label, $url)
        {
            $menuItem = new MenuItem($label, $url);

            if (Yii::$app->request->getAbsoluteUrl() === $url) {
                $menuItem->setActive(true);
            }

            $this->items[] = $menuItem;
        }
    }