<?php
    namespace frontend\utilities\Menu;

    use common\models\Category;
    use yii\helpers\Url;

    /**
     * Class CategoriesMinimalDataProvider
     * @package frontend\utilities\Menu
     */
    class CategoriesMinimalDataProvider extends DataProvider
    {
        /**
         * @return void
         */
        protected function prepareItems()
        {
            $categories = Category::getMain();

            foreach ($categories as $category) {
                $this->items[] = $this->addItem($category);
            }

            $this->prepared = true;
        }

        /**
         * @param Category $category
         * @return MenuItem
         */
        protected function addItem(Category $category)
        {
            $menuItem = new MenuItem();

            $menuItem->setLabel($category->name)->setUrl(Url::toRoute(['category/index',
                'id' => $category->id,
                'url' => $category->url
            ]));

            if (\Yii::$app->request->getAbsoluteUrl() === $menuItem->getUrl()) {
                $menuItem->setActive(true);
            }

            return $menuItem;
        }
    }