<?php
    namespace frontend\utilities\Menu;

    use common\models\Category;

    /**
     * Class MobileCategoriesDataProvider
     * @package frontend\utilities\Menu
     *
     * @property Category $currentCategory
     */
    class MobileCategoriesDataProvider extends CategoriesMinimalDataProvider
    {
        protected $currentCategory;

        public function __construct(Category $category)
        {
            parent::__construct();

            $this->currentCategory = $category;
        }

        /**
         * @return void
         */
        protected function prepareItems()
        {
            $categories = $this->currentCategory->children;

            foreach ($categories as $category) {
                $this->items[] = $this->addItem($category);
            }

            $this->prepared = true;
        }
    }