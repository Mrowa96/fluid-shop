<?php
    namespace frontend\utilities;

    use Yii;
    use backend\models\Attribute;
    use common\models\Category;
    use yii\helpers\ArrayHelper;
    use frontend\services\Layout\SidebarComponent;

    /**
     * Class ProductsFilterWidget
     * @package frontend\utilities
     *
     * @property array $options
     * @property array $defaultOptions
     * @property Category $category
     * @property array $cachedAttributes
     * @property Attribute[] $attributes
     */
    class ProductsFilter implements SidebarComponent
    {
        protected $options;
        protected $category;
        protected $defaultOptions;
        protected $cachedAttributes;
        protected $attributes = [];

        public function __construct(Category $category, array $options = [])
        {
            $this->category = $category;
            $this->options = $options;
            $this->defaultOptions = [
                'priceRange' => true,
                'priceMin' => 0,
                'priceMax' => 100000
            ];
            $this->cachedAttributes = [];
            $this->attributes = [];
        }

        public function render()
        {
            $this->prepareAttributes();

            return Yii::$app->view->render('@frontend/views/components/product-filter', [
                'attributes' => $this->attributes,
                'options' => $this->options
            ]);
        }

        /**
         * @return array
         */
        protected function getOptions()
        {
            return ArrayHelper::merge($this->defaultOptions, $this->options);
        }

        /**
         * @return void
         */
        protected function prepareAttributes()
        {
            $this->cachedAttributes = Yii::$app->cache->get(Category::CACHE_CATEGORY_ATTRIBUTES_PREFIX . $this->category->id);

            if (empty($this->cachedAttributes)) {
                $this->cachedAttributes = $this->category->createAttributeCache();
            }

            foreach ($this->cachedAttributes as $attributeId) {
                $this->attributes[$attributeId] = Attribute::find()->where(['id' => $attributeId])->one();
            }
        }
    }