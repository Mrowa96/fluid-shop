<?php
    namespace frontend\utilities;

    use frontend\services\Layout\SidebarComponent;
    use frontend\utilities\Menu\DataProvider;
    use yii\helpers\ArrayHelper;

    /**
     * Class Menu
     * @package frontend\utilities
     *
     * @property DataProvider $provider
     * @property array $options
     * @property array $defaultOptions
     */
    class Menu implements SidebarComponent
    {
        protected $provider;
        protected $options;
        protected $defaultOptions;

        /**
         * Menu constructor.
         * @param DataProvider $provider
         * @param array $options
         */
        public function __construct(DataProvider $provider, array $options = [])
        {
            $this->provider = $provider;
            $this->options = $options;
            $this->defaultOptions = [
                'addWrapper' => true,
                'wrapperClass' => 'navigation',
                'listClass' => '',
                'itemClass' => 'item',
                'activeItemClass' => 'active'
            ];
        }

        /**
         * @return string
         */
        public function render()
        {
            return \Yii::$app->getView()->render('@frontend/views/components/menu', [
                'items' => $this->provider->getItems(),
                'options' => $this->getOptions(),
                'subMenu' => false
            ]);
        }

        /**
         * @return DataProvider
         */
        public function getProvider()
        {
            return $this->provider;
        }

        /**
         * @return array
         */
        protected function getOptions()
        {
            return ArrayHelper::merge($this->defaultOptions, $this->options);
        }
    }