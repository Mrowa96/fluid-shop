<?php
    namespace frontend\utilities;

    /**
     * Class ListOrder
     * @package frontend\utilities
     */
    class ListOrder
    {
        const NAME_ASC = 1;
        const NAME_DESC = 2;
        const PRICE_ASC = 3;
        const PRICE_DESC = 4;
    }