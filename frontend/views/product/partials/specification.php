<?php
    use common\models\Product;
    use yii\helpers\Html;

    /** @var Product $product */
?>

<?php if (!empty($product) && !empty($product->productAttributes)): ?>
    <div class="specification">
        <div class="title">
            <h4><?= Yii::t("product", "Specification"); ?></h4>
        </div>
        <table class="table table-striped">
            <?php foreach ($product->productAttributes as $productAttribute): ?>
                <tr>
                    <td class="name">
                        <?= Html::decode($productAttribute->productAttribute->name); ?>
                    </td>
                    <td class="value">
                        <?= Html::decode($productAttribute->option->name); ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        </table>
    </div>
<?php endif; ?>
