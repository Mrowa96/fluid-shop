<?php
    use common\models\Product;

    /** @var Product $product */
?>

<?php if (!empty($product)): ?>
    <div class="description-block">
        <div class="title">
            <h4><?= Yii::t("product", "Description"); ?></h4>
        </div>
        <div class="content">
            <?= $product->description; ?>
        </div>
    </div>
<?php endif; ?>
