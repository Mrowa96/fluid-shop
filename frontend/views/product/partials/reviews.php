<?php
    use common\models\Product;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;

    /** @var Product $product */
?>

<?php if (!empty($product)): ?>
    <div class="reviews-block">
        <div class="title">
            <h4><?= Yii::t("product", "Reviews") . " (" . count($product->reviews) . ")"; ?></h4>
        </div>

        <?php if (!empty($product->reviews)): ?>
            <div class="reviews-wrap">
                <ul class="reviews">
                    <?php $index = 0; ?>
                    <?php foreach ($product->reviews as $review): ?>
                        <?php if ($review->accepted): ?>
                            <li <?php if ($index >= 2): ?> class="<?= "hidden"; ?>" <?php endif; ?>>
                                <div class="rating">
                                    <?php for ($i = 0; $i < $review->rating; $i++): ?>
                                        <i class="fa fa-star"></i>
                                    <?php endfor; ?>
                                    <?php for (; $i < $review::MAX_RATING; $i++): ?>
                                        <i class="fa fa-star-o"></i>
                                    <?php endfor; ?>
                                </div>
                                <div class="review">
                                    <?= Html::decode($review->text); ?>
                                </div>
                                <div class="information">
                                    <div class="author">
                                        <?= Html::decode($review->user->name); ?>
                                    </div>
                                    <div class="date">
                                        <?= Html::decode($review->user->created_at); ?>
                                    </div>
                                </div>
                            </li>

                            <?php $index++; ?>
                        <?php endif; ?>
                    <?php endforeach; ?>
                </ul>

                <?php if (count($product->reviews) > 2): ?>
                    <div class="buttons-block">
                        <a href="#" class="btn btn-load-more"><?= Yii::t("product-rating", "Show more"); ?></a>
                    </div>
                <?php endif; ?>
            </div>
        <?php endif; ?>

        <?php if (!Yii::$app->user->isGuest && !empty($reviewForm)): ?>
            <div class="rating-wrap">
                <?php $form = ActiveForm::begin(['action' => Url::toRoute('product/save-review', true)]); ?>
                    <?= $form->field($reviewForm, 'product_id')->input('hidden', ['value' => $product->id])->label(false); ?>
                    <?= $form->field($reviewForm, 'rating')->input("number", ['min' => 1, 'max' => 5]); ?>
                    <?= $form->field($reviewForm, 'text')->textarea(); ?>

                    <div class="buttons-block">
                        <?= Html::submitButton(Yii::t("system", "Send"), ['class' => 'btn btn-add-review']) ?>
                    </div>
                <?php ActiveForm::end(); ?>
            </div>
        <?php else: ?>
            <div class="rating-message">
                <span class="message">
                    <?= Yii::t("product-rating", "You must be logged in to write a review."); ?>
                </span>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>
