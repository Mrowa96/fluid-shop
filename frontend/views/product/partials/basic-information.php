<?php
    use common\models\Product;
    use yii\helpers\Html;
    use yii\helpers\Url;

    /**
     * @var Product $product
     * @var bool $isSpecial
     */
?>
<?php if (!empty($product)): ?>
    <div class="basic-information-block">
        <div class="title">
            <h4><?= Yii::t("product", "Information"); ?></h4>
        </div>
        <div class="information-block">
            <div class="name">
                <?= Html::decode($product->name); ?>
            </div>
            <div class="price">
                <?= Html::decode($product->getCost(true, true)); ?>
            </div>
        </div>
        <div class="buttons-block">
            <?php if ($isSpecial === false): ?>
                <a href="<?= Url::toRoute(['product/add-to-favorites', 'id' => $product->id]); ?>" class="btn btn-add-to-favorites">
                    <?= Yii::t("system", "Add to favorites"); ?>
                </a>
                <a href="<?= Url::toRoute(['basket/add-product', 'id' => $product->id]); ?>" class="btn btn-buy">
                    <?= Yii::t("system", "Buy"); ?>
                </a>
            <?php else: ?>
                <?php if (!empty($productOfADay)): ?>
                    <a href="<?= Url::toRoute(['basket/add-product-special', 'id' => $productOfADay->id]); ?>" class="btn btn-buy">
                        <?= Yii::t("system", "Buy"); ?>
                    </a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </div>
<?php endif; ?>
