<?php
    use common\models\Product;
    use frontend\forms\BuyProduct;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;

    /**
     * @var Product $product
     * @var BuyProduct $model
     */
?>

<?php if (!empty($product)): ?>
    <?php $form = ActiveForm::begin([
        'id' => 'buy-form',
        'action' => Url::toRoute(['basket/add-product']),
        'options' => ['class' => 'hidden']
    ]); ?>
        <?= $form->field($model, 'id')->input('hidden', ['value' => $product->id]) ?>
        <?= $form->field($model, 'quantity')->input('hidden', ['value' => 1]) ?>

    <?php ActiveForm::end(); ?>
<?php endif; ?>