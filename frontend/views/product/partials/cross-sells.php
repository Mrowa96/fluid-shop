<?php
    use common\models\Product;
    use frontend\components\GroupedProductsWidget;
    use frontend\components\ProductWidget;
    use yii\helpers\Html;
    use yii\helpers\Url;

    /** @var Product $product */
?>

<?php if (!empty($product) && !empty($product->crossSell)): ?>
    <div class="cross-sells-block">
        <div class="title">
            <h4><?= Yii::t("product", "Cross sells"); ?></h4>
        </div>

        <?= GroupedProductsWidget::widget([
            'products' => $product->crossSell,
            'type' => ProductWidget::TYPE_CROSS_SELL
        ]); ?>

        <div class="buttons-block">
            <a href="<?= Url::toRoute(['basket/add-product', 'id' => $product->id]); ?>" class="btn btn-buy">
                <?= Yii::t("system", "Buy"); ?>
            </a>
        </div>
    </div>
<?php endif; ?>
