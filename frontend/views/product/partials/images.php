<?php
    /** @var \common\models\Product $product */
?>
<?php if (!empty($product) && !empty($product->images)): ?>
    <div class="images-block">
        <div class="swiper-container swiper-slider">
            <ul class="swiper-wrapper">
                <?php foreach ($product->images as $image): ?>
                    <li class="swiper-slide">
                        <img src="<?= $image->path; ?>" alt="<?= $image->name; ?>">
                    </li>
                <?php endforeach; ?>
            </ul>

            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next"></div>
        </div>
        <?php if (count($product->images) > 1): ?>
            <div class="swiper-container swiper-thumbs">
                <ul class="swiper-wrapper">
                    <?php foreach ($product->images as $image): ?>
                        <li class="swiper-slide">
                            <img src="<?= $image->path; ?>" alt="<?= $image->name; ?>">
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>