<?php
    use common\models\Product;
    use yii\helpers\Html;
    use yii\helpers\Url;

    /** @var Product $product */
?>

<?php if (!empty($product) && !empty($product->upSell)): ?>
    <div class="up-sells-block">
        <div class="title">
            <h4><?= Yii::t("product", "Up sells"); ?></h4>
        </div>
        <ul class="up-sells">
            <?php foreach ($product->upSell as $upSell): ?>
                <li>
                    <div class="form-group">
                        <label for="up-sell-<?= $upSell->id; ?>">
                            <input type="checkbox" name="upSells[<?= $upSell->id; ?>]" data-id="<?= $upSell->id; ?>" id="up-sell-<?= $upSell->id; ?>">
                            <?= Html::decode($upSell->product->name); ?>
                        </label>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
        <div class="buttons-block">
            <a href="<?= Url::toRoute(['basket/add-product', 'id' => $product->id]); ?>" class="btn btn-buy">
                <?= Yii::t("system", "Buy"); ?>
            </a>
        </div>
    </div>
<?php endif; ?>
