<?php
    use common\models\Product;
    use common\models\ProductOfADay;
    use frontend\forms\BuyProduct;
    use frontend\forms\ProductReview;
    use yii\helpers\Url;

    /**
     * @var Product $product
     * @var ProductOfADay $productOfADay
     * @var BuyProduct $buyForm
     * @var bool $isSpecial
     */
?>
<div class="product-detail-special">
    <?= $this->render("@frontend/views/product/partials/images", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/basic-information", ['product' => $product, 'isSpecial' => $isSpecial, 'productOfADay' => $productOfADay]); ?>
    <?= $this->render("@frontend/views/product/partials/description", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/specification", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/hidden-form", ['product' => $product, 'model' => $buyForm]); ?>
</div>