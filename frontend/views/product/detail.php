<?php
    use common\models\Product;
    use frontend\forms\BuyProduct;
    use frontend\forms\ProductReview;
    use yii\helpers\Url;

    /**
     * @var Product $product
     * @var ProductReview | null $reviewForm
     * @var bool $disableCrossSell
     * @var BuyProduct $buyForm
     * @var bool $isSpecial
     */
?>
<div class="product-detail">
    <?= $this->render("@frontend/views/product/partials/images", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/basic-information", ['product' => $product, 'isSpecial' => $isSpecial]); ?>
    <?= $this->render("@frontend/views/product/partials/description", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/specification", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/up-sells", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/cross-sells", ['product' => $product]); ?>
    <?= $this->render("@frontend/views/product/partials/reviews", ['product' => $product, 'reviewForm' => $reviewForm]); ?>
    <?= $this->render("@frontend/views/product/partials/hidden-form", ['product' => $product, 'model' => $buyForm]); ?>
</div>