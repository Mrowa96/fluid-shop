<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="client-change-password">
    <?php $form = ActiveForm::begin(); ?>
    <?= $form->field($model, 'currentPassword')->passwordInput(['autofocus' => true])->label(Yii::t("client-change-password", "Current password")) ?>
    <?= $form->field($model, 'newPassword')->passwordInput()->label(Yii::t("client-change-password", "New password")) ?>
    <?= $form->field($model, 'repeatNewPassword')->passwordInput()->label(Yii::t("client-change-password", "Repeat new password")) ?>

    <div class="buttons-block">
        <?= Html::submitButton(Yii::t("system", "Update"), ['class' => 'btn btn-success']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>