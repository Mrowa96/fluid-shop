<?php
    use yii\data\ActiveDataProvider;
    use yii\grid\GridView;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $orders
     */
?>

<div class="client-orders-list">
    <?= GridView::widget([
        'dataProvider' => $orders,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t("client-order", "Order id"),
                'attribute' => 'key'
            ],
            [
                'label' => Yii::t("client-order", "Order total cost"),
                'attribute' => 'full_cost'
            ],
            [
                'label' => Yii::t("client-order", "Create date"),
                'attribute' => 'create_date'
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<a href="' . Url::toRoute(['client/order-details', 'id' => $model->id]) . '" title="' . Yii::t("system", "Details") . '" class="btn btn-info">' . Yii::t("system", "Details") . '</a>';
                    },
                ]
            ]
        ],
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>
