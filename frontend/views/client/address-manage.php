<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="client-address-manage">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true, 'autofocus' => true])->label(Yii::t("client-address", "Name")) ?>
        <?= $form->field($model, 'surname')->textInput(['maxlength' => true])->label(Yii::t("client-address", "Surname")) ?>
        <?= $form->field($model, 'email')->textInput(['maxlength' => true])->label(Yii::t("client-address", "Email")) ?>
        <?= $form->field($model, 'phone_no')->textInput(['maxlength' => true])->label(Yii::t("client-address", "Phone number")) ?>
        <?= $form->field($model, 'street')->textInput(['maxlength' => true])->label(Yii::t("client-address", "Street")) ?>
        <?= $form->field($model, 'building_no')->textInput(['maxlength' => true])->label(Yii::t("client-address", "House number")) ?>
        <?= $form->field($model, 'zip')->textInput(['maxlength' => true])->label(Yii::t("client-address", "Zip code")) ?>
        <?= $form->field($model, 'city')->textInput(['maxlength' => true])->label(Yii::t("client-address", "City")) ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("system", "Update"), ['class' => 'btn']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>