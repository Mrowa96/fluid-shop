<?php
    use common\models\User;
    use yii\helpers\Html;
    use yii\helpers\Url;

    /** @var User $client */
?>
<div class="client-index">
    <h2 class="first-line"><?= Yii::t("client", "Hello, {clientName}", ['clientName' => Html::encode($client->name)]); ?></h2>

    <p class="text">
        <?= Yii::t("client", "You are in My account section."); ?>
    </p>
    <p class="text">
        <?= Yii::t("client", "Here, you can change yours address details, list orders and favorites products and make a complaint."); ?>
    </p>
</div>