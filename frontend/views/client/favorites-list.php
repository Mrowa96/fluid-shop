<?php
    use yii\data\ActiveDataProvider;
    use yii\grid\GridView;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     */
?>

<div class="client-favorites-list">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t("client-favorite", "Product name"),
                'attribute' => 'product.name',
            ],
            [
                'label' => Yii::t("client-favorite", "Add date"),
                'attribute' => 'created_date',
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view}',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<a href="' . Url::toRoute(['product/detail', 'id' => $model->product->id, 'url' => $model->product->processed_name]) . '" class="btn btn-info">' . Yii::t("system", "Details") . '</a>';
                    },
                ]
            ]
        ],
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>