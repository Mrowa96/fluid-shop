<?php
    use yii\data\ActiveDataProvider;
    use yii\grid\GridView;

    /**
     * @var ActiveDataProvider $dataProvider
     */
?>
<div class="client-complaint-list">
    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            [
                'label' => Yii::t("client-rma", "Order id"),
                'attribute' => 'order.key',
            ],
            [
                'label' => Yii::t("client-rma", "Product name"),
                'attribute' => 'product.name',
            ],
            [
                'label' => Yii::t("client-rma", "Complaint status"),
                'attribute' => 'status_text',
            ],
        ],
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>