<?php
    use common\models\Order;
    use common\models\OrderDetail;
    use yii\grid\GridView;
    use yii\helpers\Url;
    use yii\widgets\DetailView;

    /**
     * @var OrderDetail[] $orderDetails
     * @var Order $order
     */
?>

<div class="client-order-details">
    <?= DetailView::widget([
        'model' => $order,
        'options' => [
            'class' => 'table table-bordered'
        ],
        'attributes' => [
            [
                'label' => Yii::t("client-order", "Order id"),
                'attribute' => 'key'
            ],
            [
                'label' => Yii::t("client-order", "Order total cost"),
                'attribute' => 'cost'
            ],
            [
                'label' => Yii::t("client-order", "Transport cost"),
                'attribute' => 'transport.cost'
            ],
            [
                'label' => Yii::t("client-order", "Status"),
                'attribute' => 'orderStatus.status_name',
            ],
            [
                'label' => Yii::t("client-order", "Create date"),
                'attribute' => 'create_date'
            ],
            [
                'label' => Yii::t("client-order", "Payment method"),
                'attribute' => 'payment.name',
            ],
            [
                'label' => Yii::t("client-order", "Transport method"),
                'attribute' => 'transport.name',
            ],
            [
                'label' => Yii::t("client-order", "Comment"),
                'attribute' => 'note',
            ],
        ],
    ]); ?>
    <?= GridView::widget([
        'dataProvider' => $orderDetails,
        'tableOptions' => [
            'class' => 'table table-bordered'
        ],
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            [
                'label' => Yii::t("client-order", "Product name"),
                'value' => function ($model) {
                    /** @var OrderDetail $model */
                    return $model->product->name;
                }
            ],

            [
                'label' => Yii::t("client-order", "Product price"),
                'value' => function ($model) {
                    /** @var OrderDetail $model */
                    return $model->product_cost;
                }
            ],
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{complaint}',
                'buttons' => [
                    'complaint' => function ($order, $model) use ($order) {
                        /**
                         * @var OrderDetail $model
                         * @var Order $order
                         */
                        if ($order->productNotComplaint($model->product)) {
                            return '<a href="' . Url::toRoute(['client/complaint-create', 'productId' => $model->product->id, 'orderId' => $order->id]) . '" title="' . Yii::t("client-rma", "Make a complaint") . '" class="btn btn-info">' . Yii::t("client-rma", "Make a complaint") . '</a>';
                        }
                    }
                ]
            ]
        ],
        'layout' => "{items}\n{pager}"
    ]); ?>
</div>