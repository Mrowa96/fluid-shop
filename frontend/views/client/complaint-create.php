<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
     * @var \common\models\Product $product
     */
?>
<div class="client-complaint-create">

    <?php $form = ActiveForm::begin(); ?>
        <div class="form-group">
            <span class="product-name">
                <?= $product->name;?>
            </span>
        </div>
        <?= $form->field($model, 'text')->textarea(['autofocus' => true])->label(Yii::t("client-rma", "Description of the problem")); ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t('client-rma', 'Make a complaint'), ['class' => 'btn btn-success']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>