<?php
    use frontend\components\BreadcrumbsWidget;
    use frontend\components\FooterWidget;
    use frontend\components\CookieLawWidget;
    use frontend\components\HeaderWidget;
    use frontend\components\SidebarWidget;
    use yii\helpers\Html;
    use frontend\assets\AppAsset;
    use common\components\AlertWidget;

    /** @var \yii\web\View $this */

    AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
            <meta name="theme-color" content="#eee">
            <?= Html::csrfMetaTags() ?>
            <?php $this->head() ?>

            <title><?= Html::encode($this->title) ?></title>
        </head>
        <body>
            <?php $this->beginBody() ?>
                <noscript>
                    <div id="no-script-wrapper">
                        <h4>
                            <?= Yii::t("site", "Application cannot be used without JavaScript. Please update your browser or turn on JavaScript."); ?>
                        </h4>
                    </div>
                </noscript>

                <div id="wrapper">
                    <?= HeaderWidget::widget(); ?>

                    <section id="main-content" class="<?= (Yii::$app->user->can("admin")) ? "has-manage-bar " : ""; ?><?= Yii::$app->layoutService->getContentContainer() ? "add-container " : ""; ?><?= Yii::$app->layoutService->hasSidebar() ? (Yii::$app->layoutService->getSidebar()->getDisplay() ? "has-sidebar " : "") : ""; ?>">
                        <?= BreadcrumbsWidget::widget();?>
                        <?= SidebarWidget::widget(); ?>

                        <?php if (isset($content)): ?>
                            <div id="inner-content">
                                <?= $content; ?>
                            </div>
                        <?php endif; ?>

                        <?= AlertWidget::widget();?>
                    </section>
                </div>

                <?= FooterWidget::widget();?>
                <?= CookieLawWidget::widget(); ?>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>
