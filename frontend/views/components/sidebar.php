<?php
    use frontend\services\Layout\SidebarComponent;

    /**
     * @var bool $hidden
     * @var SidebarComponent[] $components
     */
?>

<?php if (!$hidden): ?>
    <aside id="main-sidebar">
        <?php if (!empty($components)): ?>
            <?php foreach ($components as $component): ?>
                <?= $component->render(); ?>
            <?php endforeach; ?>
        <?php endif; ?>
    </aside>
<?php endif; ?>
