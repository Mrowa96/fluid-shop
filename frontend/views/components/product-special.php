<?php
    use common\models\ProductOfADay;
    use yii\helpers\Url;

    /** @var ProductOfADay $product */
?>
<div class="text-block">
    <?= Yii::t('site', 'Product of a day'); ?>
</div>
<div class="product product-standard product-special">
    <div class="image-block">
        <a href="<?= Url::toRoute(['product/detail', 'url' => $product->product->processed_name, 'id' => $product->product->id]); ?>">
            <img src="<?= $product->product->mainImage->path; ?>" alt="<?= $product->product->name; ?>" class="img-responsive">
        </a>
    </div>
    <div class="information-block">
        <div class="name">
            <?= $product->product->name; ?>
        </div>
        <div class="price">
            <?= $product->getCost(true); ?>
        </div>
        <div class="availability">
            <?= Yii::t("site", "Offer expire in"); ?> <span class="date"><?= $product->expire_date; ?></span>
        </div>
    </div>
    <div class="buttons-layer">
        <div class="buttons-block">
            <a href="<?= Url::toRoute(['product/detail-special', 'id' => $product->id]); ?>" class="btn-details"></a>
            <a href="<?= Url::toRoute(['basket/add-product-special', 'id' => $product->id]); ?>"
               class="btn-buy-product"></a>
        </div>
    </div>
</div>