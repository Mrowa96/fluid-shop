<?php
    use frontend\utilities\Menu\MenuItem;
    use yii\helpers\ArrayHelper;
    use yii\web\View;

    /**
     * @var MenuItem[] $items
     * @var array $options
     * @var View $this
     */
?>
<?php if (!empty($items)): ?>
    <?php if ($options['addWrapper']): ?>
        <div class="<?= $options['wrapperClass']; ?>">
    <?php endif; ?>
            <ul class="<?= $options['listClass']; ?>">
                <?php foreach ($items as $item): ?>
                    <li class="<?= $options['itemClass']; ?> <?= $item->isActive() ? $options['activeItemClass'] : ""; ?>">
                        <a href="<?= $item->getUrl(); ?>">
                            <?= $item->getLabel(); ?>
                        </a>

                        <?php if ($item->hasChildren()): ?>
                            <?= $this->render('@frontend/views/components/menu', [
                                'items' => $item->getChildren(),
                                'options' => ArrayHelper::merge($options, ['addWrapper' => false])
                            ]); ?>
                        <?php endif; ?>
                    </li>
                <?php endforeach; ?>
            </ul>
    <?php if ($options['addWrapper']): ?>
        </div>
    <?php endif; ?>
<?php endif; ?>
