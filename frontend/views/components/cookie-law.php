<?php
    use yii\helpers\Html;

    /**
     * @var string $text
     */
?>

<div id="cookie-law-bar" class="hidden">
    <div class="container">
        <div class="text-wrapper">
        <span class="text">
            <?= Html::encode($text); ?>
        </span>
        </div>
        <div class="button-wrapper">
            <a href="#" class="btn btn-success">
                <?= Yii::t("site", "I agree"); ?>
            </a>
        </div>
    </div>
</div>
