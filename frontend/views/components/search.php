<?php
    use yii\helpers\Url;

    /**
     * @var string $phrase
     */
?>
<div <?= (isset($id)) ? 'id="' . $id . '"' : '' ?>>
    <form action="<?= Url::toRoute(['search/by-phrase']); ?>" method="POST">
        <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfTokenFromHeader(); ?>">
        <input type="text" name="phrase" class="search-input" value="<?= $phrase; ?>" placeholder="<?= Yii::t("site", "Search"); ?>">

        <button type="submit" class="submit-btn"></button>
    </form>
</div>
