<?php
    use common\models\Product;
    use frontend\components\ProductWidget;

    /**
     * @var Product[] $products
     * @var string $title
     * @var string $type
     */
?>
<div class="grouped-products <?= $type; ?>-products">
    <?php if (!empty($title)): ?>
        <div class="title">
            <h4><?= $title ?></h4>
        </div>
    <?php endif; ?>

    <?php if (!empty($products)): ?>
        <ul class="products">
            <?php foreach ($products as $product): ?>
                <li>
                    <?= ProductWidget::widget(['product' => $product, 'type' => $type]); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p class="no-products">
            <?= Yii::t("site", "No products found."); ?>
        </p>
    <?php endif; ?>
</div>


