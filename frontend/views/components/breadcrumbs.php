<?php
    use frontend\services\Layout\Breadcrumbs as BreadcrumbsService;
    use yii\helpers\Url;
    use yii\widgets\Breadcrumbs;

    /**
     * @var BreadcrumbsService $breadcrumbs
     */
?>
<?php if($breadcrumbs): ?>
    <div id="breadcrumbs">
        <?= Breadcrumbs::widget([
            'links' => $breadcrumbs->getData(),
            'homeLink' => [
                'label' => '',
                'template' => '<li class="home"><a href="' . Url::base() . '"></a></li>'
            ]
        ]) ?>
    </div>
<?php endif; ?>