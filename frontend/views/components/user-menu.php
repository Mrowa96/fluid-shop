<?php
    use yii\helpers\Url;

    /** @var string|null $id */
?>
<div class="user-block" <?= (isset($id)) ? 'id="' . $id . '"' : '' ?>>
    <ul>
        <?php if (Yii::$app->user->isGuest): ?>
            <li>
                <a href="<?= Url::toRoute(['site/login']); ?>" class="login-btn"><?= Yii::t('site', 'Login'); ?></a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['site/signup']); ?>"
                   class="register-btn"><?= Yii::t('site', 'Sign up'); ?></a>
            </li>
        <?php else: ?>
            <li>
                <a href="<?= Url::toRoute(['client/index']); ?>"
                   class="my-account-btn"><?= Yii::t('site', 'My account'); ?></a>
            </li>
            <li>
                <a href="<?= Url::toRoute(['site/logout']); ?>" class="logout-btn"><?= Yii::t('site', 'Logout'); ?></a>
            </li>
        <?php endif; ?>
    </ul>
</div>