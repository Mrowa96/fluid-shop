<?php
    use common\models\Product;
    use common\models\ProductOfADay;
use frontend\components\ProductSpecialWidget;
use frontend\components\ProductWidget;

    /**
     * @var Product[] $products
     * @var ProductOfADay $specialProduct
     * @var string $title
     * @var string $type
     */

    $firstProducts = $products;
    $secondProducts = $products;
?>
<div class="grouped-products grouped-products-with-special <?= $type; ?>-products">
    <div class="title">
        <h4><?= $title ?></h4>
    </div>

    <?php if (!empty($products)): ?>
        <ul class="products">
            <?php foreach (array_splice($firstProducts,0,2) as $index => $product): ?>
                <li>
                    <?= ProductWidget::widget(['product' => $product]); ?>
                </li>
            <?php endforeach; ?>

            <?php if($specialProduct): ?>
                <li class="product-special-wrap">
                    <?= ProductSpecialWidget::widget(['product' => $specialProduct]); ?>
                </li>
            <?php endif; ?>

            <?php foreach (array_splice($secondProducts,2,2) as $index => $product): ?>
                <li>
                    <?= ProductWidget::widget(['product' => $product]); ?>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <p>
            <?= Yii::t("site", "No products found."); ?>
        </p>
    <?php endif; ?>
</div>


