<?php
    use frontend\utilities\Menu;

    /** @var Menu $menu */
?>

<div id="category-list-dropdown" class="invisible">
    <?php if ($menu->getProvider()->hasItems()): ?>
        <?= $menu->render(); ?>
    <?php else: ?>
        <span class="empty-block">
            <?= Yii::t("site", "Categories list is empty."); ?>
        </span>
    <?php endif; ?>
</div>
