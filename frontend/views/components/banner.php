<?php
    use common\models\Banner;

    /**
     * @var Banner[] $banners
     * @var bool $pagination
     * @var bool $buttons
     * @var bool $scrollbar
     */
?>

<?php if (!empty($banners)): ?>
    <div class="banner-widget banner-widget-<?= $type; ?>" data-pagination="<?= $pagination; ?>" data-buttons="<?= $buttons; ?>" data-scrollbar="<?= $scrollbar; ?>">
        <?php if (count($banners) === 1 && !empty($banners[0])): ?>
            <?php $banner = $banners[0]; ?>

            <div data-display-on-desktop="<?= ($banner->display_on_desktop) ? "true" : "false"; ?>" data-display-on-mobile="<?= ($banner->display_on_mobile) ? "true" : "false"; ?>">
                <a href=<?= $banner->link; ?>>
                    <img src="<?= $banner->path; ?>" alt="<?= $banner->name; ?>">
                </a>
            </div>
        <?php else: ?>
            <div class="swiper-container">
                <ul class="swiper-wrapper">
                    <?php foreach ($banners as $banner): ?>
                        <li class="swiper-slide" data-display-on-desktop="<?= ($banner->display_on_desktop) ? "true" : "false"; ?>" data-display-on-mobile="<?= ($banner->display_on_mobile) ? "true" : "false"; ?>">
                            <a href=<?= $banner->link; ?>>
                                <img src="<?= $banner->path; ?>" alt="<?= $banner->name; ?>">
                            </a>
                        </li>
                    <?php endforeach; ?>
                </ul>

                <?php if ($pagination === true): ?>
                    <div class="swiper-pagination"></div>
                <?php endif; ?>


                <?php if ($buttons === true): ?>
                    <div class="swiper-button-prev"></div>
                    <div class="swiper-button-next"></div>
                <?php endif; ?>

                <?php if ($scrollbar === true): ?>
                    <div class="swiper-scrollbar"></div>
                <?php endif; ?>
            </div>
        <?php endif; ?>
    </div>
<?php endif; ?>

