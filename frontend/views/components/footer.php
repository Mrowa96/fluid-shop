<?php
    use common\models\Page;
    use common\models\PageCategory;
    use frontend\components\UserMenuWidget;
    use yii\helpers\Url;

    /**
     * @var PageCategory[] $groupedPages
     * @var Page $page
     */
?>
<footer id="main-footer">
    <div class="navigation-block">
        <div class="logo-block">
            <a href="<?= Url::base(); ?>" title="Fluid Shop">
                <i class="fa fa-shopping-bag icon" aria-hidden="true"></i>
                <span class="text">Fluid Shop</span>
            </a>
        </div>
        <div class="pages-list-block">
            <?php if (!empty($groupedPages)): ?>
                <?php foreach ($groupedPages as $pageCategory): ?>
                    <ul>
                        <li class="title"><?= $pageCategory->name; ?></li>

                        <?php foreach ($pageCategory->pages as $page): ?>
                            <li>
                                <a href="<?= \yii\helpers\Url::toRoute(['site/page', 'id' => $page->id, 'url' => $page->url]); ?>">
                                    <?= $page->title; ?>
                                </a>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                <?php endforeach; ?>
            <?php endif; ?>
        </div>

        <?= UserMenuWidget::widget(); ?>
    </div>
    <div class="copyright-block">
        <p>Based on <a href="http://fluid-shop.pl/">Fluid Shop &copy;</a></p>
        <p>Copyright &copy; 2017 <a href="http://tavoite.pl/">Tavoite</a>. All rights reserved.</p>
    </div>
</footer>

