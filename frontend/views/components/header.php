<?php
    use common\models\Page;
    use frontend\components\BasketDropdownWidget;
    use frontend\components\FavoritesDropdownWidget;
    use frontend\components\CategoriesDropdownWidget;
    use frontend\components\ManageBarWidget;
    use common\models\Settings;
    use common\models\Settings\System;
    use frontend\components\NavigationMobileDropdownWidget;
    use frontend\components\SearchWidget;
    use frontend\components\UserMenuWidget;
    use frontend\services\Basket;
    use frontend\utilities\Menu;
    use frontend\utilities\Menu\PagesDataProvider;
    use yii\helpers\Url;

    /**
     * @var Basket $basket
     * @var string $currentController
     * @var Page $page
     */
?>

<header id="main-header">
    <?= ManageBarWidget::widget(); ?>

    <div class="header-block">
        <div class="logo">
            <a href="<?= Url::base(); ?>" title="Fluid Shop">
                <i class="fa fa-shopping-bag icon" aria-hidden="true"></i>
                <span class="text">Fluid Shop</span>
            </a>
        </div>

        <div class="mobile-navigation-button-wrapper">
            <a href="#" class="mobile-navigation-button">
                <i class="fa fa-bars icon" aria-hidden="true"></i>
            </a>
        </div>

        <div id="navigation-top-desktop">
            <ul>
                <li class="categories-btn">
                    <a href="#" title="<?= Yii::t('site', 'Catalog'); ?>">
                        <span class="text"><?= Yii::t('site', 'Catalog'); ?></span>
                    </a>
                </li>

                <?= (new Menu(new PagesDataProvider(), ['addWrapper' => false]))->render(); ?>
            </ul>
        </div>

        <div class="buttons-block">
            <a href="#" class="search-btn" title="<?= Yii::t('site', 'Search'); ?>"></a>
            <a href="#" class="user-menu-btn" title="<?= Yii::t('site', 'User menu'); ?>"></a>
            <a href="#" class="favorites-btn" title="<?= Yii::t('site', 'Favorites'); ?>"></a>

            <div class="basket-block">
                <a href="<?= Url::to(['basket/index']); ?>" class="basket-link-btn"
                   title="<?= Yii::t('site', 'Basket link'); ?>"></a>

                <?php if ($basket instanceof Basket): ?>
                    <span class="total-cost"><?= $basket->getTotalCost(true, false, true); ?></span>
                <?php else: ?>
                    <span class="total-cost">0<?= Settings::getOne('currency', System::MODULE); ?></span>
                <?php endif; ?>

                <?php if ($currentController !== "basket"): ?>
                    <a href="#" class="basket-btn" title="<?= Yii::t('site', 'Basket Dropdown'); ?>"
                       aria-hidden="true"></a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?= SearchWidget::widget(['id' => 'search-bar-dropdown']); ?>
    <?= UserMenuWidget::widget(['id' => 'user-menu-dropdown']); ?>

    <?= FavoritesDropdownWidget::widget(); ?>
    <?= CategoriesDropdownWidget::widget(); ?>
    <?= BasketDropdownWidget::widget(); ?>
    <?= NavigationMobileDropdownWidget::widget(); ?>
</header>
