<?php
    use backend\models\Attribute;

    /**
     * @var Attribute[] $attributes
     * @var array $options
     */
?>

<div class="filter-sidebar">
    <form method="GET" id="filterForm">
        <?php if (!empty($options['priceRange']) && $options['priceRange'] === true): ?>
            <div class="price-range filter">
                <div class="header">
                    <span class="name">Cena</span>
                </div>
                <div class="min">
                    <label for="priceMin">od</label>
                    <input name="priceMin" id="priceMin" class="input"
                           value="<?= Yii::$app->request->get('priceMin', $options['priceMin']); ?>">
                </div>

                <div class="max">
                    <label for="priceMax">do</label>
                    <input name="priceMax" id="priceMax" class="input"
                           value="<?= Yii::$app->request->get('priceMax', $options['priceMax']); ?>">
                </div>
            </div>
            <?php endif; ?>
            <?php foreach ($attributes as $attribute): ?>
                <div class="filter">
                    <div class="header">
                        <span class="name"><?= $attribute->name; ?></span>
                    </div>

                    <ul class="options">
                        <?php foreach ($attribute->options as $option): ?>
                            <?php
                                $name = $attribute->id . '-' . $option->id;
                                $value = '';

                                if (\Yii::$app->request->get('filter')) {
                                    $value = array_key_exists($name, \Yii::$app->request->get('filter')) ? "checked" : '';
                                }
                            ?>

                            <li>
                                <div class="checkbox-wrap">
                                    <label class="checkbox-label">
                                        <input type="checkbox" name="filter[<?= $name; ?>]" <?= $value; ?>>
                                        <div class="checkbox-box"></div>
                                    </label>

                                    <span class="option-name"><?= $option->name; ?></span>
                                </div>
                            </li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endforeach; ?>

        <input type="hidden" name="per-page" value="<?= Yii::$app->request->get('per-page'); ?>">
        <input type="hidden" name="page" value="<?= Yii::$app->request->get('page'); ?>">
        <input type="hidden" name="order-by" value="<?= Yii::$app->request->get('order-by'); ?>">
    </form>
</div>
