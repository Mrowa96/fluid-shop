<?php
    use frontend\services\ManageBar\Action;

    /**
     * @var bool $displayPanelUrl
     * @var string $panelUrl
     * @var Action[] $actions
     */
?>
<div id="manage-bar">
    <?php if ($displayPanelUrl === true): ?>
        <a href="<?= $panelUrl; ?>" class="panel-btn">
            <?= Yii::t("site", "Management panel"); ?>
        </a>
    <?php endif; ?>

    <?php if (!empty($actions)): ?>
        <?php foreach ($actions as $action): ?>
            <a href="<?= $action->getUrl(); ?>" class="<?= $action->getParsedName(); ?>-btn">
                <?= $action->getName(); ?>
            </a>
        <?php endforeach; ?>
    <?php endif; ?>
</div>
