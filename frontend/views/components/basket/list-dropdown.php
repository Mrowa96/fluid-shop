<?php
    use frontend\services\Basket;
    use frontend\services\Basket\Item as BasketItem;

    /**
     * @var Basket $basket
     * @var BasketItem $item
     * \yii\web\View $this
     */
?>

<div id="basket-list-dropdown" class="invisible">
    <?php if ($basket && $basket->notEmpty()): ?>
        <ul>
            <?php foreach ($basket->getItems() as $item): ?>
                <?= $this->render("@frontend/views/components/basket/item", ['item' => $item]); ?>
            <?php endforeach; ?>
        </ul>

        <span class="empty-block hidden">
            <?= Yii::t("site", "Basket items list is empty."); ?>
        </span>
    <?php else: ?>
        <span class="empty-block">
            <?= Yii::t("site", "Basket items list is empty."); ?>
        </span>
    <?php endif; ?>
</div>
