<?php
    use frontend\services\Basket\Item as BasketItem;
    use yii\helpers\Url;

    /** @var BasketItem $item */
?>
<?php if (isset($item)): ?>
    <li data-item-id="<?= $item->getModel()->id; ?>" data-item-price="<?= $item->getCost(); ?>"
        data-item-quantity="<?= $item->getQuantity(); ?>"
        <?php if (isset($isChildren) && $isChildren === true): ?>
            data-item-parent-id="<?= $item->getRelatedParent()->getModel()->id; ?>"
        <?php endif; ?>
    >
        <div class="image">
            <img src="<?= $item->getProduct()->mainImage->path; ?>" alt="<?= $item->getName(); ?>">
        </div>
        <div class="name">
            <?php if ($item->isSpecial()): ?>
                <a href="<?= Url::toRoute(['product/detail-special', 'id' => $item->getTypedObject()->id]); ?>">
                    <?= $item->getName(); ?>
                </a>
            <?php else: ?>
                <a href="<?= Url::toRoute(['product/detail',
                    'id' => $item->getProduct()->id,
                    'url' => $item->getProduct()->processed_name
                ]); ?>">
                    <?= $item->getName(); ?>
                </a>
            <?php endif; ?>
        </div>
        <div class="delete-btn">
            <?php if ($item->isSpecial()): ?>
                <a href="<?= Url::toRoute(['basket/remove-product-special', 'id' => $item->getId()]); ?>"></a>
            <?php else: ?>
                <?php if (isset($isChildren) && $isChildren === true): ?>
                    <a href="<?= Url::toRoute(['basket/remove-product',
                        'id' => $item->getId(),
                        'type' => $item->getType(),
                        'relatedParentId' => $item->getRelatedParent()->getModel()->id
                    ]); ?>"></a>
                <?php else: ?>
                    <a href="<?= Url::toRoute(['basket/remove-product',
                        'id' => $item->getId(),
                        'type' => $item->getType()
                    ]); ?>"></a>
                <?php endif; ?>
            <?php endif; ?>
        </div>
    </li>

    <?php if (is_array($item->getRelatedChildren())): ?>
        <?php foreach ($item->getRelatedChildren() as $childItem): ?>
            <?= $this->render("@frontend/views/components/basket/item", ['item' => $childItem, 'isChildren' => true]); ?>
        <?php endforeach; ?>
    <?php endif; ?>
<?php endif; ?>