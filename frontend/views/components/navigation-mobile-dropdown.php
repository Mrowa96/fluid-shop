<?php
    use frontend\utilities\Menu;
    use yii\helpers\Url;

    /**
     * @var Menu $categoriesMenu
     * @var Menu $pagesMenu
     */
?>

<div id="navigation-mobile-dropdown" class="invisible">
    <ul>
        <li>
            <a href="<?= Url::base(); ?>" title="Fluid Shop">
                <span class="text">Home</span>
            </a>
        </li>
    </ul>
    <?php if ($categoriesMenu->getProvider()->hasItems()): ?>
        <?= $categoriesMenu->render(); ?>
    <?php else: ?>
        <span class="empty-block">
            <?= Yii::t("site", "Categories list is empty."); ?>
        </span>
    <?php endif; ?>

    <?php if ($pagesMenu->getProvider()->hasItems()): ?>
        <?= $pagesMenu->render(); ?>
    <?php endif; ?>
</div>
