<?php
    use common\models\ProductSellCross;
    use yii\helpers\Url;

    /**
     * @var ProductSellCross $product
     * @var string $type
     * @var string $viewType
     */
?>
<div class="product product-<?= $viewType; ?>">
    <div class="image-block">
        <a href="<?= Url::toRoute(['product/detail', 'url' => $product->product->processed_name, 'id' => $product->product->id]); ?>">
            <img src="<?= $product->product->mainImage->path; ?>" alt="<?= $product->product->name; ?>">
        </a>
    </div>
    <div class="information-block">
        <div class="name">
            <?= $product->product->name; ?>
        </div>
        <div class="prices">
            <div class="prev-price">
                <?= $product->product->getCost(true, true); ?>
            </div>
            <div class="current-price">
                <?= $product->getCost(true); ?>
            </div>
        </div>
    </div>
    <div class="buttons-layer">
        <div class="buttons-block">
            <a href="#" class="btn-add-or-remove" title="<?= Yii::t("product", "Add to basket"); ?>" data-id="<?= $product->id; ?>"></a>
        </div>
    </div>
</div>