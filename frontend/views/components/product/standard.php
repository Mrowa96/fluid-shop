<?php
    use common\models\Product;
    use yii\helpers\Url;

    /**
     * @var Product $product
     * @var string $type
     * @var string $viewType
     */
?>
<div class="product product-<?= $viewType;?>">
    <div class="image-block">
        <a href="<?= Url::toRoute(['product/detail', 'url' => $product->processed_name, 'id' => $product->id]); ?>">
            <img src="<?= $product->mainImage->path; ?>" alt="<?= $product->name; ?>">
        </a>
    </div>
    <div class="information-block">
        <div class="name">
            <?= $product->name; ?>
        </div>
        <div class="price">
            <?= $product->getCost(true, true); ?>
        </div>
    </div>
    <div class="buttons-layer">
        <div class="buttons-block">
            <a href="<?= Url::toRoute(['product/detail', 'url' => $product->processed_name, 'id' => $product->id]); ?>" class="btn-details" title="<?= Yii::t("system", "Details");?>"></a>
            <a href="<?= Url::toRoute(['basket/add-product', 'url' => $product->processed_name, 'id' => $product->id]); ?>" class="btn-buy-product" title="<?= Yii::t("system", "Buy");?>"></a>
            <a href="<?= Url::toRoute(['product/add-to-favorites', 'id' => $product->id]);?>" class="btn-add-to-favorites" title="<?= Yii::t("system", "Add to favorites");?>"></a>
        </div>
    </div>
</div>