<?php
    use common\forms\Login;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var Login $model */
?>
<div class="site-login">
    <?php $form = ActiveForm::begin(['id' => 'login-form']); ?>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
        <?= $form->field($model, 'rememberMe')->checkbox() ?>

        <span class="text">
            <?= Yii::t("login", "If you forget your password, you can reset it here");?> <?= Html::a(Yii::t("login", "Reset password"), ['site/request-password-reset']) ?>. <br>
        </span>
        <span class="text">
            <?= Yii::t("login", "Click here if you want to sign up");?> <?= Html::a(Yii::t("login", "Sign up"), ['site/signup']) ?>.
        </span>
        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("site", "Login"), ['class' => 'btn', 'name' => 'login-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
