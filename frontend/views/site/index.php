<?php
    use common\models\Banner;
    use frontend\components\BannerWidget;
    use frontend\components\GroupedProductsWidget;
    use frontend\components\GroupedProductsWithSpecialWidget;
    use frontend\components\NewsletterWidget;
    use frontend\components\ProductWidget;

?>

<div class="site-index">
    <?= BannerWidget::widget(['type' => Banner::HOME_PAGE_SLIDER, 'limit' => false]); ?>

    <?= GroupedProductsWidget::widget([
        'title' => Yii::t("site", "Recommended products"),
        'type' => ProductWidget::TYPE_RECOMMENDED,
        'quantity' => 6
    ]); ?>

    <?= BannerWidget::widget(['type' => Banner::FIRST_BANNER, 'limit' => 1, 'random' => true]); ?>

    <?= NewsletterWidget::widget(['title' => Yii::t('site', 'Newsletter')]); ?>

    <?= GroupedProductsWithSpecialWidget::widget([
        'title' => Yii::t("site", "Promotional products"),
        'type' => ProductWidget::TYPE_PROMOTIONAL,
        'quantity' => 4
    ]); ?>

    <?= BannerWidget::widget(['type' => Banner::SECOND_BANNER, 'limit' => 1, 'random' => true]); ?>

    <?= GroupedProductsWidget::widget([
        'title' => Yii::t("site", "Check out products"),
        'type' => ProductWidget::TYPE_CHECK_OUT,
        'quantity' => 6
    ]); ?>
</div>