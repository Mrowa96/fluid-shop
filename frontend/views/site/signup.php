<?php
    use frontend\forms\Signup;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
    * @var Signup $model
    */
?>
<div class="site-signup">
    <?php $form = ActiveForm::begin(['id' => 'signup-form']); ?>
        <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'password')->passwordInput() ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("site", "Sign up"), ['class' => 'btn btn-primary', 'name' => 'signup-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
