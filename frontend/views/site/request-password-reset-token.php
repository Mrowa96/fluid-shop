<?php
    use frontend\forms\PasswordResetRequest;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var $model PasswordResetRequest */
?>
<div class="site-request-password-reset-token">
    <?php $form = ActiveForm::begin(['id' => 'request-password-reset-form']); ?>
        <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("system", "Send"), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>