<?php
    use common\models\Page;
    use yii\helpers\Html;

    /** @var Page $data */
?>
<div class="site-page">
    <?= $data->content; ?>
</div>
