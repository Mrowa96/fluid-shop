<?php
    /* @var $this yii\web\View */
    /* @var $name string */
    /* @var $message string */
    /* @var $exception Exception */

    use yii\helpers\Html;

    $this->title = $name;
?>
<div class="site-error">
    <div class="title">
        <h2><?= Html::encode($this->title) ?> </h2>
    </div>
    <div class="content">
        <?= nl2br(Html::encode($message)) ?>
    </div>
</div>
