<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
     * @var \common\models\Page $page
     * @var \frontend\forms\Contact $model
     */
?>
<div class="site-contact">
    <p class="text">
        <?= $page->content; ?>
    </p>
    <?php $form = ActiveForm::begin(['id' => 'contact-form']); ?>
        <?= $form->field($model, 'name')->textInput(['autofocus' => true]) ?>
        <?= $form->field($model, 'email') ?>
        <?= $form->field($model, 'subject') ?>
        <?= $form->field($model, 'body')->textArea(['rows' => 6]) ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("system", "Send"), ['class' => 'btn btn-primary', 'name' => 'contact-button']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
