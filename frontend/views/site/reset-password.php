<?php
    use frontend\forms\ResetPassword;
    use yii\helpers\Html;
    use yii\bootstrap\ActiveForm;

    /* @var $this yii\web\View */
    /* @var $form yii\bootstrap\ActiveForm */
    /* @var ResetPassword $model */
?>
<div class="site-reset-password">
    <p class="text"><?= Yii::t("password-reset", "Please choose your new password");?>:</p>

    <?php $form = ActiveForm::begin(['id' => 'reset-password-form']); ?>
        <?= $form->field($model, 'password')->passwordInput(['autofocus' => true]) ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("system", "Send"), ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
