<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;
?>

<div class="basket-address">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput() ?>
        <?= $form->field($model, 'surname')->textInput() ?>
        <?= $form->field($model, 'email')->textInput() ?>
        <?= $form->field($model, 'phone_no')->textInput() ?>
        <?= $form->field($model, 'street')->textInput() ?>
        <?= $form->field($model, 'building_no')->textInput() ?>
        <?= $form->field($model, 'city')->textInput() ?>
        <?= $form->field($model, 'zip')->textInput() ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("basket", "Next"), ['class' => 'btn']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>