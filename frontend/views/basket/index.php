<?php
    use common\models\Settings;
    use common\models\Settings\System;
    use frontend\forms\Discount;
    use frontend\services\Basket;
    use frontend\services\Basket\Item as BasketItem;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;

    /**
     * @var BasketItem[] $items
     * @var Basket $basket
     * @var \yii\web\View $this
     * @var Discount $discountModel
     */
?>
<div class="basket-index">
    <?php if(Yii::$app->basketHelper->exists()): ?>
        <?php
            $items = Yii::$app->basket->getItems(false, true, true);
        ?>
        <?php if(!empty($items)): ?>
            <table class="table" id="items">
                <thead>
                <tr>
                    <th><?= Yii::t("basket", "Product"); ?></th>
                    <th><?= Yii::t("basket", "Price"); ?></th>
                    <th><?= Yii::t("basket", "Quantity"); ?></th>
                    <th class="total"><?= Yii::t("basket", "Total"); ?></th>
                    <th>&nbsp;</th>
                </tr>
                </thead>
                <tbody>
                    <?php foreach ($items as $item): ?>
                        <tr class="item">
                            <td class="product-column">
                                <span class="image">
                                    <img src="<?= $item->getProduct()->getMainImage()->path; ?>" alt="<?= $item->getName(); ?>">
                                </span>

                                <?php if($item->getType() === BasketItem::TYPE_STANDARD || $item->getType() === BasketItem::TYPE_CROSS_SELL): ?>
                                    <a href="<?= Url::toRoute(['product/detail', 'id' => $item->getProduct()->id, 'url' => $item->getProduct()->processed_name]);?>" class="name">
                                        <?= $item->getName(); ?>
                                    </a>
                                <?php elseif ($item->getType() === BasketItem::TYPE_SPECIAL): ?>
                                    <a href="<?= Url::toRoute(['product/detail-special', 'id' => $item->getTypedObject()->id]);?>" class="name">
                                        <?= $item->getName(); ?>
                                    </a>
                                <?php else: ?>
                                    <span class="name">
                                         <?= $item->getName(); ?>
                                    </span>
                                <?php endif; ?>
                            </td>
                            <td class="price-column">
                                <span class="price">
                                    <?= $item->getCost(true); ?>
                                </span>
                            </td>
                            <td class="quantity-column">
                                <span class="quantity">
                                    <?php if ($item->getType() == BasketItem::TYPE_STANDARD): ?>
                                        <i class="fa fa-minus minus-btn"></i>
                                        <?php //TODO Change to model form?>
                                        <form action="<?= Url::toRoute(["basket/recount-product-quantity", 'id' => $item->getId(), 'type' => $item->getType()]); ?>" method="POST">
                                            <input type="number" min="1" name="quantity" value="<?= $item->getQuantity(); ?>" class="quantity-input">
                                            <input type="hidden" name="_csrf" value="<?= Yii::$app->request->getCsrfTokenFromHeader(); ?>">
                                        </form>

                                        <i class="fa fa-plus plus-btn"></i>
                                    <?php else: ?>
                                        <span class="constant-quantity">1</span>
                                    <?php endif; ?>
                                </span>
                            </td>
                            <td class="total-column">
                                <span class="total">
                                     <?= $item->getCost() * $item->getQuantity() . Settings::getOne('currency', System::MODULE); ?>
                                </span>
                            </td>
                            <td class="remove-column">
                                <?php if ($item->getType() !== BasketItem::TYPE_SPECIAL): ?>
                                    <?php if ($item->hasRelatedParent()): ?>
                                        <a href="<?= Url::toRoute(['basket/remove-product', 'id' => $item->getId(), 'type' => $item->getType(), 'relatedParentId' => $item->getRelatedParent()->getId()]); ?>" class="remove-btn"></a>
                                    <?php else: ?>
                                        <a href="<?= Url::toRoute(['basket/remove-product', 'id' => $item->getId(), 'type' => $item->getType()]); ?>" class="remove-btn"></a>
                                    <?php endif; ?>
                                <?php else: ?>
                                    <a href="<?= Url::toRoute(['basket/remove-product-special', 'id' => $item->getId()]); ?>" class="remove-btn"></a>
                                <?php endif; ?>
                            </td>
                        </tr>
                    <?php endforeach; ?>
                </tbody>
            </table>

            <div class="discount-block">
                <?php $form = ActiveForm::begin() ?>
                    <?= $form->field($discountModel, 'code')->textInput(['placeholder' => Yii::t("basket", "Discount code")])->label(false); ?>

                    <?= Html::submitButton(Yii::t("basket", "Set discount"), ['class' => 'btn']) ?>
                <?php ActiveForm::end() ?>
            </div>

            <div class="total-amount-block">
                <span class="total-amount">
                    <?= Yii::t("basket", "Total");?>: <?= Yii::$app->basket->getTotalCost(true, false, true);?>
                </span>
            </div>

            <div class="buttons-block">
                <a href="<?= Url::toRoute(['basket/address'])?>" class="btn btn-next">
                    <?= Yii::t("basket", "Next"); ?>
                </a>
            </div>
        <?php else: ?>
            <p class="no-products"><?= Yii::t("basket","No products was added to the basket."); ?></p>
        <?php endif; ?>
    <?php else: ?>
        <p class="no-products"><?= Yii::t("basket","Basket is not initialized. Add something to the basket."); ?></p>
    <?php endif; ?>
</div>