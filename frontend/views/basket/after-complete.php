<?php
    /** @var bool $paid */
?>
<div class="basket-after-complete">
    <?php if ($paid): ?>
        <span class="text"><?= Yii::t("basket", "Order was successfully paid and waits for check by our employees."); ?></span>
    <?php else: ?>
        <span class="text"><?= Yii::t("basket", "Order wasn't paid, so our employees are waiting for your payment."); ?></span>
    <?php endif; ?>
</div>
