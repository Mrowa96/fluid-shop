<?php
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\ActiveForm;

    /** @var \common\forms\Login $model */
?>
<div class="basket-account">
    <div class="user-exists-wrapper">
        <div class="header">
            <span class="text"><?= Yii::t("basket", "I have got an account"); ?></span>
        </div>

       <div class="content">
           <?php $form = ActiveForm::begin([
               'id' => 'login-form'
           ]); ?>
               <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>
               <?= $form->field($model, 'password')->passwordInput() ?>
               <?= $form->field($model, 'rememberMe')->checkbox() ?>
               <?= $form->field($model, 'ref')->hiddenInput(['value' => Url::toRoute(['basket/address'])])->label(false) ?>

               <div class="form-group">
                   <?= Html::submitButton(Yii::t("basket", "Login"), ['class' => 'btn', 'name' => 'login-button']) ?>
                   <?= Html::a(Yii::t("basket", "I forget my password"), ['class' => 'forget-password-btn', 'site/request-password-reset']) ?>
               </div>
           <?php ActiveForm::end(); ?>
       </div>
    </div>
    <div class="user-not-exists-wrapper">
        <div class="header">
            <span class="text">
                <?= Yii::t("basket", "I haven't got an account"); ?>
            </span>
        </div>
        <div class="content">
            <a href="<?= Url::toRoute(['basket/as-guest']); ?>">
                <?= Yii::t("basket", "Continue without login"); ?>
            </a>
            <span class="or-break"><?= Yii::t("basket", "or"); ?></span>
            <a href="<?= Url::toRoute(['site/signup', 'ref' => Url::toRoute(['basket/address'])]); ?>">
                <?= Yii::t("basket", "Sign up"); ?>
            </a>
        </div>
    </div>
</div>