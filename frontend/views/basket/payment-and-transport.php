<?php
    use common\models\Payment;
    use common\models\Settings;
    use common\models\Settings\System;
    use common\models\Transport;
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;

    /**
     * @var Payment[] $payments
     * @var Transport[] $transports
     */

    $currency = (string) Settings::getOne('currency', System::MODULE);
?>
<div class="basket-payment-and-transport">
    <?php $form = ActiveForm::begin(); ?>

        <div class="title">
            <h4><?= Yii::t("basket", "Payment"); ?></h4>
        </div>

        <?php if (!empty($payments)): ?>
            <?php foreach ($payments as $payment): ?>
                <?= $form->field($payment, 'id', [
                    'template' => "<div class='radio'>\n{input}\n{label}\n{error}\n{hint}\n</div>",
                    'inputOptions' => [
                        'id' => 'payment-' . $payment->id
                    ],
                    'selectors' => ['input' => '#payment-' . $payment->id]
                ])->label($payment->name . ' - ' . $payment->cost . $currency, ['for' => "payment-" . $payment->id])->input('radio'); ?>
            <?php endforeach ?>
        <?php else: ?>
            <p class="no-payments"><?= Yii::t("basket", "No payments was set up."); ?></p>
        <?php endif; ?>

        <div class="title">
            <h4><?= Yii::t("basket", "Transport"); ?></h4>
        </div>

        <?php if (!empty($transports)): ?>
            <?php foreach ($transports AS $transport): ?>
                <?= $form->field($transport, 'id', [
                    'template' => "<div class='radio'>\n{input}\n{label}\n{error}\n{hint}\n</div>",
                    'inputOptions' => [
                        'id' => 'transport-' . $transport->id
                    ],
                    'selectors' => ['input' => '#transport-' . $transport->id]
                ])->label($transport->name . ' - ' . $transport->cost . $currency, ['for' => "transport-" . $transport->id])->input('radio'); ?>
            <?php endforeach ?>
        <?php else: ?>
            <p class="no-transports"><?= Yii::t("basket", "No transports was set up."); ?></p>
        <?php endif; ?>

        <div class="buttons-block">
            <?= Html::submitButton(Yii::t("basket", "Next"), ['class' => 'btn btn-next']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
