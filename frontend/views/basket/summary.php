<?php
    use common\models\Settings;
    use common\models\Settings\System;
    use frontend\services\Basket;
    use frontend\services\Basket\Item as BasketItem;
    use yii\helpers\Url;

    /**
     * @var \yii\web\View $this
     * @var BasketItem $item
     * @var Basket $basket
     */

    $address = $basket->getAddress();
    $transport = $basket->getTransport();
    $payment = $basket->getPayment();
    $items = $basket->getItems(false, true, true);
    $currency = (string) Settings::getOne('currency', System::MODULE);
?>
<div class="basket-summary">
    <div class="main-data">
        <div class="title">
            <h4><?= Yii::t("basket", "Items"); ?></h4>
        </div>
        <div class="content">
            <table class="table" id="items">
            <thead>
            <tr>
                <th><?= Yii::t("basket", "Product"); ?></th>
                <th><?= Yii::t("basket", "Price"); ?></th>
                <th><?= Yii::t("basket", "Quantity"); ?></th>
                <th class="total"><?= Yii::t("basket", "Total"); ?></th>
            </tr>
            </thead>
            <tbody>
            <?php if (!empty($items)): ?>
                <?php foreach ($items as $item): ?>
                    <tr class="item">
                        <td class="product-column">
                            <span class="name">
                                 <?= $item->getName(); ?>
                            </span>
                        </td>
                        <td class="price-column">
                            <span class="price">
                                <?= $item->getCost(true); ?>
                            </span>
                        </td>
                        <td class="quantity-column">
                            <span class="quantity">
                                <span class="constant-quantity"><?= $item->getQuantity(); ?></span>
                            </span>
                        </td>
                        <td class="total-column">
                            <span class="total">
                                 <?= $item->getCost() * $item->getQuantity() . $currency; ?>
                            </span>
                        </td>
                    </tr>
                <?php endforeach; ?>
            <?php endif; ?>
            </tbody>
        </table>
        </div>
    </div>
    <div class="additional-data">
        <div id="address-data">
            <div class="title">
                <h4><?= Yii::t("basket", "Address"); ?></h4>
            </div>
            <div class="content">
                <?php if (!empty($address)): ?>
                    <span><?= $address->name . " " . $address->surname; ?></span>
                    <span><?= $address->street . " " . $address->building_no; ?></span>
                    <span><?= $address->zip . " " . $address->city; ?></span>
                    <span><?= $address->phone_no; ?></span>
                    <span><?= $address->email; ?></span>
                <?php endif; ?>
            </div>
        </div>
        <div id="payment-and-transport-data">
            <div class="title">
                <h4><?= Yii::t("basket", "Payment and transport"); ?></h4>
            </div>
            <div class="content">
                <?php if (!empty($payment) && !empty($transport)): ?>
                    <span><?= $payment->name . " - " . $payment->cost. $currency; ?></span>
                    <span><?= $transport->name . " - " . $transport->cost . $currency; ?></span>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="buttons-block">
        <a href="<?= Url::toRoute(['basket/complete'])?>" class="btn btn-next">
            <?= Yii::t("basket", "Next"); ?>
        </a>
    </div>
</div>