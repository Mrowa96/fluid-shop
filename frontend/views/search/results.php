<?php
    use common\models\Product;
    use frontend\components\GroupedProductsWidget;
    use frontend\components\ProductWidget;

    /** @var Product[] $data */
?>

<div class="search-results">
    <?= GroupedProductsWidget::widget([
        'type' => ProductWidget::TYPE_ON_LIST,
        'products' => $data,
        'quantity' => count($data)
    ]); ?>
</div>
