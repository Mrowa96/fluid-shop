<?php
use common\models\Favorite;

/** @var Order[] $ */
//TODO rest
?>

<div class="order-list-dropdown-wrapper">
    <?php if (!empty($favorites)): ?>
        <ul>
            <?php foreach ($favorites as $favorite) : ?>
                <li class="product">
                    <a href="<?= \yii\helpers\Url::toRoute([
                        'product/detail',
                        'id' => $favorite->product->id,
                        'url' => $favorite->product->processed_name]); ?>" title="<?= $favorite->product->name; ?>">
                    </a>
                    <div class="image">
                        <img src="<?= $favorite->product->mainImage->path; ?>" alt="<?= $favorite->product->name; ?>">
                    </div>
                    <div class="name">
                        <?= $favorite->product->name; ?>
                    </div>
                </li>
            <?php endforeach; ?>
        </ul>
    <?php else: ?>
        <span class="empty-block">
            <?= Yii::t("site", "Favorites list is empty."); ?>
        </span>
    <?php endif; ?>
</div>
