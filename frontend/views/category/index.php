<?php
    use common\models\Banner;
    use common\models\Category;
    use common\models\Product;
    use frontend\components\BannerWidget;
    use frontend\components\GroupedProductsWidget;
    use frontend\components\ProductWidget;
    use frontend\utilities\ListOrder;
    use yii\data\Pagination;
    use yii\helpers\Url;
    use yii\widgets\LinkPager;

    /** @var array $products */
    /** @var Product $product */
    /** @var Category $category */
    /** @var Pagination $pagination */
?>

<div class="category-index">
    <?= BannerWidget::widget(['type' => Banner::SUB_PAGE_BANNER, 'limit' => 1, 'random' => true]); ?>

    <?php if(!empty($products)): ?>
        <div class="filter-bar">
            <div class="inner-block">
                <span class="text"><?= Yii::t("category", "Sort by");?>:</span>
                <ul class="filters">
                    <li><a href="<?= Url::current(['order-by' => ListOrder::NAME_ASC]);?>">A-Z</a></li>
                    <li><a href="<?= Url::current(['order-by' => ListOrder::NAME_DESC]);?>">Z-A</a></li>
                    <li><a href="<?= Url::current(['order-by' => ListOrder::PRICE_ASC]);?>"><?= Yii::t("category", "Lowest price");?></a></li>
                    <li><a href="<?= Url::current(['order-by' => ListOrder::PRICE_DESC]);?>"><?= Yii::t("category", "Highest price");?></a></li>
                </ul>
            </div>
        </div>

        <?= GroupedProductsWidget::widget([
            'type' => ProductWidget::TYPE_ON_LIST,
            'products' => $products,
            'quantity' => count($products)
        ]); ?>

        <?= LinkPager::widget([
            'pagination' => $pagination,
            'firstPageLabel' => '<i class="fa fa-angle-double-left"></i>',
            'lastPageLabel' => '<i class="fa fa-angle-double-right"></i>',
            'prevPageLabel' => '<i class="fa fa-angle-left"></i>',
            'nextPageLabel' => '<i class="fa fa-angle-right"></i>',
            'maxButtonCount' => 6,
        ]); ?>
    <?php else: ?>
        <div class="no-products">
            <?= Yii::t("category", "There are no products, which match requirements"); ?>
        </div>
    <?php endif; ?>

    <?= GroupedProductsWidget::widget([
        'title' => Yii::t("category", "Recommended products"),
        'type' => ProductWidget::TYPE_RECOMMENDED,
        'quantity' => 4
    ]); ?>
</div>