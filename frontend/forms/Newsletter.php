<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\MailSubscriber;
    use yii\helpers\Html;

    /**
     * Class Newsletter
     * @package frontend\forms
     *
     * @property string $email
     * @property string $name
     * @property bool $agree
     */
    class Newsletter extends Model
    {
        public $name;
        public $email;
        public $agree;

        public function rules()
        {
            return [
                [['email', 'name', 'agree'], 'required'],
                ['email', 'email'],
                ['name', 'string'],
                ['agree', 'boolean'],
                ['agree', 'required', 'requiredValue' => 1, 'message' => Yii::t('newsletter', 'Agreement must be accepted.')],
                ['email', 'unique', 'targetClass' => MailSubscriber::className(), 'message' => Yii::t("newsletter", "You are already signed up.")],
            ];
        }

        public function assign()
        {
            if ($this->validate()) {
                $subscriber = new MailSubscriber();

                $subscriber->email = Html::encode($this->email);
                $subscriber->name = Html::encode($this->name);
                $subscriber->join_date = (new \DateTime())->format("Y-m-d H:i:s");

                return $subscriber->save();
            }

            return false;
        }

        public function attributeLabels()
        {
            return [
                'name' => Yii::t('newsletter', 'Name'),
                'email' => Yii::t('newsletter', 'Email'),
                'agree' => '',
            ];
        }
    }
