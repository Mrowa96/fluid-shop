<?php
    namespace frontend\forms;

    use common\models\ProductReview as ProductReviewModel;
    use Yii;
    use yii\base\Model;

    /**
     * This is the model class for form "{{%product_review}}".
     *
     * @property integer $product_id
     * @property integer $rating
     * @property string $text
     */
    class ProductReview extends Model
    {
        public $product_id;
        public $rating;
        public $text;

        public function rules()
        {
            return [
                [['rating', 'text', 'product_id'], 'required'],
                [['rating'], 'in', 'range' => [1, 2, 3, 4, 5]],
                ['product_id', 'integer'],
                ['text', 'string']
            ];
        }

        public function attributeLabels()
        {
            return [
                'rating' => Yii::t("product-rating", "Rating"),
                'text' => Yii::t("product-rating", "Text"),
            ];
        }

        /**
         * @return bool
         */
        public function saveReview()
        {
            if (!Yii::$app->user->isGuest && $this->validate()) {
                $review = new ProductReviewModel();

                $review->product_id = $this->product_id;
                $review->rating = $this->rating;
                $review->text = $this->text;

                return $review->save();
            }

            return false;
        }
    }
