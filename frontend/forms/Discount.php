<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\Discount as DiscountModel;

    /**
     * Class Discount
     * @package frontend\forms
     *
     * @property string $code
     */
    class Discount extends Model
    {
        public $code;

        public function rules()
        {
            return [
                [['code'], 'required'],
                [['code'], 'string'],
                [['code'], 'validateCode']
            ];
        }

        public function attributeLabels()
        {
            return [
                'code' => Yii::t("basket", "Discount code"),
            ];
        }

        /**
         * @param $attribute
         */
        public function validateCode($attribute)
        {
            if (!DiscountModel::getDiscount($this->code)) {
                $this->addError($attribute, Yii::t("basket", "Discount code is invalid."));
            }
        }
    }
