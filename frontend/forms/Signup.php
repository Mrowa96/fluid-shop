<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\User;

    /**
     * Class Signup
     * @package frontend\forms
     *
     * @property string $username
     * @property string $email
     * @property string $password
     */
    class Signup extends Model
    {
        public $username;
        public $email;
        public $password;

        public function rules()
        {
            return [
                ['username', 'unique',
                    'targetClass' => User::className(),
                    'message' => Yii::t("signup", 'This username has already been taken.')
                ],
                ['email', 'unique',
                    'targetClass' => User::className(),
                    'message' => Yii::t("signup", 'This email address has already been taken.')
                ],
                ['password', 'required'],
                ['password', 'string', 'min' => 6],
            ];
        }

        public function attributeLabels()
        {
            return [
                'username' => Yii::t("signup", "Username"),
                'email' => Yii::t("signup", "Email"),
                'password' => Yii::t("signup", "Password"),
            ];
        }

        /**
         * @return User|null
         */
        public function signup()
        {
            if ($this->validate()) {
                $user = new User();

                $user->username = $this->username;
                $user->email = $this->email;
                $user->auth_key = User::generateAuthKey();
                $user->setPassword($this->password);

                if ($user->save() && $user->setRole(User::ROLE_CLIENT)) {
                    return $user;
                }
            }

            return null;
        }
    }
