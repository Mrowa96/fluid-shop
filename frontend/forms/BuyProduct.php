<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\Product;

    /**
     * Class BuyProduct
     * @package frontend\forms
     *
     * @property int $id
     * @property int $quantity
     * @property array|null $upSells
     * @property array|null $crossSells
     */
    class BuyProduct extends Model
    {
        public $id;
        public $quantity;
        public $upSells;
        public $crossSells;

        public function rules()
        {
            return [
                [['id', 'quantity'], 'required'],
                [['upSells', 'crossSells'], 'each', 'rule' => ['integer']],
                ['quantity', 'compare', 'compareValue' => 1, 'operator' => '>=', 'type' => 'number'],
                [['id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className()],
            ];
        }

        /**
         * @return bool
         */
        public function addToBasket()
        {
            try {
                $item = Yii::$app->basket->addItem($this->id, $this->quantity);

                if (!empty($this->upSells)) {
                    foreach ($this->upSells as $upSellId => $bought) {

                        if (!empty($bought)) {
                            Yii::$app->basket->addItemUpSell($upSellId, $item);
                        }
                    }
                }

                if (!empty($this->crossSells)) {
                    foreach ($this->crossSells as $crossSellId => $bought) {

                        if (!empty($bought)) {
                            Yii::$app->basket->addItemCrossSell($crossSellId, $item);
                        }
                    }
                }

                return true;

            } catch (\Exception $ex) {
                \Yii::error($ex, "basket");
            }

            return false;
        }
    }