<?php
    namespace frontend\forms;

    use Yii;
    use yii\base\Model;
    use common\models\Settings;

    /**
     * Class Contact
     * @package frontend\forms
     *
     * @property string $name
     * @property string $email
     * @property string $subject
     * @property string $body
     */
    class Contact extends Model
    {
        public $name;
        public $email;
        public $subject;
        public $body;

        public function rules()
        {
            return [
                [['name', 'email', 'subject', 'body'], 'required'],
                ['email', 'email'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'name' => Yii::t("contact", "Name"),
                'email' => Yii::t("contact", "Email"),
                'subject' => Yii::t("contact", "Topic"),
                'body' => Yii::t("contact", "Text"),
            ];
        }

        public function sendEmail($email = null)
        {
            if (is_null($email)) {
                $email = Settings::getOne('email', Settings\Information::MODULE);
            }

            try {
                return Yii::$app->mailer->compose()
                    ->setTo($email)
                    ->setFrom([$this->email => $this->name])
                    ->setSubject($this->subject)
                    ->setTextBody($this->body)
                    ->send();
            } catch (\Exception $ex) {
                Yii::error($ex->getMessage());

                return false;
            }
        }
    }
