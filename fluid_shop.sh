#!/bin/bash

clear
printf "Fluid Shop Initialization Tool v1.0\n"
printf "___________________________________\n"
printf "[1] Create new instance - chose this in parent directory\n"
printf "[2] Setup permissions\n"
printf "[3] Update backend assets\n"
printf "[4] Update frontend assets\n"
printf "[5] Basic configuration\n"
printf "\nOptions: ";
read option

function initInstance {
   printf "\n"
   printf "Set instance name: "
   read instanceName

   git clone git@gitlab.com:Mrowa96/fluid-shop.git $instanceName

   sudo chmod 775 -R $instanceName
   sudo chown apache:apache $instanceName -R

   cd $instanceName

   composer global require "fxp/composer-asset-plugin:~1.1.1"
   composer install
}

function setupPermission {
   sudo chmod 777 -R frontend/runtime
   sudo chmod 777 -R frontend/var
   sudo chmod 777 -R frontend/web/cache
   sudo chmod 777 -R frontend/web/data
   sudo chmod 777 -R backend/runtime
   sudo chmod 777 -R backend/var
   sudo chmod 777 -R backend/web/cache
   sudo chmod 777 -R console/runtime
}

function updateBackendAssets {
    setupPermission
    updateCommonAssets

    cd backend/web/assets/src

    if [ -d node_modules ] ; then
        sudo rm -R node_modules
    fi

    npm install
    npm run prod

    cd ../../../../
}

function updateFrontendAssets {
    setupPermission
    updateCommonAssets

    cd frontend/web/assets/src

    if [ -d node_modules ] ; then
        sudo rm -R node_modules
    fi

    npm install
    npm run prod

    cd ../../../../
}

function updateCommonAssets {
    setupPermission

    cd common/assets

    if [ -d node_modules ] ; then
        sudo rm -R node_modules
    fi

    npm install

    cd ../../
}

function basicConfiguration {
    setupPermission
    php fs_console
}

if [ $option = 1 ] ; then
    initInstance
elif [ $option = 2 ] ; then
    setupPermission
elif [ $option = 3 ] ; then
    updateBackendAssets
elif [ $option = 4 ] ; then
    updateFrontendAssets
elif [ $option = 5 ] ; then
    basicConfiguration
fi