<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
    /** @var \common\models\Rma $model */
?>

<div class="rma-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'status')->dropDownList([
            $model::STATUS_NEW => Yii::t("rma", "New"),
            $model::STATUS_ACCEPTED => Yii::t("rma", "Accepted"),
            $model::STATUS_DECLINED => Yii::t("rma", "Declined"),
            $model::STATUS_COMPLETED => Yii::t("rma", "Completed"),
        ], ['prompt' => Yii::t("rma", "Choose status")]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
