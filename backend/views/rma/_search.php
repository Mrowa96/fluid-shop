<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>

<div class="rma-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
        <?= $form->field($model, 'id') ?>
        <?= $form->field($model, 'user.username') ?>
        <?= $form->field($model, 'product.product_name') ?>
        <?= $form->field($model, 'order.key') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('system', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('system', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
