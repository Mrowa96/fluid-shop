<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = Yii::t("system", "View") . ' - ' . $model->product->name . ' (' . $model->order->key . ')';
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Rma'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="rma-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'product.product_id',
                'product.product_name',
                'user.username',
                'order.key',
                'text:ntext',
                'status',
                'created_date',
                'modified_date',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
