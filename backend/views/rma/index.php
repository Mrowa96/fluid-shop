<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;

    /**
     * @var \yii\data\ActiveDataProvider $dataProvider
     * @var \backend\searches\Rma $searchModel
     */
    $this->title = Yii::t('navigation', 'Rma');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="rma-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'user.username',
                            'product.product_name',
                            'order.key',
                            ['class' => '\kartik\grid\ActionColumn',]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ],
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
