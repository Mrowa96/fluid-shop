<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->id;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Rma'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="rma-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
