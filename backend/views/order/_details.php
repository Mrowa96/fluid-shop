<?php
    use common\models\Order;
    use yii\widgets\DetailView;

    /**
     * @var Order $data
     */

    echo DetailView::widget([
        'model' => $data,
        'attributes' => [
            'key',
            'cost',
            'transport.cost',
            'orderStatus.status_name',
            'create_date',
            'modify_date',
            'payment.payment_name',
            'transport.transport_name',
            'note',
        ],
    ]);