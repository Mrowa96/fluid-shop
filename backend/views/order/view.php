<?php
    use backend\models\OrderLog;
    use common\models\Address;
    use common\models\Order;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Html;

    /**
     * @var Order $model
     * @var ActiveDataProvider $products
     * @var Address $address
     * @var OrderLog[] $logs
     */
    $this->title = Yii::t("order", "Order no.") . ' ' . $model->key;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Orders'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box order-view">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => $this->render("_details", ['data' => $model]),
                    'label' => Yii::t("order", "Details")
                ],
                [
                    'content' => $this->render("_address", ['data' => $address]),
                    'label' => Yii::t("order", "Address")
                ],
                [
                    'content' => $this->render("_products", ['data' => $products]),
                    'label' => Yii::t("order", "Products")
                ],
                [
                    'content' => $this->render("_history", ['data' => $logs]),
                    'label' => Yii::t("order", "History")
                ],
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
    <div class="box-footer">
        <?php if($model->order_status_id !== 7): ?>
            <?=  Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], [
                'class' => 'btn btn-primary',
            ]);?>
        <?php endif; ?>

        <?php if(!$model->invoice): ?>
            <?=  Html::a(Yii::t('order', 'Generate Invoice'), ['generate-invoice', 'id' => $model->id], [
                'class' => 'btn btn-warning',
            ]);?>
        <?php else: ?>
            <?=  Html::a(Yii::t('order', 'View Invoice'), ['view-invoice', 'id' => $model->invoice->id], [
                'class' => 'btn btn-success',
            ]);?>
        <?php endif; ?>
    </div>
</div>
