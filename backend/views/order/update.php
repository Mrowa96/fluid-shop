<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
     * @var \common\models\OrderStatus[] $statuses
     * @var \common\models\Order $model
     */
    $this->title = Yii::t("system", "Update") . ' - ' . $model->key;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Orders"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-update box">
    <div class="box-content order-form">
        <div>
            <?php $form = ActiveForm::begin(); ?>
                <?= $form->field($model, "order_status_id")->dropDownList(
                    $statuses, [
                        'prompt' => Yii::t("order", "Change order status")
                    ]
                ); ?>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t("system", "Update"), ['class' => 'btn btn-primary']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>