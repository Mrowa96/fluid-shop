<?php
    use common\models\Address;
    use yii\widgets\DetailView;

    /** @var Address $data */

    echo DetailView::widget([
        'model' => $data,
        'attributes' => [
            'name',
            'surname',
            'email',
            'phone_no',
            'street',
            'building_no',
            'city',
            'zip',
        ]
    ]);