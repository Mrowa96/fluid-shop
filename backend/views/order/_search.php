<?php
    use common\models\Order;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var Order $model */
?>

<div class="order-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>
        <?= $form->field($model, 'address.fullName') ?>
        <?= $form->field($model, 'key') ?>
        <?= $form->field($model, 'cost') ?>
        <?= $form->field($model, 'create_date') ?>
        <?= $form->field($model, 'orderStatus.status_name') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('system', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('system', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
