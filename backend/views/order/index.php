<?php
    use backend\searches\Order;
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;

    /**
     * @var ActiveDataProvider $allOrders
     * @var ActiveDataProvider $inProgressOrders
     * @var ActiveDataProvider $completedOrders
     * @var ActiveDataProvider $canceledOrders
     * @var Order $searchModel
     */
    $this->title = Yii::t("navigation", "Orders");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box order-index">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => generateGridView($allOrders, $searchModel),
                    'label' => Yii::t("system", "All")
                ],
                [
                    'content' => generateGridView($inProgressOrders, $searchModel),
                    'label' => Yii::t("order", "In progress"),
                    'active'=>true
                ],
                [
                    'content' => generateGridView($completedOrders, $searchModel),
                    'label' => Yii::t("order", "Completed")
                ],
                [
                    'content' => generateGridView($canceledOrders, $searchModel),
                    'label' => Yii::t("order", "Canceled")
                ],
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>

<?php
    function generateGridView($dataProvider, $searchModel){
        return GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'columns' => [
                ['class' => '\kartik\grid\SerialColumn'],
                'address.fullName',
                'key',
                'create_date',
                'orderStatus.status_name',
                [
                    'class' => '\kartik\grid\ActionColumn',
                    'buttons' => [
                        'view' =>  function($url){
                            return  '<a class="view" href="'.$url.'" data-pjax="0"><i class="fa fa-eye"></i></a>';
                        },
                        'update' =>  function($url){
                            return  '<a class="update" href="'.$url.'" data-pjax="0"><i class="fa fa-pencil"></i></a>';
                        },
                        'delete' =>  function($url){return  '';}
                    ]
                ]
            ],
            'responsive' => true,
            'export' => false,
            'pjax' => true,
            'pjaxSettings' => [
                'neverTimeout' => true,
            ],
        ]);
    }
?>