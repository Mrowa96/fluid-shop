<?php
    use backend\models\OrderLog;
    use yii\helpers\Url;

    /** @var OrderLog[] $data */
?>
<?php if(!empty($data)): ?>
    <?php foreach($data AS $log): ?>
        <p><?= $log->date.' - '.$log->status->message;?> <?= Yii::t("system", "User"); ?>:
            <strong><a href="<?= Url::toRoute(['user/view', 'id' => $log->user->id]);?>"><?= $log->user->name . " (" . $log->user->id .")"; ?></a></strong></p>
    <?php endforeach; ?>
<?php else: ?>
    <p><?= Yii::t("order", "Order has no history"); ?></p>
<?php endif; ?>