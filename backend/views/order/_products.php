<?php
    use common\models\OrderDetail;
    use frontend\utilities\BasketItem;
    use kartik\grid\GridView;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $data
     */

    echo GridView::widget([
        'dataProvider' => $data,
        'columns' => [
            'product.name',
            'product_cost',
            'product_quantity',
            [
                'class' => '\kartik\grid\ActionColumn',
                'buttons' => [
                    'view' => function ($url, $model) {
                        return '<a href="' . Url::to(['product/view', 'id' => $model->product_id]) . '" class="view"><i class="fa fa-eye"></i></a>';
                    },
                    'delete' => function () {
                        return '';
                    },
                    'update' => function () {
                        return '';
                    }
                ],
            ]
        ],
        'rowOptions' => function ($model) {
            /** @var OrderDetail $model */
            if ($model->product_type == BasketItem::TYPE_SPECIAL) {
                return ['class' => 'productSpecial'];
            }
        },
        'responsive' => true,
        'export' => false,
        'pjax' => true,
        'pjaxSettings' => [
            'neverTimeout' => true,
        ],
        'layout' => "{items}\n{pager}",
    ]);