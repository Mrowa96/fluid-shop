<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t('navigation', 'Banners');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            array(
                                'attribute' => 'thumb_path',
                                'format' => 'html',
                                'value' => function ($model) {
                                    /** @var \common\models\Banner $model */
                                    return "<img src='$model->thumb_path' class='img-responsive img-thumb'>";
                                }
                            ),
                            'name',
                            'link',
                            'type_text',
                            ['class' => '\kartik\grid\ActionColumn']
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ],
                [
                    'label' => Yii::t("banner", "Add banner"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/banner/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
