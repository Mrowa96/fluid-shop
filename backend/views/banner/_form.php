<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var array $types */
?>

<div class="banner-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'link')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'type')->dropDownList($types, ['prompt' => Yii::t("banner", "Choose banner type")]) ?>
        <?= $form->field($model, 'imageFile')->fileInput() ?>
        <?= $form->field($model, 'display_on_desktop')->checkbox() ?>
        <?= $form->field($model, 'display_on_mobile')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
