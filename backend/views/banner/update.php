<?php
    $this->title = Yii::t('system', 'Update') . ' - ' .$model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Banners'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="banner-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'types' => $types,
        ]) ?>
    </div>
</div>
