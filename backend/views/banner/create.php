<?php
    use common\models\Banner;

    $this->title = Yii::t('system', 'Create');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Banners'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;

    /** @var Banner $model */
    /** @var array $types */
?>
<div class="banner-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'types' => $types,
        ]) ?>
    </div>
</div>
