<?php
    use common\models\Banner;
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    /**
    * @var Banner $model
    */

    $this->title = $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Banners'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="banner-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                [
                    'format' => 'html',
                    'attribute' => 'path',
                    'label' => Yii::t("banner", "Image"),
                    'value' => '<img src="'.$model->path.'" class="img-responsive" alt="Banner">'
                ],
                'name',
                'link',
                'type_text',
                'display_on_desktop_text',
                'display_on_mobile_text',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
