<?php
    use yii\widgets\ActiveForm;
    use yii\helpers\Html;

    $this->title = Yii::t("product", "Import products");;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Products"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>

<div class="box">
    <div class="box-content">
        <div class="product-import-form">
            <?php $form = ActiveForm::begin(); ?>
                <div class="row">
                    <div class="col-lg-6 col-md-6 jsonFile">
                        <span class="text">
                            <?= Yii::t("product", "Import from JSON file"); ?>
                        </span>
<pre>
   [{"id":1,
    "name":"Testowy produkt",
    "description":"Opis produktu",
    "additional_field": "Dodatkowe pole tekstowe",
    "cost": 12.99,
    "quantity": 99,
    "available": 1,
    "code":"Kod produktu, ean lub cokolwiek",
    "type": "standard lub extra"}, {...}]
</pre>
                        <?= $form->field($model, 'jsonFile')->fileInput(['multiple' => false, 'value' => ''])->error(false) ?>
                    </div>

                    <div class="col-lg-6 col-md-6 xmlFile">
                            <span class="text">
                                <?= Yii::t("product", "Import from XML file"); ?>
                            </span>
<pre>
    &lt;products>
        &lt;product>
            &lt;id>1&lt;/id>
            &lt;name>Testowy produkt&lt;/name>
            &lt;description>Opis produktu&lt;/description>
            &lt;additional_field>Dodatkowe pole tekstowe&lt;/additional_field>
            &lt;cost>12.99&lt;/cost>
            &lt;quantity>99&lt;/quantity>
            &lt;available>1&lt;/available>
            &lt;code>Kod produktu, ean lub cokolwiek&lt;/code>
            &lt;type>standard lub extra&lt;/type>
            &lt;/product>
        &lt;product>
           ...
        &lt;/product>
    &lt;/products>
</pre>
                        <?= $form->field($model, 'xmlFile')->fileInput(['multiple' => false, 'value' => ''])->error(false) ?>
                    </div>

                    <div class="col-lg-12 col-md-12">
                        <?= $form->errorSummary($model); ?>
                    </div>
                </div>

                <div class="form-group">
                    <?= Html::submitButton(Yii::t('system', 'Import'), ['class' =>'btn btn-success']) ?>
                </div>
            <?php ActiveForm::end(); ?>
        </div>
    </div>
</div>
