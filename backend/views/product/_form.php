<?php
    use backend\forms\Product as ProductForm;
    use backend\models\Attribute;
    use common\models\Category;
    use common\models\Discount;
    use common\models\Product;
    use yii\web\View;
    use yii\widgets\ActiveForm;
    use kartik\tabs\TabsX;

    /**
     * @var Product $model
     * @var Discount[] $discounts
     * @var Category[] $categories
     * @var Attribute[] $attributes
     * @var ProductForm $productsForm
     */

    $this->registerJsFile("/assets/dist/vendor/tinymce/tinymce.min.js", ['position' => View::POS_HEAD]);
    $form = ActiveForm::begin();
        echo TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => $this->render("form/_general", [
                        'form' => $form,
                        'model' => $model,
                        'discounts' => $discounts,
                        'categories' => $categories
                    ]),
                    'label' => Yii::t("product", "General")
                ],
                [
                    'content' => $this->render("form/_images", [
                        'form' => $form,
                        'model' => $model,
                    ]),
                    'label' => Yii::t("product", "Images")
                ],
                [
                    'content' => $this->render("form/_attributes", [
                        'form' => $form,
                        'model' => $model,
                        'attributes' => $attributes,
                    ]),
                    'label' => Yii::t("product", "Attributes")
                ],
                [
                    'content' => $this->render("form/_crossSelling", [
                        'form' => $form,
                        'model' => $model,
                        'productsForm' => $productsForm,
                    ]),
                    'label' => Yii::t("product", "Cross sell products")
                ],
                [
                    'content' => $this->render("form/_upSelling", [
                        'form' => $form,
                        'model' => $model,
                        'productsForm' => $productsForm,
                    ]),
                    'label' => Yii::t("product", "Up sell products")
                ],
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]);
    ActiveForm::end();