<?php
    /**
     * @var \common\models\Product $model
     * @var \common\models\Category[] $categories
     * @var \common\models\Discount[] $discounts
     * @var \backend\models\Attribute[] $attributes
     * @var \backend\forms\Product $productsForm
     */

    $this->title = Yii::t("system", "Update") . ' - ' .  $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Products"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories,
            'discounts' => $discounts,
            'attributes' => $attributes,
            'productsForm' => $productsForm,
        ]) ?>
    </div>
</div>

