<?php
    use kartik\grid\GridView;
    use yii\helpers\Url;

    /** @var \yii\data\ActiveDataProvider $data */
?>

<?= GridView::widget([
    'dataProvider' => $data,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        [
            'attribute' => 'rating',
            'value' => function ($model) use ($data) {
                $rating = $model->rating;
                $response = "";

                while ($rating-- > 0) {
                    $response .= "<i class='fa fa-star'></i>";
                }

                return $response;
            },
            'format' => 'html',
        ],
        'text',
        'accepted_text',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete} {accept}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    return '<a href="' . Url::toRoute(['product-review/delete', 'id' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-trash"></span></a>';
                },
                'accept' => function ($url, $model) {
                    if (!$model->accepted) {
                        return '<a href="' . Url::toRoute(['product-review/accept', 'id' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-ok"></span></a>';
                    }
                }
            ]
        ]
    ],
    'layout' => "{items}\n{pager}",
    'responsive' => true,
    'export' => false,
]) ?>
