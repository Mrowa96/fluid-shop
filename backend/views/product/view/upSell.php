<?php
    use kartik\grid\GridView;
    use yii\helpers\Url;

    /** @var \yii\data\ActiveDataProvider $data */
?>

<?= GridView::widget([
    'dataProvider' => $data,
    'columns' => [
        ['class' => 'yii\grid\SerialColumn'],

        'product.name',
        'cost',
        [
            'class' => 'yii\grid\ActionColumn',
            'template' => '{delete}',
            'buttons' => [
                'delete' => function ($url, $model) {
                    return '<a href="' . Url::toRoute(['product/up-sell-delete', 'id' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-trash"></span></a>';
                },
            ]
        ]
    ],
    'layout' => "{items}\n{pager}",
    'responsive' => true,
    'export' => false,
]) ?>
