<?php
    /** @var \common\models\Product $data */
?>
<div class="images row" id="productImages">
    <?php if (!empty($data->images)): ?>
        <?php foreach ($data->images AS $image): ?>
            <?php
                if ($data->mainImage && $data->mainImage->id == $image->id) {
                    $class = "mainImage";
                } else {
                    $class = "";
                }
            ?>
            <div class="imageWrap col-lg-2 col-md-3 col-sm-6 col-xs-12 <?= $class; ?>">
                <div class="innerWrapper">
                    <i class="fa fa-star icon" aria-hidden="true"></i>
                    <div class="image">
                        <img src="<?= $image->path; ?>" alt="<?= $image->name; ?>" data-id="<?= $image->id; ?>" class="img-responsive">
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    <?php else: ?>
        <div class="emptyBlock col-lg-12 col-md-12 col-sm-12 col-xs-12">
            <i class="fa fa-frown-o icon" aria-hidden="true"></i>
            <span class="text"><?= Yii::t("product", "There are no images to display."); ?></span>
        </div>
    <?php endif; ?>
</div>