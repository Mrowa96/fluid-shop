<?php
    use yii\widgets\DetailView;
?>
<?= DetailView::widget([
    'model' => $data,
    'attributes' => [
        'id',
        'name',
        [
            'attribute' => 'description',
            'format' => 'html'
        ],
        [
            'attribute' => 'additional_field',
            'format' => 'html'
        ],
        'category.name',
        'discount.name',
        'previous_cost',
        'cost',
        'costAfterDiscount',
        'quantity',
        'available',
        'code',
        'bought_count',
        'modified_date',
        'created_date',
    ],
]) ?>