<?php
    use backend\models\Attribute;
    use common\models\Product;
    use kartik\select2\Select2;
    use yii\helpers\Html;

    /**
     * @var array $attributes
     * @var Product $model
     */
?>
<div id="attributes-wrap">
    <div id="attributes" class="row">
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="form-choose-both">
            <div class="form-group attribute-choose">
                <label class="control-label" for="attribute_select">
                    <?= Yii::t("attribute", "Attribute list");?>
                </label>
                <select name="attribute_select" id="attribute_select" data-type="attribute">
                    <option value=""><?= Yii::t("attribute","Select attribute");?></option>

                    <?php foreach($attributes as $key => $name): ?>
                        <option value="<?= $key; ?>"><?= $name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group option-choose">
                <label class="control-label" for="option_select">
                    <?= Yii::t("attribute", "Options list");?>
                </label>
                <select name="option_select" id="option_select" data-type="option" disabled="disabled">
                    <option value=""><?= Yii::t("attribute","Select option");?></option>
                </select>
            </div>

            <div class="form-group">
                <?= Html::tag("span", Yii::t("system", "Add"), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="form-choose-and-create">
            <div class="form-group attribute-choose">
                <label class="control-label" for="attribute_select_2">
                    <?= Yii::t("attribute", "Attribute list");?>
                </label>
                <select name="attribute_select" id="attribute_select_2" data-type="attribute">
                    <option value=""><?= Yii::t("attribute","Select option");?></option>

                    <?php foreach($attributes as $key => $name): ?>
                        <option value="<?= $key; ?>"><?= $name; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>

            <div class="form-group option-create">
                <label class="control-label" for="option_input">
                    <?= Yii::t("attribute", "Option field");?>
                </label>
                <input name="option_input" value="" data-type="option" class="form-control" disabled>
            </div>

            <div class="form-group">
                <?= Html::tag("span", Yii::t("system", "Add"), ['class' => 'btn btn-warning']) ?>
            </div>
        </div>
        <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12" id="form-create-both">
            <div class="form-group attribute-create">
                <label class="control-label" for="attribute_input">
                    <?= Yii::t("attribute", "Attribute field");?>
                </label>
                <input name="attribute_input" value="" data-type="attribute" class="form-control">
            </div>

            <div class="form-group option-create">
                <label class="control-label" for="option_input">
                    <?= Yii::t("attribute", "Option field");?>
                </label>
                <input name="option_input" value="" data-type="option" class="form-control" disabled>
            </div>

            <div class="form-group">
                <?= Html::tag("span", Yii::t("system", "Add"), ['class' => 'btn btn-danger']) ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12 col-md-12">
            <table class="table table-bordered" id="attributes-table">
                <thead>
                    <tr>
                        <th><?= Yii::t("attribute", "Name"); ?></th>
                        <th><?= Yii::t("attribute_option", "Name"); ?></th>
                        <th>&nbsp;</th>
                    </tr>
                </thead>
                <tbody>
                    <?php if(!empty($model->productAttributes)): ?>
                        <?php foreach($model->productAttributes as $productAttribute): ?>
                            <tr data-attribute-id="<?= $productAttribute->attribute_id;?>" data-option-id="<?= $productAttribute->attribute_option_id?>">
                                <td><?= $productAttribute->productAttribute->name; ?></td>
                                <td><?= $productAttribute->option->name; ?></td>
                            </tr>
                        <?php endforeach; ?>
                    <?php else: ?>
                        <tr class="empty">
                            <td>
                                <div class="empty">
                                    <?= Yii::t("system", "No data"); ?>
                                </div>
                            </td>
                            <td>&nbsp;</td>
                        </tr>
                    <?php endif; ?>
                </tbody>
            </table>
        </div>
    </div>
</div>