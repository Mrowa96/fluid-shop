<?php
    use backend\forms\Product as ProductForm;
    use common\models\Product;
    use yii\helpers\Html;

    /**
     * @var Product $model
     * @var ProductForm $productsForm
     */
?>

<div id="crossSellingWrap">
    <div id="form-cross-sell" class="row">
        <div class="col-lg-12 col-md-12">
            <?= $form->field($productsForm, 'products_list')->input("number",[ 'class' => 'productsList form-control', 'data-type' => 'product-id']); ?>
            <?= $form->field($productsForm, 'product_cost')->textInput(['data-type' => 'cost']); ?>

            <div class="form-group">
                <?= Html::tag("span", Yii::t("system", "Add"), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <table class="table table-bordered" id="cross-sells-table">
        <thead>
            <tr>
                <th><?= Yii::t("product", "Name"); ?></th>
                <th><?= Yii::t("product", "Product cost"); ?></th>
                <th>&nbsp;</th>
            </tr>
        </thead>
        <tbody>
            <?php if(!empty($model->crossSell)): ?>
                <?php foreach($model->crossSell as $sellCross): ?>
                    <tr data-product-id="<?= $sellCross->cross_product_id;?>" data-cost-id="<?= $sellCross->cost?>">
                        <td><?= $sellCross->product->name; ?></td>
                        <td><?= $sellCross->product->cost; ?></td>
                    </tr>
                <?php endforeach; ?>
            <?php else: ?>
                <tr class="empty">
                    <td>
                        <div class="empty">
                            <?= Yii::t("system", "No data"); ?>
                        </div>
                    </td>
                    <td>&nbsp;</td>
                </tr>
            <?php endif; ?>
        </tbody>
    </table>
</div>