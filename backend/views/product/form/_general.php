<?php
    use yii\helpers\Html;

    /**
     * @var \common\models\Product $model
     * @var \common\models\Category[] $categories
     * @var \common\models\Discount[] $discounts
     */
?>
<div class="product-general-form">
    <?php if($model->id): ?>
        <?= $form->field($model, 'id')->textInput(['disabled' => true]) ?>
    <?php endif; ?>
    <?= $form->field($model, 'category_id')->dropDownList(
        $categories,
        ['prompt'=> Yii::t("product", "Choose category")]
    ) ?>
    <?= $form->field($model, 'discount_id')->dropDownList(
        $discounts,
        ['prompt'=> Yii::t("product", "No discount")]
    ) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6, 'data-tinymce' => "true"]) ?>
    <?= $form->field($model, 'additional_field')->textarea(['rows' => 6, 'data-tinymce' => "true"]) ?>
    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'cost')->textInput() ?>
    <?= $form->field($model, 'quantity')->textInput() ?>
    <?= $form->field($model, 'recommended_product')->checkbox() ?>
    <?= $form->field($model, 'promotional_product')->checkbox() ?>
    <?= $form->field($model, 'check_out_product')->checkbox() ?>
    <?= $form->field($model, 'available')->checkbox() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>