<?php
    /**
     * @var \common\models\Product $model
     * @var \yii\widgets\ActiveForm $form
     */
?>
<div id="productImagesWrap">
    <div id="productImages" class="row">
        <?php if (!empty($model->imagesCustom)): ?>
            <?php foreach ($model->imagesCustom as $image): ?>
                <?php
                    if ($model->mainImage && $model->mainImage->id == $image->id) {
                        $class = "mainImage";
                    } else {
                        $class = "";
                    }
                ?>
                <div class="imageWrap col-lg-2 col-md-3 col-sm-6 col-xs-12 <?= $class; ?>">
                    <div class="innerWrapper">
                        <i class="fa fa-star icon" aria-hidden="true"></i>
                        <div class="image">
                            <img src="<?= $image->path; ?>" alt="<?= $image->name; ?>" data-id="<?= $image->id; ?>" class="img-responsive">
                        </div>
                        <div class="buttonsBar">
                            <i class="fa fa-trash deleteBtn" aria-hidden="true"></i>
                            <i class="fa fa-star mainBtn" aria-hidden="true"></i>
                            <i class="fa fa-undo revertBtn" aria-hidden="true"></i>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        <?php else: ?>
            <div class="emptyBlock col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <i class="fa fa-frown-o icon" aria-hidden="true"></i>
                <span class="text"><?= Yii::t("system", "There are no images to display"); ?></span>
            </div>
        <?php endif; ?>

        <input type="hidden" name="mainImage" id="mainImageInput" value="<?= isset($model->mainImage, $model->mainImage->id) ? $model->mainImage->id : '0'; ?>">
    </div>

    <?= $form->field($model, 'imageFiles[]')->fileInput(['multiple' => true, 'accept' => 'image/*']) ?>
</div>
