<?php
    use common\models\Product;
    use backend\forms\Product as ProductForm;
    use yii\helpers\Html;

    /**
     * @var Product $model
     * @var ProductForm $productsForm
     */
?>

<div id="upSellingWrap">
    <div id="form-up-sell" class="row">
        <div class="col-lg-12 col-md-12">
            <?= $form->field($productsForm, 'products_list')->input("number",[ 'class' => 'productsList form-control', 'data-type' => 'product-id']); ?>
            <?= $form->field($productsForm, 'product_cost')->textInput(['data-type' => 'cost']); ?>

            <div class="form-group">
                <?= Html::tag("span", Yii::t("system", "Add"), ['class' => 'btn btn-success']) ?>
            </div>
        </div>
    </div>

    <table class="table table-bordered" id="up-sells-table">
        <thead>
        <tr>
            <th><?= Yii::t("product", "Name"); ?></th>
            <th><?= Yii::t("product", "Product cost"); ?></th>
            <th>&nbsp;</th>
        </tr>
        </thead>
        <tbody>
        <?php if(!empty($model->upSell)): ?>
            <?php foreach($model->upSell as $upSell): ?>
                <tr data-product-id="<?= $upSell->up_product_id;?>" data-cost-id="<?= $upSell->cost?>">
                    <td><?= $upSell->product->name; ?></td>
                    <td><?= $upSell->product->cost; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php else: ?>
            <tr class="empty">
                <td>
                    <div class="empty">
                        <?= Yii::t("system", "No data"); ?>
                    </div>
                </td>
                <td>&nbsp;</td>
            </tr>
        <?php endif; ?>
        </tbody>
    </table>
</div>