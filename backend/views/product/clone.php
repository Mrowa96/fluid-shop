<?php
    $this->title = Yii::t("system", "Clone");;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Products"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-clone box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories,
            'discounts' => $discounts,
            'attributesForm' => $attributesForm,
            'productsForm' => $productsForm,
        ]) ?>
    </div>
</div>

