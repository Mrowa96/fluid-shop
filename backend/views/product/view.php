<?php
    use common\models\Product;
    use kartik\tabs\TabsX;
    use yii\helpers\Html;

    /**
     * @var Product $model
     */

    $this->title = Yii::t("system", "View") . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Products'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-view box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => $this->render("view/general", ['data' => $model]),
                    'label' => Yii::t("product", "General")
                ],
                [
                    'content' => $this->render("view/images", ['data' => $model]),
                    'label' => Yii::t("product", "Images")
                ],
                [
                    'content' => $this->render("view/attributes", ['data' => $attributes, 'model' => $model]),
                    'label' => Yii::t("product", "Attributes")
                ],
                [
                    'content' => $this->render("view/crossSell", ['data' => $crossSell, 'model' => $model]),
                    'label' => Yii::t("product", "Cross sell products")
                ],
                [
                    'content' => $this->render("view/upSell", ['data' => $upSell, 'model' => $model]),
                    'label' => Yii::t("product", "Up sell products")
                ],
                [
                    'content' => $this->render("view/reviews", ['data' => $reviews, 'model' => $model]),
                    'label' => Yii::t("product", "Reviews")
                ],
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('system', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>