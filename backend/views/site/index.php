<?php
    use common\models\User;
    use yii\helpers\Url;

    /**
     * @var int $ordersAmount
     * @var int $productsAmount
     * @var int $rmaAmount
     * @var int $clientsAmount
     * @var User[] $workers
     */

    $this->title = Yii::t("navigation", "Home");
?>

<div class="row">
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-aqua"><i class="fa fa-map"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= Yii::t("home", "Order quantity"); ?></span>
                <span class="info-box-number"><?= $ordersAmount; ?></span>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-red"><i class="fa fa-book"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= Yii::t("home", "Product quantity"); ?></span>
                <span class="info-box-number"><?= $productsAmount; ?></span>
            </div>
        </div>
    </div>

    <div class="clearfix visible-sm-block"></div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-green"><i class="fa fa-object-group"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= Yii::t("home", "RMA quantity"); ?></span>
                <span class="info-box-number"><?= $rmaAmount; ?></span>
            </div>
        </div>
    </div>

    <div class="col-md-3 col-sm-6 col-xs-12">
        <div class="info-box">
            <span class="info-box-icon bg-orange"><i class="fa fa-group"></i></span>
            <div class="info-box-content">
                <span class="info-box-text"><?= Yii::t("home", "Client quantity"); ?></span>
                <span class="info-box-number"><?= $clientsAmount; ?></span>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="box">
            <div class="box-header with-border">
                <h3 class="box-title"><?= Yii::t("home", "Order statistics"); ?></h3>
                <div class="box-tools pull-right">
                </div>
            </div>
            <div class="box-body">
                <div class="row">
                    <div class="col-md-12">
                        <p class="text-center">
                            <strong><?= Yii::t("home", "Order quantity in {year} year", ['year' => date("Y")]); ?></strong>
                        </p>
                        <div class="chart">
                            <canvas id="salesChart" style="height: 180px;"></canvas>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <?php if(isset($workers) && count($workers) > 0): ?>
        <div class="col-md-12">
            <div class="box box-danger">
                <div class="box-header with-border">
                    <h3 class="box-title"><?= Yii::t("system", "Workers"); ?></h3>
                    <div class="box-tools pull-right">
                        <span class="label label-danger"><?= count($workers); ?> <?= (count($workers) > 1) ? Yii::t("system", "Users (Plural)") : Yii::t("system", "Users (Singular)"); ?></span>
                    </div>
                </div>
                <div class="box-body no-padding">
                    <ul class="users-list clearfix">
                        <?php if (isset($workers) && !empty($workers)): ?>
                            <?php foreach ($workers AS $worker): ?>
                                <li>
                                    <a class="users-list-avatar" href="<?= Url::toRoute(["/user/view", 'id' => $worker->id]); ?>">
                                        <img src="<?= $worker->avatar; ?>" alt="User Avatar">
                                    </a>

                                    <a class="users-list-name" href="<?= Url::toRoute(["/user/view", 'id' => $worker->id]); ?>">
                                        <?= $worker->username; ?>
                                    </a>
                                    <span class="users-list-date"><?= $worker->created_at; ?></span>
                                </li>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </ul>
                </div>
                <div class="box-footer text-center">
                    <a href="<?= Url::toRoute("/user"); ?>" class="uppercase"><?= Yii::t("system", "Show all"); ?></a>
                </div>
            </div>
        </div>
    <?php endif; ?>
</div>