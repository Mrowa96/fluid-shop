<?php
    $this->title = Yii::t('page-category', 'Create category');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('page-category', 'Page Categories'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
