<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = Yii::t("page-category", "Category") . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('page-category', 'Page Categories'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-category-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
