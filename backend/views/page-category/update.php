<?php
    $this->title = Yii::t('page-category', 'Update category') . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('page-category', 'Page Categories'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="page-category-update box">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
