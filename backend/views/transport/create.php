<?php
    $this->title = Yii::t('system', 'Create');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Transports'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="transport-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
