<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="transport-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'range_from')->textInput() ?>
        <?= $form->field($model, 'range_to')->textInput() ?>
        <?= $form->field($model, 'cost')->textInput() ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <span>
            Ikona
        </span>
        <?= $form->field($model, 'iconFile')->fileInput(['accept' => 'image/*', 'value' => '']) ?>
        <?= $form->field($model, 'active')->checkbox() ?>
        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
