<?php
    use common\models\Language;
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\helpers\Url;

    /**
     * @var \yii\data\ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t("navigation", "Languages");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'name',
                            'symbol',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'template' => '{update}{manage}{delete} ',
                                'buttons' => [
                                    'view' => function () {
                                        return '';
                                    },
                                    'manage' => function ($url, $model) {
                                        /** @var Language $model */
                                        return '<a href="' . Url::toRoute(['language/manage', 'id' => $model->id]) . '" data-pjax="no"><i class="fa fa-braille"></i></a>';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("user", "All users")
                ],
                [
                    'label' => Yii::t("language", "Add language"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/language/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
