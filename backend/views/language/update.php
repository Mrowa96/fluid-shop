<?php
    $this->title = Yii::t("language", "Update language");;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Languages"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="language-update box box-form">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
