<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="product-extra-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'cost')->textInput() ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'quantity')->input("number") ?>
        <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
        <span>Obrazek</span>
        <?= $form->field($model, 'imageFile')->fileInput(['accept' => 'image/*']) ?>
        <?= $form->field($model, 'available')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
