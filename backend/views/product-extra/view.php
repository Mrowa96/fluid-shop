<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Extras'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-extra-view box">
    <div  class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                [
                    'attribute' => 'image',
                    'value' => ($model->image) ? "<div class='row'><div class='col-lg-1'><img src='".$model->image->thumb_path."' class='img-responsive'></div></div>" : Yii::t("system", "No data"),
                    'format' => 'html'
                ],
                'description:ntext',
                'cost',
                'quantity',
                'available_text',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
