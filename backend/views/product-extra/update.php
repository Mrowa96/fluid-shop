<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Extras'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-extra-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>