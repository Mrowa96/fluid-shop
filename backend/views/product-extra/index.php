<?php
    use backend\searches\Product;
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     * @var Product $searchModel
     */

    $this->title = Yii::t("navigation", "Extras");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box product-extra-index">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'name',
                            'cost',
                            'available_text',
                            ['class' => '\kartik\grid\ActionColumn',]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ],
                [
                    'label' => Yii::t("product_extra", "Create new product"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/product-extra/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
