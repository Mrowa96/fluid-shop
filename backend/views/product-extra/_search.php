<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="product-extra-search">
    <?php $form = ActiveForm::begin([
        'action' => ['index'],
        'method' => 'get',
    ]); ?>

        <?= $form->field($model, 'name') ?>
        <?= $form->field($model, 'cost') ?>
        <?= $form->field($model, 'available_text') ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('system', 'Search'), ['class' => 'btn btn-primary']) ?>
            <?= Html::resetButton(Yii::t('system', 'Reset'), ['class' => 'btn btn-default']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
