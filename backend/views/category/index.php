<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t("navigation", "Categories");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box category-index">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'name',
                            'parentName',
                            ['class' => '\kartik\grid\ActionColumn']
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ],
                [
                    'label' => Yii::t("category", "Add category"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/category/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
