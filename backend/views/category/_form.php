<?php
    use yii\widgets\ActiveForm;
    use fluid\elements\TabbedView;
?>
<div class="category-form">
    <?php $form = ActiveForm::begin(); ?>

    <?=  $this->render("__general", [
        'form' => $form,
        'model' => $model,
        'discounts' => $discounts,
        'categories' => $categories
    ]); ?>
   
    <?php ActiveForm::end(); ?>
</div>
