<?php
    $this->title = Yii::t('status', 'Update') . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Categories"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories,
            'discounts' => $discounts,
        ]) ?>
    </div>
</div>
