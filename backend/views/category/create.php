<?php
    $this->title = Yii::t('system', 'Create');
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Categories"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="category-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'categories' => $categories,
            'discounts' => $discounts,
        ]) ?>
    </div>
</div>
