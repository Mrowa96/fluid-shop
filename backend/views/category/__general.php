<?php
    use yii\helpers\Html;
?>
<div class="category-form">
    <?= $form->field($model, 'parent_id')->dropDownList(
        $categories,
        ['prompt' => Yii::t("category", "Select parent category")]
    ) ?>
    <?= $form->field($model, 'discount_id')->dropDownList(
        $discounts,
        ['prompt' => Yii::t("category", "Select discount for all products in this category")]
    ) ?>
    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>
    <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
    <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>
</div>