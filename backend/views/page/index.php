<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t('navigation', 'Pages');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            'title',
                            'add_to_header_text',
                            'add_to_footer_text',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'template' => '{redirect}{view}{update}{delete}',
                                'buttons' => [
                                    'redirect' => function ($url) {
                                        return '<a href="' . $url . '" data-pjax="no""><i class="fa fa-share"></i></a>';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ],
                [
                    'label' => Yii::t("page", "Add page"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/page/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
