<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Pages'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->title, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="page-update box">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
