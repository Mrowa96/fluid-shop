<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Pages'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="page-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'title',
                'content:html',
                'seo_keywords',
                'seo_description:ntext',
                'add_to_header_text',
                'add_to_footer_text',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
