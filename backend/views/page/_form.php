<?php
    use common\models\Page;
    use yii\helpers\Html;
    use yii\web\View;
    use yii\widgets\ActiveForm;
    use common\models\PageCategory;

    $this->registerJsFile("/assets/dist/vendor/tinymce/tinymce.min.js", ['position' => View::POS_HEAD]);
?>

<div class="page-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'page_category_id')->dropDownList(PageCategory::prepareForForm(), [
            'prompt' => Yii::t("page", "Select page category")
        ]) ?>
        <?= $form->field($model, 'type')->dropDownList(Page::getTypes(), [
            'prompt' => Yii::t("page", "Select page type")
        ]) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 6, 'data-tinymce' => 'true']) ?>
        <?= $form->field($model, 'seo_keywords')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'seo_description')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'add_to_header')->checkbox() ?>
        <?= $form->field($model, 'add_to_footer')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
