<?php
    $this->title = Yii::t("user", "Add user");;
    $this->params['breadcrumbs'][] = ['label' => Yii::t("navigation", "Users"), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'roles' => $roles,
            'avatars' => $avatars,
        ]) ?>
    </div>
</div>
