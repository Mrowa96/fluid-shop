<?php
    use backend\forms\User as UserForm;
    use common\models\Language;
    use common\models\Settings;
    use common\models\Settings\System;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
     * @var UserForm $model
     * @var array $roles
     */
?>
<div class="user-form">
    <?php $form = ActiveForm::begin([
        'id' => 'user-form',
        'enableAjaxValidation' => true,
    ]); ?>
    <?= $form->field($model, 'username') ?>
    <?= $form->field($model, 'name') ?>
    <?= $form->field($model, 'email') ?>
    <?= $form->field($model, 'password')->passwordInput() ?>
    <?= $form->field($model, 'password_repeat')->passwordInput() ?>
    <?= $form->field($model, 'role')->dropDownList($roles, [
            'prompt' => Yii::t("system", "Choose account type")
        ]); ?>

    <?php if (Settings::getOne('language_level', System::MODULE) === "user"): ?>
        <?= $form->field($model, 'language')->dropDownList(Language::prepareForForm("backend"), [
                'prompt' => Yii::t("settings", "Choose language")
            ]); ?>
    <?php endif; ?>
    <div class="form-group" id="avatars-block">
        <?= $form->field($model, 'avatar')->hiddenInput() ?>

        <ul class="avatars-list">
            <?php if (!empty($avatars)): ?>
                <?php foreach ($avatars as $avatar): ?>
                    <li>
                        <img src="<?= Yii::getAlias("@dataLink") . '/' . $avatar; ?>" alt="Avatar" class="img-responsive" data-url="<?= $avatar; ?>">
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>

    <div class="form-group">
        <?= Html::submitButton($model->exists() ? Yii::t("system", "Create") : Yii::t("system", "Update"), ['class' => $model->exists() ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
