<?php
    use common\models\User;
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = Yii::t("system", "View") . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => "Użytkownicy", 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;

    /**
     * @var User $model
     */
?>
<div class="box user-profile">
    <div class="box-header">
        <img src="<?= $model->avatar; ?>" class="img-responsive img-circle center-block">
    </div>

    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'username',
                'name',
                'email:email',
                [
                    'label' => Yii::t("user", "Role"),
                    'value' => function ($model) {
                        return $model->role->description;
                    }
                ],
                'created_at',
            ],
        ]) ?>
    </div>
    <?php if (Yii::$app->user->can("update users") || (Yii::$app->user->can("update self user")) && Yii::$app->user->getId() === $model->id): ?>
        <div class="box-footer">
            <?= Html::a(Yii::t("system", "Update"), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        </div>
    <?php endif; ?>
</div>
