<?php
    use common\models\User;
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\helpers\Url;

    /**
     * @var \yii\data\ActiveDataProvider $dataProvider
     * @var \backend\searches\User $searchModel
     */

    $this->title = Yii::t("navigation", "Users");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box user-index">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'filterModel' => $searchModel,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            [
                                'attribute' => 'avatar',
                                'format' => 'html',
                                'value' => function ($model) {
                                    /** @var User $model */
                                    return "<img src='$model->avatar' class='img-responsive img-avatar'>";
                                }
                            ],
                            'name',
                            'email',
                            ['class' => '\kartik\grid\ActionColumn']
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("user", "All users")
                ],
                [
                    'label' => Yii::t("system", "Create"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/user/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
