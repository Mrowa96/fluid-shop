<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;

    /**
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t('navigation', 'Statuses');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="order-status-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dataProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'status_name',
                            'message',
                            'description',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function () {
                                        return '';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("system", "All")
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
