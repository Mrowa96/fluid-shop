<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="order-status-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'message')->textarea(['rows' => 6]) ?>
        <?= $form->field($model, 'description')->textarea(['rows' => 6]) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
