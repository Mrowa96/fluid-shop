<?php
    $this->title = Yii::t('system', 'Create');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Attributes'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
