<?php
    use backend\searches\Attribute;
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     * @var Attribute $searchModel
     */

    $this->title = Yii::t('navigation', 'Attributes');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-content">
        <div class="attribute-index">
            <?= TabsX::widget([
                'enableStickyTabs' => true,
                'items' => [
                    [
                        'content' => GridView::widget([
                            'dataProvider' => $dataProvider,
                            'filterModel' => $searchModel,
                            'columns' => [
                                ['class' => '\kartik\grid\SerialColumn'],
                                'name',
                                ['class' => '\kartik\grid\ActionColumn']
                            ],
                            'responsive' => true,
                            'export' => false,
                            'pjax' => true,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                            ]
                        ]),
                        'label' => Yii::t("user", "All users")
                    ],
                    [
                        'label' => Yii::t("system", "Create"),
                        'linkOptions' => [
                            'data-url' => Url::to(['/attribute/create']),
                            'class' => 'link link-success'
                        ],
                        'headerOptions' => [
                            'class' => 'link-parent'
                        ]
                    ]
                ],
                'position' => TabsX::POS_ABOVE,
                'encodeLabels' => false,
                'fade' => false
            ]); ?>
        </div>
    </div>
</div>

