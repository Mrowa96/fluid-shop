<?php
    use backend\models\Attribute;
    use kartik\grid\GridView;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Html;
    use yii\helpers\Url;
    use yii\widgets\DetailView;

    /**
     * @var Attribute $model
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Attributes'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="attribute-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                [
                    'attribute' => 'options',
                    'format' => 'raw',
                    'value' => '<div class="optionsList">' .
                        GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                'name',
                                [
                                    'class' => '\kartik\grid\ActionColumn',
                                    'buttons' => [
                                        'view' => function () {
                                            return '';
                                        },
                                        'update' => function ($model, $optionModel) use ($model) {
                                            return '<a href="' . Url::toRoute(['attribute-option/update', 'id' => $optionModel->id, 'attributeId' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-pencil"></span></a>';
                                        },
                                        'delete' => function ($model, $optionModel) use ($model) {
                                            return '<a href="' . Url::toRoute(['attribute-option/delete', 'id' => $optionModel->id, 'attributeId' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-trash"></span></a>';
                                        },
                                    ],
                                ]
                            ],
                            'layout' => "{items}\n{pager}",
                            'responsive' => true,
                            'export' => false,
                        ]) . '
                        </div>'
                ]
            ],
        ]) ?>
    </div>

    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('system', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>
