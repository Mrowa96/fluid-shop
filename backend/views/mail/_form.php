<?php
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /** @var \backend\models\Mail $model */
?>

<div class="mail-form">
    <?php $form = ActiveForm::begin(['id' => 'mailForm']); ?>
        <?= $form->field($model, 'mail_message_id')->dropDownList(
            $messages,
            ['prompt' => Yii::t("mail", "Chose the message")]
        ) ?>
        <?= $form->field($model, 'send_date')->input("datetime-local") ?>
        <?= $form->field($model, 'send_to')->input("hidden", ['id' => 'hiddenInput'])->label(false) ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t('mail', 'To All'), ['class' => 'btn btn-success', 'id' => 'toAllBtn', 'data-send-to' => 'all']) ?>
            <?= Html::submitButton(Yii::t('mail', 'To Buyers'), ['class' => 'btn btn-info', 'id' => 'toBuyersBtn', 'data-send-to' => 'toBuyers']) ?>
            <?= Html::submitButton(Yii::t('mail', 'To NonBuyers'), ['class' => 'btn btn-warning', 'id' => 'toNonBuyersBtn', 'data-send-to' => 'toNonBuyers']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
