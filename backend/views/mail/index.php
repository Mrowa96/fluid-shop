<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $newsletters
     * @var ActiveDataProvider $messagesProvider
     * @var ActiveDataProvider $messagesPartsProvider
     * @var ActiveDataProvider $subscribersProvider
     */
    $this->title = Yii::t('navigation', 'Mailing');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="newsletter-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $newsletters,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'message.title',
                            'status_text',
                            'send_date',
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("mail", "Send")
                ],
                [
                    'content' => GridView::widget([
                        'dataProvider' => $messagesProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'title',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        /** @var \backend\models\MailMessage $model */
                                        if ($model->deletable == 1) {
                                            return '<a href="' . Url::toRoute(['mail-message/delete', 'id' => $model->id]) . ' data-pjax="no"><span class="glyphicon glyphicon-trash"></span></a>';
                                        }
                                    },
                                    'create' => function () {
                                        return '';
                                    },
                                    'update' => function () {
                                        return '';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("mail", "Messages")
                ],
                [
                    'content' => GridView::widget([
                        'dataProvider' => $messagesPartsProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'title',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'buttons' => [
                                    'update' => function ($url, $model) {
                                        return '<a href="' . Url::toRoute(['mail-message-part/update', 'id' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-pencil"></span></a>';
                                    },
                                    'create' => function () {
                                        return '';
                                    },
                                    'delete' => function () {
                                        return '';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("mail", "Message parts")
                ],
                [
                    'content' => GridView::widget([
                        'dataProvider' => $subscribersProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'name',
                            'email',
                            'join_date',
                            [
                                'class' => '\kartik\grid\ActionColumn',
                                'buttons' => [
                                    'delete' => function ($url, $model) {
                                        return '<a href="' . Url::toRoute(['mail-subscriber/delete', 'id' => $model->id]) . '" data-pjax="no"><span class="glyphicon glyphicon-trash"></span></a>';
                                    },
                                    'create' => function () {
                                        return '';
                                    },
                                    'update' => function () {
                                        return '';
                                    }
                                ]
                            ]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("mail", "Subscribers")
                ],
                [
                    'label' => Yii::t("mail", "Send message"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/mail/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => ['class' => 'link-parent']
                ],
                [
                    'label' => Yii::t("mail", "Create message"),
                    'linkOptions' => [
                        'data-url' => Url::to(['mail-message/create']),
                        'class' => 'link link-blue'
                    ],
                    'headerOptions' => ['class' => 'link-parent']
                ],
                [
                    'label' => Yii::t("mail", "Import subscribers"),
                    'linkOptions' => [
                        'data-url' => Url::to(['mail-subscriber/import']),
                        'class' => 'link link-yellow'
                    ],
                    'headerOptions' => ['class' => 'link-parent']
                ],
                [
                    'label' => Yii::t("mail", "Export subscribers"),
                    'linkOptions' => [
                        'data-url' => Url::to(['mail-subscriber/export']),
                        'class' => 'link link-brown'
                    ],
                    'headerOptions' => ['class' => 'link-parent']
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
