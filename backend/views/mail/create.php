<?php
    $this->title = Yii::t('mail', 'Send message');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailing'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'messages' => $messages,
        ]) ?>
    </div>
</div>
