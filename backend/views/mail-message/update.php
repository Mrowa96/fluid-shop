<?php
    $this->title = Yii::t('system', 'Update') . ' - ' .  $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('mail', 'Mailing'), 'url' => ['mail/index']];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="mail-message-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
