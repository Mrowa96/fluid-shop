<?php
    use yii\helpers\Html;
    use yii\web\View;
    use yii\widgets\ActiveForm;

    $this->registerJsFile("/assets/dist/vendor/tinymce/tinymce.min.js", ['position' => View::POS_HEAD]);
?>

<div class="mail-message-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

        <?php if(!empty($model->tags)): ?>
            <div class="form-message form-message-primary">
                <p class="text-big">
                    <?= Yii::t("mail_message", "In this message, you can use tags, which will be replaced by content during send.");?>
                </p>
                <ul>
                    <?php foreach($model->tags AS $tag): ?>
                        <li>{<?= $tag->name; ?>} - <?= Yii::t("mail", $tag->description); ?></li>
                    <?php endforeach; ?>
                </ul>
            </div>
        <?php endif; ?>

        <?= $form->field($model, 'content')->textarea(['rows' => 6, 'data-tinymce' => 'true']) ?>
        <?= $form->field($model, 'add_header')->checkbox() ?>
        <?= $form->field($model, 'add_footer')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
