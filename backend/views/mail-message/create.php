<?php
    $this->title = Yii::t('mail_message', 'Create Message');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailing'), 'url' => ['mail/index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="mail-message-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
