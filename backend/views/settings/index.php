<?php
    use common\models\Settings\Information;
    use common\models\Settings\PayPal;
    use common\models\Settings\PayU;
    use common\models\Settings\Smtp;
    use common\models\Settings\System;
    use kartik\tabs\TabsX;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
     * @var System $system
     * @var array $languages
     * @var Information $information
     * @var Smtp $smtp
     * @var PayU $payu
     * @var PayPal $paypal
     */

    $this->title = Yii::t("navigation", "Settings");
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="settings-update box">
    <div class="box-content">
        <?php $form = ActiveForm::begin([
            'id' => 'settings-form',
        ]); ?>

        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => $this->render("_system", ['form' => $form, 'model' => $system, 'languages' => $languages]),
                    'label' => Yii::t("settings", "System")
                ],
                [
                    'content' => $this->render("_general", ['form' => $form, 'model' => $information]),
                    'label' => Yii::t("settings", "Information")
                ],
                [
                    'content' => $this->render("_smtp", ['form' => $form, 'model' => $smtp]),
                    'label' => Yii::t("settings", "SMTP")
                ],
                [
                    'content' => $this->render("_payu", ['form' => $form, 'model' => $payu]),
                    'label' => Yii::t("settings", "PayU")
                ],
                [
                    'content' => $this->render("_paypal", ['form' => $form, 'model' => $paypal]),
                    'label' => Yii::t("settings", "PayPal")
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>

        <div class="form-group">
            <?= Html::submitButton(Yii::t("system", "Update"), ['class' => 'btn btn-primary']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>