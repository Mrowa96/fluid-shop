<div class="payU">
    <?= $form->field($model, 'environment'); ?>
    <?= $form->field($model, 'oauth_client_id'); ?>
    <?= $form->field($model, 'oauth_client_secret'); ?>
    <?= $form->field($model, 'active')->checkbox(); ?>
</div>