<div class="payPal">
    <?= $form->field($model, 'environment'); ?>
    <?= $form->field($model, 'client_id'); ?>
    <?= $form->field($model, 'client_secret'); ?>
    <?= $form->field($model, 'active')->checkbox(); ?>
</div>