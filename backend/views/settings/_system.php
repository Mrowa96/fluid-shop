<?php
    use common\models\Settings;
    use common\models\Settings\System;
    use common\utilities\Sitemap;
    use yii\helpers\Url;

    /** @var System $model */
?>
<div class="system">
    <?= $form->field($model, 'site_title'); ?>
    <?= $form->field($model, 'footer_text'); ?>
    <?= $form->field($model, 'currency'); ?>

    <div class="form-group">
        <div class="form-message form-message-primary">
            <?= Yii::t("settings", "Language level is only applied for panel."); ?>
        </div>
    </div>

    <?= $form->field($model, 'language_level')->dropDownList(System::getLanguageLevel(), [
        'prompt' => Yii::t("settings", "Choose language level")
    ]) ?>

    <?php if (Settings::getOne('language_level', System::MODULE) === "global"): ?>
        <?= $form->field($model, 'language')->dropDownList($languages, [
            'prompt' => Yii::t("settings", "Choose language")
        ]) ?>
    <?php endif; ?>

    <?= $form->field($model, 'dropped_basket_remind')->dropDownList(
        System::getRemindBasketTimes(),
        ['prompt' => 'Wybierz czas po jakim zostanie wyłane powiadomienie do klienta']
    ); ?>

    <?php
        /*
            <div class="form-group">
                <label class="control-label"><?= Yii::t("settings", "Logo");?></label>
                <?php if(Settings::getOne('logo', System::MODULE)):?>
                    <img src="<?= Yii::getAlias("@dataLink") .'/logos/' . Settings::getOne('logo', System::MODULE);?>" class="img-responsive">
                <?php endif; ?>
                <?= $form->field($model, 'logoFile')->fileInput() ?>
            </div>
         */
    ?>
</div>