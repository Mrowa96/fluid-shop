<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Payments'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="payment-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'apiData' => $apiData,
        ]) ?>
    </div>
</div>