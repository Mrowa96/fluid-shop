<?php
    $this->title = Yii::t('system', 'Create');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Payments'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-create box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
            'apiData' => $apiData,
        ]) ?>
    </div>
</div>