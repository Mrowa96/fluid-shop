<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Payments'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="payment-view box">
    <div  class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'name',
                'cost',
                [
                    'attribute' => 'icon',
                    'value' => "<div class='row'><div class='col-lg-1'><img src='".$model->icon."' class='img-responsive'></div></div>",
                    'format' => 'html'
                ],
                'description:ntext',
                'active',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
