<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dataProvider
     */

    $this->title = Yii::t('navigation', 'Payments');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="box">
    <div class="box-content">
        <div class="payment-index">
            <?= TabsX::widget([
                'enableStickyTabs' => true,
                'items' => [
                    [
                        'content' => GridView::widget([
                            'dataProvider' => $dataProvider,
                            'columns' => [
                                ['class' => '\kartik\grid\SerialColumn'],
                                array(
                                    'attribute' => 'icon',
                                    'format' => 'html',
                                    'value' => function ($model) {
                                        /** @var \common\models\Payment $model */
                                        return "<img src='$model->icon' class='img-responsive img-icon'>";
                                    }
                                ),
                                'name',
                                'cost',
                                ['class' => '\kartik\grid\ActionColumn',]
                            ],
                            'responsive' => true,
                            'export' => false,
                            'pjax' => true,
                            'pjaxSettings' => [
                                'neverTimeout' => true,
                            ]
                        ]),
                        'label' => Yii::t("system", "All")
                    ],
                    [
                        'label' => Yii::t("payment", "Add payment"),
                        'linkOptions' => [
                            'data-url' => Url::to(['/payment/create']),
                            'class' => 'link link-success'
                        ],
                        'headerOptions' => [
                            'class' => 'link-parent'
                        ]
                    ]
                ],
                'position' => TabsX::POS_ABOVE,
                'encodeLabels' => false,
                'fade' => false
            ]); ?>
        </div>
    </div>
</div>
