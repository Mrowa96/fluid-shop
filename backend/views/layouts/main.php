<?php
    use backend\assets\AppAsset;
    use common\components\AlertWidget;
    use yii\helpers\Html;
    use yii\widgets\Breadcrumbs;
    use \common\models\Settings;

    AppAsset::register($this);
?>

<?php $this->beginPage() ?>
<!DOCTYPE html>

<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
        <?= Html::csrfMetaTags() ?>

        <title><?= Html::encode($this->title) ?></title>
        <?php $this->head() ?>
    </head>

    <body class="skin-yellow">
        <?php $this->beginBody() ?>

        <div class="wrapper" >
            <?= $this->render("@app/views/partials/header"); ?>
            <?= $this->render("@app/views/partials/sidebar"); ?>
            <?= $this->render("@app/views/partials/loadScreen"); ?>

            <div class="content-wrapper">
                <section class="content-header">
                    <div class="hidden-xs hidden-sm">
                        <?php if(Yii::$app->request->referrer): ?>
                            <a href="<?= Yii::$app->request->referrer; ?>" class="back">
                                <i class="fa fa-angle-left"></i>
                            </a>
                        <?php endif; ?>

                        <h1 class="title"><?= $this->title; ?></h1>
                    </div>

                    <?= Breadcrumbs::widget([
                        'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
                        'homeLink' => [
                            'label' => Settings::getOne('site_title', Settings\System::MODULE)
                        ]
                    ]) ?>
                </section>

                <div class="clearFloat"></div>

                <section class="content">
                    <?php
                        if(isset($content)){
                            echo $content;
                        }
                    ?>
                </section>
            </div>
            <?= AlertWidget::widget();?>
            <?= $this->render("@app/views/partials/footer"); ?>
        </div>
        <?php $this->endBody() ?>
    </body>
</html>
<?php $this->endPage() ?>


