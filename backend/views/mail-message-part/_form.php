<?php
    use yii\helpers\Html;
    use yii\web\View;
    use yii\widgets\ActiveForm;

    $this->registerJsFile("/assets/dist/vendor/tinymce/tinymce.min.js", ['position' => View::POS_HEAD]);
?>

<div class="mail-message-part-form">
    <?php $form = ActiveForm::begin(); ?>
        <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
        <?= $form->field($model, 'content')->textarea(['rows' => 6, 'data-tinymce' => 'true']) ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
