<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . $model->title;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Mailing'), 'url' => ['mail/index']];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="mail-message-part-update box">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
