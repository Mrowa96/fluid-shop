<?php
    use kartik\grid\GridView;
    use kartik\tabs\TabsX;
    use yii\data\ActiveDataProvider;
    use yii\helpers\Url;

    /**
     * @var ActiveDataProvider $dynamicProvider
     * @var ActiveDataProvider $staticProvider
     */
    $this->title = Yii::t('navigation', 'Discounts');
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-index box">
    <div class="box-content">
        <?= TabsX::widget([
            'enableStickyTabs' => true,
            'items' => [
                [
                    'content' => GridView::widget([
                        'dataProvider' => $dynamicProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'threshold_percentage',
                            'threshold_numeric',
                            'code',
                            'amount',
                            'expire',
                            ['class' => '\kartik\grid\ActionColumn',]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("discount", "Dynamic")
                ],
                [
                    'content' => GridView::widget([
                        'dataProvider' => $staticProvider,
                        'columns' => [
                            ['class' => '\kartik\grid\SerialColumn'],
                            'threshold_percentage',
                            'threshold_numeric',
                            ['class' => '\kartik\grid\ActionColumn',]
                        ],
                        'responsive' => true,
                        'export' => false,
                        'pjax' => true,
                        'pjaxSettings' => [
                            'neverTimeout' => true,
                        ]
                    ]),
                    'label' => Yii::t("discount", "Static")
                ],
                [
                    'label' => Yii::t("discount", "Add discount"),
                    'linkOptions' => [
                        'data-url' => Url::to(['/discount/create']),
                        'class' => 'link link-success'
                    ],
                    'headerOptions' => [
                        'class' => 'link-parent'
                    ]
                ]
            ],
            'position' => TabsX::POS_ABOVE,
            'encodeLabels' => false,
            'fade' => false
        ]); ?>
    </div>
</div>
