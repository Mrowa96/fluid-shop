<?php
    $this->title = Yii::t('system', 'Update') . ' - ' . Yii::t('discount', 'Discount') . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Discounts'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="discount-update box">
    <div class="box-content">
        <?= $this->render('_form', [
            'model' => $model,
        ]) ?>
    </div>
</div>
