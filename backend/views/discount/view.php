<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = Yii::t("discount", "Discount") . ' - ' . $model->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Discounts'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="discount-view box">
    <div class="box-content">
        <?= DetailView::widget([
            'model' => $model,
            'attributes' => [
                'threshold_percentage',
                'threshold_numeric',
                'code',
                'amount',
                'expire',
            ],
        ]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => Yii::t('system', 'Are you sure you want to delete this item?'),
                'method' => 'post',
            ],
        ]) ?>
    </div>
</div>
