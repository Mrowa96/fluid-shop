<?php
    use common\models\Discount;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;

    /**
    * @var Discount $model
     */
?>

<div class="discount-form">
    <?php $form = ActiveForm::begin(); ?>
        <div class="row">
            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'threshold_percentage')->input("number") ?>
            </div>
            <div class="col-lg-6 col-md-6">
                <?= $form->field($model, 'threshold_numeric')->input("number") ?>
            </div>
        </div>

        <div class="row">
            <div class="col-lg-11 col-md-10 col-sm-8 col-xs-7">
                <?= $form->field($model, 'code')->textInput() ?>
            </div>
            <div class="col-lg-1 col-md-2 col-sm-4 col-xs-5">
                <span class="btn btn-warning generateCodeBtn"><?= Yii::t("discount", "Generate code"); ?></span>
            </div>
        </div>

        <?= $form->field($model, 'amount')->input("number") ?>
        <?= $form->field($model, 'expire')->input("datetime-local") ?>

        <div class="form-group">
            <div class="form-message form-message-info">
                <?= Yii::t("discount", "Static (not dynamic) discounts can be used to lower cost of some products. Dynamic discount are passed by user in basket, and lower cost of all items in the basket."); ?>
            </div>
        </div>

        <?= $form->field($model, 'dynamic')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>
</div>
