<?php
    use yii\helpers\Html;
    use yii\widgets\DetailView;

    $this->title = $model->product->name;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Product of a day'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-of-aday-view box">
    <div class="box-content">
<?= DetailView::widget([
    'model' => $model,
    'attributes' => [
            'product_id',
            'cost',
            'quantity',
            'expire_date',
    ],
]) ?>
    </div>
    <div class="box-footer">
        <?= Html::a(Yii::t('system', 'Update'), ['update', 'id' => $model->id], ['class' => 'btn btn-primary']) ?>
        <?= Html::a(Yii::t('system', 'Delete'), ['delete', 'id' => $model->id], ['class' => 'btn btn-danger']) ?>
    </div>
</div>
