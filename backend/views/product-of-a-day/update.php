<?php
    $this->title = Yii::t('system', 'Update') . ' ' . $model->product->name;;
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Product of a day'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = ['label' => $model->product->name, 'url' => ['view', 'id' => $model->id]];
    $this->params['breadcrumbs'][] = Yii::t('system', 'Update');
?>
<div class="product-of-aday-update box">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
