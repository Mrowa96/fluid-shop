<?php
    use common\models\Product;
    use kartik\datetime\DateTimePicker;
    use kartik\select2\Select2;
    use yii\helpers\Html;
    use yii\widgets\ActiveForm;
?>

<div class="product-of-aday-form">
    <?php $form = ActiveForm::begin(); ?>
        <?php /* $form->field($model, 'product_id')->widget(Select2::classname(), [
            'data' => Product::prepareForForm(),
            'options' => ['placeholder' => Yii::t("product", "Choose product"), 'id' => 'productInput'],
            'theme' => Select2::THEME_DEFAULT,
            'pluginOptions' => [
                'allowClear' => false
            ],
        ]) */?>

        <?= $form->field($model, 'product_id')->input("number", ['placeholder' => 'id']) ?>
        <?= $form->field($model, 'cost')->textInput() ?>
        <?= $form->field($model, 'quantity')->input("number") ?>
        <?= $form->field($model, 'expire_date')->widget(DateTimePicker::className(),[
            'name' => 'expire_date',
            'type' => DateTimePicker::TYPE_INPUT,
            'options' => ['id' => 'expireDateInput'],
            'convertFormat' => true,
            'pluginOptions' => [
                'autoclose' => true,
                'format' => 'yyyy-MM-dd H:i:s',
                'todayHighlight' => true
            ]
        ]) ?>

        <?= $form->field($model, 'active')->checkbox() ?>

        <div class="form-group">
            <?= Html::submitButton($model->isNewRecord ? Yii::t('system', 'Create') : Yii::t('system', 'Update'), ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div>
