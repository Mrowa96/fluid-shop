<?php
    $this->title = Yii::t('product-of-a-day', 'Assign product');
    $this->params['breadcrumbs'][] = ['label' => Yii::t('navigation', 'Product of a day'), 'url' => ['index']];
    $this->params['breadcrumbs'][] = $this->title;
?>
<div class="product-of-aday-create box">
    <div class="box-content">
        <?= $this->render('_form', [
        'model' => $model,
        ]) ?>
    </div>
</div>
