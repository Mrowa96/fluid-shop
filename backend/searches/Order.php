<?php
    namespace backend\searches;

    use Yii;
    use yii\base\Model;
    use yii\data\ActiveDataProvider;
    use common\models\Order as OrderModel;
    use common\models\OrderStatus;

    class Order extends OrderModel
    {
        public $addressName;
        public $addressSurname;

        public function rules()
        {
            return [
                [['id', 'address_id', 'transport_id', 'payment_id', 'order_status_id', 'key'], 'integer'],
                [['cost'], 'number'],
                [['create_date', 'orderStatus.status_name', 'address.fullName'], 'safe'],
            ];
        }

        public function attributes()
        {
            return array_merge(parent::attributes(), ['orderStatus.status_name', 'address.fullName']);
        }

        public function scenarios()
        {
            return Model::scenarios();
        }

        public function load($data, $formName = null)
        {
            if(isset($data['OrderSearch']['address.fullName'])){
                $fullName = explode(" ", $data['OrderSearch']['address.fullName']);

                if(count($fullName) === 1){
                    $this->addressName = $fullName[0];
                    $this->addressSurname = $fullName[0];
                }
                else if(count($fullName) === 2){
                    $this->addressName = $fullName[0];
                    $this->addressSurname = $fullName[1];
                }
            }

            return parent::load($data, $formName);
        }

        public function search($params, $type)
        {
            switch($type){
                case "inProgress":
                    $query = OrderModel::find()
                        ->where('order_status_id<' . OrderStatus::STATUS_FINISHED)
                        ->andWhere('order_status_id>' . OrderStatus::STATUS_NEW_NOT_COMPLETE);
                    break;
                case "completed":
                    $query = OrderModel::find()
                        ->where('order_status_id=' . OrderStatus::STATUS_FINISHED)
                        ->andWhere('order_status_id>' . OrderStatus::STATUS_NEW_NOT_COMPLETE);
                    break;
                case "canceled":
                    $query = OrderModel::find()
                        ->where('order_status_id>' . OrderStatus::STATUS_FINISHED)
                        ->andWhere('order_status_id>' . OrderStatus::STATUS_NEW_NOT_COMPLETE);
                    break;
                default:
                    $query = OrderModel::find()->andWhere('order_status_id>' . OrderStatus::STATUS_NEW_NOT_COMPLETE);
                    break;
            }

            $query->andWhere(OrderModel::tableName() . '.deleted = 0');
            $query->joinWith('orderStatus AS orderStatus');
            $query->joinWith('address AS address');

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'create_date' => SORT_DESC
                    ]
                ]
            ]);

            $this->load($params);

            if (!$this->validate()) {
                return $dataProvider;
            }

            $query->andFilterWhere(['like', 'cost', $this->cost])
                ->andFilterWhere(['like', 'address.name', $this->addressName])
                ->orFilterWhere(['like', 'address.surname', $this->addressSurname])
                ->andFilterWhere(['like', 'create_date', $this->create_date])
                ->andFilterWhere(['like', 'orderStatus.status_name', $this->getAttribute('orderStatus.status_name')])
                ->andFilterWhere(['like', 'key', $this->key]);

            return $dataProvider;
        }
    }
