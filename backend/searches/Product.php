<?php
    namespace backend\searches;
    
    use Yii;
    use yii\base\Model;
    use yii\data\ActiveDataProvider;
    use common\models\Product as ProductModel;
    
    class Product extends ProductModel
    {
        public function rules()
        {
            return [
                [['id', 'category_id', 'discount_id', 'quantity', 'available'], 'integer'],
                [['name', 'processed_name', 'description', 'additional_field', 'code', 'modified_date', 'created_date', 'available_text'], 'safe'],
                [['previous_cost', 'cost'], 'number'],
            ];
        }
    
        public function scenarios()
        {
            return Model::scenarios();
        }

        public function search($params, $type = Product::TYPE_STANDARD)
        {
            $query = ProductModel::find()->where(['type' => $type, 'deleted' => 0]);

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
                'sort' => [
                    'defaultOrder' => [
                        'created_date' => SORT_DESC
                    ]
                ]
            ]);
    
            $this->load($params);
    
            if (!$this->validate()) {
                return $dataProvider;
            }

            if(!empty($this->available_text)){
                $this->available = (mb_strtolower($this->available_text) === Yii::t("system", mb_strtolower("yes"))) ? 1 : 0;

                $query->andFilterWhere(['like', 'available', $this->available]);
            }

            $query->andFilterWhere(['like', 'name', $this->name])
                ->andFilterWhere(['like', 'cost', $this->cost]);

            return $dataProvider;
        }
    }
