<?php
    namespace backend\searches;

    use Yii;
    use yii\base\Model;
    use yii\data\ActiveDataProvider;
    use backend\models\Attribute as AttributeModel;

    class Attribute extends AttributeModel
    {
        public function rules()
        {
            return [
                [['id'], 'integer'],
                [['name'], 'safe'],
            ];
        }

        public function scenarios()
        {
            return Model::scenarios();
        }

        public function search($params)
        {
            $query = Attribute::find();

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
            ]);

            $this->load($params);

            if (!$this->validate()) {
                return $dataProvider;
            }

            $query->andFilterWhere([
                'id' => $this->id,
            ]);

            $query->andFilterWhere(['like', 'name', $this->name]);

            return $dataProvider;
        }
    }
