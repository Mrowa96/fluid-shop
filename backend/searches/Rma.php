<?php
    namespace backend\searches;

    use Yii;
    use yii\base\Model;
    use yii\data\ActiveDataProvider;
    use common\models\Rma as RmaModel;

    class Rma extends RmaModel
    {
        public function rules()
        {
            return [
                [['id', 'user_id', 'product_id', 'order_id', 'status'], 'integer'],
                [['text', 'created_date', 'modified_date'], 'safe'],
                [['order.key', 'user.username', 'product.product_name'], 'safe']
            ];
        }

        public function attributes()
        {
            return array_merge(parent::attributes(), ['order.key', 'user.username', 'product.product_name']);
        }

        public function scenarios()
        {
            return Model::scenarios();
        }

        public function search($params)
        {
            $query = RmaModel::find();
            $query->joinWith('order AS order');
            $query->joinWith('user AS user');
            $query->joinWith('product AS product');

            $dataProvider = new ActiveDataProvider([
                'query' => $query,
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            $this->load($params);

            if (!$this->validate()) {
                return $dataProvider;
            }

            $query->andFilterWhere(['id' => $this->id]);
            $query->andFilterWhere(['like', 'user.username', $this->getAttribute('user.username')]);
            $query->andFilterWhere(['like', 'order.key', $this->getAttribute('order.key')]);
            $query->andFilterWhere(['like', 'product.product_name', $this->getAttribute('product.product_name')]);

            return $dataProvider;
        }
    }
