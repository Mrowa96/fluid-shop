<?php
    namespace backend\models;

    use common\models\Settings;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Url;

    /**
     * This is the model class for table "{{%backend_menu}}".
     *
     * @property integer $id
     * @property integer $parent_id
     * @property string $name
     * @property string $symbol
     * @property string $route
     * @property string $routeOptions
     */
    class BackendMenu extends ActiveRecord
    {
        public $url;
        public $children = [];

        public static function tableName()
        {
            return '{{%backend_menu}}';
        }

        public function afterFind()
        {
            parent::afterFind();

            if ($this->symbol === "HOME") {
                $this->url = Url::home();
            } else {
                $this->url = Url::toRoute("/" . $this->route);

                if ($this->routeOptions) {
                    $this->url .= '?' . $this->routeOptions;
                }
            }
        }

        public static function fetchForMenu()
        {
            $groups = BackendMenu::find()->orderBy('position')->all();
            $result = [];
            $children = [];

            if (!empty($groups)) {
                /** @var BackendMenu $group */
                foreach ($groups AS $group) {
                    if ($group->symbol === "FILEMANAGER") {
                        $fileManagerEnabled = Settings::getOne("file_manager", Settings\System::MODULE);

                        if (!$fileManagerEnabled) {
                            continue;
                        }
                    }

                    if ($group->symbol === "TRACK") {
                        $track = Settings::getOne("track", Settings\System::MODULE);

                        if (!$track) {
                            continue;
                        }
                    }

                    if ($group->parent_id == 0) {
                        $result[$group->id] = $group;
                    } else {
                        $children[] = $group;
                    }
                }

                if (!empty($children)) {
                    foreach ($children AS $group) {
                        if (isset($result[$group->parent_id])) {
                            $result[$group->parent_id]->children[] = $group;
                        }
                    }
                }

                Yii::$app->cache->set('backendMenu', $result, 60 * 60);
            }

            return $result;
        }
    }