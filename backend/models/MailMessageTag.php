<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mail_message_tag}}".
     *
     * @property integer $id
     * @property integer $mail_message_id
     * @property string $name
     * @property string $description
     *
     * @property MailMessage $mailMessage
     */
    class MailMessageTag extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mail_message_tag}}';
        }

        public function rules()
        {
            return [
                [['mail_message_id', 'name', 'description'], 'required'],
                [['mail_message_id'], 'integer'],
                [['description'], 'string'],
                [['name'], 'string', 'max' => 125],
                [['mail_message_id'], 'exist', 'skipOnError' => true, 'targetClass' => MailMessage::className(), 'targetAttribute' => ['mail_message_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mail_message_tag', 'ID'),
                'mail_message_id' => Yii::t('mail_message_tag', 'Mail Message ID'),
                'name' => Yii::t('mail_message_tag', 'Name'),
                'description' => Yii::t('mail_message_tag', 'Description'),
            ];
        }

        public function getNewsletterMessage()
        {
            return $this->hasOne(MailMessage::className(), ['id' => 'mail_message_id']);
        }
    }
