<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use common\models\Order;
    use Imos\Invoice\DateRange;
    use Imos\Invoice\Formatter;
    use Imos\Invoice\Generator\MpdfGenerator;
    use Imos\Invoice\Invoice;
    use Imos\Invoice\LineItem;
    use mPDF;

    /**
     * This is the model class for table "{{%order_invoice}}".
     *
     * @property integer $id
     * @property integer $order_id
     * @property string $name
     * @property string $created_date
     * @property string $modified_date
     *
     * @property Order $order
     */
    class OrderInvoice extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%order_invoice}}';
        }

        public function rules()
        {
            return [
                [['order_id', 'name', 'created_date', 'modified_date'], 'required'],
                [['order_id'], 'integer'],
                [['created_date', 'modified_date'], 'safe'],
                [['name'], 'string', 'max' => 256],
                [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('order-invoice', 'ID'),
                'order_id' => Yii::t('order-invoice', 'Order ID'),
                'path' => Yii::t('order-invoice', 'Path'),
                'created_date' => Yii::t('order-invoice', 'Created Date'),
                'modified_date' => Yii::t('order-invoice', 'Modified Date'),
            ];
        }

        public function getOrder()
        {
            return $this->hasOne(Order::className(), ['id' => 'order_id']);
        }
        public function getPath()
        {
            return Yii::getAlias("@dataInvoices") . "/";
        }

        public function beforeValidate()
        {
            if($this->isNewRecord){
                $this->name = $this->generate();
                $this->created_date = (new \DateTime())->format("Y-m-d H:i:s");
            }

            $this->modified_date = (new \DateTime())->format("Y-m-d H:i:s");

            return parent::beforeValidate();
        }

        private function generate()
        {
            $invoiceName = "invoice_" . $this->order_id . "_" . (new \DateTime())->format("Ymd\_His") . ".pdf";
            $invoice = (new Invoice)
                ->setPriceType(Invoice::PRICE_NET)
                ->setCurrency('PLN')
                ->setCustomerAddress([
                    $this->order->address->name ." ".$this->order->address->surname,
                    $this->order->address->email,
                    $this->order->address->phone_no,
                    $this->order->address->street ." ".$this->order->address->building_no,
                    $this->order->address->zip ." ".$this->order->address->city,
                ])
                ->setInvoiceNumber($invoiceName)
                ->setInvoiceDate(new \DateTime)
                ->setDueDate(new \DateTime('+30 days'))
                ->setBillingPeriod(DateRange::create('2016-01-01', '2016-02-01'))
                ->setCommission('ACME Partner')
                ->addTaxId('VATIN', 'DE999999999')
                ->addTaxId('Steuernummer', '12345 / 67890')
                ->setPaymentTerms('30 days strictly net')
                ->addExtraInfo('Payable by bank transfer.')
                ->addExtraInfo('IBAN: DE99 1234 5678 9012 3456 78');

            if($this->order->address->user_id){
                $invoice->setCustomerNumber($this->order->address->user_id);
            }

            foreach($this->order->orderDetails AS $detail){
                $lineItem = (new LineItem)
                    ->setDescription($detail->product->name)
                    ->setReference($detail->product->code)
                    ->setQuantity($detail->product_quantity)
                    ->setUnit('??')
                    ->setUnitPrice($detail->product_cost)
                    ->setTaxRate(23)
                    ->setTaxName('VAT');

                $invoice ->addLineItem($lineItem);
            }

            $generator = new MpdfGenerator(new Formatter, new mPDF);
            $generator->generateMpdf($invoice)->Output(Yii::getAlias("@dataInvoices") . "/" . $invoiceName, "F");

            return $invoiceName;
        }
    }
