<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%attribute_option}}".
     *
     * @property integer $id
     * @property integer $attribute_id
     * @property string $name
     */
    class AttributeOption extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%attribute_option}}';
        }

        public function rules()
        {
            return [
                [['attribute_id', 'name'], 'required'],
                [['attribute_id'], 'integer'],
                [['name'], 'string', 'max' => 128],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('attribute_option', 'ID'),
                'attribute_id' => Yii::t('attribute_option', 'Attribute ID'),
                'name' => Yii::t('attribute_option', 'Name'),
            ];
        }

        public function getProductAttributes()
        {
            return $this->hasMany(Attribute::className(), ['attribute_id' => 'id']);
        }
    }
