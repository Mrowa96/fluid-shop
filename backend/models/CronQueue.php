<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Json;

    /**
     * This is the model class for table "{{%cron_queue}}".
     *
     * @property integer $id
     * @property string $data
     * @property string $description
     * @property string $type
     * @property integer $status
     */
    class CronQueue extends ActiveRecord
    {
        const TYPE_NEWSLETTER = "newsletter-request";
        const TYPE_GOOGLE_MERCHANT = "google-merchant-request";
        const STATUS_WAITING = 0;
        const STATUS_DONE = 1;

        public static function tableName()
        {
            return '{{%cron_queue}}';
        }

        public function rules()
        {
            return [
                [['type'], 'required'],
                [['id', 'status'], 'integer'],
                [['data'], 'string'],
                [['description'], 'string', 'max' => 256],
                [['type'], 'string', 'max' => 128],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('cron_queue', 'ID'),
                'data' => Yii::t('cron_queue', 'Data'),
                'status' => Yii::t('cron_queue', 'Status'),
                'description' => Yii::t('cron_queue', 'Description'),
                'type' => Yii::t('cron_queue', 'Type'),
            ];
        }

        public static function createRequest($type, $data = [], $description = null)
        {
            $cronQueue = new CronQueue();

            $cronQueue->status = self::STATUS_WAITING;
            $cronQueue->type = $type;
            $cronQueue->description = $description;
            $cronQueue->data = Json::encode($data);

            return $cronQueue->save();
        }
    }
