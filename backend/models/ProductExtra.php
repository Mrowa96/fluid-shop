<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;
    use common\models\ProductImage;
    use common\models\ProductSellUp;
    use fluid\fileManager\File;

    /**
     * This is the model class for table "{{%product}}".
     *
     * @property integer $id
     * @property string $name
     * @property double $cost
     * @property string $description
     * @property integer $available
     * @property integer $quantity
     * @property string $code
     * @property string $created_date
     * @property string $modified_date
     *
     * @property ProductSellUp[] $productSellUps
     * @property ProductImage[] $image
     * @property string type
     */
    class ProductExtra extends ActiveRecord
    {
        /** @var UploadedFile $imageFile */
        public $available_text;
        public $imageFile;
        public $image;

        public static function tableName()
        {
            return '{{%product}}';
        }

        public function rules()
        {
            return [
                [['name', 'cost', 'quantity', 'code', 'created_date', 'modified_date'], 'required'],
                [['cost'], 'number'],
                [['description'], 'string'],
                [['available', 'quantity'], 'integer'],
                [['created_date', 'modified_date'], 'safe'],
                [['name'], 'string', 'max' => 256],
                [['code'], 'string', 'max' => 128],
                [['imageFile'], 'file'],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product_extra', 'ID'),
                'name' => Yii::t('product_extra', 'Name'),
                'cost' => Yii::t('product_extra', 'Cost'),
                'description' => Yii::t('product_extra', 'Description'),
                'available' => Yii::t('product_extra', 'Available'),
                'quantity' => Yii::t('product_extra', 'Quantity'),
                'code' => Yii::t('product_extra', 'Code'),
                'created_date' => Yii::t('product_extra', 'Created Date'),
                'modified_date' => Yii::t('product_extra', 'Modified Date'),
                'available_text' => Yii::t('product_extra', 'Available'),
            ];
        }

        public function getProductSellUps()
        {
            return $this->hasMany(ProductSellUp::className(), ['product_extra_id' => 'id']);
        }

        public function load($data, $formName = null)
        {
            if(parent::load($data, $formName)){
                $this->imageFile = UploadedFile::getInstance($this, 'imageFile');

                return true;
            }

            return false;
        }
        public function afterFind()
        {
            $this->available_text = ($this->available) ? Yii::t("system", "Yes") : Yii::t("system", "No");
            $this->image = ProductImage::find()->where(['product_id' => $this->id])->one();

            parent::afterFind();
        }
        public function beforeValidate()
        {
            if($this->isNewRecord){
                $this->created_date = (new \DateTime())->format("Y-m-d\TH:i:s");
            }

            $this->type = "extra";
            $this->modified_date = (new \DateTime())->format("Y-m-d\TH:i:s");

            return parent::beforeValidate();
        }
        public function save($runValidation = true, $attributeNames = null)
        {
            if(parent::save($runValidation, $attributeNames)){
                if($this->imageFile){
                    $this->saveImage();
                }

                return true;
            }

            return false;
        }

        private function saveImage()
        {
            if ($this->validate()) {
                $fileName = mb_substr(hash("sha256", $this->imageFile->basename), 0, 32);
                $filePath = Yii::getAlias("@dataProductsExtra") . '/' . $fileName . '.' . $this->imageFile->extension;
                $fileObject = new File($filePath);
                $fileObject->saveUploadedFile(ArrayHelper::toArray($this->imageFile), true);

                $existingImage = ProductImage::find()->where(['product_id' => $this->id])->one();

                $image = ($existingImage) ? $existingImage : (new ProductImage());
                $image->product_id = $this->id;
                $image->name = $fileName . '.' . $this->imageFile->extension;
                $image->path = Yii::getAlias("@dataLink") . '/products-extra/' . $fileName . '.' . $this->imageFile->extension;
                $image->thumb_path =  Yii::getAlias("@dataLink") . '/products-extra/' . $fileName . '.thumb.' . $this->imageFile->extension;
                $image->save();

                return true;
            }
            else {
                return false;
            }
        }
    }
