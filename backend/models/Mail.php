<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Json;
    use common\models\MailSubscriber;

    /**
     * This is the model class for table "{{%mail}}".
     *
     * @property integer $id
     * @property integer $mail_message_id
     * @property integer $mail_subscriber_id
     * @property string $send_date
     * @property string $status_text
     * @property string $status
     */
    class Mail extends ActiveRecord
    {
        public $send_to;
        public $status_text;

        public static function tableName()
        {
            return '{{%mail}}';
        }

        public function rules()
        {
            return [
                [['mail_message_id', 'send_date'], 'required'],
                [['mail_message_id', 'status'], 'integer'],
                [['send_date'], 'safe'],
                [['send_to'], 'safe'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mail', 'ID'),
                'mail_message_id' => Yii::t('mail', 'Mail Message ID'),
                'status' => Yii::t('mail', 'Status'),
                'status_text' => Yii::t('mail', 'Status'),
                'send_date' => Yii::t('mail', 'Send Date'),
            ];
        }

        public function afterFind()
        {
            if($this->status === 0){
                if($this->send_date <= (new \DateTime())->format("Y-m-d H:i:s")){
                    $this->status_text = Yii::t("mail", "Not send");
                }
                else{
                    $this->status_text = Yii::t("mail", "Waiting for send");
                }
            }
            else{
                $this->status_text = Yii::t("mail", "Send");
            }

            parent::afterFind();
        }

        public function getMessage()
        {
            return $this->hasOne(MailMessage::className(), ['id' => 'mail_message_id']);
        }

        public function send()
        {
            switch($this->send_to){
                case "toBuyers":
                    $subscribers = MailSubscriber::getBuyers();

                    break;
                case "toNonBuyers":
                    $subscribers = MailSubscriber::getNonBuyers();

                    break;
                case "all":
                    $subscribers = MailSubscriber::find()->where(['active' => 1])->all();
                    break;
            }

            if(!empty($subscribers)){
                foreach($subscribers AS $subscriber){
                    $mailSubscriberData = new MailSubscriberData();

                    $mailSubscriberData->mail_id = $this->id;
                    $mailSubscriberData->mail_subscriber_id = $subscriber['id'];

                    $mailSubscriberData->save();
                }

                $cronInstance = new CronQueue();

                $cronInstance->data = Json::encode(['id' => $this->id]);
                $cronInstance->description = "Sending mail to clients.";
                $cronInstance->type = "newsletter-request";

                return $cronInstance->save();
            }

            return false;
        }
    }
