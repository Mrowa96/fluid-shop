<?php
    namespace backend\models;
    
    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mail_subscriber_data}}".
     *
     * @property integer $id
     * @property integer $mail_id
     * @property integer $mail_subscriber_id
     *
     * @property Mail $mail
     */
    class MailSubscriberData extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mail_subscriber_data}}';
        }
    
        public function rules()
        {
            return [
                [['mail_id', 'mail_subscriber_id'], 'required'],
                [['mail_id', 'mail_subscriber_id'], 'integer'],
                [['mail_id'], 'exist', 'skipOnError' => true, 'targetClass' => Mail::className(), 'targetAttribute' => ['mail_id' => 'id']],
            ];
        }
    
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mail_subscriber_data', 'ID'),
                'mail_id' => Yii::t('mail_subscriber_data', 'Mail ID'),
                'mail_subscriber_id' => Yii::t('mail_subscriber_data', 'Mail Subscriber ID'),
            ];
        }
    
        public function getNewsletter()
        {
            return $this->hasOne(Mail::className(), ['id' => 'mail_id']);
        }
    }
