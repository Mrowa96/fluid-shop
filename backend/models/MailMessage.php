<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%mail_message}}".
     *
     * @property integer $id
     * @property string $title
     * @property string $name
     * @property string $content
     * @property bool $add_header
     * @property bool $add_footer
     * @property bool $deletable
     */
    class MailMessage extends ActiveRecord
    {
        const ORDER_CONFIRMATION = 1;
        const PASSWORD_RESET_TOKEN = 2;
        const ORDER_CHANGE_STATUS = 3;
        const FORGOTTEN_BASKET = 4;

        public static function tableName()
        {
            return '{{%mail_message}}';
        }

        public function rules()
        {
            return [
                [['title', 'name', 'content', 'add_header', 'add_footer'], 'required'],
                [['content'], 'string'],
                [['add_header', 'add_footer'], 'boolean'],
                [['title', 'name'], 'string', 'max' => 256],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mail_message', 'ID'),
                'title' => Yii::t('mail_message', 'Title'),
                'name' => Yii::t('mail_message', 'Name'),
                'content' => Yii::t('mail_message', 'Content'),
                'add_header' => Yii::t('mail_message', 'Include default header'),
                'add_footer' => Yii::t('mail_message', 'Include default footer'),
            ];
        }

        public function prepareForSent()
        {
            /**
             * @var MailMessagePart $header
             * @var MailMessagePart $footer
             */
            if($this->add_header === 1){
                $header = MailMessagePart::find()->where(['name' => 'header'])->one();

                $this->content = $header->content . $this->content;
            }
            if($this->add_footer === 1){
                $footer = MailMessagePart::find()->where(['name' => 'footer'])->one();

                $this->content = $this->content . $footer->content;
            }
        }
        public function getTags()
        {
            return $this->hasMany(MailMessageTag::className(), ['mail_message_id' => 'id']);
        }
        public function replaceTag($tag, $replacement)
        {
            if(strpos($this->content, '{' . $tag . '}') !== false){
                $this->content = str_replace('{' . $tag . '}', $replacement, $this->content);

                return true;
            }

            return false;
        }
        public function replaceTags(array $data)
        {
            if(!empty($data)){
                foreach($data AS $key => $value){
                    if(!$this->replaceTag($key, $value)){
                        return false;
                    }
                }

                return true;
            }

            return false;
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(self::find()->where(['deletable' => 1])->all(), 'id', 'title');
        }
    }
