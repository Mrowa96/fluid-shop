<?php
    namespace backend\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%mail_message_part}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $content
     */
    class MailMessagePart extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%mail_message_part}}';
        }

        public function rules()
        {
            return [
                [['name', 'title', 'content'], 'required'],
                [['content'], 'string'],
                [['name'], 'string', 'max' => 128],
                [['title'], 'string', 'max' => 128],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('mail_message_part', 'ID'),
                'name' => Yii::t('mail_message_part', 'Name'),
                'title' => Yii::t('mail_message_part', 'Title'),
                'content' => Yii::t('mail_message_part', 'Content'),
            ];
        }
    }
