<?php
    namespace backend\models;

    use common\models\Order;
    use common\models\OrderStatus;
    use common\models\User;
    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%order_log}}".
     *
     * @property integer $id
     * @property integer $order_id
     * @property integer $user_id
     * @property integer $order_status_id
     * @property string $date
     *
     * @property Order $order
     * @property OrderStatus $status
     * @property User $user
     */
    class OrderLog extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%order_log}}';
        }

        public function rules()
        {
            return [
                [['order_id', 'user_id', 'order_status_id', 'date'], 'required'],
                [['order_id', 'user_id', 'order_status_id'], 'integer'],
                ['date', 'string'],
                [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
                [['order_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'id']],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('order', 'ID'),
                'order_id' => Yii::t('order', 'Order ID'),
                'user_id' => Yii::t('order', 'User ID'),
                'order_status_id' => Yii::t('order', 'Order Status ID'),
                'date' => Yii::t('order', 'Date'),
            ];
        }

        public function getOrder()
        {
            return $this->hasOne(Order::className(), ['id' => 'order_id']);
        }
        public function getStatus()
        {
            return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
        }
        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }

        public function beforeValidate()
        {
            $this->date = (new \DateTime())->format("Y-m-d H:i:s");

            return parent::beforeValidate();
        }
    }
