<?php
    namespace backend\models;

    use common\models\ProductAttribute;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\db\Query;

    /**
     * This is the model class for table "{{%attribute}}".
     *
     * @property integer $id
     * @property string $name
     *
     * @property ProductAttribute[] $productsAttributes
     * @property AttributeOption[] $options
     */
    class Attribute extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%attribute}}';
        }

        public function rules()
        {
            return [
                [['name'], 'required'],
                [['name'], 'string', 'max' => 128],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('attribute', 'ID'),
                'name' => Yii::t('attribute', 'Name'),
                'options' => Yii::t('attribute', 'Options'),
            ];
        }

        public function getProductsAttributes()
        {
            return $this->hasMany(ProductAttribute::className(), ['attribute_id' => 'id']);
        }
        public function getOptions()
        {
            return $this->hasMany(AttributeOption::className(), ['attribute_id' => 'id']);
        }

        /**
         * @param int|null $productId
         * @param bool $forSelect
         * @return array
         */
        public static function getAllWithoutUsed($productId = null, $forSelect = false)
        {
            /** @var Attribute[] $attributes */
            if (!$productId) {
                $attributes = self::find()->all();
            } else {
                $attributes = (new Query())
                    ->select('*')
                    ->from(['a' => Attribute::tableName()])
                    ->where(['not exists', (new Query)
                        ->select('*')
                        ->from(['pa' => ProductAttribute::tableName()])
                        ->where('a.id = pa.attribute_id')
                        ->andWhere(['=', 'pa.product_id', $productId])])
                    ->all();
            }

            if ($forSelect === true) {
                $formResult = [];

                foreach ($attributes as $key => $attribute) {
                    $formResult[$attribute['id']] = $attribute['name'];
                }

                unset($attributes);

                return $formResult;
            }

            return $attributes;
        }
    }
