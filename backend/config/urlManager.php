<?php
    return [
        'enablePrettyUrl' => true,
        'showScriptName' => false,
        'baseUrl' => Yii::getAlias("@backendLink"),
        'rules' => [
            'logout' => 'site/logout',
            'login' => 'site/login',
            'stats' => 'site/stats',
            'get-model-data' => 'site/get-model-data',
            'save-grid-view-settings' => 'site/save-grid-view-settings',
        ],
        'suffix' => '.html'
    ];