<?php
    $config = [
        'id' => Yii::getAlias('@backendModuleId'),
        'language' => 'pl-PL',
        'basePath' => dirname(__DIR__),
        'controllerNamespace' => 'backend\controllers',
        'bootstrap' => ['log'],
        'modules' => [
            'gridview' => [
                'class' => '\kartik\grid\Module'
            ]
        ],
        'components' => [
            'log' => [
                'traceLevel' => YII_DEBUG ? 3 : 0,
                'targets' => [
                    [
                        'class' => 'yii\log\FileTarget',
                        'levels' => ['error', 'warning'],
                    ],
                ],
            ],
            'errorHandler' => [
                'errorAction' => 'site/error',
            ],
            'alertsService' => 'common\services\Alerts',
            'urlManager' => require_once 'urlManager.php',
            'urlManagerFrontend' => array_merge(
                require_once dirname(__DIR__) . '/../frontend/config/urlManager.php',
                ['class' => 'yii\web\urlManager']
            ),
            'i18n' => [
                'translations' => [
                    '*' => [
                        'class' => 'backend\utilities\i18n',
                        'forceTranslation' => true,
                    ],
                ],
            ],
            'assetManager' => [
                'basePath' => '@webroot/cache',
                'baseUrl' => '@web/cache',
                'appendTimestamp' => YII_ENV === 'prod' ? true : false,
                'bundles' => [
                    'yii\web\JqueryAsset' => false,
                    'yii\bootstrap\BootstrapAsset' => false,
                    'yii\web\YiiAsset' => [
                        'depends' => [
                            'backend\assets\ScriptAsset',
                        ],
                    ],
                ],
            ],
        ],
    ];

    if (YII_ENV === 'dev') {
        $config['bootstrap'][] = 'gii';
        $config['bootstrap'][] = 'debug';
        $config['modules']['gii'] = [
            'class' => 'yii\gii\Module',
            'generators' => [
                'crud' => [
                    'class' => 'yii\gii\generators\crud\Generator',
                    'templates' => [
                        'customCrud' => '@common/utilities/Gii/crud/default',
                    ]
                ],
                'model' => [
                    'class' => 'yii\gii\generators\model\Generator',
                    'templates' => [
                        'customModel' => '@common/utilities/Gii/model/default',
                    ]
                ]
            ],
        ];
        $config['modules']['debug'] = [
            'class' => 'yii\debug\Module',
            'allowedIPs' => ['127.0.0.1', '::1']
        ];
    }

    return $config;