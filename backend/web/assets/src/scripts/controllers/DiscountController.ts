import Helper from "../services/Helper";
import BaseController from "./BaseController";

export default class Discount extends BaseController {
    public manage() {
        let $generateCodeBtn = $(".generateCodeBtn"),
            $codeInput = $("#discount-code");

        $generateCodeBtn.click(() => {
            $codeInput.val(Helper.generateRandomKey(16).toUpperCase());
        });
    }
}