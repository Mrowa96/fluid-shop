import ImagesManager from "../services/Product/ImagesManager";
import AttributeManager from "../services/Product/AttributeManager";
import CrossSellsManager from "../services/Product/CrossSellsManager";
import UpSellsManager from "../services/Product/UpSellsManager";
import BaseController from "./BaseController";

export default class Product extends BaseController {
    protected ImagesManager: ImagesManager;
    protected AttributeManager: AttributeManager;
    protected CrossSellsManager: CrossSellsManager;
    protected UpSellsManager: UpSellsManager;

    public constructor() {
        super();

        this.ImagesManager = new ImagesManager();
    }

    public manage() {
        this.ImagesManager.handleImages();

        this.manageAttributes();
        this.manageCrossSells();
        this.manageUpSells();
    }

    protected manageAttributes() {
        let $chooseBothForm = $("#form-choose-both"),
            $chooseAndCreateForm = $("#form-choose-and-create"),
            $createBothForm = $("#form-create-both");

        this.AttributeManager = new AttributeManager({
            forms: [
                {
                    $form: $chooseBothForm,
                    $button: $chooseBothForm.find(".btn"),
                    fields: [
                        $chooseBothForm.find(".attribute-choose select"),
                        $chooseBothForm.find(".option-choose select"),
                    ],
                },
                {
                    $form: $chooseAndCreateForm,
                    $button: $chooseAndCreateForm.find(".btn"),
                    fields: [
                        $chooseAndCreateForm.find(".attribute-choose select"),
                        $chooseAndCreateForm.find(".option-create input")
                    ],
                },
                {
                    $form: $createBothForm,
                    $button: $createBothForm.find(".btn"),
                    fields: [
                        $createBothForm.find(".attribute-create input"),
                        $createBothForm.find(".option-create input")
                    ],
                }
            ],
            table: $("#attributes-table")
        });
        this.AttributeManager.initConfig();
    }

    protected manageCrossSells() {
        this.CrossSellsManager = new CrossSellsManager($("#form-cross-sell"), $("#cross-sells-table"));
    }

    protected manageUpSells() {
        this.UpSellsManager = new UpSellsManager($("#form-up-sell"), $("#up-sells-table"));
    }
}