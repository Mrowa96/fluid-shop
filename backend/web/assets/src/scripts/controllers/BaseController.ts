import Layout from "../services/Layout";

export default class BaseController {
    protected Layout: Layout;

    public constructor() {
        this.Layout = new Layout();
    }

    public beforeAction() {
        this.Layout.init();
    }

    public afterAction() {

    }
}