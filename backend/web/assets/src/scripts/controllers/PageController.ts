import BaseController from "./BaseController";

export default class Page extends BaseController {
    public manage() {
        let $pageForm = $(".page-form"),
            $typeSelect = $pageForm.find("#page-type");

        this.changeVisibility($pageForm, $typeSelect);

        $typeSelect.change(() => {
            this.changeVisibility($pageForm, $typeSelect);
        });
    }

    protected changeVisibility($pageForm: JQuery, $typeSelect: JQuery) {
        let $content = $pageForm.find('.field-page-content');

        switch ($typeSelect.val()) {
            case "contact":
                $content.hide();
                break;
            case "standard":
            default:
                $content.show();
                break;
        }
    }
}