import BaseController from "./BaseController";

export default class User extends BaseController {
    protected readonly AVATAR_SELECT_CLASS: string = 'selected';

    public manage() {
        this.handleAvatars();
    }

    protected handleAvatars() {
        let $avatarsBlock = $("#avatars-block"),
            $avatarsList = $avatarsBlock.find(".avatars-list"),
            $avatarInput = $avatarsBlock.find("input");

        $avatarsList.find("img").each((i, avatar) => {
            $(avatar).click(() => {
                $(avatar).parents("ul").find("." + this.AVATAR_SELECT_CLASS).removeClass(this.AVATAR_SELECT_CLASS);
                $(avatar).addClass(this.AVATAR_SELECT_CLASS);

                $avatarInput.val($(avatar).attr("src"));
            })
        })
    }
}