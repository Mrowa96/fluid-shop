import BaseController from "./BaseController";

export default class Mail extends BaseController {
    public create() {
        let $mailForm = $("#mailForm"),
            $hiddenInput = $("#hiddenInput"),
            $toAllBtn = $mailForm.find("#toAllBtn"),
            $toBuyersBtn = $mailForm.find("#toBuyersBtn"),
            $toNonBuyersBtn = $mailForm.find("#toNonBuyersBtn"),
            sendTo: string = "";

        $toAllBtn.click(() => {
            sendTo = $toAllBtn.data("send-to");
        });
        $toBuyersBtn.click(() => {
            sendTo = $toBuyersBtn.data("send-to");
        });
        $toNonBuyersBtn.click(() => {
            sendTo = $toNonBuyersBtn.data("send-to");
        });

        $mailForm.submit((e) => {
            $hiddenInput.val(sendTo);
        });
    }
}