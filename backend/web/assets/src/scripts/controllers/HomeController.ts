import OrdersChart from "../services/Home/OrdersChart";
import BaseController from "./BaseController";

export default class Home extends BaseController {
    protected readonly STATS_URL: string = '/ajax/get-stats.html';
    protected chart: Chart;

    public index() {
        try {
            this.initChart();
        }
        catch (error) {
            console.warn(error)
        }
    }

    protected initChart() {
        let ordersChart = new OrdersChart(this.STATS_URL);

        ordersChart.build((chart: Chart) => {
            this.chart = chart;
        });
    }
}
