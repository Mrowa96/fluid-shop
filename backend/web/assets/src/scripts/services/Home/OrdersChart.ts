import {Chart, ChartOptions, LinearChartData, ChartDataSets} from 'chart.js';

export default class OrdersChart {
    protected canvas: HTMLCanvasElement;
    protected context: CanvasRenderingContext2D;
    protected chart: Chart;

    protected readonly url: string;

    public data: {
        months: Array<number>
    };

    constructor(url: string) {
        try {
            this.url = url;
            this.canvas = <HTMLCanvasElement> $("#salesChart").get(0);
            this.context = this.canvas.getContext("2d");
        }
        catch (error) {
            console.warn(error)
        }
    }

    public build(resolve: Function): Chart {
        this.create(resolve);

        return this.chart;
    }

    protected create(resolve: Function) {
        this.loadData(() => {
            let options = OrdersChart.getOptions(),
                data = <LinearChartData> {
                    labels: OrdersChart.getLabels(),
                    datasets: OrdersChart.getDatasets(this.data.months)
                };

            this.chart = new Chart(this.context, {
                type: 'line',
                data: data,
                options: options
            });

            resolve(this.chart);
        });
    }

    protected loadData(callback: Function) {
        $.ajax({
            url: <string> this.url,
            success: (data) => {
                try {
                    this.data = data;
                    callback();
                }
                catch (error) {
                    console.warn(error);
                }
            }
        });
    }

    protected static getOptions(): ChartOptions {
        return <ChartOptions> {
            responsive: true,
            maintainAspectRatio: false,
            legend: {
                display: false
            },
            scales: {
                gridLines: {
                    display: false
                },
                xAxes: [{
                    gridLines: {
                        display: false
                    }
                }],
            }
        }
    }

    protected static getDatasets(data: Array<number>): Array<ChartDataSets> {
        return [<ChartDataSets>{
            label: "Zamówienia",
            fill: true,
            backgroundColor: "#3b8bba",
            strokeColor: "rgba(60,141,188,0.8)",
            pointBackgroundColor: "#3b8bba",
            pointBorderColor: "rgba(60,141,188,1)",
            pointHoverBackgroundColor: "#fff",
            pointHoverBorderColor: "rgba(60,141,188,1)",
            data: data
        }];
    }

    protected static getLabels(): Array<string> {
        return ["Styczeń", "Luty", "Marzec", "Kwiecień", "Maj", "Czerwiec", "Lipiec", "Sierpień", "Wrzesień", "Październik", "Listopad", "Grudzień"];
    }
}