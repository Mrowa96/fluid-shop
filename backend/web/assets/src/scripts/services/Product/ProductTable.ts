import Helper from "../Helper";
import ITable from "./ITable";
import IProductTableConfig from "./IProductTableConfig";

export default class ProductTable extends ITable {
    public constructor($element: JQuery, config: IProductTableConfig) {
        super();

        this.$element = $element;
        this.config = config;
    }

    public initialize(): void {
        this.$element.find("tr").each((i, row) => {
            let randomKey = Helper.generateRandomKey(16),
                $row = $(row),
                productId = $row.data("productId");

            if (productId) {
                $row.attr("data-key", randomKey);

                $("<td></td>").append(this.createDeleteButton(randomKey)).appendTo($row);
            }
        });

        this.addListeners();
    }

    protected addInput(data: {productId: number, cost: number, randomKey: string}): void {
        $("<input>").attr({
            name: this.config.addInputName + "[" + data.randomKey + "][product_id]",
            type: "hidden"
        }).val(data.productId).attr({
            'data-key': data.randomKey
        }).appendTo(this.$element.parent());

        $("<input>").attr({
            name: this.config.addInputName  + "[" + data.randomKey + "][cost]",
            type: "hidden"
        }).val(data.cost).attr({
            'data-key': data.randomKey
        }).appendTo(this.$element.parent());
    }

    protected removeInput(data: {productId: string}): void {
        $("<input>").attr({
            name: this.config.removeInputName + "[]",
            type: "hidden"
        }).val(data.productId).appendTo(this.$element.parent());
    }

    protected onAddRow(event: Event, data: {content: any, cost: number}): void {
        let $row = $("<tr></tr>"),
            randomKey = Helper.generateRandomKey(16),
            alreadyExists = false;

        super.beforeOnAddRow();

        this.$element.find("tr").each((i, row) => {
            let $row = $(row);

            if ($row.data('productId') == data.content.id) {
                alreadyExists = true;
            }
        });

        if (alreadyExists === false) {
            if (data.content && data.cost > 0) {
                $("<td></td>").text(data.content.name).appendTo($row);
                $("<td></td>").text(data.cost).appendTo($row);

                $row.data({
                    productId: data.content.id,
                    cost: data.cost,
                }).attr({
                    'data-key': randomKey
                });
                $("<td></td>").append(this.createDeleteButton(randomKey)).appendTo($row);
            }
            this.$element.append($row);
            this.addInput({productId: data.content.id, cost: data.cost, randomKey: randomKey});
        }
    }

    protected onRemoveRow(event: Event, data: any): void {
        if (data.key) {
            let $row = $('tr[data-key="' + data.key + '"]'),
                productId = $row.data("productId");

            $row.remove();
            $('input[data-key="' + data.key + '"]').remove();

            this.removeInput({productId: productId});
        }

        super.afterOnRemoveRow();
    }
}