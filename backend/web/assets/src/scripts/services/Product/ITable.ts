import IProductTableConfig from "./IProductTableConfig";
abstract class ITable {
    static readonly EVENT_ADD_ROW: string = 'add-row';
    static readonly EVENT_REMOVE_ROW: string = 'remove-row';

    protected $element: JQuery;
    protected config: IProductTableConfig;

    /** Getter for $element */
    public getElement(): JQuery {
        return this.$element;
    }

    /** Method which creates delete button in a row */
    protected createDeleteButton(randomKey: string): JQuery {
        let $button = $("<i></i>");

        $button.attr({
            'class': 'fa fa-close deleteBtn',
            'aria-hidden': true
        }).click(() => {
            this.$element.trigger(ITable.EVENT_REMOVE_ROW, {
                key: randomKey
            })
        });

        return $button;
    }

    /** Method adds listeners*/
    protected addListeners() {
        this.$element.on(ITable.EVENT_ADD_ROW, (event: Event, data: any) => {
            this.onAddRow(event, data);
        });
        this.$element.on(ITable.EVENT_REMOVE_ROW, (event: Event, data: any) => {
            this.onRemoveRow(event, data);
        });
    }

    /** Method is called just after EVENT_ADD_ROW trigger */
    protected beforeOnAddRow() {
        this.$element.find("tr").each((i, row) => {
            let $row = $(row);

            if ($row.hasClass("empty")) {
                $row.hide();
            }
        });
    }

    /** Method is called after EVENT_REMOVE_ROW execution */
    protected afterOnRemoveRow() {
        if (this.$element.find("tbody tr").length === 1) {
            let $row = this.$element.find(".empty");

            $row.show();
        }
    }

    /** Method handle table with existing data */
    public abstract initialize(): void;

    /** Method adds input with content to add */
    protected abstract addInput(data: any): void;

    /** Method adds input with content to remove */
    protected abstract removeInput(data: any): void;

    /** Method handle EVENT_ADD_ROW */
    protected abstract onAddRow(event: Event, data: any): void;

    /** Method handle EVENT_REMOVE_ROW */
    protected abstract onRemoveRow(event: Event, data: any): void;
}

export default ITable;