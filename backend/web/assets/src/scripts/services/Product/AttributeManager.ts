import AttributeForm from "./AttributeForm";
import IForm from "./IForm";
import AttributeTable from "./AttributeTable";
import ITable from "./ITable";

export default class AttributeManager {
    protected config: {
        forms?: Array<IForm>,
        table?: JQuery
    };
    protected table: ITable;
    protected $forms: Array<AttributeForm> = [];

    public constructor(config: {}) {
        this.config = config;
    }

    public initConfig() {
        if (this.config.table && this.config.table.length) {
            this.table = new AttributeTable(this.config.table);
            this.table.initialize();
        }

        if (this.config.forms && this.config.forms.length) {
            for (let form of this.config.forms) {
                if (form.$form.length) {
                    let attributeForm: AttributeForm = new AttributeForm(form);

                    try {
                        if (attributeForm.handleForm()) {
                            attributeForm.getForm().on(AttributeForm.EVENT_SUBMIT_FORM, (event, data) => {
                                if(this.table instanceof ITable){
                                    this.table.getElement().trigger(ITable.EVENT_ADD_ROW, data);
                                }

                            });

                            this.$forms.push(attributeForm);
                        }
                    }
                    catch (error) {
                        console.warn(error);
                    }
                }
                else{
                    console.warn("AttributeManager: $form is undefined");
                }
            }
        }
    }
}