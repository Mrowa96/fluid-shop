interface IForm{
    $form: JQuery,
    $button: JQuery,
    fields: Array<JQuery>
}

export default IForm;