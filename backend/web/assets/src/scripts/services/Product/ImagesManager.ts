export default class ImagesManager {
    $rootElement: JQuery;
    $mainImageInput: JQuery;

    constructor() {
        this.$rootElement = $("#productImages");
        this.$mainImageInput = $("#mainImageInput");
    }

    public handleImages() {
        this.$rootElement.find(".imageWrap").each((i, element) => {
            let $deleteBtn = $(element).find(".deleteBtn"),
                $mainBtn = $(element).find(".mainBtn"),
                $revertBtn = $(element).find(".revertBtn"),
                $image = $(element).find("img"),
                imageId = <number>$image.data("id");

            $deleteBtn.click(() => {
                this.onDeleteClick(element, imageId);
            });

            $mainBtn.click(() => {
                this.onMainClick(element, imageId);
            });

            $revertBtn.click(() => {
                this.onRevertClick(element, imageId);
            });
        })
    }

    protected onDeleteClick(element: Element, imageId: number): void {
        if (!$(element).hasClass('deleted')) {
            let $deleteInput = $("<input>");

            $deleteInput.attr({
                id: 'deleted' + imageId,
                type: 'hidden',
                name: 'imageToDelete[]'
            }).val(imageId).appendTo(this.$rootElement);

            $(element).addClass("deleted");

            if ($(element).hasClass("mainImage")) {
                $(element).removeClass("mainImage");
                this.$mainImageInput.val("");
            }
        }
    }

    protected onMainClick(element: Element, imageId: number): void {
        if (!$(element).hasClass('deleted')) {
            this.$rootElement.find(".mainImage").removeClass("mainImage");
            $(element).addClass("mainImage");
            this.$mainImageInput.val(imageId);
        }
    }

    protected onRevertClick(element: Element, imageId: number): void {
        if ($(element).hasClass('deleted')) {
            $(element).removeClass("deleted");
            this.$rootElement.find("#deleted" + imageId).remove();
        }
    }
}