import ITable from "./ITable";
import ProductTable from "./ProductTable";
import IProductTableConfig from "./IProductTableConfig";

export default class CrossSellsManager {
    static readonly TYPE_PRODUCT_ID: string = 'product-id';
    static readonly TYPE_COST: string = 'cost';
    static readonly URL_GET_PRODUCT: string = '/ajax/get-product.html';

    protected $form: JQuery;
    protected $table: JQuery;
    protected $button: JQuery;
    protected fields: {
        productId: JQuery,
        cost: JQuery
    };
    protected ProductTable: ITable;

    public constructor($form: JQuery, $table: JQuery) {
        this.$form = $form;
        this.$table = $table;

        this.initialize();
    }

    protected initialize(): boolean {
        try {
            this.ProductTable = new ProductTable(this.$table, {
                removeInputName: "crossSellProductToDelete",
                addInputName: "crossSellToAdd"
            });
            this.$button = this.$form.find(".btn");
            this.fields = {
                productId: this.$form.find('input[data-type="' + CrossSellsManager.TYPE_PRODUCT_ID + '"]'),
                cost: this.$form.find('input[data-type="' + CrossSellsManager.TYPE_COST + '"]'),
            };

            if (this.ProductTable instanceof ITable) {
                this.ProductTable.initialize();
            }

            if (this.$button) {
                this.$button.on("click", (event) => {
                    $.ajax({
                        url: CrossSellsManager.URL_GET_PRODUCT,
                        method: 'GET',
                        data: {
                            id: parseInt(this.fields.productId.val())
                        },
                        success: (response) => {
                            this.ProductTable.getElement().trigger(ITable.EVENT_ADD_ROW, {
                                content: response,
                                cost: parseFloat(this.fields.cost.val())
                            });

                            $(this.fields.productId).val('');
                            $(this.fields.cost).val('');
                        }
                    });

                    event.preventDefault();
                })
            }
        }
        catch (error) {
            console.warn("CrossSellsManager error: ", error);
        }

        return false;
    }
}