import AttributeForm from "./AttributeForm";
import Helper from "../Helper";
import ITable from "./ITable";

export default class AttributeTable extends ITable {
    public constructor($element: JQuery) {
        super();

        this.$element = $element;
    }

    public initialize(): void {
        this.$element.find("tr").each((i, row) => {
            let randomKey = Helper.generateRandomKey(16),
                $row = $(row),
                attributeId = $row.data("attributeId"),
                optionId = $row.data("optionId");

            if (attributeId && optionId) {
                $row.attr("data-key", randomKey);

                $("<td></td>").append(this.createDeleteButton(randomKey)).appendTo($row);
            }
        });

        this.addListeners();
    }

    protected addInput(data: {attributeId: number, optionId: number, randomKey: string}): void {
        $("<input>").attr({
            name: "attributeToAdd[" + data.randomKey + "][attribute_id]",
            type: "hidden"
        }).val(data.attributeId).attr({
            'data-key': data.randomKey
        }).appendTo(this.$element.parent());

        $("<input>").attr({
            name: "attributeToAdd[" + data.randomKey + "][attribute_option_id]",
            type: "hidden"
        }).val(data.optionId).attr({
            'data-key': data.randomKey
        }).appendTo(this.$element.parent());
    }

    protected removeInput(data: {attributeId: string}): void {
        $("<input>").attr({
            name: "attributeToDelete[]",
            type: "hidden"
        }).val(data.attributeId).appendTo(this.$element.parent());
    }

    protected onAddRow(event: Event, data: any): void {
        let $row = $("<tr></tr>"), attributeId, optionId,
            randomKey = Helper.generateRandomKey(16);

        super.beforeOnAddRow();

        if (data.data.length) {
            for (let field of data.data) {
                if (field.type === AttributeForm.FIELD_ATTRIBUTE) {
                    $("<td></td>").text(field.text).appendTo($row);
                    attributeId = field.id;
                }
                else if (field.type === AttributeForm.FIELD_OPTION) {
                    $("<td></td>").text(field.text).appendTo($row);
                    optionId = field.id;
                }
            }

            $row.data({
                attributeId: attributeId,
                optionId: optionId,
            }).attr({
                'data-key': randomKey
            });
            $("<td></td>").append(this.createDeleteButton(randomKey)).appendTo($row);
        }
        this.$element.append($row);
        this.addInput({attributeId: attributeId, optionId: optionId, randomKey: randomKey});
    }

    protected onRemoveRow(event: Event, data: any): void {
        if (data.key) {
            let $row = $('tr[data-key="' + data.key + '"]'),
                attributeId = $row.data("attributeId");

            $row.remove();
            $('input[data-key="' + data.key + '"]').remove();

            this.removeInput({attributeId: attributeId});
        }

        super.afterOnRemoveRow();
    }
}