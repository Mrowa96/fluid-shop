import IForm from "./IForm";
import 'select2';

export default class AttributeForm {
    static readonly FIELD_ATTRIBUTE: string = 'attribute';
    static readonly FIELD_OPTION: string = 'option';
    static readonly URL_GET_ATTRIBUTE: string = '/ajax/get-attribute.html';
    static readonly URL_GET_OPTION: string = '/ajax/get-attribute-option.html';
    static readonly URL_GET_ATTRIBUTE_OPTIONS: string = '/ajax/get-attribute-options.html';
    static readonly URL_CREATE_ATTRIBUTE: string = '/ajax/create-attribute.html';
    static readonly URL_CREATE_OPTION: string = '/ajax/create-attribute-option.html';
    static readonly EVENT_CHANGE_FIELD_SELECT: string = 'field-select-change';
    static readonly EVENT_CREATE_FROM_FIELD: string = 'create-from-field';
    static readonly EVENT_CREATED_FROM_FIELD: string = 'created-from-field';
    static readonly EVENT_SUBMIT_FORM: string = 'form-submit';

    protected form: IForm;

    public constructor(form: IForm) {
        this.form = form;
    }

    public handleForm(): boolean {
        let fields = this.form.fields,
            $button = this.form.$button;

        return this.handleFields(fields) && this.handleButton($button, fields);
    }

    public getForm(): JQuery {
        return this.form.$form;
    }

    /**
     * Method assign listeners for standards event e.g. 'change',
     * but also catches custom events e.g. creating new attribute option.
     * There are two tags in every type, so every tag must be handled differently.
     *
     * @param fields
     * @returns {boolean}
     */
    protected handleFields(fields: Array<JQuery>): boolean {
        if (fields.length) {
            for (let $field of fields) {
                let type = $field.data("type"),
                    tagName = $field.prop('tagName');

                switch (type) {
                    case AttributeForm.FIELD_ATTRIBUTE:
                        if (tagName === "SELECT") {
                            $field.select2().change(() => {
                                $.ajax({
                                    url: AttributeForm.URL_GET_ATTRIBUTE, method: 'GET',
                                    data: {
                                        id: $field.val()
                                    },
                                    success: (response) => {
                                        if (response.hasOwnProperty('id')) {
                                            $field.data('content', response);

                                            this.enableOptionsForAttribute(response.id);
                                            this.getForm().trigger(AttributeForm.EVENT_CHANGE_FIELD_SELECT, {
                                                field: $field,
                                                data: response,
                                                type: type
                                            });
                                        }
                                    }
                                });
                            });
                        }
                        else if (tagName === "INPUT") {
                            $field.on("input", () => {
                                let $optionField = this.getFieldByType(AttributeForm.FIELD_OPTION)

                                if ($field.val().length) {
                                    $optionField.removeAttr("disabled");
                                }
                                else {
                                    $optionField.attr("disabled", "disabled");
                                }
                            });
                            $field.on(AttributeForm.EVENT_CREATE_FROM_FIELD, () => {
                                $.ajax({
                                    url: AttributeForm.URL_CREATE_ATTRIBUTE, method: 'GET', async: false,
                                    data: {
                                        name: $field.val()
                                    },
                                    success: (response) => {
                                        if (response.hasOwnProperty('id')) {
                                            $field.data('content', response);

                                            this.getForm().trigger(AttributeForm.EVENT_CREATED_FROM_FIELD, {
                                                field: $field,
                                                data: response,
                                                type: type
                                            });
                                        }
                                    }
                                });
                            });
                        }
                        break;
                    case AttributeForm.FIELD_OPTION:
                        if (tagName === "SELECT") {
                            $field.select2().change(() => {
                                $.ajax({
                                    url: AttributeForm.URL_GET_OPTION, method: 'GET',
                                    data: {
                                        id: $field.val()
                                    },
                                    success: (response) => {
                                        if (response.hasOwnProperty('id')) {
                                            $field.data('content', response);

                                            this.getForm().trigger(AttributeForm.EVENT_CHANGE_FIELD_SELECT, {
                                                field: $field,
                                                data: response,
                                                type: type
                                            });
                                        }
                                    }
                                });
                            });
                        }
                        else if (tagName === "INPUT") {
                            $field.on(AttributeForm.EVENT_CREATE_FROM_FIELD, () => {
                                $.ajax({
                                    url: AttributeForm.URL_CREATE_OPTION, method: 'GET', async: false,
                                    data: {
                                        name: $field.val(),
                                        attributeId: this.getFieldByType(AttributeForm.FIELD_ATTRIBUTE).data("content").id
                                    },
                                    success: (response) => {
                                        if (response.hasOwnProperty('id')) {
                                            $field.data('content', response);

                                            this.getForm().trigger(AttributeForm.EVENT_CREATED_FROM_FIELD, {
                                                field: $field,
                                                data: response,
                                                type: type
                                            });
                                        }
                                    }
                                });
                            });
                        }
                        break;
                    default:
                        console.warn("AttributeForm: $field has unknown type - " + type, $field);
                        break;
                }
            }

            return true;
        }

        throw new Error("AttributeForm: fields cannot be empty");
    }

    /**
     * Method checks fields, and triggers 'complete' event.
     * Also if field type is INPUT, method triggers 'create' event.
     *
     * @param $button
     * @param fields
     * @returns {boolean}
     */
    protected handleButton($button: JQuery, fields: Array<JQuery>): boolean {
        if ($button.length && fields.length) {
            $button.click((event) => {
                let fieldsNotEmpty = true,
                    triggerData = [];

                event.preventDefault();

                for (let $field of fields) {
                    let type = $field.data("type"),
                        tagName = $field.prop('tagName');

                    if (!$field.val().length) {
                        fieldsNotEmpty = false;

                        break;
                    }
                    else {
                        if (tagName === "INPUT") {
                            /** Synchronous ajax request :( */
                            $field.trigger(AttributeForm.EVENT_CREATE_FROM_FIELD)
                        }

                        triggerData.push({
                            id: $field.data('content').id,
                            text: $field.data('content').name,
                            type: $field.data("type"),
                            field: $field
                        });
                    }
                }

                if (fieldsNotEmpty === true) {
                    this.getForm().trigger(AttributeForm.EVENT_SUBMIT_FORM, {
                        data: triggerData
                    });

                    for (let $field of fields) {
                        $field.val("").trigger("change");
                    }
                }
            });

            return true;
        }

        throw new Error("AttributeForm: $button cannot be empty");
    }

    private getFieldByType(type: string): JQuery | null {
        for (let $field of this.form.fields) {
            if ($field.data("type") === type) {
                return $field;
            }
        }

        return null;
    }

    private enableOptionsForAttribute(attributeId: number): void {
        let $field = this.getFieldByType(AttributeForm.FIELD_OPTION);

        if ($field && $field.prop("tagName") === "SELECT") {
            $.ajax({
                url: AttributeForm.URL_GET_ATTRIBUTE_OPTIONS,
                method: 'GET',
                data: {
                    id: attributeId
                },
                success: (response) => {
                    if (response.length) {
                        let optionsList = [],
                            firstOption = $field.find("option").first().text();

                        optionsList.push({
                            id: '',
                            text: firstOption
                        });

                        for (let option of response) {
                            optionsList.push({
                                id: <string> option.id,
                                text: option.name
                            });
                        }

                        $field.select2('destroy').empty().select2({data: optionsList});
                    }
                }
            })
        }

        $field.removeAttr("disabled");
    }
}