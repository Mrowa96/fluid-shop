import Layout from "./Layout";

export default class Sidebar {
    protected static readonly COLLAPSE_CLASS: string = 'sidebar-collapse';
    protected static readonly OPEN_CLASS: string = 'sidebar-open';
    protected static readonly EVENT_EXPANDED: string = 'expanded.pushMenu';
    protected static readonly EVENT_COLLAPSED: string = 'collapsed.pushMenu';

    protected $body: JQuery;

    public constructor() {
        this.$body = $('body');

        this.listen();
    }

    protected listen() {
        $(Layout.options.sidebarToggleSelector).on('click', (e) => {
            e.preventDefault();

            if ($(window).width() > (Layout.options.screenSizes.sm - 1)) {
                if (this.$body.hasClass(Sidebar.COLLAPSE_CLASS)) {
                    this.$body.removeClass(Sidebar.COLLAPSE_CLASS).trigger(Sidebar.EVENT_EXPANDED);
                } else {
                    this.$body.addClass(Sidebar.COLLAPSE_CLASS).trigger(Sidebar.EVENT_COLLAPSED);
                }
            }
            else {
                if (this.$body.hasClass(Sidebar.OPEN_CLASS)) {
                    this.$body.removeClass(Sidebar.OPEN_CLASS).removeClass(Sidebar.COLLAPSE_CLASS).trigger(Sidebar.EVENT_COLLAPSED);
                } else {
                    this.$body.addClass(Sidebar.OPEN_CLASS).trigger(Sidebar.EVENT_COLLAPSED);
                }
            }
        });

        $(Layout.$content).click(() => {
            if ($(window).width() <= (Layout.options.screenSizes.sm - 1) && this.$body.hasClass(Sidebar.OPEN_CLASS)) {
                this.$body.removeClass(Sidebar.OPEN_CLASS);
            }
        });

        $(document).on('click', '.sidebar li', (e) => {
            let $element = e.target.tagName == "A" ? $(e.target) : $(e.target).parent("a"),
                $checkElement = $element.next();

            if (($checkElement.is('.treeview-menu')) && ($checkElement.is(':visible'))) {
                $checkElement.slideUp(Layout.options.animationSpeed, () => {
                    $checkElement.removeClass('menu-open');
                });
                $checkElement.parent("li").removeClass("active");
            }
            else if (($checkElement.is('.treeview-menu')) && (!$checkElement.is(':visible'))) {
                let $parent = $element.parents('ul').first(),
                    ul = $parent.find('ul:visible').slideUp(Layout.options.animationSpeed);

                ul.removeClass('menu-open');

                $checkElement.slideDown(Layout.options.animationSpeed, () => {
                    $checkElement.addClass('menu-open');
                    $parent.find('li.active').removeClass('active');
                    $element.parent("li").addClass('active');

                    Layout.fix();
                });
            }
            if ($checkElement.is('.treeview-menu')) {
                e.preventDefault();
            }
        });
    }
}