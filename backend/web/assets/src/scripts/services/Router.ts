import HomeController from "../controllers/HomeController";
import UserController from "../controllers/UserController";
import ProductController from "../controllers/ProductController";
import MailController from "../controllers/MailController";
import DiscountController from "../controllers/DiscountController";
import PageController from "../controllers/PageController";
import BaseController from "../controllers/BaseController";
import App from "../App";
import Route from "./Route";

export default class Router {
    protected url: string;
    protected match: Route;
    protected routes: Array<Route>;

    public constructor(url: string) {
        this.url = url;
        this.routes = this.loadRoutes();
    }

    public route(): boolean {
        for (let route of this.routes) {
            if (Array.isArray(route.pattern)) {
                for (let pattern of route.pattern) {
                    if (this.matchPattern(pattern, route)) {
                        return true;
                    }
                }
            }
            else {
                if (this.matchPattern(route.pattern, route)) {
                    return true;
                }
            }
        }

        return false;
    }

    public initMatch(): void {
        if (this.match instanceof Route) {
            let controller = this.match.controllerObject,
                action = this.match.actionName;

            App.setController(this.match.controllerObject);

            controller.beforeAction();

            if (typeof (<any>controller)[action] !== 'undefined') {
                (<any>controller)[action]();
            }

            controller.afterAction();
        }
    }

    public initBaseMatch(): void {
        let controller = new BaseController();

        App.setController(controller);

        controller.beforeAction();
        controller.afterAction();
    }

    protected matchPattern(pattern: string, route: Route): boolean {
        //TODO Change to regexp
        let match: Route | null = null;

        pattern = pattern.trim();

        if (route.strict === true) {
            if (this.url === pattern) {
                match = route;
            }
        }
        else {
            if (this.url.indexOf(pattern) !== -1) {
                match = route;
            }
        }

        if (match instanceof Route) {
            this.match = match;

            return true;
        }

        return false;
    }

    protected loadRoutes(): Array<Route> {
        return [
            new Route("product", ['/product/update', '/product/create', '/product/clone'], new ProductController(), 'manage', true),
            new Route("home", ['/', '/site/index', '/site'], new HomeController(), 'index', true),
            new Route("user", ['/user/update', '/user/create'], new UserController(), 'manage', true),
            new Route("mailCreate", ['/mail/create'], new MailController(), 'create', true),
            new Route("discountManage", ['/discount/create', '/discount/update'], new DiscountController(), 'manage', true),
            new Route("pageManage", ['/page/create', '/page/update'], new PageController(), 'manage', true)
        ];
    }
}