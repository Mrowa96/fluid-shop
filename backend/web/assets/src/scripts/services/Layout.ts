declare var tinymce: any;

import Sidebar from "./Sidebar";
import UserMenu from "./UserMenu";
import AlertWidget from "../../../../../../common/assets/scripts/widgets/AlertWidget";
import "jquery";

export default class Layout {
    public static readonly options = {
        animationSpeed: 500,
        sidebarToggleSelector: "[data-toggle='offcanvas']",
        sidebarPushMenu: true,
        sidebarSlimScroll: true,
        sidebarExpandOnHover: false,
        controlSidebarOptions: {
            toggleBtnSelector: "[data-toggle='control-sidebar']",
            selector: ".control-sidebar",
            slide: true
        },
        colors: {
            lightBlue: "#3c8dbc",
            red: "#f56954",
            green: "#00a65a",
            aqua: "#00c0ef",
            yellow: "#f39c12",
            blue: "#0073b7",
            navy: "#001F3F",
            teal: "#39CCCC",
            olive: "#3D9970",
            lime: "#01FF70",
            orange: "#FF851B",
            fuchsia: "#F012BE",
            purple: "#8E24AA",
            maroon: "#D81B60",
            black: "#222222",
            gray: "#d2d6de"
        },
        screenSizes: {
            xs: 480,
            sm: 768,
            md: 992,
            lg: 1200
        }
    };

    public static $header: JQuery;
    public static $content: JQuery;
    public static $sidebar: JQuery;
    public static $footer: JQuery;

    protected Sidebar: Sidebar;
    protected UserMenu: UserMenu;

    public constructor() {
        /**
         * Nothing to do in there
         */
    }

    /**
     * Method must be called fater document load e.g. in jQuery ready method.
     */
    public init() {
        this.initElements();

        this.handleLayout();
        this.handleCheckboxes();
        this.handleRadios();
        this.handleSidebar();
        this.handleUserMenu();
        this.handleTabs();
        this.handleTinyMCE();
        this.handleAlerts();
    }

    public static fix() {
        let offset: number = Layout.$header.outerHeight() + Layout.$footer.outerHeight();

        if ($(window).height() >= Layout.$sidebar.height()) {
            Layout.$content.css('min-height', $(window).height() - offset);
        } else {
            Layout.$content.css('min-height', Layout.$sidebar.height());
        }
    };

    protected initElements() {
        Layout.$header = $('.main-header');
        Layout.$footer = $('.main-footer');
        Layout.$content = $('.content-wrapper');
        Layout.$sidebar = $('.sidebar');
    }

    protected handleLayout() {
        Layout.fix();

        $(window).resize(() => {
            Layout.fix();
        });
    }

    protected handleCheckboxes() {
        let $checkboxes = Layout.$content.find('input[type="checkbox"]');

        $checkboxes.each((i, element) => {
            let $checkbox = $(element),
                $label = $(element).parent("label"),
                $formGroup = $checkbox.parents('.form-group');

            if ($checkbox.length && $formGroup.length) {
                $formGroup.addClass("checkbox-wrap");
                $label.addClass("checkbox-label");

                $checkbox.after($("<div></div>").addClass("checkbox-box"));
            }
        })
    }

    protected handleRadios() {
        let $radios = Layout.$content.find('input[type="radio"]');

        $radios.each((i, element) => {
            let $radio = $(element),
                $label = $(element).parent().find("label"),
                $formGroup = $radio.parents('.form-group');

            if ($radio.length && $formGroup.length) {
                $formGroup.addClass("radio-wrap");
                $label.addClass("radio-label");

                $label.prepend($("<div></div>").addClass("radio-box"));
            }
        })
    }

    protected handleAlerts() {
        let $alertsWrapper = $("#alerts-wrapper");

        if ($alertsWrapper.length) {
            let alertWidget = new AlertWidget($alertsWrapper);

            if (alertWidget.hasAlerts()) {
                alertWidget.showAll();
            }
        }
    }

    protected handleTabs() {
        let $tabs = $(".tabs-above").find('a[role="tab"]');

        $tabs.each((index, tab) => {
            let $tab = $(tab),
                url = $tab.data("url");

            if (url) {
                $tab.attr("href", "#");
                $tab.off('click').click((e) => {
                    e.preventDefault();

                    window.location.href = url;

                    return false;
                })
            }
        })
    }

    protected handleTinyMCE() {
        $('textarea[data-tinymce="true"]').each((index, element) => {
            let $element = $(element);

            try {
                tinymce.init({
                    selector: "#" + $element.attr("id"),
                    height: 300,
                    language: 'pl',
                    entity_encoding: "utf-8",
                    setup: function (editor: any) {
                        editor.on('NodeChange', function () {
                            $element.html(editor.getContent());
                        });
                        editor.on('init', function () {
                            this.getDoc().body.style.fontSize = '14px';
                        });
                    }
                });
            }
            catch (e) {
                console.warn("TinyMCE is not defined.")
            }
        });
    }

    protected handleUserMenu() {
        this.UserMenu = new UserMenu();
    }

    protected handleSidebar() {
        this.Sidebar = new Sidebar();
    }
};

