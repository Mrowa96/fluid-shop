export default class Url {
    protected node;

    constructor() {
        this.node = document.createElement("a");
        this.node.setAttribute("href", window.location.href);
    }

    public getProtocol(): string {
        return this.node.protocol;
    }

    public getHost(): string {
        return this.node.host;
    }

    public getHostName(): string {
        return this.node.hostname;
    }

    public getPort(): string {
        return this.node.port;
    }

    public getPathname(): string {
        return this.node.pathname;
    }

    public getPathnameShort(): string {
        let pathname = this.node.pathname;

        if (pathname.indexOf(".") !== -1) {
            return pathname.substr(0, pathname.indexOf("."));
        }
        else {
            return pathname;
        }
    }

    public getHash(): string {
        return this.node.hash;
    }

    public getQuery(): string {
        return this.node.search;
    }
}