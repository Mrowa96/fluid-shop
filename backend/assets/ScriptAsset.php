<?php
    namespace backend\assets;

    use yii\web\AssetBundle;
    use yii\web\View;

    class ScriptAsset extends AssetBundle
    {
        public $basePath = '@webroot';
        public $baseUrl = '@web';
        public $js = ['assets/dist/build/js/app.js'];
        public $jsOptions = ['position' => View::POS_HEAD];
    }