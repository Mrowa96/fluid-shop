<?php
    namespace backend\utilities;

    class i18n extends \common\utilities\i18n
    {
        public $sourceMessageTable = '{{%i18n_source_backend}}';

        public $module = 'backend';
    }
