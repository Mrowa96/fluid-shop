<?php
    namespace backend\forms;

    use Yii;
    use yii\base\Model;

    class Product extends Model
    {
        public $products_list;
        public $product_cost;
        public $product_cost2;

        public function attributeLabels()
        {
            return [
                'products_list' => Yii::t('product', 'Products list'),
                'product_cost' => Yii::t('product', 'Product cost'),
                'product_cost2' => Yii::t('product', 'Product cost'),
            ];
        }
    }