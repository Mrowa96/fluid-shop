<?php
    namespace backend\forms;

    use Yii;
    use yii\base\Model;

    class Attributes extends Model
    {
        public $attributes_list;
        public $attributes_list_create;
        public $attribute_field;
        public $options_list;
        public $option_field_in_select;
        public $option_field;

        public function attributeLabels()
        {
            return [
                'attributes_list' => Yii::t('attribute', 'Attribute list'),
                'attribute_field' => Yii::t('attribute', 'Attribute field'),
                'attributes_list_create' => Yii::t('attribute', 'Attribute list create'),
                'options_list' => Yii::t('attribute', 'Options list'),
                'option_field' => Yii::t('attribute', 'Option field'),
                'option_field_in_select' => Yii::t('attribute', 'Option field in select'),
            ];
        }
    }