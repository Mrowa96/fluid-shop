<?php
    namespace backend\forms;

    use Yii;
    use yii\base\Model;
    use yii\helpers\ArrayHelper;
    use common\models\User as UserModel;
    use common\utilities\Language;

    /**
     * Class User
     * @package backend\forms
     *
     * @property string $username
     * @property string $name
     * @property string $email
     * @property string $password
     * @property string $password_repeat
     * @property string $role
     * @property string $language
     * @property string $avatar
     * @property UserModel $user
     */
    class User extends Model
    {
        public $id;
        public $username;
        public $name;
        public $email;
        public $password;
        public $password_repeat;
        public $role;
        public $language;
        public $avatar;
        protected $user;

        const SCENARIO_CREATE = 'create';
        const SCENARIO_UPDATE = 'update';

        /**
         * @return array
         */
        public function rules()
        {
            return ArrayHelper::merge(UserModel::getBaseRules(), [
                ['username', 'unique',
                    'targetClass' => UserModel::className(),
                    'message' => Yii::t("user", 'This username has already been taken.'),
                    'on' => User::SCENARIO_CREATE
                ],
                ['username', 'unique',
                    'targetClass' => UserModel::className(),
                    'message' => Yii::t("user", 'This username has already been taken.'),
                    'when' => function ($model) {
                        return $model->username !== $this->user->username;
                    },
                    'on' => User::SCENARIO_UPDATE
                ],

                ['email', 'unique',
                    'targetClass' => UserModel::className(),
                    'message' => Yii::t("user", 'This email address has already been taken.'),
                    'on' => User::SCENARIO_CREATE
                ],
                ['email', 'unique',
                    'targetClass' => UserModel::className(),
                    'message' => Yii::t("user", 'This email address has already been taken.'),
                    'when' => function ($model) {
                        return $model->email !== $this->user->email;
                    },
                    'on' => User::SCENARIO_UPDATE
                ],

                [['password', 'password_repeat'], 'string', 'min' => 6],
                ['password_repeat', 'compare', 'compareAttribute' => 'password', 'message' => Yii::t("site", "Passwords don't match")],

                [['password', 'password_repeat'], 'required', 'on' => User::SCENARIO_CREATE],

                ['password', 'required',
                    'when' => function ($model) {
                        return !empty($model->password);
                    },
                    'whenClient' => "function (attribute, value) {
                        return value.length > 0;
                    }",
                    'on' => User::SCENARIO_UPDATE
                ],
                ['password_repeat', 'required',
                    'when' => function ($model) {
                        return !empty($model->password_repeat);
                    },
                    'whenClient' => "function (attribute, value) {
                        return value.length > 0;
                    }",
                    'on' => User::SCENARIO_UPDATE
                ],

                ['role', 'required'],
                ['avatar', 'required'],
                ['language', 'string', 'max' => 6]
            ]);
        }

        /**
         * @return array
         */
        public function attributeLabels()
        {
            return UserModel::getBaseAttributeLabels();
        }

        /**
         * @return bool
         */
        public function exists()
        {
            return $this->getUser()->isNewRecord;
        }

        /**
         * @return bool
         */
        public function save()
        {
            if ($this->validate()) {
                $user = $this->getUser();

                $user->username = $this->username;
                $user->name = $this->name;
                $user->email = $this->email;
                $user->avatar = $this->avatar;
                $user->auth_key = UserModel::generateAuthKey();

                if ($this->password) {
                    $user->setPassword($this->password);
                }

                if ($this->language) {
                    $language = new Language();

                    $language->setUserLanguage($user, $this->language);
                }

                if ($user->save()) {
                    return $user->setRole($this->role);
                }
            }

            return false;
        }

        /**
         * @return UserModel
         */
        public function getUser()
        {
            if (!($this->user instanceof UserModel)) {
                $this->user = new UserModel();
            }

            return $this->user;
        }

        /**
         * @param UserModel $user
         * @param bool $withData
         * @return $this
         */
        public function setUser(UserModel $user, $withData = true)
        {
            $this->user = $user;

            if ($withData) {
                $this->load(['User' => $this->user->attributes]);

                if ($this->user->hasRole()) {
                    $this->role = $this->user->getRole()->name;
                }
            }

            return $this;
        }
    }