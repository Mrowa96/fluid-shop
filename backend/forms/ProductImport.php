<?php
    namespace backend\forms;

    use Yii;
    use yii\base\Exception;
    use yii\base\Model;
    use yii\helpers\Json;
    use common\models\Product;

    class ProductImport extends Model
    {
        public $jsonFile;
        public $xmlFile;

        public function rules()
        {
            return [
                [['jsonFile'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'json'],
                [['xmlFile'], 'file', 'checkExtensionByMimeType' => false, 'extensions' => 'xml'],
            ];
        }

        public function attributes()
        {
            return [
                'jsonFile' => Yii::t("product", "JSON File"),
                'xmlFile' => Yii::t("product", "XML File"),
            ];
        }

        public function import()
        {
            if(!empty($this->jsonFile)){
                return $this->importJson();
            }
            else if(!empty($this->xmlFile)){
                return $this->importXml();
            }
            else{
                $this->addError("jsonFile", Yii::t("product", "You don't choose any file to import."));

                return false;
            }
        }

        protected function importJson()
        {
            if ($this->validate()) {
                try{
                    $data = file_get_contents($this->jsonFile->tempName);
                    $items = Json::decode($data);

                    return $this->importItems($items);
                }
                catch(\Exception $ex){
                    $this->addError("jsonFile", $ex->getMessage() . ". Line: " . $ex->getLine());
                }
            }

            return false;
        }

        protected function importXml()
        {
            if ($this->validate()) {
                try{
                    $data = file_get_contents($this->xmlFile->tempName);
                    $object = new \SimpleXMLElement($data);

                    if(isset($object->product)){
                        $items = Json::decode(Json::encode((array) $object));

                        return $this->importItems($items);
                    }
                    else{
                        throw new Exception("No product found.");
                    }

                }
                catch(\Exception $ex){
                    $this->addError("jsonFile", $ex->getMessage() . ". Line: " . $ex->getLine());
                }
            }

            return false;
        }

        private function importItems(array $items){
            if(!empty($items)){
                foreach($items['product'] AS $item){
                    if(isset($item['id'])){
                        $product = Product::findOne($item['id']);

                        if(!$product){
                            $product = new Product();
                        }
                    }
                    else{
                        $product = new Product();
                    }

                    foreach($item AS $key => $value){
                        $product->{$key} = $value;
                    }

                    if(!isset($item['category_id'])){
                        $product->category_id = 1;
                        $product->available = 0;
                    }

                    $product->save();
                }

                return true;
            }
            else{
                return false;
            }
        }
    }