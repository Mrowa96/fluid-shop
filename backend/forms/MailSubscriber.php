<?php
    namespace backend\forms;

    use fluid\fileManager\File;
    use Yii;
    use yii\base\Model;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;

    class MailSubscriber extends Model
    {
        public $csvFile;

        public function rules()
        {
            return [
                [['csvFile'], 'file', 'extensions' => 'csv', 'checkExtensionByMimeType' => false]
            ];
        }

        public function attributes()
        {
            return [
                'csvFile' => Yii::t("mail", "CSV File"),
            ];
        }

        public function load($data, $formName = null)
        {
            if(parent::load($data, $formName)){
                $this->csvFile = UploadedFile::getInstance($this, 'csvFile');

                return true;
            }

            return false;
        }

        public function getCsvFile()
        {
            if($this->validate()){
                $filePath = Yii::getAlias("@dataSubscribers") . '/' . $this->csvFile->basename . '.' . $this->csvFile->extension;
                $fileObject = new File($filePath);
                $fileObject->saveUploadedFile(ArrayHelper::toArray($this->csvFile));

                return $fileObject;
            }
            else{
                return false;
            }
        }
    }