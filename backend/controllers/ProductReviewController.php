<?php
    namespace backend\controllers;

    use Yii;
    use common\models\ProductReview;
    use yii\web\NotFoundHttpException;

    class ProductReviewController extends BaseController
    {
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(Yii::$app->request->referrer);
        }

        public function actionAccept($id)
        {
            $model = $this->findModel($id);

            if ($model) {
                $model->accepted = true;
                $model->save();
            }

            return $this->redirect(Yii::$app->request->referrer);
        }

        protected function findModel($id)
        {
            if (($model = ProductReview::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
