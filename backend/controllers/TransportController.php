<?php
    namespace backend\controllers;

    use backend\models\GridViewConfig;
    use Yii;
    use common\models\Transport;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use yii\web\UploadedFile;

    class TransportController extends BaseController
    {
        public function actionIndex()
        {
            $gridViewColumns = GridViewConfig::prepareData(Transport::className());
            $dataProvider = new ActiveDataProvider([
                'query' => Transport::find()->where(['deleted' => 0]),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'gridViewColumns' => $gridViewColumns,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new Transport();

            if ($model->load(Yii::$app->request->post())) {
                $model->iconFile = UploadedFile::getInstance($model, 'iconFile');

                if($model->saveIcon() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    //error
                }
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $model->iconFile = UploadedFile::getInstance($model, 'iconFile');

                if($model->saveIcon() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    return $this->redirectWithMessage(['index'],"Some error", "error");
                }
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);
            $model->deleted = 1;
            $model->save();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /* @var $model Transport */
            if (($model = Transport::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
