<?php
    namespace backend\controllers;

    use backend\models\Attribute;
    use Yii;
    use common\models\Category;
    use common\models\Discount;
    use backend\models\GridViewConfig;
    use common\models\Product;
    use backend\searches\Product as ProductSearch;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use yii\web\UploadedFile;
    use backend\forms\Attributes as AttributesForm;
    use backend\forms\Product as ProductForm;
    use backend\forms\ProductImport as ProductImportForm;
    use common\models\ProductAttribute;
    use common\models\ProductReview;
    use common\models\ProductSellCross;
    use common\models\ProductSellUp;

    class ProductController extends BaseController
    {
        public function actionIndex()
        {
            $searchModel = new ProductSearch();
            $gridViewColumns = GridViewConfig::prepareData(Product::className());
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
                'gridViewColumns' => $gridViewColumns,
            ]);
        }

        public function actionView($id)
        {
            $attributes = new ActiveDataProvider([
                'query' => ProductAttribute::find()->where(['product_id' => $id])
            ]);
            $crossSells = new ActiveDataProvider([
                'query' => ProductSellCross::find()->where(['product_id' => $id])
            ]);
            $upSells = new ActiveDataProvider([
                'query' => ProductSellUp::find()->where(['product_id' => $id])
            ]);
            $reviews = new ActiveDataProvider([
                'query' => ProductReview::find()->where(['product_id' => $id])
            ]);

            return $this->render('view', [
                'model' => $this->findModel($id),
                'attributes' => $attributes,
                'crossSell' => $crossSells,
                'upSell' => $upSells,
                'reviews' => $reviews,
            ]);
        }
        public function actionCreate()
        {
            $model = new Product();
            $productsForm = new ProductForm();
            $categories = Category::prepareForForm();
            $discounts = Discount::prepareForForm();
            $attributes = Attribute::getAllWithoutUsed(null, true);

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirectWithMessage(['view', 'id' => $model->id],
                        Yii::t("system", "Object was created successfully."));
                }
                else{
                    return $this->redirectWithMessage(['view', 'id' => $model->id],
                        Yii::t("system", "An unexpected error occurred, please contact with administrator."), "error");
                }
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                    'attributes' => $attributes,
                    'categories' => $categories,
                    'discounts' => $discounts,
                    'productsForm' => $productsForm,
                ]);
            }
        }
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            $productsForm = new ProductForm();
            $categories = Category::prepareForForm();
            $discounts = Discount::prepareForForm();
            $attributes = Attribute::getAllWithoutUsed($model->id, true);

            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) {
                    return $this->redirectWithMessage(['view', 'id' => $model->id],
                        Yii::t("system", "Object was created successfully."));
                }
                else{
                    return $this->redirectWithMessage(['view', 'id' => $model->id],
                        Yii::t("system", "An unexpected error occurred, please contact with administrator."), "error");
                }
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                    'attributes' => $attributes,
                    'categories' => $categories,
                    'discounts' => $discounts,
                    'productsForm' => $productsForm,
                ]);
            }
        }
        public function actionClone($id)
        {
            /** @var Product $model */
            /** @var Product $clonedModel */
            $model = $this->findModel($id);
            $clonedModel = $model->cloneProduct();

            if($clonedModel){
                return $this->redirect(['product/update', 'id' => $clonedModel->id]);
            }
            else{
                return $this->redirect(['product/index']);
            }
        }
        public function actionDelete($id)
        {
            $model = $this->findModel($id);
            $model->deleted = 1;
            $model->save();

            return $this->redirect(['index']);
        }
        public function actionDeleteSelected($ids)
        {
            /** @var Product[] $models */
            $models = Product::find()->where(['id' => explode(",", $ids)])->all();

            foreach($models AS $model){
                $model->deleted = 1;
                $model->save();
            }

            return $this->redirect(['index']);
        }
        public function actionImport()
        {
            $form = new ProductImportForm();

            if(Yii::$app->request->isPost){
                $form->jsonFile = UploadedFile::getInstance($form, 'jsonFile');
                $form->xmlFile = UploadedFile::getInstance($form, 'xmlFile');

                if($form->import()){
                    return $this->redirect(['index']);
                }
            }

            return $this->render("import", [
                'model' => $form
            ]);
        }

        public function actionCrossSellDelete($id)
        {
            $model = ProductSellCross::findOne($id);

            if ($model) {
                $model->delete();
            }

            return $this->redirect(Yii::$app->request->referrer);
        }


        public function actionUpSellDelete($id)
        {
            $model = ProductSellUp::findOne($id);

            if ($model) {
                $model->delete();
            }

            return $this->redirect(Yii::$app->request->referrer);
        }

        protected function findModel($id)
        {
            /** @var Product $model */
            if (($model = Product::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
