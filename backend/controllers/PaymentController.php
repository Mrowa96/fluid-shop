<?php
    namespace backend\controllers;

    use backend\models\GridViewConfig;
    use common\models\PaymentApi;
    use Yii;
    use common\models\Payment;
    use yii\data\ActiveDataProvider;
    use backend\controllers\BaseController;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;
    use yii\web\UploadedFile;

    class PaymentController extends BaseController
    {
        public function actionIndex()
        {
            $gridViewColumns = GridViewConfig::prepareData(Payment::className());
            $dataProvider = new ActiveDataProvider([
                'query' => Payment::find()->where(['deleted' => 0]),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'gridViewColumns' => $gridViewColumns
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new Payment();

            if ($model->load(Yii::$app->request->post())) {
                $model->iconFile = UploadedFile::getInstance($model, 'iconFile');

                if($model->saveIcon() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->render('create', [
                'model' => $model,
                'apiData' => PaymentApi::prepareForForm()
            ]);
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                $model->iconFile = UploadedFile::getInstance($model, 'iconFile');

                if($model->saveIcon() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->render('update', [
                'model' => $model,
                'apiData' => PaymentApi::prepareForForm()
            ]);
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);

            $model->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /* @var $model Payment */
            if (($model = Payment::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
