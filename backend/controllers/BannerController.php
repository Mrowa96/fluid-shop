<?php
    namespace backend\controllers;

    use backend\models\GridViewConfig;
    use Yii;
    use common\models\Banner;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;

    class BannerController extends BaseController
    {
        public function actionIndex()
        {
            $gridViewColumns = GridViewConfig::prepareData(Banner::className());
            $dataProvider = new ActiveDataProvider([
                'query' => Banner::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'gridViewColumns' => $gridViewColumns,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id)
            ]);
        }

        public function actionCreate()
        {
            $model = new Banner();

            if ($model->load(Yii::$app->request->post())) {
                if($model->saveImage() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->render('create', [
                'model' => $model,
                'types' => Banner::prepareTypeForForm()
            ]);
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                if($model->saveImage() && $model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
            }

            return $this->render('update', [
                'model' => $model,
                'types' => Banner::prepareTypeForForm()
            ]);
        }

        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /**
             * @var Banner $model
             */
            if (($model = Banner::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
