<?php
    namespace backend\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use backend\models\MailMessage;
    use backend\models\MailMessagePart;
    use common\models\MailSubscriber;
    use backend\models\Mail;

    class MailController extends BaseController
    {
        public function actionIndex()
        {
            $messagesProvider = new ActiveDataProvider([
                'query' => MailMessage::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $messagesPartsProvider = new ActiveDataProvider([
                'query' => MailMessagePart::find(),
            ]);
            $subscribersProvider = new ActiveDataProvider([
                'query' => MailSubscriber::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);
            $newsletters = new ActiveDataProvider([
                'query' => Mail::find(),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index',[
                'messagesProvider' => $messagesProvider,
                'messagesPartsProvider' => $messagesPartsProvider,
                'subscribersProvider' => $subscribersProvider,
                'newsletters' => $newsletters,
            ]);
        }
        
        public function actionCreate()
        {
            $model = new Mail();
            $messages = MailMessage::prepareForForm();

            if($model->load(Yii::$app->request->post())){
                if($model->save() && $model->send()){
                    return $this->redirect(['index']);
                }
                else{
                    return $this->redirect(['index']);
                }
            }
            else{
                return $this->render("create", [
                    'model' => $model,
                    'messages' => $messages,
                ]);
            }
        }
    }
