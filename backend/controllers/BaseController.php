<?php
    namespace backend\controllers;

    use Yii;
    use yii\filters\AccessControl;
    use yii\web\Controller;
    use backend\models\BackendMenu;
    use common\models\Settings;
    use common\utilities\ClassicMenu;
    use common\utilities\Language;
    use common\services\Alerts\Builder as AlertBuilder;

    class BaseController extends Controller
    {
        public function behaviors()
        {
            return [
                'access' => [
                    'class' => AccessControl::className(),
                    'rules' => [
                        [
                            'allow' => true,
                            'actions' => ['login'],
                            'roles' => ['?'],
                        ],
                        [
                            'allow' => true,
                            'roles' => ['admin', 'worker'],
                        ],
                    ]
                ],
            ];
        }

        public function actions()
        {
            return [
                'error' => [
                    'class' => 'yii\web\ErrorAction',
                ],
            ];
        }

        public function beforeAction($action)
        {
            if (Yii::$app->user->id) {
                foreach (Yii::$app->authManager->getRolesByUser(Yii::$app->user->id) AS $role => $data) {
                    if ($role !== "admin" && $role !== "worker") {
                        Yii::info('User (' . Yii::$app->user->id . ") tried to log in to the panel.", "backend");

                        return $this->redirect(Yii::getAlias("@frontendLink"));
                    }
                }
            }

            $language = new Language();
            $language->manage();

            $session = Yii::$app->session;
            $sidebarMenu = new ClassicMenu();
            $sidebarMenu->addData(BackendMenu::fetchForMenu())->addHeader(Yii::t("system", "Navigation"));

            if ($session->has("messageCounter")) {
                $counter = $session->get("messageCounter");

                if ($counter < 1) {
                    if ($session->has("message")) {
                        $session->remove("message");
                    }
                    if ($session->has("type")) {
                        $session->remove("type");
                    }
                } else {
                    $session->set("messageCounter", 0);
                }
            }

            $this->view->title = Settings::getOne('site_title', Settings\System::MODULE);
            $this->view->params['sidebarMenu'] = $sidebarMenu;

            return parent::beforeAction($action);
        }

        public function redirectWithMessage(array $url, $message, $type = "success", $statusCode = 302)
        {
            Yii::$app->alertsService->addAlert(AlertBuilder::build($message, $type));

            return parent::redirect($url, $statusCode);
        }

        public function renderWithMessage($view, $message, $params = [], $type = "success")
        {
            Yii::$app->alertsService->addAlert(AlertBuilder::build($message, $type));

            return parent::render($view, $params);
        }
    }
