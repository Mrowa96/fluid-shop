<?php
    namespace backend\controllers;

    use Yii;
    use backend\models\Attribute;
    use backend\searches\Attribute as AttributeSearch;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use backend\models\AttributeOption;

    class AttributeController extends BaseController
    {

        public function actionIndex()
        {
            $searchModel = new AttributeSearch();
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

            return $this->render('index', [
                'searchModel' => $searchModel,
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionView($id)
        {
            $dataProvider = new ActiveDataProvider([
                'query' => AttributeOption::find()->where(['attribute_id' => $id])
            ]);

            return $this->render('view', [
                'model' => $this->findModel($id),
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionCreate()
        {
            $model = new Attribute();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            if (($model = Attribute::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
