<?php
    namespace backend\controllers;

    use Yii;
    use yii\web\NotFoundHttpException;
    use backend\models\AttributeOption;

    class AttributeOptionController extends BaseController
    {
        public function actionUpdate($id, $attributeId)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['attribute/view', 'id' => $attributeId]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id, $attributeId)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['attribute/view', 'id' => $attributeId]);
        }

        protected function findModel($id)
        {
            /** @var AttributeOption $model */
            if (($model = AttributeOption::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
