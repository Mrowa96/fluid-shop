<?php
    namespace backend\controllers;

    use common\models\Discount;
    use common\models\Product;
    use Yii;
    use common\models\Category;
    use yii\data\ActiveDataProvider;
    use yii\helpers\ArrayHelper;
    use yii\web\NotFoundHttpException;
    use backend\models\GridViewConfig;

    class CategoryController extends BaseController
    {
        public function actionIndex()
        {
            $dataColumns = GridViewConfig::prepareData(Category::className());
            $dataProvider = new ActiveDataProvider([
                'query' => Category::find()->where(['deleted' => 0]),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'dataColumns' => $dataColumns
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new Category();
            $discounts = ArrayHelper::map(Discount::find()->all(), 'id', 'name');
            $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                    'categories' => $categories,
                    'discounts' => $discounts,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            $discounts = ArrayHelper::map(Discount::find()->all(), 'id', 'name');
            $categories = ArrayHelper::map(Category::find()->all(), 'id', 'name');

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                    'categories' => $categories,
                    'discounts' => $discounts
                ]);
            }
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);
            $model->deleted = 1;
            $model->save();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /** @var Category $model */
            if (($model = Category::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
