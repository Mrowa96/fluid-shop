<?php
    namespace backend\controllers;

    use Yii;
    use common\models\Order;
    use backend\searches\Order as OrderSearch;
    use yii\data\ActiveDataProvider;
    use yii\helpers\ArrayHelper;
    use yii\web\NotFoundHttpException;
    use backend\models\GridViewConfig;
    use backend\models\OrderInvoice;
    use common\models\Address;
    use common\models\OrderDetail;
    use common\models\OrderStatus;

    class OrderController extends BaseController
    {
        public function actionIndex()
        {
            $searchModel = new OrderSearch();
            $gridViewColumns = GridViewConfig::prepareData(Order::className());
            $allOrders = $searchModel->search(Yii::$app->request->queryParams, "all");
            $inProgressOrders = $searchModel->search(Yii::$app->request->queryParams, "inProgress");
            $completedOrders = $searchModel->search(Yii::$app->request->queryParams, "completed");
            $canceledOrders = $searchModel->search(Yii::$app->request->queryParams, "canceled");

            return $this->render('index', [
                'allOrders' => $allOrders,
                'inProgressOrders' => $inProgressOrders,
                'completedOrders' => $completedOrders,
                'canceledOrders' => $canceledOrders,
                'searchModel' => $searchModel,
                'gridViewColumns' => $gridViewColumns,
            ]);
        }
        public function actionView($id)
        {
            /** @var Order $order */
            $order = Order::findOne($id);
            $products = new ActiveDataProvider([
                'query' => OrderDetail::find()->where(["order_id" => $id])
            ]);

            return $this->render('view', [
                'model' => $order,
                'products' => $products,
                'address' => $order->address,
                'logs' => $order->logs,
            ]);
        }
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);
            $runOnStatusChange = false;
            $statuses = ArrayHelper::map(OrderStatus::find()->all(), "id", "status_name");

            if($model->load(Yii::$app->request->post())){
                if($model->isAttributeChanged('order_status_id', false)){
                    $runOnStatusChange = true;
                }

                if($model->save()){
                    if($runOnStatusChange === true){
                        $model->onStatusChange(Yii::$app->user->id);
                    }

                    return $this->redirectWithMessage(['index'],
                        Yii::t("system", "Object was updated successfully."));
                }
                else{
                    return $this->redirectWithMessage(['index'],
                        Yii::t("system", "An unexpected error occurred, please contact with administrator."), "error");
                }
            }
            else{
                return $this->render('update', [
                    'model' => $model,
                    'statuses' => $statuses
                ]);
            }
        }

        public function actionGenerateInvoice($id)
        {
            /** @var OrderInvoice $invoice */
            $invoice = OrderInvoice::find()->where(['order_id' => $id])->one();

            if(!$invoice){
                $invoice = new OrderInvoice();
                $invoice->order_id = $id;

                if($invoice->save()){
                    return $this->redirect(['view-invoice', 'id' => $invoice->id]);
                }
            }
            else{
                return $this->redirect(['view-invoice', 'id' => $invoice->id]);
            }
        }
        public function actionViewInvoice($id)
        {
            /** @var OrderInvoice $invoice */
            $invoice = OrderInvoice::findOne($id);

            if(file_exists($invoice->getPath() . $invoice->name)){
                header('Content-type: application/pdf');
                header('Content-Disposition: inline; filename="' . $invoice->name . '"');
                header('Content-Transfer-Encoding: binary');
                header('Accept-Ranges: bytes');

                readfile($invoice->getPath() . $invoice->name);
            }
        }

        protected function findModel($id)
        {
            /** @var Order $model */
            if (($model = Order::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
