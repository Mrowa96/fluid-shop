<?php
    namespace backend\controllers;

    use common\models\Order;
    use Yii;
    use yii\db\Query;
    use yii\helpers\Json;
    use backend\models\GridViewConfig;
    use backend\models\Attribute;
    use backend\models\AttributeOption;
    use common\models\Product;
    use common\models\ProductAttribute;
    use common\models\ProductSellCross;
    use common\models\ProductSellUp;
    use yii\web\Response;

    class AjaxController extends BaseController
    {
        public function beforeAction($action)
        {
            $this->layout = false;
            $this->enableCsrfValidation = false;

            \Yii::$app->response->format = Response::FORMAT_JSON;

            return parent::beforeAction($action);
        }

        public function actionGetStats()
        {
            return [
                'months' => Order::getGroupedByMonth(date("Y"))
            ];
        }

        public function actionGetModelData($name)
        {
            $results = [];

            if (class_exists($name)) {
                $results = GridViewConfig::prepareColumns($name);
            }

            return $results;
        }

        public function actionSaveGridViewSettings()
        {
            $data = Yii::$app->request->post('data');
            $response = ['status' => 0, 'message' => Yii::t("system", "An unexpected error occurred, please contact with administrator.")];

            if ($data) {
                $data = Json::decode($data, false);

                if (class_exists($data->model)) {
                    if (!Yii::$app->user->can("configure grid view")) {
                        $response = ['status' => 0, 'message' => Yii::t("system", "You are not allowed to save this settings.")];
                    } else if (GridViewConfig::saveColumns($data->keys, $data->model)) {
                        $response = ['status' => 1, 'message' => Yii::t("system", "Settings have been successfully saved.")];
                    }
                }
            }

            return $response;
        }

        public function actionGetAttribute($id)
        {
            return Attribute::find()->where(['id' => $id])->one();
        }

        public function actionGetAttributeOption($id)
        {
            return AttributeOption::find()->where(['id' => $id])->one();
        }

        public function actionGetAttributes($productId)
        {
            return Attribute::getAllWithoutUsed($productId);
        }

        public function actionGetAttributeOptions($id)
        {
            return AttributeOption::find()->where(['attribute_id' => $id])->orderBy('name')->all();
        }

        public function actionGetAttributeAndOption($attributeId, $optionId)
        {
            $attribute = Attribute::findOne($attributeId);
            $option = AttributeOption::findOne($optionId);

            if ($option && $option->attribute_id == $attributeId) {
                $response = [
                    'attribute' => $attribute,
                    'option' => $option,
                    'status' => true
                ];
            } else {
                $response = [
                    'status' => false
                ];
            }

            return $response;
        }

        public function actionCreateAttribute($name)
        {
            $attribute = new Attribute();
            $attribute->name = $name;

            if ($attribute->save()) {
                $response = $attribute;
            }

            return isset($response) ? $response : [];
        }

        public function actionCreateAttributeOption($name, $attributeId)
        {
            $option = new AttributeOption();
            $option->name = $name;
            $option->attribute_id = $attributeId;

            if ($option->save()) {
                $response = $option;
            }

            return isset($response) ? $response : [];
        }

        public function actionGetProductAttributes($id)
        {
            /** @var ProductAttribute[] $productAttributes */
            $productAttributes = ProductAttribute::find()->where(['product_id' => $id])->all();
            $result = [];

            foreach ($productAttributes AS $attribute) {
                $result[] = [
                    'attribute' => $attribute->productAttribute,
                    'option' => $attribute->option
                ];
            }

            return $result;
        }

        public function actionGetCrossSellProductsAll($id)
        {
            if ($id == 0) {
                $products = Product::find()->all();
            } else {
                $products = (new Query())
                    ->select('*')
                    ->from('fs_product product')
                    ->where(['not exists', (new Query)
                        ->select('*')
                        ->from('fs_product_sell_cross psc')
                        ->where('product.id = psc.cross_product_id')
                        ->andWhere(['=', 'psc.product_id', $id])])
                    ->andWhere(['!=', 'product.id', $id])
                    ->all();
            }

            return $products;
        }

        public function actionGetUpSellProductsAll($id)
        {
            if ($id == 0) {
                $products = Product::find()->where(['type' => 'extra', 'deleted' => 0])->all();
            } else {
                $products = (new Query())
                    ->select('*')
                    ->from('fs_product product')
                    ->where(['not exists', (new Query)
                        ->select('*')
                        ->from('fs_product_sell_up psu')
                        ->where('product.id = psu.up_product_id')
                        ->andWhere(['=', 'psu.product_id', $id])])
                    ->andWhere(['!=', 'product.id', $id])
                    ->andWhere(['=', 'product.type', 'extra'])
                    ->all();
            }

            return $products;
        }

        public function actionGetCrossSellProduct($id)
        {
            $products = ProductSellCross::find()->where(['product_id' => $id])->all();
            $result = [];

            foreach ($products AS $product) {
                $result[] = [
                    'product' => Product::findOne($product->cross_product_id),
                    'packageCost' => $product->cost
                ];
            }

            return $result;
        }

        public function actionGetUpSellProduct($id)
        {
            /** @var ProductSellUp[] $products */
            $products = ProductSellUp::find()->where(['product_id' => $id])->all();
            $result = [];

            foreach ($products AS $product) {
                $productObject = Product::findOne($product->up_product_id);
                $productObject->cost = $product->cost;

                $result[] = [
                    'product' => $productObject
                ];
            }

            return $result;
        }

        public function actionGetProduct($id, $type = Product::TYPE_STANDARD)
        {
            return Product::find()->where(['id' => $id, 'deleted' => 0, 'type' => $type])->one();
        }
    }
