<?php
    namespace backend\controllers;

    use Yii;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use backend\models\MailMessagePart;

    class MailMessagePartController extends BaseController
    {
        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => MailMessagePart::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }
        
        public function actionUpdate($id)
        {
            /** MailMessagePart $model */
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['mail/index']);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        protected function findModel($id)
        {
            /** MailMessagePart $model */
            if (($model = MailMessagePart::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
