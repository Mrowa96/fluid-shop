<?php
    namespace backend\controllers;

    use Yii;
    use common\models\OrderStatus;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;

    class OrderStatusController extends BaseController
    {
        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => OrderStatus::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['index']);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        protected function findModel($id)
        {
            /** @var OrderStatus $model*/
            if (($model = OrderStatus::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
