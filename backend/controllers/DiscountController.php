<?php
    namespace backend\controllers;

    use Yii;
    use common\models\Discount;
    use yii\data\ActiveDataProvider;
    use backend\controllers\BaseController;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    class DiscountController extends BaseController
    {
        public function actionIndex()
        {
            $dynamicProvider = new ActiveDataProvider([
                'query' => Discount::find()->where(['dynamic' => 1, 'deleted' => 0]),
            ]);
            $staticProvider = new ActiveDataProvider([
                'query' => Discount::find()->where(['dynamic' => 0, 'deleted' => 0]),
            ]);

            return $this->render('index', [
                'dynamicProvider' => $dynamicProvider,
                'staticProvider' => $staticProvider,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new Discount();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            } else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);
            $model->deleted = 1;
            $model->save();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /** @var Discount $model */
            if (($model = Discount::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
