<?php
    namespace backend\controllers;

    use Yii;
    use common\models\ProductAttribute;
    use yii\data\ActiveDataProvider;
    use backend\controllers\BaseController;
    use yii\web\NotFoundHttpException;
    use yii\filters\VerbFilter;

    class ProductAttributeController extends BaseController
    {
        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(Yii::$app->request->referrer);
        }

        protected function findModel($id)
        {
            if (($model = ProductAttribute::findOne($id)) !== null) {
                return $model;
            } else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
