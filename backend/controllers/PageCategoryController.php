<?php
    namespace backend\controllers;

    use common\models\Page;
    use Yii;
    use common\models\PageCategory;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;

    class PageCategoryController extends BaseController
    {
        public function actionIndex()
        {
            $dataProvider = new ActiveDataProvider([
                'query' => PageCategory::find(),
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new PageCategory();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /** @var Page $model */
            if (($model = PageCategory::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
