<?php
    namespace backend\controllers;

    use Yii;
    use yii\web\NotFoundHttpException;
    use backend\models\MailMessage;

    class MailMessageController extends BaseController
    {
        public function actionCreate()
        {
            $model = new MailMessage();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['mail/index']);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['mail/index']);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $model = $this->findModel($id);

            if($model->deletable === 1){
                $model->delete();
            }

            return $this->redirect(['mail/index']);
        }

        protected function findModel($id)
        {
            /** @var MailMessage $model */
            if (($model = MailMessage::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
