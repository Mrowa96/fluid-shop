<?php
    namespace backend\controllers;

    use Yii;
    use backend\models\ProductExtra;
    use yii\web\NotFoundHttpException;
    use backend\models\GridViewConfig;
    use backend\searches\Product as ProductSearch;
    use common\models\Product;

    class ProductExtraController extends BaseController
    {
        public function actionIndex()
        {
            $searchModel = new ProductSearch();
            $gridViewColumns = GridViewConfig::prepareData(Product::className());
            $dataProvider = $searchModel->search(Yii::$app->request->queryParams, "extra");

            return $this->render('index', [
                'searchModel' => $searchModel,
                'gridViewColumns' => $gridViewColumns,
                'dataProvider' => $dataProvider,
            ]);
        }

        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }

        public function actionCreate()
        {
            $model = new ProductExtra();

            if ($model->load(Yii::$app->request->post())) {
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    return $this->redirect(['index']);
                }
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }

        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post())) {
                if($model->save()){
                    return $this->redirect(['view', 'id' => $model->id]);
                }
                else{
                    return $this->redirect(['index']);
                }
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }

        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /** @var Product $model */
            if (($model = ProductExtra::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
