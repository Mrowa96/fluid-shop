<?php
    namespace backend\controllers;

    use Yii;
    use common\models\Page;
    use yii\data\ActiveDataProvider;
    use yii\web\NotFoundHttpException;
    use backend\models\GridViewConfig;
    use yii\web\UrlManager;

    class PageController extends BaseController
    {
        public function actionIndex()
        {
            $gridViewColumns = GridViewConfig::prepareData(Page::className());
            $dataProvider = new ActiveDataProvider([
                'query' => Page::find()->where(['deleted' => 0]),
                'pagination' => [
                    'pageSize' => 10,
                ],
            ]);

            return $this->render('index', [
                'dataProvider' => $dataProvider,
                'gridViewColumns' => $gridViewColumns,
            ]);
        }
        public function actionView($id)
        {
            return $this->render('view', [
                'model' => $this->findModel($id),
            ]);
        }
        public function actionRedirect($id)
        {
            /** @var UrlManager $urlManager */
            $model = $this->findModel($id);
            $urlManager = Yii::$app->urlManagerFrontend;

            return $this->redirect($urlManager->createUrl(['site/page', 'url' => $model->url, 'id' => $model->id]));
        }
        public function actionCreate()
        {
            $model = new Page();

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('create', [
                    'model' => $model,
                ]);
            }
        }
        public function actionUpdate($id)
        {
            $model = $this->findModel($id);

            if ($model->load(Yii::$app->request->post()) && $model->save()) {
                return $this->redirect(['view', 'id' => $model->id]);
            }
            else {
                return $this->render('update', [
                    'model' => $model,
                ]);
            }
        }
        public function actionDelete($id)
        {
            $model = $this->findModel($id);
            $model->deleted = 1;
            $model->save();

            return $this->redirect(['index']);
        }
        public function actionDeleteSelected($ids)
        {
            /** @var Product[] $models */
            $models = Product::find()->where(['id' => explode(",", $ids)])->all();

            foreach($models AS $model){
                $model->deleted = 1;
                $model->save();
            }

            return $this->redirect(['index']);
        }

        protected function findModel($id)
        {
            /** @var Page $model */
            if (($model = Page::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
