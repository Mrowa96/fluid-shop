<?php
    namespace backend\controllers;

    use League\Csv\Writer;
    use Yii;
    use yii\web\NotFoundHttpException;
    use common\models\MailSubscriber;
    use backend\forms\MailSubscriber as MailSubscriberForm;

    class MailSubscriberController extends BaseController
    {
        public function actionExport()
        {
            /** @var Writer $csv */
            $csv = MailSubscriber::export();
            $csv->output('subscribers.csv');

            exit;
        }

        public function actionImport()
        {
            $model = new MailSubscriberForm();

            if($model->load(Yii::$app->request->post())){
                if(MailSubscriber::import($model->getCsvFile())){
                    return $this->redirect(['mail/index']);
                }
                else{
                    return $this->redirect(['mail/index']);
                }
            }
            else{
                return $this->render("import-subscribers", [
                    'model' => $model
                ]);
            }
        }

        public function actionDelete($id)
        {
            $this->findModel($id)->delete();

            return $this->redirect(['mail/index']);
        }

        protected function findModel($id)
        {
            /** MailSubscriber $model */
            if (($model = MailSubscriber::findOne($id)) !== null) {
                return $model;
            }
            else {
                throw new NotFoundHttpException('The requested page does not exist.');
            }
        }
    }
