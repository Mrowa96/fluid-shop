SELECT sb.category, sb.message, m.translation
FROM fs_i18n_source_backend sb -- also fs_i18n_source_frontend
INNER JOIN fs_i18n_message AS m ON m.source_id = sb.id
WHERE m.i18n_id = 1 -- language id