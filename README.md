Fluid Shop
============================

DIRECTORY STRUCTURE
-------------------

```
backend
    assets/              contains application assets configuration
    config/              contains backend configurations
    controllers/         contains Web controller classes
    forms/               contains backend-specific form classes
    models/              contains backend-specific model classes
    runtime/             contains files generated during runtime
    searches/            contains backend-specific search classes
    utilities/           contains backend-specific classes wchic does not operate on database directly
    var/                 contains invoices, rma raport and others
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
common
    assets/              contains application assets such as JavaScript and CSS used in frontend and backend
    components/          contains shared view components
    config/              contains shared configurations
    fixtures/            contains fixtures used during tests
    forms/               contains common-specific form classes
    interfaces/          contains interfaces
    models/              contains model classes used in both backend and frontend
    runtime/             contains files generated during runtime?
    services/            contains services used in frontend and backend
    utilities/           contains common-specific classes wchic does not operate on database directly
    views/               contains common-specific views e.g for common components
console
    config/              contains console configurations
    controllers/         contains console controllers (commands)
    migrations/          contains database migrations
    models/              contains console-specific model classes
    runtime/             contains files generated during runtime
    utilities/           contains classes for e.g. InitManager
extras/                  contains extras e.g. database dump, or example files to test product import
frontend
    assets/              contains application assets configuration
    components/          contains widgets used in views
    config/              contains frontend configurations
    controllers/         contains Web controller classes
    forms/               contains frontend-specific form classes
    models/              contains frontend-specific model classes
    runtime/             contains files generated during runtime
    services/            contains services for frontend
    tests/               contains tests for frontend
    utilities/           contains frontend-specific classes which does not operate on database directly and are not a service
    var/                 contains generated files which are used in appliaction
    views/               contains view files for the Web application
    web/                 contains the entry script and Web resources
vendor/                  contains dependent 3rd-party packages
```

REQUIREMENTS
------------

The minimum requirement by this project template that your Web server supports PHP 5.5.0 or even 7.0 (recommended).

INSTALLATION
------------
If you do not have [Composer](http://getcomposer.org/), you may install it by following the instructions
at [getcomposer.org](http://getcomposer.org/doc/00-intro.md#installation-nix).

Next, just run init_instance file in root directory and follow the steps or
```
php composer.phar global require "fxp/composer-asset-plugin:~1.1.1"
php composer.phar install
```

Remember to create manually bootstrap.php in common/config dir.

ADDITIONAL CONFIGURATION
------------------------
#### Vhost

Create your vhost file, it can be based on example vhost, which you can find in extras directory.

### Elasticsearch

Remember to setup elasticsearch server for searching. For example, Fedora developers can install it by one command:
```dnf install elasticsearch```

#### Permissions

Some directories must have full permissions (777):
 - frontend/runtime
 - frontend/var
 - frontend/web/cache
 - frontend/web/data
 - backend/runtime
 - backend/var
 - backend/web/cache
 - console/runtime

 Also it is good when whole project is owned by httpd server user and group, so I'm recommending recursive chown on whole directory

PREVIEW INITIALIZATION
--------------------
After installing vendor packages, run:
 - sh fluid_shop.sh 
    - choose 2
 - php fs_console init/set-database-connection
 - php fs_console init/generate-app-configuration
 - php fs_console init/prepare-preview-environment
 - sh fluid_shop.sh 
    - choose 3
    - choose 4
 - yii_test migrate
 - vendor/bin/codecept run -- -c frontend
 
ACCESS
------
##### System account
Login: system
Password: generated during initialization

#### Admin account
Login: admin
Password: generated during initialization

### Google account - for Google Merchant Centre
Url: /get-google-merchant-data.html
Login: google
Password: generated during initialization

CRON
----
To use cron function just type php ``` fs_console cron/<action> ```. To list all available command type ``` php fs_console cron ```

INIT
----
To use init function just type php ``` fs_console init/<action> ```. To list all available command type ``` php fs_console init ```

TESTS
-----
- You must set up test database by execution ```php yii_test migrate```

For now, only frontend unit tests are supported. You can launch them by executing command like this: ``` vendor/bin/codecept run -- -c frontend ```
You can read more here: https://github.com/yiisoft/yii2-app-advanced/blob/master/docs/guide/start-testing.md

CAUTION: 
- Fixtures are working only with codecept
- Custom mailer is override by codeception own mailer, so be careful