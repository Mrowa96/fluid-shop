<?php
    namespace common\fixtures;

    use yii\test\ActiveFixture;

    class Settings extends ActiveFixture
    {
        public $modelClass = 'common\models\Settings';
    }