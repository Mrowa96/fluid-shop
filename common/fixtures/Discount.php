<?php
    namespace common\fixtures;

    use yii\test\ActiveFixture;

    class Discount extends ActiveFixture
    {
        public $modelClass = 'common\models\Discount';
    }