<?php
    namespace common\fixtures;

    use yii\test\ActiveFixture;

    class PageCategory extends ActiveFixture
    {
        public $modelClass = 'common\models\PageCategory';
    }