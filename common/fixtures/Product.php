<?php
    namespace common\fixtures;

    use yii\test\ActiveFixture;

    class Product extends ActiveFixture
    {
        public $modelClass = 'common\models\Product';
    }