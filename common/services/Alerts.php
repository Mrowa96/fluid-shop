<?php
    namespace common\services;

    use common\services\Alerts\Alert;

    /**
     * Class Alerts
     * @package common\services
     *
     * @property Alert[] $alerts
     */
    class Alerts
    {
        protected $alerts;

        const SESSION_ALERTS_KEY = 'fs-alerts';

        /**
         * Alerts constructor.
         */
        public function __construct()
        {
            $this->alerts = \Yii::$app->session->getFlash(Alerts::SESSION_ALERTS_KEY, []);
        }

        /**
         * @param Alert $alert
         *
         * @return int
         */
        public function addAlert(Alert $alert)
        {
            $index = array_push($this->alerts, $alert);

            $this->triggerUpdate();

            return $index;
        }

        /**
         * @param int $index
         * @return bool
         */
        public function removeAlert($index)
        {
            $index = (int)$index;

            if (!empty($this->alerts[$index])) {
                unset($this->alerts[$index]);

                $this->triggerUpdate();

                return true;
            }

            return false;
        }

        /**
         * @param int $index
         * @return bool
         */
        public function hasAlert($index)
        {
            return !empty($this->alerts[$index]);
        }

        /**
         * @return bool
         */
        public function hasAlerts()
        {
            return !empty($this->alerts);
        }

        /**
         * @return Alert[]
         */
        public function getAlerts()
        {
            return $this->alerts;
        }

        /**
         * @param int $index
         * @return Alert|null
         */
        public function getAlert($index)
        {
            if ($this->hasAlert($index)) {
                return $this->alerts[$index];
            }

            return null;
        }

        /**
         * @return Alert|null
         */
        public function getLastAlert()
        {
            $alert = null;

            if ($this->hasAlerts()) {
                $alert = end($this->alerts);
            }

            reset($this->alerts);

            return $alert;
        }

        /**
         * @return Alert|null
         */
        public function getFirstAlert()
        {
            return $this->hasAlerts() ? current($this->alerts) : null;
        }

        /**
         * If more methods will be used like this, implement Proxy or events from Component
         *
         * @return void
         */
        protected function triggerUpdate()
        {
            \Yii::$app->session->setFlash(Alerts::SESSION_ALERTS_KEY, $this->getAlerts());
        }
    }