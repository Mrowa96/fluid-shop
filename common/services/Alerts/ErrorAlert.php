<?php
    namespace common\services\Alerts;

    /**
     * Class ErrorAlert
     * @package common\services\Alerts
     */
    final class ErrorAlert extends Alert
    {
        protected $type = Alert::TYPE_ERROR;
    }