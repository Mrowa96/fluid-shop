<?php
    namespace common\services\Alerts;

    /**
     * Class SuccessAlert
     * @package common\services\Alerts
     */
    final class SuccessAlert extends Alert
    {
        protected $type = Alert::TYPE_SUCCESS;
    }