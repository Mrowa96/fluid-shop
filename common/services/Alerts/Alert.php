<?php
    namespace common\services\Alerts;

    /**
     * Class Alert
     * @package common\services\Alerts
     *
     * @property string $message
     * @property string $type
     */
    abstract class Alert
    {
        protected $message;
        protected $type;

        const TYPE_SUCCESS = 'success';
        const TYPE_WARNING = 'warning';
        const TYPE_ERROR = 'error';

        /**
         * Alert constructor.
         * @param string $message
         */
        public function __construct($message)
        {
            $this->setMessage($message);
        }

        /**
         * @return string
         */
        public function getMessage()
        {
            return $this->message;
        }

        /**
         * @param string $message
         */
        public function setMessage($message)
        {
            $this->message = (string)$message;
        }

        /**
         * @return string
         */
        public function getType()
        {
            return $this->type;
        }
    }