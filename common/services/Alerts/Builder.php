<?php
    namespace common\services\Alerts;

    /**
     * Class Builder
     * @package common\services\Alerts
     */
    class Builder
    {
        /**
         * @param string $message
         * @param string $type
         * @return Alert
         */
        public static function build($message, $type = Alert::TYPE_SUCCESS)
        {
            switch ($type) {
                case Alert::TYPE_ERROR:
                    $alert = new ErrorAlert($message);
                    break;
                case Alert::TYPE_WARNING:
                    $alert = new WarningAlert($message);
                    break;
                case Alert::TYPE_SUCCESS:
                default:
                    $alert = new SuccessAlert($message);
                    break;
            }

            return $alert;
        }
    }