<?php
    namespace common\services\Alerts;

    /**
     * Class WarningAlert
     * @package common\services\Alerts
     */
    final class WarningAlert extends Alert
    {
        protected $type = Alert::TYPE_WARNING;
    }