<?php
    namespace common\utilities;

    use Yii;
    use yii\web\UrlManager;
    use common\models\Product;

    class GoogleMerchant
    {
        const UNIVERSAL_DATA_PACKAGE = 1000;

        /** @var UrlManager $urlManager */
        protected $urlManager;
        protected $filePath;

        public function __construct()
        {
            $this->filePath = Yii::getAlias('@googleMerchantData') . DIRECTORY_SEPARATOR . 'feed_' . (new \DateTime())->format('Y-m-d H:i:s') . '.xml';
        }

        public function setUrlManager(UrlManager $urlManager)
        {
            $this->urlManager = $urlManager;
        }

        public function indexAll()
        {
            if($this->urlManager){
                /** @var Product[] $products */
                $offset = 0;
                $xml = new \SimpleXMLElement('<?xml version="1.0" encoding="UTF-8" ?><rss xmlns:g="http://base.google.com/ns/1.0"></rss>');
                $xml->addAttribute('version', "2.0");

                $channel = $xml->addChild('channel');
                $channel->addChild('title', 'Fluid Shop');
                $channel->addChild('link', Yii::getAlias("@frontendLink"));
                $channel->addChild('description',
                    "This is a sample feed containing the required and recommended attributes for a variety of different products");

                while($products = Product::find()->where(['deleted' => 0, 'type' => 'standard'])->offset($offset)->limit(self::UNIVERSAL_DATA_PACKAGE)->all()){
                    foreach($products AS $product){
                        $mainImage = $product->getMainImage();
                        $item = $xml->addChild('item');

                        $item->addChild('g:id', $product->id);
                        $item->addChild('g:title', html_entity_decode(strip_tags($product->name), null, "UTF-8"));
                        $item->addChild('g:description', html_entity_decode(strip_tags($product->description), null, "UTF-8"));
                        $item->addChild('g:link', htmlspecialchars($this->urlManager->createUrl(['product/details',
                                'id' => $product->id,
                                'url' => $product->processed_name]
                        )));
                        $item->addChild('g:image_link', $mainImage->path);
                        $item->addChild('g:condition', 'new');
                        $item->addChild('g:availability', ($product->available) ? 'in stock' : 'out of stock');
                        $item->addChild('g:price', $product->getCost(true));
                        $item->addChild('g:brand', $product->brand);
                        $item->addChild('g:gtin', $product->code);
                        $item->addChild('g:mpn', $product->id);
                    }

                    $offset += self::UNIVERSAL_DATA_PACKAGE;
                }

                return $xml->saveXML($this->filePath);
            }
            else{
                throw new \Exception("Frontend urlManager is not set up correctly.");
            }
        }
    }