<?php
    namespace common\utilities;

    use Yii;
    use yii\base\Widget;
    use yii\helpers\Html;

    class Message extends Widget
    {
        private $message;
        private $type;
        public $view;

        public function init()
        {
            parent::init();

            if($this->view !== null){
                $this->handleMessage();
                $this->handleType();
            }
            else{
                throw new \Exception("You must set view property.");
            }
        }

        public function run()
        {
            if($this->message !== null){
                return $this->view->registerJs("window.addEventListener('load', function(){
                    new Fluid.Notify('".$this->message."', '".$this->type."');
                });");
            }
        }

        private function handleMessage()
        {
            if(isset($this->view->params['message'])){
                $this->message = $this->view->params['message'];
            }
            elseif(Yii::$app->session->get("message")){
                $this->message = Yii::$app->session->get("message");
            }
            else{
                $this->message = null;
            }
        }

        private function handleType()
        {
            if(isset($this->view->params['type'])){
                $this->type = $this->view->params['type'];
            }
            elseif(Yii::$app->session->get("type")){
                $this->type = Yii::$app->session->get("type");
            }
            else{
                $this->type = "success";
            }
        }
    }