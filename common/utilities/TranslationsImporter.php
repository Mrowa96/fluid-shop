<?php
    namespace common\utilities;

    use backend\models\LanguageSource as BackendLanguageSource;
    use common\models\LanguageMessage;
    use frontend\models\LanguageSource as FrontendLanguageSource;
    use common\models\Language;
    use fluid\fileManager\Directory;
    use fluid\fileManager\File;
    use Yii;
    use yii\helpers\Json;

    /**
     * Class TranslationsImporter
     * @package common\utilities
     *
     * @property File[] files
     * @property Language[] i18ns
     * @property Directory backendDir
     * @property Directory frontendDir
     * @property string activeModule
     * @property Directory activeDirectory
     * @property BackendLanguageSource | FrontendLanguageSource activeSourceClass
     */
    class TranslationsImporter
    {
        const MODULE_BACKEND = 'backend';
        const MODULE_FRONTEND = 'frontend';
        const SOURCE_FILENAME = 'source.json';

        /** Index is id of i18n language*/
        protected $files = [];
        protected $i18ns = [];
        protected $backendDir;
        protected $frontendDir;
        protected $activeModule = null;
        protected $activeDirectory = null;
        protected $activeSourceClass = null;

        public function __construct()
        {
            $this->backendDir = new Directory(Yii::getAlias('@dataBackendTranslations'));
            $this->frontendDir = new Directory(Yii::getAlias('@dataFrontendTranslations'));
        }

        public function importTranslations($module)
        {
            switch ($module) {
                case self::MODULE_BACKEND:
                    if ($this->backendDir->exists) {
                        $this->activeDirectory = $this->backendDir;
                        $this->activeModule = self::MODULE_BACKEND;

                        $reflectionSource = new \ReflectionClass(BackendLanguageSource::className());
                        $this->activeSourceClass = $reflectionSource->newInstanceWithoutConstructor();
                    }
                    break;
                case self::MODULE_FRONTEND:
                    if ($this->frontendDir->exists) {
                        $this->activeDirectory = $this->frontendDir;
                        $this->activeModule = self::MODULE_FRONTEND;

                        $reflectionSource = new \ReflectionClass(FrontendLanguageSource::className());
                        $this->activeSourceClass = $reflectionSource->newInstanceWithoutConstructor();
                    }
                    break;
            }

            if ($this->activeModule && $this->activeDirectory) {
                $this->loadFilesForModule();
                $this->clearTables();
                $this->insertSource();

                return $this->insertTranslations();
            }
        }

        protected function clearTables()
        {
            /** @var BackendLanguageSource[] | FrontendLanguageSource[] $sources */
            $sources = $this->activeSourceClass->find()->all();
            $i18nIds = array_map(function ($i18n) {
                return $i18n->id;
            }, $this->i18ns);

            foreach ($sources AS $source) {
                LanguageMessage::deleteAll(['source_id' => $source->id, 'i18n_id' => $i18nIds]);
            }

            $this->activeSourceClass->deleteAll();
        }

        protected function loadFilesForModule()
        {
            foreach ($this->activeDirectory->objects as $object) {
                $file = new File($this->activeDirectory->path . DS . $object);

                if ($file->exists && $file->basename !== self::SOURCE_FILENAME) {
                    /** @var Language $i18n */
                    if ($i18n = Language::existsForModule($this->activeModule, $file->name)) {
                        $this->files[$i18n->id] = $file;
                        $this->i18ns[] = $i18n;
                    }
                }
            }
        }

        protected function insertSource()
        {
            $file = new File($this->activeDirectory->path . DS . self::SOURCE_FILENAME);

            if ($file->exists) {
                $source = Json::decode($file->read());

                Yii::$app->db->createCommand()
                    ->batchInsert('{{%i18n_source_' . $this->activeModule . '}}', ['id','category', 'message'], $source)
                    ->execute();
            }
        }

        protected function insertTranslations()
        {
            foreach ($this->files as $i18nId => $file) {
                $translations = Json::decode($file->read());

                foreach ($translations as &$translation) {
                    $translation['i18n_id'] = $i18nId;
                }

                Yii::$app->db->createCommand()
                    ->batchInsert('{{%i18n_message}}', ['source_id', 'translation', 'i18n_id'], $translations)
                    ->execute();

            }

            return true;
        }
    }