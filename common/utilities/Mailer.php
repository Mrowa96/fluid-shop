<?php
    namespace common\utilities;

    use common\models\Settings;
    use common\models\Settings\Smtp;
    use yii\swiftmailer\Mailer as SwiftMailer;

    class Mailer extends SwiftMailer{

        private $host;
        private $login;
        private $password;
        private $port;
        private $encryption;

        public function __construct(array $config = [])
        {
            $this->host = Settings::getOne('host', Smtp::MODULE);
            $this->port = Settings::getOne('port', Smtp::MODULE);
            $this->login = Settings::getOne('login', Smtp::MODULE);
            $this->password = Settings::getOne('password', Smtp::MODULE);
            $this->encryption = Settings::getOne('encryption', Smtp::MODULE);

            $config['transport'] = [
                'class' => 'Swift_SmtpTransport',
                'host' => $this->host,
                'username' => $this->login,
                'password' => $this->password,
                'port' => $this->port,
                'encryption' => $this->encryption,
            ];

            parent::__construct($config);
        }

        public function compose($view = null, array $params = [])
        {
            if(empty($this->host) || empty($this->port) || empty($this->login) || empty($this->password) || empty($this->encryption)){
                throw new \Exception("Mailer is not configured in Settings");
            }

            return parent::compose($view, $params);
        }

        public function getHost()
        {
            return $this->host;
        }

        public function getLogin()
        {
            return $this->login;
        }

        public function getPassword()
        {
            return $this->password;
        }

        public function getPort()
        {
            return $this->port;
        }

        public function getEncryption()
        {
            return $this->encryption;
        }
    }