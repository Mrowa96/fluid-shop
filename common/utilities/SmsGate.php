<?php
    namespace common\utilities;

    use common\models\Settings;
    use common\models\Settings\Sms;

    class SmsGate
    {
        private $login;
        private $password;
        private $sender;
        private $host;
        protected $body;
        protected $phoneNo;

        public function __construct(array $config = [])
        {
            $this->host = "https://api.mobitex.pl/sms.php";
            $this->port = Settings::getOne('port', Sms::MODULE);
            $this->login = Settings::getOne('login', Sms::MODULE);
            $this->sender = Settings::getOne('password', Sms::MODULE);
        }

        public function compose()
        {
            if(empty($this->login) || empty($this->password) || empty($this->sender)){
                throw new \Exception("SMS is not configured in Settings");
            }

            return $this;
        }
        public function setBody($body)
        {
            $this->body = $body;

            return $this;
        }
        public function setTo($phoneNo)
        {
            $phoneNo = str_replace(' ','', $phoneNo);
            $phoneNo = str_replace('-','', $phoneNo);

            if(mb_strlen($phoneNo) < 11) {
                $phoneNo = "48" . $phoneNo ;
            }

            if(mb_strlen($phoneNo) === 11){
                $this->phoneNo = $phoneNo;

                return $this;
            }
            else{
                throw new \Exception("Phone number is in wrong format");
            }
        }
        public function send()
        {
            $postData = [
                "user" => $this->login,
                "pass" => md5($this->password),
                "from" => $this->sender,
                "number" => $this->phoneNo,
                "text" => $this->body,
                "type" => "sms"
            ];

            $curl = curl_init($this->host);
            curl_setopt($curl, CURLOPT_TIMEOUT, 5);
            curl_setopt($curl, CURLOPT_POST, 1);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $postData);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);

            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, false);

            return curl_exec($curl);
        }
    }