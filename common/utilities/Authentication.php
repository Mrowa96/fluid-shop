<?php
    namespace common\utilities;

    use fluid\fileManager\File;
    use GuzzleHttp\Client;
    use yii\helpers\Json;

    /**
     * Class Authentication
     * @package common\utilities
     *
     * @property string $licencePath
     * @property File $licenceFile
     * @property array $messages
     * @property string $key
     *
     * @var string KEY_PREFIX
     * @var int SERVER_FAIL_TIMEOUT
     * @var int CHECKER_DELAY
     * @var string CHECKER_URL
     */
    class Authentication
    {
        protected $licencePath;
        protected $licenceFile;
        protected $messages;
        protected $key;

        const KEY_PREFIX = 'licence_';
        const SERVER_FAIL_TIMEOUT = 60 * 60;
        const CHECKER_DELAY = 60;
        const CHECKER_URL = 'https://tavoite.pl/fluid-shop/check-key.html';

        public function __construct()
        {
            $this->licencePath = \Yii::getAlias('@licence');
            $this->licenceFile = new File($this->licencePath);
            $this->messages = $this->setMessages();
        }

        public function checkLicence($clearCache = false)
        {
            $this->loadKey();

            if ($clearCache === true) {
                \Yii::$app->cache->delete($this->getCacheKey());
            }


            if (\Yii::$app->cache->exists($this->getCacheKey())) {
                return true;
            } else {
                try {
                    $client = new Client(['timeout' => 2.0]);
                    $response = $client->request('GET', self::CHECKER_URL, [
                        'query' => ['key' => $this->key]
                    ]);
                } catch (\Exception $ex) {
                    $this->setCachedLicence(self::SERVER_FAIL_TIMEOUT);
                    \Yii::error($ex, "authentication");

                    return true;
                }

                if (isset($response) && $response->getStatusCode() === 200) {
                    $responseData = Json::decode($response->getBody(), false);

                    if ($responseData instanceof \stdClass && isset($responseData->status)) {
                        if ($responseData->status && isset($responseData->expire)) {
                            $this->setCachedLicence($responseData->expire);

                            return true;
                        } else {
                            if (isset($responseData->message)) {
                                throw new \Exception($responseData->message, 110);
                            }
                        }
                    }
                }

                throw new \Exception($this->getMessage(102), 102);
            }
        }

        protected function setMessages()
        {
            return [
                100 => \Yii::t('licence', 'Licence file is not provided.'),
                101 => \Yii::t('licence', 'Licence file exists, but key is not provided.'),
                102 => \Yii::t('licence', 'Problem with licence checking server.'),
            ];
        }

        protected function getMessage($key)
        {
            if (array_key_exists($key, $this->messages)) {
                return $this->messages[$key];
            }
        }

        protected function loadKey()
        {
            if ($this->licenceFile->exists) {
                $data = Json::decode($this->licenceFile->read(), false);

                if (isset($data->key)) {
                    $this->key = $data->key;
                } else {
                    throw new \Exception($this->getMessage(101), 101);
                }
            } else {
                throw new \Exception($this->getMessage(100), 100);
            }
        }

        protected function setCachedLicence($expire)
        {
            \Yii::$app->cache->set($this->getCacheKey(), 'authenticated', $expire + self::CHECKER_DELAY);
        }

        protected function getCacheKey()
        {
            return self::KEY_PREFIX . $this->key;
        }

    }