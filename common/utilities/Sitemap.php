<?php
    namespace common\utilities;

    use Yii;
    use yii\web\UrlManager;
    use common\interfaces\SitemapItem as SitemapItem;
    use fluid\fileManager\File;

    /**
     * Class Sitemap
     * @package common\utilities
     *
     * @property \SimpleXMLElement $xml
     * @property array $defaultData
     */
    class Sitemap
    {
        protected $xml;
        protected $defaultData = [];

        const NO_ACTIVE_RECORD_AS_PARENT = 100;

        public function __construct()
        {
            $this->xml = new \SimpleXMLElement("<xml/>");
            $this->xml->addChild('urlset')->addAttribute('xmlns', 'http://www.sitemaps.org/schemas/sitemap/0.9');

            $this->setUpDefault();
        }

        public function add($className)
        {
            $reflectionClass = new \ReflectionClass($className);

            if ($reflectionClass->implementsInterface(SitemapItem::class)) {
                $instance = $reflectionClass->newInstanceWithoutConstructor();
                $data = $instance->fetchForSitemap();

                /** @var SitemapItem $item */
                foreach ($data AS $item) {
                    $urlRoot = $this->xml->addChild('url');

                    $urlRoot->addChild('loc', htmlspecialchars($item->getUrl()));
                    $urlRoot->addChild('lastmod', $item->getLastModified());
                    $urlRoot->addChild('changefreq', $item->getChangeFrequency());
                    $urlRoot->addChild('priority', $item->getPriority());
                }
            } else {
                throw new \Exception("You must pass class which is a child of ActiveRecord class.", self::NO_ACTIVE_RECORD_AS_PARENT);
            }
        }

        public function generate()
        {
            $this->addDefault();

            return $this->xml->saveXML(self::getPath());
        }

        public static function exists()
        {
            $file = new File(self::getPath());

            return $file->exists;
        }

        public static function getPath()
        {
            return \Yii::getAlias('@frontend') . "/web/sitemap.xml";
        }

        public static function getUrl()
        {
            return \Yii::getAlias("@frontendLink") . "/sitemap.xml";
        }

        protected function setUpDefault()
        {
            $this->defaultData = [
                [
                    'loc' => Yii::$app->urlManagerFrontend->getBaseUrl(),
                    'lastmod' => (new \DateTime())->format("Y-m-d H:i:s"),
                    'changefreq' => SitemapItem::FREQ_ALWAYS,
                    'priority' => SitemapItem::PRIORITY_MAX,
                ],
                [
                    'loc' => Yii::$app->urlManagerFrontend->createUrl(['site/login']),
                    'lastmod' => (new \DateTime())->format("Y-m-d H:i:s"),
                    'changefreq' => SitemapItem::FREQ_NEVER,
                    'priority' => SitemapItem::PRIORITY_LOW,
                ],
                [
                    'loc' => Yii::$app->urlManagerFrontend->createUrl(['site/signup']),
                    'lastmod' => (new \DateTime())->format("Y-m-d H:i:s"),
                    'changefreq' => SitemapItem::FREQ_NEVER,
                    'priority' => SitemapItem::PRIORITY_LOW,
                ]
            ];
        }

        protected function addDefault()
        {
            foreach ($this->defaultData AS $data) {
                $urlRoot = $this->xml->addChild('url');

                $urlRoot->addChild('loc', $data['loc']);
                $urlRoot->addChild('lastmod', $data['lastmod']);
                $urlRoot->addChild('changefreq', $data['changefreq']);
                $urlRoot->addChild('priority', $data['priority']);
            }
        }
    }