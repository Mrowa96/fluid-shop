<?php
    namespace common\utilities;

    use common\models\Product;
    use common\models\Settings;
    use Elasticsearch\ClientBuilder;

    /**
     * Class ElasticSearch
     * @package common\utilities
     *
     * @property \Elasticsearch\Client $client
     * @property string $index
     * @property array $hosts
     * @property int $documentsPerPackage
     */
    class ElasticSearch
    {
        protected $client;
        protected $index;
        protected $hosts = ['127.0.0.1:9200'];
        protected $documentsPerPackage = 1000;

        public function __construct()
        {
            $this->client = ClientBuilder::create()->setHosts($this->hosts)->build();
            $this->index = Settings::getOne('search_index_name', Settings\System::MODULE);
        }

        public function indexAll()
        {
            $offset = 0;
            $params = [
                'body' => []
            ];

            if($this->client->indices()->exists(['index' => $this->index])) {
                $this->client->indices()->delete([
                    'index' => $this->index
                ]);
            }

            while($products = Product::find()->where(['deleted' => 0, 'type' => 'standard'])->offset($offset)->limit($this->documentsPerPackage)->all()){
                foreach($products AS $product){
                    /** @var Product $product */
                    $params['body'][] = [
                        'index' => [
                            '_index' => $this->index,
                            '_type' => 'product',
                            '_id' => $product->id
                        ]
                    ];

                    $params['body'][] = $this->createSingleDocument($product);
                }

                $this->client->bulk($params);
                $params = ['body' => []];
                $offset += $this->documentsPerPackage;
            }

            if (!empty($params['body'])) {
                $this->client->bulk($params);
            }
        }
        public function indexOne(Product $product)
        {
            if($product->deleted === 0 && $product->type === "standard"){
                $params = [
                    'index' => $this->index,
                    'type' => 'product',
                    'id' => $product->id,
                    'body' => [
                        'doc' => $this->createSingleDocument($product),
                        'upsert' => $this->createSingleDocument($product)
                    ]
                ];

                try{
                    $this->client->update($params);
                }
                catch(\Exception $ex){
                    \Yii::error($ex, "search");
                }
            }

            return false;
        }
        public function queryProducts($phrase, $size = 20)
        {
            $params = [
                'index' => $this->index,
                'type' => 'product',
                'body' => [
                    'query' => [
                        'bool' => [
                            'should' => [
                                [
                                    'multi_match' => [
                                        'query' => $phrase,
                                        'fuzziness' => 'AUTO',
                                        'operator' => 'and',
                                        'fields' => ['name', 'description']
                                    ]
                                ],
                                [
                                    'match_phrase_prefix' => [
                                        'name' => $phrase
                                    ]
                                ],
                            ]
                        ],
                    ],
                    'size' => $size,
                ]
            ];

            try{
                $results = $this->client->search($params);

                return $this->prepareProductsResults($results);
            }
            catch(\Exception $ex) {
                \Yii::error($ex, "search");

                return false;
            }
        }

        protected function prepareProductsResults(array $data)
        {
            $indexes = $data['hits']['hits'];
            $results = [];

            foreach($indexes AS $index){
                $results[] = Product::findOne($index['_source']['id']);
            }

            return $results;
        }
        protected function createSingleDocument(Product $product)
        {
            return [
                'id' => $product->id,
                'name' => mb_strtolower($product->name),
                'processed_name' => $product->processed_name,
                'cost' => $product->cost,
                'cost_after_discount' => $product->costAfterDiscount,
                'description' => strip_tags($product->description),
            ];
        }
    }