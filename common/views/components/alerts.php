<?php
    use common\services\Alerts\Alert;

    /**
     * @var Alert[] $alerts
     */
?>

<?php if (!empty($alerts)): ?>
    <div id="alerts-wrapper">
        <?php foreach ($alerts as $alert): ?>
            <div class="alert hidden" data-type="<?= $alert->getType(); ?>">
                <div class="inner-wrapper">
                    <div class="message-wrapper">
                        <span class="message">
                            <?= $alert->getMessage(); ?>
                        </span>
                    </div>
                    <div class="button-wrapper">
                        <a href="#" class="hide-btn"></a>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
<?php endif; ?>

