<?php
    namespace common\interfaces;

    /**
     * Interface SitemapItem
     * @package common\interfaces
     */
    interface SitemapItem
    {
        const FREQ_ALWAYS = 'always';
        const FREQ_HOURLY = 'hourly';
        const FREQ_DAILY = 'daily';
        const FREQ_WEEKLY = 'weekly';
        const FREQ_MONTHLY = 'monthly';
        const FREQ_YEARLY = 'yearly';
        const FREQ_NEVER = 'never';

        const PRIORITY_MAX = 1;
        const PRIORITY_MEDIUM = 0.5;
        const PRIORITY_LOW = 0;

        public function getUrl();

        public function getLastModified();

        public function getChangeFrequency();

        public function getPriority();

        public static function fetchForSitemap();
    }