import Alert from "./AlertWidget/Alert";

export default class AlertWidget {
    protected $wrapper: JQuery;
    protected alerts: Array<Alert>;

    public static readonly ON_ALERT_HIDE: string = 'on-alert-hide';
    protected static readonly ALERT_SELECTOR: string = '.alert';

    public constructor($wrapper: JQuery) {
        this.$wrapper = $wrapper;
        this.alerts = [];

        if (this.$wrapper.length) {
            this.addListeners();

            this.$wrapper.find(AlertWidget.ALERT_SELECTOR).each((i, element) => {
                this.alerts.push(new Alert($(element)))
            })
        }
    }

    public getAlerts(): Array<Alert> {
        return this.alerts;
    }

    public hasAlerts(): boolean {
        return !!this.alerts.length
    }

    public countAlerts(): number {
        return this.alerts.length
    }

    public removeAlerts(hide: boolean = true): void {
        this.alerts = [];

        if (hide === true) {
            this.hideAll();
        }
    }

    public showAll(): void {
        let bottom: number = 0,
            index: number = this.countAlerts();

        for (let alert of this.alerts) {
            if (alert.isClosed() === false) {
                alert.show(bottom, index);
                bottom += <number> alert.getElement().innerHeight();
                index--;
            }
        }
    }

    protected hideAll(): void {
        for (let alert of this.alerts) {
            alert.hide();
        }
    }

    protected addListeners() {
        $("#" + this.$wrapper.attr("id")).on(AlertWidget.ON_ALERT_HIDE, () => {
            this.showAll();
        });

        $(window).resize(() => {
            this.showAll();
        })
    }
}