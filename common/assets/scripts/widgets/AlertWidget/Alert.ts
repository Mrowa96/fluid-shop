import AlertWidget from "../AlertWidget";

export default class Alert {
    protected $element: JQuery;
    protected $parent: JQuery;
    protected hidden: boolean;
    protected closed: boolean;

    protected readonly ALERT_HIDDEN_CLASS: string = 'hidden';
    protected readonly ALERT_TRANSITION_CLASS: string = "has-transition";

    public constructor($element: JQuery) {
        this.$element = $element;
        this.$parent = $element.parent();
        this.closed = false;

        this.initialize();
    }

    public show(bottom: number = 0, index: number = 1) {
        setTimeout(() => {
            this.$element.css({
                bottom: bottom + "px",
                zIndex: index
            });
            this.hidden = false;
        }, 100)
    }

    public hide() {
        this.$element.css("bottom", "-" + (2 * this.$element.outerHeight(true)) + "px");
        this.hidden = true;
    }

    public getElement(): JQuery {
        return this.$element;
    }

    public isClosed(): boolean {
        return this.closed;
    }

    protected initialize() {
        if (!this.$element.data("initialized")) {
            this.$element.removeClass(this.ALERT_HIDDEN_CLASS);

            this.addListeners();
            this.hide();

            this.$element.addClass(this.ALERT_TRANSITION_CLASS).data('initialized', true);
        }
    }

    protected addListeners() {
        this.$element.find(".hide-btn").click((e) => {
            e.preventDefault();

            this.hide();
            this.closed = true;
            this.$parent.trigger(AlertWidget.ON_ALERT_HIDE);

            return false;
        });

        $(window).resize(() => {
            if (this.hidden) {
                /** Recalculate value of bottom */
                this.hide();
            }
        })
    }
}