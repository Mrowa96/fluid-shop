<?php
    namespace common\forms;

    use Yii;
    use yii\base\Model;
    use common\models\User;

    /**
     * Class Login
     * @package common\forms
     *
     * @property string $username
     * @property string $password
     * @property bool $rememberMe
     * @property string $ref
     * @property User $_user
     */
    class Login extends Model
    {
        public $username;
        public $password;
        public $rememberMe = true;
        public $ref;
        private $_user = null;

        public function rules()
        {
            return [
                [['username', 'password'], 'required'],
                ['rememberMe', 'boolean'],
                ['ref', 'string'],
                ['password', 'validatePassword'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'username' => Yii::t("login", "Username"),
                'password' => Yii::t("login", "Password"),
                'rememberMe' => Yii::t("login", "Remember me")
            ];
        }

        /**
         * @param $attribute
         */
        public function validatePassword($attribute)
        {
            if (!$this->hasErrors()) {
                $user = $this->getUser();

                if (!$user || !$user->validatePassword($this->password)) {
                    $this->addError($attribute, Yii::t("login", "Incorrect username or password."));
                }
            }
        }

        /**
         * @return bool
         */
        public function login()
        {
            if ($this->validate() && $this->getUser()) {
                return Yii::$app->user->login($this->getUser(), $this->rememberMe ? 3600 * 24 * 30 : 0);
            }

            return false;
        }

        /**
         * @return bool|User
         */
        public function getUser()
        {
            if (!$this->_user) {
                $this->_user = User::findByUsername($this->username);

                if (!$this->_user || Yii::$app->authManager->getAssignment('system', $this->_user->getId())) {
                    return false;
                }
            }

            return $this->_user;
        }
    }
