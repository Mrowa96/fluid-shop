<?php
    namespace common\components;

    use Yii;
    use yii\base\Widget;
    use common\services\Alerts\Alert;

    /**
     * Class AlertWidget
     * Corresponding TypeScript class can be found under common\assets\scripts\widgets namespace
     * @package frontend\components
     *
     * @property Alert[] $alerts
     */
    class AlertWidget extends Widget
    {
        public $alerts;

        public function run()
        {
            $this->alerts = Yii::$app->alertsService->getAlerts();

            return $this->render('@common/views/components/alerts', [
                'alerts' => $this->alerts,
            ]);
        }
    }