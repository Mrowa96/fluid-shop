<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;

    /**
     * This is the model class for table "{{%address}}".
     *
     * @property integer $id
     * @property integer $user_id
     * @property integer $active
     * @property string $name
     * @property string $email
     * @property string $surname
     * @property string $street
     * @property string $building_no
     * @property string $city
     * @property string $zip
     * @property string $phone_no
     * @property string $fullName
     * @property bool $deleted
     *
     * @property User $user
     * @property Order[] $orders
     */
    class Address extends ActiveRecord
    {
        public $fullName;
        public $additionalId;

        public static function tableName()
        {
            return '{{%address}}';
        }

        public function rules()
        {
            return [
                [['name', 'surname', 'street', 'building_no', 'city', 'zip', 'phone_no', 'email'], 'required'],
                [['user_id'], 'integer'],
                [['email'], 'email'],
                [['name', 'fullName', 'surname', 'street'], 'string', 'max' => 256],
                [['building_no'], 'string', 'max' => 64],
                [['city', 'email'], 'string', 'max' => 128],
                [['zip'], 'string', 'max' => 6],
                [['phone_no'], 'string', 'max' => 24],
                [['deleted'], 'boolean'],
                ['deleted', 'default', 'value' => 0],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('address', 'ID'),
                'user_id' => Yii::t('address', 'User ID'),
                'name' => Yii::t('address', 'Name'),
                'surname' => Yii::t('address', 'Surname'),
                'email' => Yii::t('address', 'Email'),
                'fullName' => Yii::t('address', 'Full Name'),
                'street' => Yii::t('address', 'Street'),
                'building_no' => Yii::t('address', 'Building No'),
                'city' => Yii::t('address', 'City'),
                'zip' => Yii::t('address', 'Zip'),
                'phone_no' => Yii::t('address', 'Phone No'),
                'additionalId' => Yii::t('address', 'Defined address'),
                'deleted' => Yii::t('address', 'Deleted'),
            ];
        }

        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }

        public function getOrders()
        {
            return $this->hasMany(Order::className(), ['address_id' => 'id']);
        }

        public function beforeValidate()
        {
            foreach ($this->getAttributes() AS $attribute => $value) {
                $this->{$attribute} = Html::encode($this->{$attribute});
            }

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            $this->fullName = $this->name . " " . $this->surname;

            parent::afterFind();
        }

        public function getInReadableForm()
        {
            $result = $this->name . " " . $this->surname . PHP_EOL;
            $result .= $this->email . PHP_EOL;
            $result .= $this->phone_no . PHP_EOL;
            $result .= $this->street . " " . $this->building_no . PHP_EOL;
            $result .= $this->zip . " " . $this->city . PHP_EOL;

            return $result;
        }



        /**
         * @deprecated
         * @return bool
         */
        public function setAsPrimary()
        {
            $addresses = Address::find()->where(['user_id' => Yii::$app->user->id])->all();

            /** @var Address $address */
            foreach ($addresses AS $address) {
                $address->type = "additional";
                $address->save();
            }

            $this->type = "primary";

            return $this->save();
        }

        /**
         * @deprecated
         * @return array|null|ActiveRecord
         */
        public function checkHash()
        {
            if (!$this->hash) {
                $this->generateHash();
            }

            return Address::find()->where(['hash' => $this->hash])->one();
        }

        /**
         * @deprecated
         * @param bool $asArray
         * @return array|Address|null|ActiveRecord
         */
        public static function getPrimaryAddress($asArray = false)
        {
            if (Yii::$app->user->getId()) {
                if ($asArray === true) {
                    $existingAddress = Address::find()->where(['user_id' => Yii::$app->user->id, 'type' => 'primary'])->asArray()->one();
                } else {
                    $existingAddress = Address::find()->where(['user_id' => Yii::$app->user->id, 'type' => 'primary'])->one();
                }

                if ($existingAddress) {
                    return $existingAddress;
                } else {
                    $model = new Address();
                    $model->user_id = Yii::$app->user->id;

                    return $model;
                }
            } else {
                return new Address();
            }
        }

        /**
         * @@deprecated
         * @return array
         */
        public static function getAdditionalAddresses()
        {
            $results = [];

            if (Yii::$app->user->getId()) {
                $addresses = Address::find()->where(['user_id' => Yii::$app->user->id, 'type' => 'additional'])->all();

                if (!empty($addresses)) {
                    /** @var Address $address */
                    foreach ($addresses AS $address) {
                        $results[$address->id] = $address->getInReadableForm();
                    }
                }
            }

            return $results;
        }

        /**
         *  @deprecated
         */
        private function generateHash()
        {
            $this->hash = hash('sha256', $this->getInReadableForm());
        }
    }
