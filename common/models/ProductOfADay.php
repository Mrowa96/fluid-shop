<?php
    namespace common\models;

    use common\models\Settings\System;
    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%product_of_a_day}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property double $cost
     * @property integer $quantity
     * @property string $expire_date
     * @property bool $deleted
     * @property bool $active
     *
     * @property Product $product
     */
    class ProductOfADay extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%product_of_a_day}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'cost', 'quantity', 'expire_date', 'active'], 'required'],
                [['product_id', 'quantity'], 'integer'],
                [['active', 'deleted'], 'boolean'],
                [['cost'], 'number'],
                [['expire_date'], 'safe'],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product-of-a-day', 'ID'),
                'product_id' => Yii::t('product-of-a-day', 'Product ID'),
                'cost' => Yii::t('product-of-a-day', 'Cost'),
                'quantity' => Yii::t('product-of-a-day', 'Quantity'),
                'expire_date' => Yii::t('product-of-a-day', 'Expire Date'),
                'active' => Yii::t('product-of-a-day', 'Active'),
                'deleted' => Yii::t('product-of-a-day', 'Deleted'),
            ];
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }

        public function afterSave($insert, $changedAttributes)
        {
            if ($this->active) {
                Yii::$app->db->createCommand()
                    ->update(self::tableName(), ['active' => 0], 'id != ' . $this->id)
                    ->execute();
            }

            parent::afterSave($insert, $changedAttributes);
        }

        /**
         * @param bool $withCurrency
         * @return string
         */
        public function getCost($withCurrency = false)
        {
            $cost = "<span class='price-value'>" . $this->cost . "</span>";

            if ($withCurrency === true) {
                return $cost . "<span class='currency'>" . Settings::getOne('currency', System::MODULE) . "</span>";
            } else {
                return $cost;
            }
        }

        public static function getActive()
        {
            return ProductOfADay::find()->where(['active' => 1, 'deleted' => 0])
                ->andWhere(['>=', 'expire_date', (new \DateTime())->format("Y-m-d H:i:s")])
                ->andWhere(['>', 'quantity', 0])->one();
        }
    }
