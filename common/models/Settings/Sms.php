<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class Sms extends Settings
    {
        public $login;
        public $password;
        public $sender;

        const MODULE = 'SMS';

        public function rules()
        {
            return [
                [['login', 'password', 'sender',], 'string']
            ];
        }

        public function attributeLabels()
        {
            return [
                'login' => Yii::t("settings", "Login"),
                'password' => Yii::t("settings", "Password"),
                'sender' => Yii::t("settings", "Sender"),
            ];
        }
    }