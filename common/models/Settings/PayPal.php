<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class PayPal extends Settings
    {
        public $client_id;
        public $client_secret;
        public $active;
        public $environment;

        const MODULE = 'PAYPAL';

        public function rules()
        {
            return [
                [['client_id', 'client_secret', 'environment'], 'string'],
                [['active'], 'boolean']
            ];
        }

        public function attributeLabels()
        {
            return [
                'active' => Yii::t("settings", "Active"),
                'environment' => Yii::t("settings", "Environment"),
                'client_id' => Yii::t("settings", "oAuth client ID"),
                'client_secret' => Yii::t("settings", "oAuth client key"),
            ];
        }
    }