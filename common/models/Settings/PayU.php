<?php
    namespace common\models\Settings;

    use common\models\Settings;
    use Yii;

    class PayU extends Settings
    {
        public $environment;
        public $oauth_client_id;
        public $oauth_client_secret;
        public $active;

        const MODULE = 'PAYU';

        public function rules()
        {
            return [
                [['environment', 'oauth_client_id', 'oauth_client_secret'], 'string'],
                [['active'], 'boolean']
            ];
        }

        public function attributeLabels()
        {
            return [
                'environment' => Yii::t("settings", "Environment"),
                'active' => Yii::t("settings", "Active"),
                'oauth_client_id' => Yii::t("settings", "oAuth client ID"),
                'oauth_client_secret' => Yii::t("settings", "oAuth client key"),
            ];
        }
    }