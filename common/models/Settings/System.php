<?php
    namespace common\models\Settings;

    use Yii;
    use common\models\Settings;
    use fluid\fileManager\File;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;

    class System extends Settings
    {
        public $site_title;
        public $file_manager;
        public $dropped_basket_remind;
        public $currency;
        public $footer_text;
        public $language;
        public $language_level;
        public $logo;
        public $layout_configuration;
        public $search_index_name;

        /**
         * This value is required only for form input
         * @var UploadedFile $logoFile
         */
        public $logoFile;
        protected $excludeIfEmpty = ['logo'];
        const MODULE = 'SYSTEM';

        public function rules()
        {
            return [
                [['site_title', 'dropped_basket_remind', 'currency', 'footer_text', 'language', 'language_level',
                    'logo', 'layout_configuration', 'search_index_name'], 'string'],
                ['file_manager', 'boolean'],
                [['logoFile'], 'file', 'skipOnEmpty' => true],
            ];
        }
        public function attributeLabels()
        {
            return [
                'site_title' => Yii::t("settings", "Default title"),
                'footer_text' => Yii::t("settings", "Footer text"),
                'file_manager' => Yii::t("settings", "File manager"),
                'currency' => Yii::t("settings", "Currency"),
                'dropped_basket_remind' => Yii::t("settings", "Time to remind about dropped basket"),
                'language' => Yii::t("settings", "Language"),
                'language_level' => Yii::t("settings", "Language level"),
                'logoFile' => Yii::t("settings", "Logo"),
                'search_index_name' => Yii::t("settings", "Search index name"),
            ];
        }

        public function load($data, $formName = null)
        {
            if(parent::load($data, $formName)){
                $this->logoFile = UploadedFile::getInstance($this, 'logoFile');

                if($this->logoFile){

                    $logoPath = $this->saveLogo();

                    if($logoPath){
                        $this->logo = $logoPath;
                    }
                }

                return true;
            }

            return false;
        }

        protected function saveLogo()
        {
            if ($this->validate()) {
                $fileName = mb_substr(hash("sha256", $this->logoFile->getBaseName()), 0, 32) . time() . '.' . $this->logoFile->getExtension();
                $this->logoFile->saveAs(Yii::getAlias("@dataLogos") . '/' . $fileName);

                return $fileName;
            }
            else {
                return false;
            }
        }

        public static function getRemindBasketTimes()
        {
            return [
                '48h' => Yii::t("settings", "48 hours"),
                '1 week' => Yii::t("settings", "1 week"),
                '2 week' => Yii::t("settings", "2 weeks"),
            ];
        }
        public static function getLanguageLevel()
        {
            return [
                'cookie' => Yii::t("settings", "Cookie"),
                'user' => Yii::t("settings", "User"),
                'global' => Yii::t("settings", "Global"),
            ];
        }
    }