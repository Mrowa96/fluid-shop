<?php
    namespace common\models;

    use backend\models\MailMessage;
    use backend\models\OrderLog;
    use common\models\Product;
    use common\utilities\Mailer;
    use Defuse\Crypto\Crypto;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\db\Query;
    use backend\models\OrderInvoice;
    use yii\helpers\Json;

    /**
     * This is the model class for table "{{%order}}".
     *
     * @property integer $id
     * @property integer $user_id
     * @property integer $address_id
     * @property integer $transport_id
     * @property integer $payment_id
     * @property integer $order_status_id
     * @property integer $key
     * @property string $type
     * @property string $note
     * @property double $cost
     * @property string $data
     * @property string $create_date
     * @property string $modify_date
     * @property string $basket_create_date
     * @property bool $deleted
     *
     * @property double $full_cost
     * @property object $parsed_data
     *
     * @property User $user
     * @property Address $address
     * @property Transport $transport
     * @property Payment $payment
     * @property OrderStatus $orderStatus
     * @property OrderLog $logs
     * @property OrderInvoice $invoice
     * @property OrderDetail[] $orderDetails
     */
    class Order extends ActiveRecord
    {
        const TYPE_STANDARD = "standard";
        const KEY_KEY = 'def00000e4103a54db3d453e834f2b5855edc76236ef7a39f29fe3cf811d4fdb31c3cc897445d1296be8e720b58a2b78e4a7530ec29e54487fee7c67dd3af46dfdecc245';

        public $full_cost;
        public $parsed_data;

        public static function tableName()
        {
            return '{{%order}}';
        }

        public function rules()
        {
            return [
                [['address_id', 'transport_id', 'payment_id', 'order_status_id', 'cost', 'create_date', 'modify_date', 'basket_create_date'], 'required'],
                [['address_id', 'transport_id', 'payment_id', 'order_status_id'], 'integer'],
                [['cost'], 'number'],
                [['note', 'data'], 'string'],
                [['deleted'], 'boolean'],
                ['type', 'default', 'value' => 'standard'],
                [['create_date', 'modify_date', 'basket_create_date', 'note'], 'safe'],
                [['order_status_id'], 'exist', 'skipOnError' => true, 'targetClass' => OrderStatus::className(), 'targetAttribute' => ['order_status_id' => 'id']],
                [['address_id'], 'exist', 'skipOnError' => true, 'targetClass' => Address::className(), 'targetAttribute' => ['address_id' => 'id']],
                [['transport_id'], 'exist', 'skipOnError' => true, 'targetClass' => Transport::className(), 'targetAttribute' => ['transport_id' => 'id']],
                [['payment_id'], 'exist', 'skipOnError' => true, 'targetClass' => Payment::className(), 'targetAttribute' => ['payment_id' => 'id']],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('order', 'ID'),
                'address_id' => Yii::t('order', 'Address ID'),
                'transport_id' => Yii::t('order', 'Transport ID'),
                'payment_id' => Yii::t('order', 'Payment ID'),
                'order_status_id' => Yii::t('order', 'Status ID'),
                'key' => Yii::t('order', 'Key'),
                'type' => Yii::t('order', 'Type'),
                'cost' => Yii::t('order', 'Cost'),
                'note' => Yii::t('order', 'Note'),
                'create_date' => Yii::t('order', 'Create Date'),
                'modify_date' => Yii::t('order', 'Modify Date'),
                'basket_create_date' => Yii::t('order', 'Basket create Date'),
                'deleted' => Yii::t('order', 'Deleted'),
            ];
        }

        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }
        public function getAddress()
        {
            return $this->hasOne(Address::className(), ['id' => 'address_id']);
        }
        public function getTransport()
        {
            return $this->hasOne(Transport::className(), ['id' => 'transport_id']);
        }
        public function getPayment()
        {
            return $this->hasOne(Payment::className(), ['id' => 'payment_id']);
        }
        public function getOrderDetails()
        {
            return $this->hasMany(OrderDetail::className(), ['order_id' => 'id']);
        }
        public function getOrderStatus()
        {
            return $this->hasOne(OrderStatus::className(), ['id' => 'order_status_id']);
        }
        public function getInvoice()
        {
            return $this->hasOne(OrderInvoice::className(), ['order_id' => 'id']);
        }
        public function getLogs()
        {
            return $this->hasMany(OrderLog::className(), ['order_id' => 'id']);
        }

        public function afterFind()
        {
            if($this->transport){
                $this->full_cost = $this->cost + $this->transport->cost;
            }
            if($this->data){
                $this->parsed_data = Json::decode($this->data, false);
            }

            parent::afterFind();
        }

        public function getGridViewExample(){
            return [
                [
                    'property' => 'address.fullName',
                    'value' => 1,
                    'position' => 1
                ],
                [
                    'property' => 'key',
                    'value' => 1,
                    'position' => 2
                ],
                [
                    'property' => 'cost',
                    'position' => 3
                ],
                [
                    'property' => 'create_date',
                    'value' => 1,
                    'position' => 4
                ],
                [
                    'property' => 'orderStatus.name',
                    'position' => 5
                ],
            ];
        }
        public function generateKey()
        {
            return uniqid();
        }
        public function onStatusChange($userId)
        {
            $model = new OrderLog();

            $model->order_id = $this->id;
            $model->order_status_id = $this->order_status_id;
            $model->user_id = $userId;

            if($model->save()){
                /**
                 * @var MailMessage $message
                 * @var Mailer $mailer
                 */
                $message = MailMessage::find()->where(['name' => 'order_change_status'])->one();
                $mailer = Yii::$app->mailer;
                $smsGate = Yii::$app->smsGate;

                if($message){
                    $message->prepareForSent();
                    $message->replaceTags([
                        'order_key' => $this->key,
                        'order_status_message' => $this->orderStatus->message
                    ]);

                    try{
                        if($mailer){
                            $mailer->compose()
                                ->setFrom($mailer->getHost())
                                ->setTo($this->address->email)
                                ->setSubject($message->title)
                                ->setHtmlBody($message->content)
                                ->send();
                        }

                        if($smsGate){
                            $smsGate->compose()
                                ->setTo($this->address->phone_no)
                                ->setBody($message->content)
                                ->send();
                        }
                    }
                    catch(\Exception $ex){
                        Yii::error($ex);
                    }
                }

                return true;
            }
        }
        public function setStatus($statusId, User $user)
        {
            $this->order_status_id = $statusId;

            return $this->save() && $this->onStatusChange($user->id);
        }
        public function productNotComplaint(Product $product)
        {
            $rma = Rma::find()->where(['product_id' => $product->id, 'order_id' => $this->id])->one();

            return !$rma;
        }
        public function isProductInOrder($productId)
        {
            foreach($this->orderDetails AS $detail){
                    if($detail->product->id == $productId){
                        return true;
                    }
                }

            return false;
        }

        public static function getProducts($orderId)
        {
            $order = Order::findOne($orderId);
            $result = [];

            if($order){
                $details = $order->orderDetails;

                foreach($details AS $detail){
                    $result[] = $detail->product;
                }
            }

            return $result;
        }
        public static function getWithoutInvoice()
        {
            return Order::find()->leftJoin(OrderInvoice::tableName(), [
                OrderInvoice::tableName() . '.order_id' => Order::tableName() . '.id'
            ])->where(OrderInvoice::tableName() . '.order_id IS NULL')
                ->andWhere(Order::tableName() . '.order_status_id = ' . OrderStatus::STATUS_PAID)
                ->all();
        }
        public static function getGroupedByMonth($year)
        {
            $orders = Order::find()
                ->where('order_status_id > ' . OrderStatus::STATUS_NEW_NOT_COMPLETE)
                ->andWhere('YEAR(create_date) = ' . $year)
                ->all();
            $results = [];

            for($i = 0; $i< 12; $i++){
                $results[$i] = 0;
            }

            foreach($orders AS $order){
                $date = new \DateTime($order->create_date);
                $month = $date->format('n');

                $results[$month - 1]++;
            }

            return $results;
        }
        public static function getAmount()
        {
            return Order::find()->where('order_status_id > ' . OrderStatus::STATUS_NEW_NOT_COMPLETE)->count();
        }
        public static function encryptKey($key)
        {
            return Crypto::encryptWithPassword($key, self::KEY_KEY);
        }
        public static function decryptKey($encryptedKey)
        {
            return Crypto::decryptWithPassword($encryptedKey, self::KEY_KEY);
        }
    }
