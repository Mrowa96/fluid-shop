<?php
    namespace common\models;

    use fluid\fileManager\File;
    use ReflectionObject;
    use ReflectionProperty;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Json;

    /**
     * @property int $id
     * @property string $key
     * @property string $value
     * @property string $default
     * @property string $description
     * @property string $module
     */
    class Settings extends ActiveRecord
    {
        private $modules = [];
        protected $excludeIfEmpty = [];

        /**
         * @return string
         */
        public static function tableName()
        {
            return '{{%settings}}';
        }

        /**
         * @param array $data
         * @return bool
         */
        public function loadData(array $data)
        {
            foreach ($data as $key => $value) {
                $className = Settings::className() .  '\\' . $key;

                if (class_exists($className)) {
                    $model = new $className();

                    if (method_exists($model, "load")) {
                        $model->load($data);

                        $this->modules[] = $model;
                    }
                }
            }

            return true;
        }

        /**
         * @return bool
         */
        public function saveData()
        {
            /** @var Settings $model */
            foreach ($this->modules AS $model) {
                $model->saveKeys();
            }

            return true;
        }

        /**
         * @return bool
         */
        public function saveKeys()
        {
            $module = new ReflectionObject($this);
            $properties = $module->getProperties(ReflectionProperty::IS_PUBLIC);
            $moduleName = mb_strtoupper($module->getShortName());

            if (!empty($properties)) {
                foreach ($properties AS $prop) {
                    /** @var Settings $option */
                    $option = Settings::find()->where(['module' => $moduleName])->andWhere(['key' => $prop->name])->one();

                    if ($option) {
                        if (empty($this->{$prop->name}) && in_array($prop->name, $this->excludeIfEmpty)) {
                            $this->{$prop->name} = $option->value;
                        } else if (empty($this->{$prop->name})) {
                            $this->{$prop->name} = $option->default;
                        }

                        $option->value = $this->{$prop->name};

                        $option->save();
                    }
                }
            }

            return true;
        }

        /**
         * @return $this
         */
        public function prepareForForm()
        {
            $module = new ReflectionObject($this);
            $moduleName = mb_strtoupper($module->getShortName());
            /** @var Settings $settings */
            $settings = Settings::find()->where(['module' => $moduleName])->all();

            if (!empty($settings)) {
                foreach ($settings AS $setting) {
                    if (property_exists($this, $setting->key)) {
                        $this->{$setting->key} = $setting->value;
                    }
                }
            }

            return $this;
        }

        /**
         * @param $key
         * @param $module
         * @param bool $onlyValue
         * @return bool|string|Settings
         */
        public static function getOne($key, $module, $onlyValue = true)
        {
            $option = Settings::findOne(['key' => $key, 'module' => $module]);

            if ($option) {
                if ($onlyValue) {
                    return trim($option->value);
                } else {
                    return $option;
                }
            }

            return false;
        }

        /**
         * @param $key
         * @param $value
         * @param $module
         * @return bool
         */
        public static function setOne($key, $value, $module)
        {
            /** @var Settings $option */
            $option = Settings::find()->where(['key' => $key, 'module' => $module])->one();

            if ($option) {
                $option->value = $value;

                return $option->save();
            }

            return false;
        }
    }
