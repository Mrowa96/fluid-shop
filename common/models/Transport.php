<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\web\UploadedFile;

    /**
     * This is the model class for table "{{%transport}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $transport_name
     * @property string $icon
     * @property string $description
     * @property double $range_from
     * @property double $range_to
     * @property integer $active
     * @property double $cost
     * @property bool $deleted
     * @property UploadedFile $iconFile
     *
     * @property Order[] $orders
     */
    class Transport extends ActiveRecord
    {
        public $iconFile;
        public $transport_name;

        public static function tableName()
        {
            return '{{%transport}}';
        }

        public function rules()
        {
            return [
                [['name', 'range_from', 'range_to', 'cost', 'active'], 'required'],
                [['description', 'icon'], 'string'],
                [['range_from', 'range_to', 'cost'], 'number'],
                [['active'], 'integer'],
                [['name'], 'string', 'max' => 128],
                [['deleted'], 'boolean'],
                [['iconFile'], 'file', 'extensions' => 'jpg, png', 'checkExtensionByMimeType' => false],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('transport', 'ID'),
                'name' => Yii::t('transport', 'Name'),
                'transport_name' => Yii::t('transport', 'Transport name'),
                'description' => Yii::t('transport', 'Description'),
                'range_from' => Yii::t('transport', 'Range From'),
                'range_to' => Yii::t('transport', 'Range To'),
                'cost' => Yii::t('transport', 'Cost'),
                'active' => Yii::t('transport', 'Active'),
                'icon' => Yii::t('transport', 'Icon'),
                'deleted' => Yii::t('transport', 'Deleted'),
            ];
        }

        public function getOrders()
        {
            return $this->hasMany(Order::className(), ['transport_id' => 'id']);
        }

        public function getGridViewExample(){
            return [
                [
                    'property' => 'icon',
                    'type' => 'image',
                    'position' => 1
                ],
                [
                    'property' => 'name',
                    'value' => 1,
                    'position' => 2
                ],
                [
                    'property' => 'cost',
                    'value' => 1,
                    'position' => 3
                ],
                [
                    'property' => 'active',
                    'value' => 0,
                    'position' => 4
                ],
            ];
        }

        public function afterFind()
        {
            $this->transport_name = $this->name;

            if($this->icon){
                $this->icon = Yii::getAlias("@frontendLink") . '/data/' . $this->icon;
            }
            else{
                $this->icon = Yii::getAlias("@frontendLink") . '/data/no-image.png';
            }

            parent::afterFind();
        }
        public function beforeSave($insert)
        {
            $this->icon = str_replace(Yii::getAlias("@frontendLink") . '/data/', "", $this->icon);

            return parent::beforeSave($insert);
        }
        public function delete()
        {
            $this->deleted = 1;

            return $this->save();
        }

        public function saveIcon()
        {
            if($this->iconFile){
                if($this->validate()) {
                    $this->icon = mb_substr(hash("sha256", $this->iconFile->baseName), 0, 32) . time() . '.' . $this->iconFile->extension;
                    $this->iconFile->saveAs(Yii::getAlias("@data") .'/'. $this->icon);

                    return true;
                }

                return false;
            }

            return true;
        }
    }
