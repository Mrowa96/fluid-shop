<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%page_category}}".
     *
     * @property integer $id
     * @property string $name
     * @property bool $deleted
     *
     * @property Page[] $pages
     */
    class PageCategory extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%page_category}}';
        }

        public function rules()
        {
            return [
                [['name'], 'required'],
                [['deleted'], 'boolean'],
                [['name'], 'string', 'max' => 128],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('page-category', 'ID'),
                'name' => Yii::t('page-category', 'Name'),
                'deleted' => Yii::t('page-category', 'Deleted'),
            ];
        }

        public function getPages()
        {
            return $this->hasMany(Page::className(), ['page_category_id' => 'id']);
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(PageCategory::find()->where(['deleted' => 0])->all(), 'id', 'name');
        }
        public static function getAllForLayout()
        {
            return PageCategory::find()->where(['deleted' => 0])->all();
        }
    }
