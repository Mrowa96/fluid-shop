<?php
    namespace common\models;

    use backend\utilities\i18n;
    use Yii;
    use yii\base\Exception;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%order_status}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $status_name
     * @property string $message
     * @property string $description
     */
    class OrderStatus extends ActiveRecord
    {
        const STATUS_NEW_NOT_COMPLETE = 1;
        const STATUS_NEW_COMPLETE = 2;
        const STATUS_PAID = 3;
        const STATUS_IN_PROGRESS = 4;
        const STATUS_PREPARED_TO_SEND = 5;
        const STATUS_SEND = 6;
        const STATUS_FINISHED = 7;
        const STATUS_CANCELED = 8;

        public $status_name;

        static function tableName()
        {
            return '{{%order_status}}';
        }

        public function rules()
        {
            return [
                [['name', 'description', 'message'], 'required'],
                [['name'], 'string', 'max' => 128],
                [['description', 'message'], 'string']
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('order_status', 'ID'),
                'name' => Yii::t('order_status', 'Name'),
                'status_name' => Yii::t('order_status', 'Status name'),
                'message' => Yii::t('order_status', 'Message'),
                'description' => Yii::t('order_status', 'Description'),
            ];
        }

        public function beforeValidate()
        {
            $oldStatus = OrderStatus::findOne($this->id);

            if($this->name !== $oldStatus->name){
                $this->name = $oldStatus->name;
            }

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            if(Yii::$app->id === Yii::getAlias('@frontendModuleId')){
                /** @var i18n $backendTranslate */
                $backendTranslate = Yii::$app->i18nBackend;

                $this->status_name = $backendTranslate->translate('order_status', $this->name, Yii::$app->language);

                if (!$this->status_name) {
                    $this->status_name = Yii::t("order_status", $this->name);;
                }
            } else {
                $this->status_name = Yii::t("order_status", $this->name);
            }

            parent::afterFind();
        }

        public static function getIdByName($name)
        {
            /** @var OrderStatus $status */
            $status = OrderStatus::find()->where(['name' => $name])->one();

            if($status){
                return $status->id;
            }
            else{
                throw new Exception("Cannot find order status: " . $status->id);
            }
        }
    }
