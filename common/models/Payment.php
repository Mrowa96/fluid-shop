<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\web\UploadedFile;

    /**
     * This is the model class for table "{{%payment}}".
     *
     * @property integer $id
     * @property integer $payment_api_id
     * @property string $name
     * @property double $cost
     * @property string $payment_name
     * @property string $description
     * @property string $icon
     * @property bool $deleted
     * @property UploadedFile $iconFile
     * @property Order[] $orders
     */
    class Payment extends ActiveRecord
    {
        public $iconFile;
        public $payment_name;

        public static function tableName()
        {
            return '{{%payment}}';
        }

        public function rules()
        {
            return [
                [['name', 'cost','active'], 'required'],
                [['description', 'icon'], 'string'],
                [['payment_api_id', 'active'], 'integer'],
                [['cost'], 'double'],
                [['name'], 'string', 'max' => 128],
                [['deleted'], 'boolean'],
                [['iconFile'], 'file', 'extensions' => 'jpg, png', 'checkExtensionByMimeType' => false],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('payment', 'ID'),
                'name' => Yii::t('payment', 'Name'),
                'cost' => Yii::t('payment', 'Cost'),
                'payment_name' => Yii::t('payment', 'Payment name'),
                'description' => Yii::t('payment', 'Description'),
                'payment_api_id' => Yii::t('payment', 'Api'),
                'active' => Yii::t('payment', 'Active'),
                'icon' => Yii::t('payment', 'Icon'),
                'deleted' => Yii::t('payment', 'Deleted'),
            ];
        }

        public function getOrders()
        {
            return $this->hasMany(Order::className(), ['payment_id' => 'id']);
        }
        public function getApi()
        {
            return $this->hasOne(PaymentApi::className(), ['id' => 'payment_api_id']);
        }

        public function getGridViewExample(){
            return [
                [
                    'property' => 'icon',
                    'type' => 'image',
                    'position' => 1
                ],
                [
                    'property' => 'name',
                    'value' => 1,
                    'position' => 2
                ],
                [
                    'property' => 'active',
                    'value' => 0,
                    'position' => 3
                ],
            ];
        }
        public function afterFind()
        {
            $this->payment_name = $this->name;

            if($this->icon){
                $this->icon = Yii::getAlias("@frontendLink") . '/data/' . $this->icon;
            }
            else{
                $this->icon = Yii::getAlias("@frontendLink") . '/data/no-image.png';
            }

            parent::afterFind();
        }
        public function beforeSave($insert)
        {
            $this->icon = str_replace(Yii::getAlias("@frontendLink") . '/data/', "", $this->icon);

            return parent::beforeSave($insert);
        }
        public function delete()
        {
            $this->deleted = 1;

            return $this->save();
        }
        public function saveIcon()
        {
            if($this->iconFile){
                if($this->validate()) {
                    $this->icon = mb_substr(hash("sha256", $this->iconFile->baseName), 0, 32) . time() . '.' . $this->iconFile->extension;
                    $this->iconFile->saveAs(Yii::getAlias("@data") .'/'. $this->icon);

                    return true;
                }

                return false;
            }

            return true;
        }
    }
