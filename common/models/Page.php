<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use common\utilities\Utility;
    use yii\web\UrlManager;
    use common\interfaces\SitemapItem;

    /**
     * This is the model class for table "{{%_page}}".
     *
     * @property integer $id
     * @property integer $page_category_id
     * @property string $title
     * @property string $url
     * @property string $content
     * @property string $type
     * @property string $seo_keywords
     * @property string $seo_description
     * @property integer $add_to_header
     * @property integer $add_to_footer
     * @property integer $created_date
     * @property integer $modified_date
     * @property bool $deleted
     *
     * @property PageCategory $pageCategory
     *
     * @property mixed add_to_footer_text
     * @property mixed add_to_header_text
     */
    class Page extends ActiveRecord implements SitemapItem
    {
        public $add_to_header_text;
        public $add_to_footer_text;

        const TYPE_STANDARD = 'standard';
        const TYPE_CONTACT = 'contact';

        public static function tableName()
        {
            return '{{%page}}';
        }

        public function rules()
        {
            return [
                [['title', 'page_category_id', 'created_date', "modified_date", "type"], 'required'],
                [['content', 'seo_description', 'url'], 'string'],
                [['add_to_header', 'add_to_footer'], 'boolean'],
                [['title'], 'string', 'max' => 256],
                [['deleted'], 'boolean'],
                [['seo_keywords'], 'string', 'max' => 512],
                [['page_category_id'], 'integer'],
                [['page_category_id'], 'exist', 'skipOnError' => true, 'targetClass' => PageCategory::className(), 'targetAttribute' => ['page_category_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('page', 'ID'),
                'page_category_id' => Yii::t('page', 'Page Category ID'),
                'title' => Yii::t('page', 'Title'),
                'url' => Yii::t('page', 'Url'),
                'content' => Yii::t('page', 'Content'),
                'type' => Yii::t('page', 'Type'),
                'seo_keywords' => Yii::t('page', 'Seo Keywords'),
                'seo_description' => Yii::t('page', 'Seo Description'),
                'add_to_header' => Yii::t('page', 'Add To Header'),
                'add_to_footer' => Yii::t('page', 'Add To Footer'),
                'add_to_header_text' => Yii::t('page', 'Add To Header'),
                'add_to_footer_text' => Yii::t('page', 'Add To Footer'),
                'deleted' => Yii::t('page', 'Deleted'),
            ];
        }

        public function getPageCategory()
        {
            return $this->hasOne(PageCategory::className(), ['id' => 'page_category_id']);
        }

        public function getGridViewExample()
        {
            return [
                [
                    'property' => 'title',
                    'position' => 1,
                    'value' => 1
                ],
                [
                    'property' => 'seo_keywords',
                    'position' => 2,
                    'value' => 0
                ],
                [
                    'property' => 'add_to_header_text',
                    'position' => 3,
                    'value' => 1
                ],
                [
                    'property' => 'add_to_footer_text',
                    'position' => 4,
                    'value' => 1
                ],
            ];
        }

        public function beforeValidate()
        {
            if ($this->isNewRecord) {
                $this->created_date = (new \DateTime())->format("Y-m-d\TH:i:s");
            }

            $this->url = Utility::createUrl($this->title);
            $this->modified_date = (new \DateTime())->format("Y-m-d\TH:i:s");

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            $this->add_to_header_text = ($this->add_to_header == 1) ? Yii::t("system", "Yes") : Yii::t("system", "No");
            $this->add_to_footer_text = ($this->add_to_footer == 1) ? Yii::t("system", "Yes") : Yii::t("system", "No");

            parent::afterFind();
        }

        public function getUrl()
        {
            /** @var UrlManager $urlManagerFrontend */
            $urlManagerFrontend = Yii::$app->urlManagerFrontend;

            return $urlManagerFrontend->createUrl(['site/page', 'id' => $this->id, 'url' => $this->url]);
        }

        public function getLastModified()
        {
            return $this->modified_date;
        }

        public function getChangeFrequency()
        {
            return SitemapItem::FREQ_MONTHLY;
        }

        public function getPriority()
        {
            return SitemapItem::PRIORITY_MEDIUM;
        }

        public static function fetchForSitemap()
        {
            return Page::find()->where(['deleted' => 0])->all();
        }

        public static function getForHeader()
        {
            return Page::find()->where(['add_to_header' => 1, 'deleted' => 0])->all();
        }

        /**
         * @return PageCategory[]
         */
        public static function getForFooter()
        {
            return PageCategory::find()
                ->innerJoin(Page::tableName(), Page::tableName() . '.page_category_id = ' . PageCategory::tableName() . '.id')
                ->where([
                    Page::tableName() . '.add_to_footer' => 1,
                    Page::tableName() . '.deleted' => 0
                ])
                ->all();
        }

        public static function getTypes()
        {
            return [
                self::TYPE_STANDARD => Yii::t("page", "Standard page"),
                self::TYPE_CONTACT => Yii::t("page", "Contact page"),
            ];
        }
    }
