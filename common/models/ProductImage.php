<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%product_image}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property string $name
     * @property string $path
     * @property string $thumb_path
     * @property bool $main
     */
    class ProductImage extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%product_image}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'name', 'path'], 'required'],
                [['product_id'], 'integer'],
                ['main', 'boolean'],
                [['name'], 'string', 'max' => 128],
                [['path', 'thumb_path'], 'string', 'max' => 192],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product_image', 'ID'),
                'product_id' => Yii::t('product_image', 'Product ID'),
                'name' => Yii::t('product_image', 'Name'),
                'path' => Yii::t('product_image', 'Path'),
                'thumb_path' => Yii::t('product_image', 'Thumb path'),
                'main' => Yii::t('product_image', 'Main'),
            ];
        }
        public function beforeValidate()
        {
            $this->path =  str_replace(Yii::getAlias("@frontendLink"), "", $this->path);
            $this->thumb_path =  str_replace(Yii::getAlias("@frontendLink"), "", $this->thumb_path);

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            $this->path = Yii::getAlias("@frontendLink") . $this->path;
            $this->thumb_path = Yii::getAlias("@frontendLink") . $this->thumb_path;

            parent::afterFind();
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }
    }
