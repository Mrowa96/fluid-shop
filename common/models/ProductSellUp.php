<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%product_sell_up}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property integer $up_product_id
     * @property double $cost
     *
     * @property Product $product
     * @property Product $related
     */
    class ProductSellUp extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%product_sell_up}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'up_product_id', 'cost'], 'required'],
                [['product_id', 'up_product_id'], 'integer'],
                [['cost'], 'number'],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product_sell_up', 'ID'),
                'product_id' => Yii::t('product_sell_up', 'Product ID'),
                'up_product_id' => Yii::t('product_sell_up', 'Up Product ID'),
                'cost' => Yii::t('product_sell_up', 'Up Product Cost'),
            ];
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'up_product_id']);
        }

        public function getRelated()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }

        public function getCost($withCurrency = false)
        {
            $cost = $this->cost;

            if ($withCurrency === true) {
                return "<span class='price-value'>" . $cost . "</span>" . "<span class='currency'>" . Settings::getOne('currency', System::MODULE) . "</span>";
            } else {
                return $cost;
            }
        }
    }
