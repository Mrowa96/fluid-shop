<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%i18n_message}}".
     *
     * @property integer $id
     * @property integer $source_id
     * @property integer $i18n_id
     * @property string $translation
     *
     * @property LanguageMessage $source
    */
    class LanguageMessage extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%i18n_message}}';
        }

        public function rules()
        {
            return [
                [['i18n_id', 'source_id'], 'required'],
                [['i18n_id'], 'integer'],
                [['translation'], 'string'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('language_message', 'ID'),
                'i18n_id' => Yii::t('language_message', 'Language ID'),
                'source_id' => Yii::t('language_message', 'Source ID'),
                'translation' => Yii::t('language_message', 'Translation'),
            ];
        }

        public function getSource()
        {
            return $this->hasOne(LanguageMessage::className(), ['source_id' => 'id']);
        }
    }
