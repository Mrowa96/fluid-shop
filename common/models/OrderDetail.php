<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%order_detail}}".
     *
     * @property integer $id
     * @property integer $order_id
     * @property integer $order_detail_related_id
     * @property integer $product_id
     * @property integer $product_quantity
     * @property integer $product_type
     * @property double $product_cost
     * @property bool $deleted
     *
     * @property Order $order
     * @property Product $product
     */
    class OrderDetail extends ActiveRecord
    {

        public static function tableName()
        {
            return '{{%order_detail}}';
        }

        public function rules()
        {
            return [
                [['order_id', 'product_id', 'product_quantity', 'product_cost'], 'required'],
                [['order_id', 'product_id', 'product_quantity', 'product_type', 'order_detail_related_id'], 'integer'],
                [['deleted'], 'boolean'],
                [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('order_detail', 'ID'),
                'order_id' => Yii::t('order_detail', 'Order ID'),
                'order_detail_related_id' => Yii::t('order_detail', 'Order Detail related ID'),
                'product_id' => Yii::t('order_detail', 'Product ID'),
                'product_quantity' => Yii::t('order_detail', 'Product Quantity'),
                'product_cost' => Yii::t('order_detail', 'Product Cost'),
                'product_type' => Yii::t('order_detail', 'Product Type'),
                'deleted' => Yii::t('order_detail', 'Deleted'),
            ];
        }

        public function getOrder()
        {
            return $this->hasOne(Order::className(), ['id' => 'order_id']);
        }
        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }
        public function getRelated()
        {
            return $this->hasOne(Product::className(), ['id' => 'order_detail_related_id']);
        }
    }
