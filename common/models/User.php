<?php
    namespace common\models;

    use frontend\services\Basket\Exception;
    use Yii;
    use yii\base\NotSupportedException;
    use yii\behaviors\TimestampBehavior;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use yii\rbac\Role;
    use yii\web\IdentityInterface;
    use fluid\fileManager\Directory;

    /**
     * This is the model class for table "{{%user}}".
     *
     * @property string username
     * @property string name
     * @property string email
     * @property string avatar
     * @property string created_at
     * @property string updated_at
     * @property mixed id
     * @property mixed auth_key
     * @property string password_reset_token
     * @property string password_hash
     * @property string language
     * @property bool isAdmin
     * @property bool deletable
     * @property bool deleted
     *
     * @property Role role
     */
    class User extends ActiveRecord implements IdentityInterface
    {
        const STATUS_DELETED = 0;
        const STATUS_ACTIVE = 10;
        const ROLE_CLIENT = 'client';
        const ROLE_WORKER = 'worker';
        const ROLE_ADMIN = 'admin';
        const ROLE_SYSTEM = 'system';
        const DEFAULT_ROLE = User::ROLE_CLIENT;

        public $password;
        public $password_repeat;
        public $isAdmin;

        public function rules()
        {
            return ArrayHelper::merge(User::getBaseRules(), [
                ['status', 'default', 'value' => User::STATUS_ACTIVE],
                ['status', 'in', 'range' => [User::STATUS_ACTIVE, User::STATUS_DELETED]],

                ['deletable', 'default', 'value' => 1],
                ['deleted', 'boolean'],

                [['created_at', 'updated_at'], 'string']
            ]);
        }

        public function attributeLabels()
        {
            return User::getBaseAttributeLabels();
        }

        public function beforeValidate()
        {
            if (!$this->avatar) {
                $this->avatar = User::getRandomAvatar();
            } else {
                if (strpos($this->avatar, Yii::getAlias("@dataLink") . '/') !== false) {
                    $this->avatar = str_replace(Yii::getAlias("@dataLink") . '/', '', $this->avatar);
                }
            }

            if (!$this->name) {
                $this->name = ucfirst($this->username);
            }

            if ($this->isNewRecord) {
                $this->created_at = (new \DateTime())->format("Y-m-d H:i:s");
            }

            $this->updated_at = (new \DateTime())->format("Y-m-d H:i:s");

            return parent::beforeValidate();
        }

        public function afterSave($insert, $changedAttributes)
        {
            parent::afterSave($insert, $changedAttributes);

            if (!$this->hasRole()) {
                $this->setRole();
            }
        }

        public function afterFind()
        {
            $this->avatar = Yii::getAlias("@dataLink") . '/' . $this->avatar;

            if ($this->hasRole() && $this->role->name === User::ROLE_ADMIN) {
                $this->isAdmin = true;
            }

            parent::afterFind();
        }

        public static function findIdentity($id)
        {
            return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
        }

        public static function findIdentityByAccessToken($token, $type = null)
        {
            throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
        }

        public static function findByUsername($username)
        {
            return static::findOne(['username' => $username, 'status' => self::STATUS_ACTIVE]);
        }

        public static function findByPasswordResetToken($token)
        {
            if (!static::isPasswordResetTokenValid($token)) {
                return null;
            }

            return static::findOne([
                'password_reset_token' => $token,
                'status' => self::STATUS_ACTIVE,
            ]);
        }

        public static function isPasswordResetTokenValid($token)
        {
            if (empty($token)) {
                return false;
            }

            $timestamp = (int)substr($token, strrpos($token, '_') + 1);
            $expire = Yii::$app->params['user.passwordResetTokenExpire'];

            return $timestamp + $expire >= time();
        }

        public function getId()
        {
            return $this->getPrimaryKey();
        }

        public function getAuthKey()
        {
            return $this->auth_key;
        }

        /**
         * @return Role
         * @throws Exception
         */
        public function getRole()
        {
            if ($this->hasRole()) {
                $roles = Yii::$app->authManager->getRolesByUser($this->id);

                return reset($roles);
            }
        }

        /**
         * @return bool
         */
        public function hasRole()
        {
            $roles = Yii::$app->authManager->getRolesByUser($this->id);

            return !empty($roles);
        }

        public function generatePasswordResetToken()
        {
            $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
        }

        public function validateAuthKey($authKey)
        {
            return $this->getAuthKey() === $authKey;
        }

        public function validatePassword($password)
        {
            return Yii::$app->security->validatePassword($password, $this->password_hash);
        }

        public function setPassword($password)
        {
            $this->password_hash = User::hashPassword($password);
        }

        public function removePasswordResetToken()
        {
            $this->password_reset_token = null;
        }

        /**
         * @param bool $asArray
         * @return array|Address
         */
        public function getAddress($asArray = false)
        {
            $query = Address::find()->where(['user_id' => $this->id])->orderBy('id ASC');

            if ($asArray === true) {
                $query->asArray();
            }

            return $query->one();
        }

        /**
         * @param string $role
         * @return bool|\yii\rbac\Assignment
         */
        public function setRole($role = User::ROLE_CLIENT)
        {
            $auth = Yii::$app->authManager;

            if ($this->id) {
                $auth->revokeAll($this->id);
                $role = $auth->getRole($role);

                if ($role && $auth->assign($role, $this->id)) {
                    return true;
                }
            }

            return false;
        }

        public static function tableName()
        {
            return '{{%user}}';
        }

        public static function getAvatars()
        {
            $dir = new Directory(Yii::getAlias("@dataAvatars"));
            $avatars = [];

            if ($dir->exists) {
                $avatars = $dir->objects;

                foreach ($avatars AS &$avatar) {
                    $avatar = 'avatars/' . $avatar;
                }
            }

            return $avatars;
        }

        public static function getRandomAvatar()
        {
            $avatars = User::getAvatars();
            $index = mt_rand(0, (count($avatars) - 1));

            if (isset($avatars[$index])) {
                return $avatars[$index];
            } else {
                return User::getRandomAvatar();
            }
        }

        public static function getWorkers($limit = 16)
        {
            /** @var User[] $users */
            $users = User::find()->where(['status' => self::STATUS_ACTIVE])->limit($limit)->all();
            $results = [];

            foreach ($users AS $user) {
                if ($user->role->name == User::ROLE_WORKER) {
                    $results[] = $user;
                }
            }

            return $results;
        }

        public static function getClientsAmount()
        {
            /** @var User[] $users */
            $users = User::find()->where(['status' => self::STATUS_ACTIVE])->all();
            $results = [];

            foreach ($users AS $user) {
                if ($user->role->name == User::ROLE_CLIENT) {
                    $results[] = $user;
                }
            }

            return count($results);
        }

        public static function getSystemUser()
        {
            $systemUser = User::find()->where(['username' => User::ROLE_SYSTEM, 'status' => User::STATUS_ACTIVE, 'deleted' => 0])->one();

            if ($systemUser) {
                return $systemUser;
            }

            return false;
        }

        public static function generateAuthKey()
        {
            return Yii::$app->security->generateRandomString();
        }

        public static function hashPassword($password)
        {
            return Yii::$app->security->generatePasswordHash($password);
        }

        public static function getBaseRules()
        {
            return [
                ['username', 'filter', 'filter' => 'trim'],
                ['username', 'required'],
                ['username', 'string', 'min' => 2, 'max' => 255],

                ['name', 'filter', 'filter' => 'trim'],
                ['name', 'required'],
                ['name', 'string', 'min' => 2, 'max' => 255],

                ['email', 'filter', 'filter' => 'trim'],
                ['email', 'required'],
                ['email', 'email'],
                ['email', 'string', 'max' => 255],

                ['avatar', 'filter', 'filter' => 'trim']
            ];
        }

        public static function getBaseAttributeLabels()
        {
            return [
                'username' => Yii::t("user", "Username"),
                'name' => Yii::t("user", "Name"),
                'email' => Yii::t("user", "Adres email"),
                'password' => Yii::t("user", "Password"),
                'password_repeat' => Yii::t("user", "Password repeat"),
                'language' => Yii::t("user", "Language"),
                'role' => Yii::t("user", "Role"),
                'created_at' => Yii::t("user", "Created"),
                'updated_at' => Yii::t("user", "Updated"),
                'all' => Yii::t("user", "All users"),
                'add_user' => Yii::t("user", "Add user"),
                'deleted' => Yii::t("user", "Deleted"),
            ];
        }

        /**
         * @param bool $forForm
         * @return array|Role[]
         */
        public static function getAvailableRoles($forForm = true)
        {
            $roles = Yii::$app->authManager->getRoles();

            if ($forForm) {
                $rolesOutput = [];

                foreach ($roles as $index => $role) {
                    if ($role->name === User::ROLE_SYSTEM) {
                        unset($roles[$index]);
                    } else {
                        $rolesOutput[$role->name] = Yii::t("system", $role->description);
                    }
                }
            } else {
                $rolesOutput = $roles;
            }

            return $rolesOutput;
        }
    }
