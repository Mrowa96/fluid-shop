<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;
    use fluid\fileManager\File;

    /**
     * This is the model class for table "{{%banner}}".
     *
     * @property integer $id
     * @property integer $type
     * @property string $name
     * @property string $link
     * @property string $path
     * @property string $thumb_path
     * @property bool $display_on_desktop
     * @property bool $display_on_mobile
     *
     * @property UploadedFile $imageFile
     * @property string $display_on_desktop_text
     * @property string $display_on_mobile_text
     * @property string $type_text
     */
    class Banner extends ActiveRecord
    {
        public $imageFile;
        public $type_text;
        public $display_on_desktop_text;
        public $display_on_mobile_text;
        public static $types = [];

        const FIRST_BANNER = 1;
        const SECOND_BANNER = 2;
        const THIRD_BANNER = 3;
        const HOME_PAGE_SLIDER = 4;
        const SUB_PAGE_BANNER = 5;

        /**
         * @return void
         */
        public function init()
        {
            //TODO To translate
            self::$types = [
                self::FIRST_BANNER => Yii::t('banner', 'First banner'),
                self::SECOND_BANNER => Yii::t('banner', 'Second banner'),
                self::THIRD_BANNER => Yii::t('banner', 'Third banner'),
                self::HOME_PAGE_SLIDER => Yii::t('banner', 'Home page slider'),
                self::SUB_PAGE_BANNER => Yii::t('banner', 'Sub page banner'),
            ];

            parent::init();
        }

        /**
         * @return array
         */
        public function rules()
        {
            return [
                [['name', 'link', 'type'], 'required'],
                [['type'], 'integer'],
                [['name'], 'string', 'max' => 192],
                [['link'], 'string', 'max' => 256],
                [['path', 'thumb_path'], 'string', 'max' => 512],
                [['display_on_desktop', 'display_on_mobile'], 'boolean'],
                [['display_on_desktop', 'display_on_mobile'], 'default', 'value' => 1],
                [['imageFile'], 'file', 'extensions' => 'png, jpg, jpeg', 'checkExtensionByMimeType' => false]
            ];
        }

        /**
         * @return array
         */
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('banner', 'ID'),
                'type' => Yii::t('banner', 'Type'),
                'type_text' => Yii::t('banner', 'Type'),
                'name' => Yii::t('banner', 'Name'),
                'link' => Yii::t('banner', 'Link'),
                'path' => Yii::t('banner', 'Path'),
                'thumb_path' => Yii::t('banner', 'Thumb Path'),
                'display_on_desktop' => Yii::t('banner', 'Display on desktop'),
                'display_on_mobile' => Yii::t('banner', 'Display on mobile'),
                'display_on_desktop_text' => Yii::t('banner', 'Display on desktop'),
                'display_on_mobile_text' => Yii::t('banner', 'Display on mobile'),
            ];
        }

        /**
         * @param array $data
         * @param null $formName
         * @return bool
         */
        public function load($data, $formName = null)
        {
            if (parent::load($data, $formName)) {
                $this->imageFile = UploadedFile::getInstance($this, 'imageFile');

                return true;
            }

            return false;
        }

        /**
         * @param bool $insert
         * @return bool
         */
        public function beforeSave($insert)
        {
            $this->path = str_replace(Yii::getAlias("@frontendLink"), "", $this->path);
            $this->thumb_path = str_replace(Yii::getAlias("@frontendLink"), "", $this->thumb_path);

            return parent::beforeSave($insert);
        }

        /**
         * @return void
         */
        public function afterFind()
        {
            $this->type_text = Yii::t("banner", self::$types[$this->type]);
            $this->display_on_desktop_text = Yii::t("system", ($this->display_on_desktop) ? "Yes" : "No");
            $this->display_on_mobile_text = Yii::t("system", ($this->display_on_mobile) ? "Yes" : "No");
            $this->path = Yii::getAlias("@frontendLink") . $this->path;
            $this->thumb_path = Yii::getAlias("@frontendLink") . $this->thumb_path;

            parent::afterFind();
        }

        public function getGridViewExample()
        {
            return [
                [
                    'property' => 'thumb_path',
                    'value' => 0,
                    'position' => 1,
                    'type' => 'image'
                ],
                [
                    'property' => 'name',
                    'value' => 1,
                    'position' => 2
                ],
                [
                    'property' => 'link',
                    'position' => 3,
                    'value' => 1
                ],
                [
                    'property' => 'type_text',
                    'value' => 1,
                    'position' => 4
                ],
            ];
        }

        /**
         * @return bool
         */
        public function saveImage()
        {
            if ($this->isNewRecord) {
                if ($this->imageFile && $this->validate()) {
                    return $this->saveImageInFileSystem();
                }
            } else {
                if ($this->imageFile) {
                    if ($this->validate()) {
                        return $this->saveImageInFileSystem();
                    }
                } else {
                    return true;
                }
            }

            return false;
        }

        /**
         * @return bool
         */
        protected function saveImageInFileSystem()
        {
            try {
                $fileName = mb_substr(hash("sha256", $this->imageFile->basename), 0, 32) . time();
                $filePath = Yii::getAlias("@dataBanners") . '/' . $fileName . '.' . $this->imageFile->extension;
                $fileObject = new File($filePath);
                $fileObject->saveUploadedFile(ArrayHelper::toArray($this->imageFile));

                $this->path = '/data/banners/' . $fileName . '.' . $this->imageFile->extension;
                $this->thumb_path = '/data//banners/' . $fileName . '.thumb.' . $this->imageFile->extension;

                return true;
            } catch (\Exception $ex) {
                $this->addError('imageFile', Yii::t("banner", "Unexpected error occurred, check permissions."));

                return false;
            }
        }

        /**
         * @return string
         */
        public static function tableName()
        {
            return '{{%banner}}';
        }

        /**
         * @return array
         */
        public static function prepareTypeForForm()
        {
            return self::$types;
        }

        /**
         * @param $type
         * @param int $limit
         * @param bool $random
         * @return array|ActiveRecord[]
         */
        public static function getByType($type, $limit = 1, $random = false)
        {
            $query = Banner::find()->where(['type' => $type]);

            if ($random === true) {
                $query->orderBy('RAND()');
            }

            return $query->limit($limit)->all();
        }
    }
