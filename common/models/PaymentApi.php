<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%payment_api}}".
     *
     * @property integer $id
     * @property string $name
     * @property string $symbol
     * @property bool $active
     */
    class PaymentApi extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%payment_api}}';
        }

        public function rules()
        {
            return [
                [['name', 'symbol', 'active'], 'required'],
                [['name', 'symbol'], 'string', 'max' => 256],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('payment', 'ID'),
                'name' => Yii::t('payment', 'Name'),
                'symbol' => Yii::t('payment', 'Symbol'),
                'active' => Yii::t('payment', 'Active'),
            ];
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(self::find()->where(['active' => 1])->all(), 'id', 'name');
        }
    }
