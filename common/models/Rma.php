<?php
    namespace common\models;

    use Faker\Provider\cs_CZ\DateTime;
    use fluid\fileManager\File;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;
    use yii\helpers\Html;
    use yii\web\UploadedFile;

    /**
     * This is the model class for table "{{%rma}}".
     *
     * @property integer $id
     * @property integer $user_id
     * @property integer $product_id
     * @property integer $order_id
     * @property string $text
     * @property integer $status
     * @property string &status_text
     * @property string $created_date
     * @property string $modified_date
     *
     * @property User $user
     * @property Order $order
     * @property Product $product
     */
    class Rma extends ActiveRecord
    {
        public $imageFiles;
        public $status_text;

        const STATUS_NEW = 0;
        const STATUS_ACCEPTED = 1;
        const STATUS_DECLINED = 2;
        const STATUS_COMPLETED = 3;

        public static function tableName()
        {
            return '{{%rma}}';
        }

        public function rules()
        {
            return [
                [['user_id', 'product_id', 'order_id', 'text', 'created_date', 'modified_date'], 'required'],
                [['user_id', 'product_id', 'order_id', 'status'], 'integer'],
                [['text'], 'string'],
                [['created_date', 'modified_date'], 'safe'],
                [['imageFiles'], 'file', 'maxFiles' => 5],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                [['order_id'], 'exist', 'skipOnError' => true, 'targetClass' => Order::className(), 'targetAttribute' => ['order_id' => 'id']],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ];
        }
        public function attributeLabels()
        {
            return [
                'id' => Yii::t('rma', 'ID'),
                'user_id' => Yii::t('rma', 'User ID'),
                'product_id' => Yii::t('rma', 'Product ID'),
                'order_id' => Yii::t('rma', 'Order ID'),
                'text' => Yii::t('rma', 'Text'),
                'status' => Yii::t('rma', 'Status'),
                'status_text' => Yii::t('rma', 'Status'),
                'created_date' => Yii::t('rma', 'Created Date'),
                'modified_date' => Yii::t('rma', 'Modified Date'),
            ];
        }

        public function load($data, $formName = null)
        {
            if(parent::load($data, $formName)){
                $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');
                $this->addNewImages();

                return true;
            }

            return false;
        }
        public function beforeValidate()
        {
            $this->text = Html::encode($this->text);

            return parent::beforeValidate();
        }
        public function afterFind()
        {
            switch($this->status){
                case self::STATUS_NEW:
                    $this->status_text = Yii::t("text", "Nie rozpatrzona");
                    break;
                case self::STATUS_ACCEPTED:
                    $this->status_text = Yii::t("text", "Zaakceptowana");
                    break;
                case self::STATUS_DECLINED:
                    $this->status_text = Yii::t("text", "Odrzucona");
                    break;
                case self::STATUS_COMPLETED:
                    $this->status_text = Yii::t("text", "Zakończona");
                    break;
            }

            parent::afterFind();
        }

        public function save($runValidation = true, $attributeNames = null)
        {
            if($this->isNewRecord){
                $this->created_date = (new \DateTime())->format("Y-m-d H:i:s");
            }

            $this->modified_date = (new \DateTime())->format("Y-m-d H:i:s");

            return parent::save($runValidation, $attributeNames);
        }

        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }
        public function getOrder()
        {
            return $this->hasOne(Order::className(), ['id' => 'order_id']);
        }
        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }

        public static function getAmount()
        {
            return Rma::find()->count();
        }

        private function addNewImages()
        {
            if (!empty($this->imageFiles) && !empty($this->imageFiles[0])) {
                foreach ($this->imageFiles as $file) {
                    $fileName = mb_substr(hash("sha256", $file->basename), 0, 32);
                    $filePath = Yii::getAlias("@dataRma") . '/' . $fileName . '.' . $file->extension;
                    $fileObject = new File($filePath);
                    $fileObject->saveUploadedFile(ArrayHelper::toArray($file));

                    $image = new RmaImages();
                    $image->rma_id = $this->id;
                    $image->path = Yii::getAlias("@dataLink") . '/rma/' . $fileName . '.' . $file->extension;
                    $image->thumb_path =  Yii::getAlias("@dataLink") . '/rma/' . $fileName . '.thumb.' . $file->extension;
                    $image->save();
                }
            }
        }
    }
