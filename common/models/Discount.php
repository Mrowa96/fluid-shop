<?php
    namespace common\models;

    use common\models\Settings\System;
    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\ArrayHelper;

    /**
     * This is the model class for table "{{%discount}}".
     *
     * @property integer $id
     * @property integer $name
     * @property integer $threshold_percentage
     * @property integer $threshold_numeric
     * @property integer $code
     * @property integer $amount
     * @property string $expire
     * @property integer $dynamic
     * @property integer $deleted
     *
     * @property Category[] $categories
     * @property Product[] $products
     */
    class Discount extends ActiveRecord
    {

        public $name;

        public static function tableName()
        {
            return '{{%discount}}';
        }

        public function rules()
        {
            return [
                [['threshold_percentage', 'threshold_numeric', 'amount'], 'integer'],
                [['code'], 'string'],
                [['expire'], 'safe'],
                [['dynamic', 'deleted'], 'boolean'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('discount', 'ID'),
                'threshold_percentage' => Yii::t('discount', 'Threshold (percentage)'),
                'threshold_numeric' => Yii::t('discount', 'Threshold (numeric)'),
                'code' => Yii::t('discount', 'Code'),
                'amount' => Yii::t('discount', 'Amount'),
                'expire' => Yii::t('discount', 'Expire'),
                'name' => Yii::t('discount', 'Name'),
                'dynamic' => Yii::t('discount', 'Dynamic'),
                'deleted' => Yii::t('discount', 'Deleted'),
            ];
        }

        public function getCategories()
        {
            return $this->hasMany(Category::className(), ['discount_id' => 'id']);
        }

        public function getProducts()
        {
            return $this->hasMany(Product::className(), ['discount_id' => 'id']);
        }

        public function afterFind()
        {
            if ($this->threshold_numeric) {
                $this->name = $this->threshold_numeric . ' ' . Settings::getOne('currency', System::MODULE);
            } else {
                $this->name = $this->threshold_percentage . '%';
            }

            $this->expire = (new \DateTime($this->expire))->format("Y-m-d\Th:i:s");

            parent::afterFind();
        }

        public function recountCost($cost)
        {
            $newCost = 1000;

            if ($this->threshold_percentage) {
                $newCost = $cost - ($cost * ($this->threshold_percentage / 100));
            } elseif ($this->threshold_numeric) {
                $newCost = $cost - $this->threshold_numeric;
            }

            if ($newCost <= 0) {
                $newCost = 0.01;
            }

            return money_format('%i', $newCost);
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(Discount::find()->where(['dynamic' => 0])->all(), 'id', 'name');
        }

        public static function getDiscount($code)
        {
            /** @var Discount $discount */
            $discount = Discount::find()->where(['code' => $code])->one();

            if ($discount && strtotime($discount->expire) >= strtotime(date("Y-m-d h:i:s"))) {
                if ($discount->dynamic && $discount->amount > 0) {
                    return $discount;
                }
            }

            return false;
        }

        public static function hasDiscount($code)
        {
            return Discount::find()->where(['code' => $code])->one();
        }
    }
