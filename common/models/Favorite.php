<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%favorite}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property integer $user_id
     * @property string $created_date
     *
     * @property Product $product
     * @property User $user
     */
    class Favorite extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%favorite}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'user_id', 'created_date'], 'required'],
                [['product_id', 'user_id'], 'integer'],
                [['created_date'], 'safe'],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                ['product_id', 'unique'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('favorite', 'ID'),
                'product_id' => Yii::t('favorite', 'Product ID'),
                'user_id' => Yii::t('favorite', 'User ID'),
                'created_date' => Yii::t('favorite', 'Created Date'),
            ];
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }

        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }

        public function beforeValidate()
        {
            $this->user_id = Yii::$app->user->id;
            $this->created_date = (new \DateTime())->format("Y-m-d H:i:s");

            return parent::beforeValidate();
        }
    }
