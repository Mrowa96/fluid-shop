<?php
    namespace common\models;

    use backend\models\Attribute;
    use backend\models\AttributeOption;
    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%product_attribute}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property integer $attribute_id
     * @property integer $attribute_option_id
     *
     * @property Attribute $productAttribute
     * @property AttributeOption $option
     * @property Product $product
     */
    class ProductAttribute extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%product_attribute}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'attribute_id', 'attribute_option_id'], 'required'],
                [['product_id', 'attribute_id', 'attribute_option_id'], 'integer'],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product_attribute', 'ID'),
                'product_id' => Yii::t('product_attribute', 'Product ID'),
                'attribute_id' => Yii::t('product_attribute', 'Attribute ID'),
                'attribute_option_id' => Yii::t('product_attribute', 'Attribute Option ID'),
            ];
        }

        public function getProductAttribute()
        {
            return $this->hasOne(Attribute::className(), ['id' => 'attribute_id']);
        }

        public function getOption()
        {
            return $this->hasOne(AttributeOption::className(), ['id' => 'attribute_option_id']);
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }
    }
