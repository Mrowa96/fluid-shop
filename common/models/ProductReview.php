<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\helpers\Html;

    /**
     * This is the model class for table "{{%product_review}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property integer $user_id
     * @property integer $rating
     * @property string $text
     * @property integer $accepted
     * @property string $created_date
     *
     * @property Product $product
     * @property User $user
     *
     * @property string $accepted_text
     */
    class ProductReview extends ActiveRecord
    {
        const MAX_RATING = 5;

        public $accepted_text;

        public static function tableName()
        {
            return '{{%product_review}}';
        }

        public function rules()
        {
            return [
                [['product_id', 'user_id', 'rating', 'text', 'created_date'], 'required'],
                [['product_id', 'user_id', 'rating', 'accepted'], 'integer'],
                [['text'], 'string'],
                [['created_date'], 'safe'],
                [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
                [['product_id'], 'exist', 'skipOnError' => true, 'targetClass' => Product::className(), 'targetAttribute' => ['product_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product_review', 'ID'),
                'product_id' => Yii::t('product_review', 'Product ID'),
                'user_id' => Yii::t('product_review', 'User ID'),
                'rating' => Yii::t('product_review', 'Rating'),
                'text' => Yii::t('product_review', 'Text'),
                'accepted' => Yii::t('product_review', 'Accepted'),
                'accepted_text' => Yii::t('product_review', 'Accepted'),
                'created_date' => Yii::t('product_review', 'Created Date'),
            ];
        }

        public function getProduct()
        {
            return $this->hasOne(Product::className(), ['id' => 'product_id']);
        }

        public function getUser()
        {
            return $this->hasOne(User::className(), ['id' => 'user_id']);
        }

        public function beforeValidate()
        {
            $this->created_date = (new \DateTime())->format("Y-m-d\TH:i:s");
            $this->user_id = Yii::$app->user->id;

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            $this->text = Html::encode($this->text);
            $this->accepted_text = Yii::t("system", $this->accepted ? "Yes" : "No");

            parent::afterFind();
        }
    }
