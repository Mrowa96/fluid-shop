<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\db\Query;
    use yii\helpers\ArrayHelper;
    use common\interfaces\SitemapItem;
    use yii\web\UrlManager;
    use common\utilities\Utility;

    /**
     * This is the model class for table "{{%category}}".
     *
     * @property integer $id
     * @property integer $parent_id
     * @property integer $discount_id
     * @property string $name
     * @property string $url
     * @property string $category_name
     * @property string $description
     * @property string $seo_description
     * @property string $seo_keywords
     * @property string $parentName
     * @property integer $created_date
     * @property integer $modified_date
     * @property bool $deleted
     *
     * @property Discount $discount
     * @property Product[] $products
     * @property Category $parent
     * @property Category[] $children
     *
     * @property string $fullUrl
     */
    class Category extends ActiveRecord implements SitemapItem
    {
        public $parentName;
        public $fullUrl;
        public $category_name;

        const CACHE_CATEGORY_ATTRIBUTES_PREFIX = "category_attributes_";

        public static function tableName()
        {
            return '{{%category}}';
        }

        public function rules()
        {
            return [
                [['name', 'url', 'created_date', "modified_date"], 'required'],
                [['parent_id', 'discount_id'], 'integer'],
                [['description', 'seo_description', 'url'], 'string'],
                [['name', 'seo_keywords'], 'string', 'max' => 256],
                ['parent_id', 'default', 'value' => 0],
                [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discount::className(), 'targetAttribute' => ['discount_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('category', 'ID'),
                'parent_id' => Yii::t('category', 'Parent ID'),
                'discount_id' => Yii::t('category', 'Discount ID'),
                'name' => Yii::t('category', 'Name'),
                'url' => Yii::t('category', 'Url'),
                'category_name' => Yii::t('category', 'Category name'),
                'description' => Yii::t('category', 'Description'),
                'seo_description' => Yii::t('category', 'Seo Description'),
                'seo_keywords' => Yii::t('category', 'Seo Keywords'),
                'parentName' => Yii::t('category', 'Parent Name'),
                'deleted' => Yii::t('category', 'Deleted'),
            ];
        }

        public function getDiscount()
        {
            return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
        }

        public function getProducts()
        {
            return $this->hasMany(Product::className(), ['category_id' => 'id'])->andOnCondition(['deleted' => 0]);
        }

        public function getParent()
        {
            return $this->hasOne(Category::className(), ['id' => 'parent_id']);
        }

        public function getChildren()
        {
            return $this->hasMany(Category::className(), ['parent_id' => 'id'])->andOnCondition(['deleted' => 0]);
        }

        public function getProductsAttributes()
        {
            $attributes = [];

            /** @var Product $product */
            foreach ($this->products AS $product) {
                $attributes[] = $product->productAttributes;
            }

            return $attributes;
        }

        public function beforeValidate()
        {
            if ($this->isNewRecord) {
                $this->created_date = (new \DateTime())->format("Y-m-d\TH:i:s");
            }

            $this->url = Utility::createUrl($this->name);
            $this->modified_date = (new \DateTime())->format("Y-m-d\TH:i:s");

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            if ($this->parent_id !== 0) {
                $this->parentName = Category::findOne($this->parent_id)->name;
            } else {
                $this->parentName = Yii::t("category", "None");
            }

            $this->fullUrl = $this->url . ',' . $this->id;

            parent::afterFind();
        }

        public function getGridViewExample()
        {
            return [
                [
                    'property' => 'name',
                    'value' => 1,
                    'position' => 1
                ],
                [
                    'property' => 'parentName',
                    'value' => 1,
                    'position' => 2
                ],
                [
                    'property' => 'description',
                    'position' => 3
                ],
            ];
        }

        public function getUrl()
        {
            /** @var UrlManager $urlManagerFrontend */
            $urlManagerFrontend = Yii::$app->urlManagerFrontend;

            return $urlManagerFrontend->createUrl(['category/index', 'id' => $this->id, 'url' => $this->url]);
        }

        public function getLastModified()
        {
            return $this->modified_date;
        }

        public function getChangeFrequency()
        {
            return SitemapItem::FREQ_MONTHLY;
        }

        public function getPriority()
        {
            return SitemapItem::PRIORITY_MEDIUM;
        }

        public function addToAttributeCache(Product $product)
        {
            $cache = Yii::$app->cache;
            $attributes = $product->productAttributes;
            $cacheKey = self::CACHE_CATEGORY_ATTRIBUTES_PREFIX . $this->id;

            if ($cache->exists($cacheKey)) {
                $cachedAttributes = $cache->get($cacheKey);
            } else {
                $cachedAttributes = [];
            }

            foreach ($attributes AS $attribute) {
                if (in_array($attribute->attribute_id, $cachedAttributes) === false) {
                    $cachedAttributes[] = $attribute->attribute_id;
                }
            }

            return $cache->set($cacheKey, $cachedAttributes);
        }

        public function createAttributeCache()
        {
            $cache = Yii::$app->cache;
            $cachedAttributes = [];
            $cacheKey = self::CACHE_CATEGORY_ATTRIBUTES_PREFIX . $this->id;
            $attributesQuery = (new Query())
                ->select('DISTINCT ' . ProductAttribute::tableName() . '.attribute_id')
                ->from(ProductAttribute::tableName())
                ->leftJoin(Product::tableName(), ProductAttribute::tableName() . '.product_id = ' . Product::tableName() . '.id')
                ->andWhere([Product::tableName() . '.category_id' => $this->id]);

            foreach ($attributesQuery->all() AS $data) {
                if (array_key_exists('attribute_id', $data)) {
                    $cachedAttributes[] = $data['attribute_id'];
                }
            }

            if ($cache->set($cacheKey, $cachedAttributes)) {
                return $cache->get($cacheKey);
            }

            return [];
        }

        public function getParentsIds($withCurrent = false)
        {
            $result = [];
            $category = $this;

            while ($category->parent) {
                $result[] = $category->parent_id;

                $category = $category->parent;
            }

            if ($withCurrent === true) {
                $result[] = $this->id;
            }

            return $result;
        }

        public function getChildrenIds($withCurrent = false)
        {
            $categories = Category::find()->where(['parent_id' => $this->id, 'deleted' => 0])->all();
            $result = $this->getChildrenIdsRecursion($categories);

            if ($withCurrent === true) {
                $result[] = $this->id;
            }

            return $result;
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(Category::find()->where(['deleted' => 0])->all(), 'id', 'name');
        }

        public static function getMain()
        {
            return Category::find()->where(['parent_id' => 0, 'deleted' => 0])->all();
        }

        public static function fetchForSitemap()
        {
            return Category::find()->where(['deleted' => 0])->all();
        }

        protected function getChildrenIdsRecursion(array $categories, &$result = [])
        {
            /** @var Category $category */
            foreach ($categories AS $category) {
                if (!in_array($result, $result)) {
                    $result[] = $category->id;
                }

                $newCategories = Category::find()->where(['parent_id' => $category->id, 'deleted' => 0])->all();

                $this->getChildrenIdsRecursion($newCategories, $result);
            }

            return $result;
        }
    }
