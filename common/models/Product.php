<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;
    use yii\db\Expression;
    use yii\helpers\ArrayHelper;
    use yii\web\UploadedFile;
    use fluid\fileManager\File;
    use common\utilities\Utility;
    use common\interfaces\SitemapItem;
    use common\models\Settings\System;
    use yii\web\UrlManager;
    use backend\models\CronQueue;
    use common\utilities\ElasticSearch;

    /**
     * This is the model class for table "{{%product}}".
     *
     * @property integer $id
     * @property integer $product_id
     * @property integer $category_id
     * @property integer $discount_id
     * @property string $name
     * @property string $brand
     * @property string $product_name
     * @property string $processed_name
     * @property string $description
     * @property string $additional_field
     * @property double $previous_cost
     * @property double $cost
     * @property double $costAfterDiscount
     * @property integer $quantity
     * @property integer $bought_count
     * @property integer $amountToBuy
     * @property integer $available
     * @property string $code
     * @property string $type
     * @property string $modified_date
     * @property string $created_date
     * @property bool $recommended_product
     * @property bool $promotional_product
     * @property bool $check_out_product
     * @property bool $deleted
     *
     * @property OrderDetail[] $orderDetails
     * @property ProductReview[] $reviews
     * @property Discount $discount
     * @property Category $category
     * @property ProductImage[] $images
     * @property ProductImage $mainImage
     * @property ProductAttribute[] $productAttributes
     * @property ProductSellUp[] $upSell
     * @property ProductSellCross[] $crossSell
     */
    class Product extends ActiveRecord implements SitemapItem
    {
        const TYPE_STANDARD = 'standard';
        const TYPE_EXTRA = 'extra';

        public $imageFiles;
        public $imagesCustom = [];
        public $available_text;
        public $amountToBuy = 0;
        public $product_name;
        public $product_id;
        public $mainImageId;
        public $originalQuantity;
        public $costAfterDiscount;
        private $attributesToAdd = [];
        private $crossSellsToAdd = [];
        private $upSellsToAdd = [];
        private $imagesToDelete = [];
        private $attributesToDelete = [];
        private $crossSellProductsToDelete = [];
        private $upSellProductsToDelete = [];

        public static function tableName()
        {
            return '{{%product}}';
        }

        public function rules()
        {
            return [
                [['category_id', 'name', 'description', 'cost', 'quantity', 'code'], 'required'],
                [['category_id', 'discount_id', 'quantity', 'available', 'bought_count'], 'integer'],
                [['description', 'additional_field'], 'string'],
                [['previous_cost', 'cost'], 'number'],
                [['modified_date', 'created_date'], 'safe'],
                [['name', 'processed_name', 'brand'], 'string', 'max' => 256],
                [['code'], 'string', 'max' => 128],
                [['deleted', 'check_out_product', 'promotional_product', 'recommended_product'], 'boolean'],
                [['imageFiles'], 'file', 'maxFiles' => 5],
                [['discount_id'], 'exist', 'skipOnError' => true, 'targetClass' => Discount::className(), 'targetAttribute' => ['discount_id' => 'id']],
                [['category_id'], 'exist', 'skipOnError' => true, 'targetClass' => Category::className(), 'targetAttribute' => ['category_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('product', 'ID'),
                'product_id' => Yii::t('product', 'Product Id'),
                'category_id' => Yii::t('product', 'Category ID'),
                'discount_id' => Yii::t('product', 'Discount ID'),
                'name' => Yii::t('product', 'Name'),
                'brand' => Yii::t('product', 'Brand'),
                'product_name' => Yii::t('product', 'Product name'),
                'processed_name' => Yii::t('product', 'Processed Name'),
                'description' => Yii::t('product', 'Description'),
                'additional_field' => Yii::t('product', 'Additional Field'),
                'previous_cost' => Yii::t('product', 'Previous Cost'),
                'cost' => Yii::t('product', 'Cost'),
                'costAfterDiscount' => Yii::t('product', 'Cost after discount'),
                'quantity' => Yii::t('product', 'Quantity'),
                'available' => Yii::t('product', 'Available'),
                'available_text' => Yii::t('product', 'Available'),
                'code' => Yii::t('product', 'Code'),
                'modified_date' => Yii::t('product', 'Modified Date'),
                'created_date' => Yii::t('product', 'Created Date'),
                'recommended_product' => Yii::t('product', 'Add to recommend block'),
                'promotional_product' => Yii::t('product', 'Add to promotional block'),
                'check_out_product' => Yii::t('product', 'Add to check out block'),
                'bought_count' => Yii::t('product', 'Bought count'),
            ];
        }

        public function getOrderDetails()
        {
            return $this->hasMany(OrderDetail::className(), ['product_id' => 'id']);
        }

        public function getImages()
        {
            return $this->hasMany(ProductImage::className(), ['product_id' => 'id']);
        }

        //TODO Change to Image class
        public function getMainImage()
        {
            $mainImage = ProductImage::find()->where(['product_id' => $this->id, 'main' => 1])->one();

            if (!$mainImage) {
                $mainImage = new \stdClass();

                $mainImage->path = "/data/no-image.png";
            }

            return $mainImage;
        }

        public function getDiscount()
        {
            return $this->hasOne(Discount::className(), ['id' => 'discount_id']);
        }

        public function getCategory()
        {
            return $this->hasOne(Category::className(), ['id' => 'category_id']);
        }

        public function getReviews()
        {
            return $this->hasMany(ProductReview::className(), ['product_id' => 'id'])->andFilterWhere(['accepted' => 1]);
        }

        public function getProductAttributes()
        {
            return $this->hasMany(ProductAttribute::className(), ['product_id' => 'id']);
        }

        public function getCrossSell()
        {
            return $this->hasMany(ProductSellCross::className(), ['product_id' => 'id']);
        }

        public function getUpSell()
        {
            return $this->hasMany(ProductSellUp::className(), ['product_id' => 'id']);
        }

        public function hasUpSell($id)
        {
            return ProductSellUp::find()->where(['product_id' => $this->id, 'id' => $id])->one();
        }

        public function hasCrossSell($id)
        {
            return ProductSellCross::find()->where(['product_id' => $this->id, 'id' => $id])->one();
        }

        public function getCost($withCurrency = false, $withDiscount = false)
        {
            $cost = $this->cost;

            if ($withDiscount === true) {
                $cost = $this->calculateDiscountCost();
            }

            if ($withCurrency === true) {
                return "<span class='price-value'>" . $cost . "</span>" . "<span class='currency'>" . Settings::getOne('currency', System::MODULE) . "</span>";
            } else {
                return $cost;
            }
        }

        public function cloneProduct()
        {
            $product = new Product();
            $product->setAttributes($this->attributes);
            $product->name .= " " . Yii::t("product", "cloned");

            if ($product->save()) {
                $this->cloneRelated($this->productAttributes, $product);
                $this->cloneRelated($this->upSell, $product);
                $this->cloneRelated($this->crossSell, $product);
                $this->cloneRelated($this->images, $product);

                if ($product->category) {
                    $this->category->addToAttributeCache($this);
                }

                return $product;
            }

            return false;
        }

        public function load($data, $formName = null)
        {
            if (parent::load($data, $formName)) {
                if (isset($data['attributeToAdd']) && !empty($data['attributeToAdd'])) {
                    $this->attributesToAdd = $data['attributeToAdd'];
                }

                if (isset($data['crossSellToAdd']) && !empty($data['crossSellToAdd'])) {
                    $this->crossSellsToAdd = $data['crossSellToAdd'];
                }

                if (isset($data['upSellToAdd']) && !empty($data['upSellToAdd'])) {
                    $this->upSellsToAdd = $data['upSellToAdd'];
                }

                if (isset($data['imageToDelete']) && !empty($data['imageToDelete'])) {
                    $this->imagesToDelete = $data['imageToDelete'];
                }

                if (isset($data['attributeToDelete']) && !empty($data['attributeToDelete'])) {
                    $this->attributesToDelete = $data['attributeToDelete'];
                }

                if (isset($data['crossSellProductToDelete']) && !empty($data['crossSellProductToDelete'])) {
                    $this->crossSellProductsToDelete = $data['crossSellProductToDelete'];
                }

                if (isset($data['upSellProductToDelete']) && !empty($data['upSellProductToDelete'])) {
                    $this->upSellProductsToDelete = $data['upSellProductToDelete'];
                }

                if (isset($data['mainImage']) && !empty($data['mainImage'])) {
                    $this->mainImageId = $data['mainImage'];
                }

                $this->imageFiles = UploadedFile::getInstances($this, 'imageFiles');

                return true;
            }

            return false;
        }

        public function save($runValidation = true, $attributeNames = null)
        {
            if (parent::save($runValidation, $attributeNames)) {
                $this->addNewImages();
                $this->updateMainImage();
                $this->addProductAttributes();
                $this->addCrossSellsProduct();
                $this->addUpSellsProduct();

                $this->removeProductAttributes();
                $this->removeCrossSellProduct();
                $this->removeUpSellProduct();
                $this->removeUnusedImages();

                if ($this->category) {
                    $this->category->addToAttributeCache($this);
                }

                return true;
            }

            return false;
        }

        public function delete()
        {
            $images = ProductImage::find()->where(['product_id' => $this->id])->all();

            if (!empty($images)) {
                foreach ($images AS $image) {
                    $file = new File(Yii::getAlias("@dataProducts") . '/' . $image->name);
                    $file->remove();
                    $image->delete();
                }
            }

            return parent::delete();
        }

        public function beforeValidate()
        {
            if ($this->isNewRecord) {
                $this->created_date = (new \DateTime())->format("Y-m-d\TH:i:s");
                $this->previous_cost = $this->cost;
            } else {
                $this->processed_name = Utility::createUrl($this->name);
            }

            $this->modified_date = (new \DateTime())->format("Y-m-d\TH:i:s");

            return parent::beforeValidate();
        }

        public function afterFind()
        {
            if (!empty($this->images)) {
                $productImages = [];

                foreach ($this->images AS $index => $image) {
                    $file = new File(Yii::getAlias("@dataProducts") . '/' . $image->name);

                    if ($file->exists) {
                        $productImages[] = $image;
                    }
                }

                $this->imagesCustom = $productImages;
            }

            $this->available_text = ($this->available) ? Yii::t("system", "Yes") : Yii::t("system", "No");
            $this->originalQuantity = $this->quantity;
            $this->product_name = $this->name;
            $this->product_id = $this->id;
            $this->costAfterDiscount = $this->calculateDiscountCost();

            parent::afterFind();
        }

        public function afterSave($insert, $changedAttributes)
        {
            try {
                (new ElasticSearch())->indexOne($this);
                CronQueue::createRequest(CronQueue::TYPE_GOOGLE_MERCHANT);
            } catch (\Exception $ex) {
                Yii::error($ex, "product");
            }

            parent::afterSave($insert, $changedAttributes);
        }

        public function getGridViewExample()
        {
            return [
                [
                    'property' => 'name',
                    'value' => 1,
                    'position' => 1
                ],
                [
                    'property' => 'cost',
                    'position' => 2,
                    'value' => 1
                ],
                [
                    'property' => 'available_text',
                    'value' => 1,
                    'position' => 3
                ],
            ];
        }

        public function getUrl()
        {
            return Yii::$app->urlManagerFrontend->createUrl(['product/detail', 'id' => $this->id, 'url' => $this->processed_name]);
        }

        public function getLastModified()
        {
            return $this->modified_date;
        }

        public function getChangeFrequency()
        {
            return SitemapItem::FREQ_WEEKLY;
        }

        public function getPriority()
        {
            return SitemapItem::PRIORITY_MAX;
        }

        public static function fetchForSitemap()
        {
            return Product::find()->where(['deleted' => 0, 'type' => 'standard'])->all();
        }

        public function calculateDiscountCost()
        {
            $cost = $this->cost;

            if ($this->discount) {
                $cost = $this->discount->recountCost($cost);
            }
            if ($this->category && $this->category->discount) {
                $cost = $this->category->discount->recountCost($cost);
            }

            return $cost;
        }

        public static function getAmount()
        {
            return Product::find()->where(['deleted' => 0, 'available' => 1])->count();
        }

        //TODO Dynamic groups assignment
        public static function getByGroup($group, $limit = 12)
        {
            $query = Product::find()
                ->where([$group . '_product' => 1, 'deleted' => 0, 'available' => 1])
                ->orderBy(new Expression('rand()'))
                ->limit($limit);

            return $query->all();
        }

        public static function prepareForForm()
        {
            return ArrayHelper::map(Product::find()->where(['deleted' => 0, 'available' => 1])->all(), 'id', 'name');
        }

        public static function createQueryForCategory(Category $category)
        {

            $categoriesIds = $category->getChildrenIds(true);

            return Product::find()
                ->select(
                    'DISTINCT `fs_product`.*,
                        CASE
                            WHEN `fscd`.threshold_numeric IS NOT NULL AND `fscd`.threshold_numeric != 0
                            THEN
                                `fscd`.threshold_numeric
                            WHEN `fscd`.threshold_percentage IS NOT NULL AND `fscd`.threshold_percentage != 0
                            THEN
                                (`fs_product`.cost * (`fscd`.threshold_percentage / 100))
                            ELSE
                                0
                        END AS category_discount,
                        CASE
                            WHEN
                                `fs_discount`.threshold_numeric IS NOT NULL AND `fs_discount`.threshold_numeric != 0
                                THEN
                                    `fs_discount`.threshold_numeric
                            WHEN
                                `fs_discount`.threshold_percentage IS NOT NULL AND `fs_discount`.threshold_percentage != 0
                                THEN
                                    (`fs_product`.cost * (`fs_discount`.threshold_percentage / 100))
                            ELSE 0
                        END AS product_discount'
                )
                ->leftJoin(Discount::tableName(), Discount::tableName() . '.id = ' . Product::tableName() . '.discount_id')
                ->leftJoin(Category::tableName(), Category::tableName() . '.id = ' . Product::tableName() . '.category_id')
                ->leftJoin(Discount::tableName() . ' AS `fscd`', Category::tableName() . '.discount_id = `fscd`.id')
                ->where([
                    Product::tableName() . '.deleted' => 0,
                    Product::tableName() . '.category_id' => $categoriesIds,
                ]);
        }

        protected function addNewImages()
        {
            if (!empty($this->imageFiles) && !empty($this->imageFiles[0])) {
                foreach ($this->imageFiles as $key => $file) {
                    $fileName = mb_substr(hash("sha256", $file->basename), 0, 32) . time();
                    $filePath = Yii::getAlias("@dataProducts") . '/' . $fileName . '.' . $file->extension;
                    $fileObject = new File($filePath);
                    $fileObject->saveUploadedFile(ArrayHelper::toArray($file), true);

                    $image = new ProductImage();
                    $image->product_id = $this->id;
                    $image->name = $fileName . '.' . $file->extension;
                    $image->path = '/data/products/' . $fileName . '.' . $file->extension;
                    $image->thumb_path = '/data/products/' . $fileName . '.thumb.' . $file->extension;

                    if ($key === 0) {
                        if ((ProductImage::find()->where(['product_id' => $this->id])->count())) {
                            $image->main = false;
                        } else {
                            $image->main = true;
                        }
                    } else {
                        $image->main = false;
                    }

                    $image->save();
                }
            }
        }

        protected function updateMainImage()
        {
            if ($this->mainImageId) {
                $images = ProductImage::find()->where(['product_id' => $this->id])->all();

                /** @var ProductImage $image */
                foreach ($images AS $image) {
                    if ($image->id != $this->mainImageId) {
                        $image->main = false;
                    } else {
                        $image->main = true;
                    }

                    $image->path = str_replace(Yii::getAlias("@frontendLink"), "", $image->path);
                    $image->thumb_path = str_replace(Yii::getAlias("@frontendLink"), "", $image->thumb_path);
                    $image->save();
                }
            }
        }

        protected function addProductAttributes()
        {
            foreach ($this->attributesToAdd AS $value) {
                $attribute = new ProductAttribute();

                $attribute->attribute_id = $value['attribute_id'];
                $attribute->attribute_option_id = $value['attribute_option_id'];
                $attribute->product_id = $this->id;

                $attribute->save();
            }
        }

        protected function addCrossSellsProduct()
        {
            foreach ($this->crossSellsToAdd AS $data) {
                $product = new ProductSellCross();

                $product->product_id = $this->id;
                $product->cross_product_id = $data['product_id'];
                $product->cost = $data['cost'];

                $product->save();
            }
        }

        protected function addUpSellsProduct()
        {
            foreach ($this->upSellsToAdd AS $data) {
                $product = new ProductSellUp();

                $product->product_id = $this->id;
                $product->cost = $data['cost'];
                $product->up_product_id = $data['product_id'];

                $product->save();
            }
        }

        protected function removeProductAttributes()
        {
            foreach ($this->attributesToDelete AS $attributeId) {
                $productAttribute = ProductAttribute::find()
                    ->where(['product_id' => $this->id])
                    ->andWhere(['attribute_id' => $attributeId])
                    ->one();

                $productAttribute->delete();
            }
        }

        protected function removeCrossSellProduct()
        {
            foreach ($this->crossSellProductsToDelete AS $productId) {
                $product = ProductSellCross::find()
                    ->where(['product_id' => $this->id])
                    ->andWhere(['cross_product_id' => $productId])
                    ->one();

                $product->delete();
            }
        }

        protected function removeUpSellProduct()
        {
            foreach ($this->upSellProductsToDelete AS $productId) {
                $product = ProductSellUp::find()
                    ->where(['product_id' => $this->id])
                    ->andWhere(['up_product_id' => $productId])
                    ->one();

                $product->delete();
            }
        }

        protected function cloneRelated(array $relatedData, Product $clonedProduct)
        {
            /** @var ProductSellUp | ProductSellCross | ProductAttribute $model */
            /** @var ProductSellUp | ProductSellCross | ProductAttribute $clone */
            if (!empty($relatedData)) {
                foreach ($relatedData AS $model) {
                    $modelClass = new \ReflectionClass($model);
                    $clone = $modelClass->newInstance();

                    $clone->setAttributes($model->attributes);
                    $clone->product_id = $clonedProduct->id;


                    $clone->save();
                }
            }

            return false;
        }

        protected function removeUnusedImages()
        {
            foreach ($this->imagesToDelete AS $imageId) {
                $image = ProductImage::findOne($imageId);
                $file = new File(Yii::getAlias("@dataProducts") . '/' . $image->name);
                $file->remove();
                $image->delete();
            }
        }
    }
