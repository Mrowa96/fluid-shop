<?php
    namespace common\models;

    use Yii;
    use yii\db\ActiveRecord;

    /**
     * This is the model class for table "{{%rma_images}}".
     *
     * @property integer $id
     * @property integer $rma_id
     * @property string $path
     * @property string $thumb_path
     *
     * @property Rma $rma
     */
    class RmaImages extends ActiveRecord
    {
        public static function tableName()
        {
            return '{{%rma_images}}';
        }

        public function rules()
        {
            return [
                [['rma_id', 'path', 'thumb_path'], 'required'],
                [['rma_id'], 'integer'],
                [['path'], 'string', 'max' => 512],
                [['thumb_path'], 'string', 'max' => 768],
                [['rma_id'], 'exist', 'skipOnError' => true, 'targetClass' => Rma::className(), 'targetAttribute' => ['rma_id' => 'id']],
            ];
        }

        public function attributeLabels()
        {
            return [
                'id' => Yii::t('rma', 'ID'),
                'rma_id' => Yii::t('rma', 'Rma ID'),
                'path' => Yii::t('rma', 'Path'),
                'thumb_path' => Yii::t('rma', 'Thumb Path'),
            ];
        }

        public function getRma()
        {
            return $this->hasOne(Rma::className(), ['id' => 'rma_id']);
        }
    }
